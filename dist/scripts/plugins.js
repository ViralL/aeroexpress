/*!
 * jQuery JavaScript Library v2.2.4
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2016-05-20T17:23Z
 */

(function( global, factory ) {

	if ( typeof module === "object" && typeof module.exports === "object" ) {
		// For CommonJS and CommonJS-like environments where a proper `window`
		// is present, execute the factory and get jQuery.
		// For environments that do not have a `window` with a `document`
		// (such as Node.js), expose a factory as module.exports.
		// This accentuates the need for the creation of a real `window`.
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info.
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
}(typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Support: Firefox 18+
// Can't be in strict mode, several libs including ASP.NET trace
// the stack via arguments.caller.callee and Firefox dies if
// you try to trace through "use strict" call chains. (#13335)
//"use strict";
var arr = [];

var document = window.document;

var slice = arr.slice;

var concat = arr.concat;

var push = arr.push;

var indexOf = arr.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var support = {};



var
	version = "2.2.4",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {

		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	},

	// Support: Android<4.1
	// Make sure we trim BOM and NBSP
	rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,

	// Matches dashed string for camelizing
	rmsPrefix = /^-ms-/,
	rdashAlpha = /-([\da-z])/gi,

	// Used by jQuery.camelCase as callback to replace()
	fcamelCase = function( all, letter ) {
		return letter.toUpperCase();
	};

jQuery.fn = jQuery.prototype = {

	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// Start with an empty selector
	selector: "",

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {
		return num != null ?

			// Return just the one element from the set
			( num < 0 ? this[ num + this.length ] : this[ num ] ) :

			// Return all the elements in a clean array
			slice.call( this );
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;
		ret.context = this.context;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	each: function( callback ) {
		return jQuery.each( this, callback );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map( this, function( elem, i ) {
			return callback.call( elem, i, elem );
		} ) );
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	eq: function( i ) {
		var len = this.length,
			j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[ j ] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor();
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: arr.sort,
	splice: arr.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[ 0 ] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// Skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !jQuery.isFunction( target ) ) {
		target = {};
	}

	// Extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {

		// Only deal with non-null/undefined values
		if ( ( options = arguments[ i ] ) != null ) {

			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject( copy ) ||
					( copyIsArray = jQuery.isArray( copy ) ) ) ) {

					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && jQuery.isArray( src ) ? src : [];

					} else {
						clone = src && jQuery.isPlainObject( src ) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend( {

	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	isFunction: function( obj ) {
		return jQuery.type( obj ) === "function";
	},

	isArray: Array.isArray,

	isWindow: function( obj ) {
		return obj != null && obj === obj.window;
	},

	isNumeric: function( obj ) {

		// parseFloat NaNs numeric-cast false positives (null|true|false|"")
		// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
		// subtraction forces infinities to NaN
		// adding 1 corrects loss of precision from parseFloat (#15100)
		var realStringObj = obj && obj.toString();
		return !jQuery.isArray( obj ) && ( realStringObj - parseFloat( realStringObj ) + 1 ) >= 0;
	},

	isPlainObject: function( obj ) {
		var key;

		// Not plain objects:
		// - Any object or value whose internal [[Class]] property is not "[object Object]"
		// - DOM nodes
		// - window
		if ( jQuery.type( obj ) !== "object" || obj.nodeType || jQuery.isWindow( obj ) ) {
			return false;
		}

		// Not own constructor property must be Object
		if ( obj.constructor &&
				!hasOwn.call( obj, "constructor" ) &&
				!hasOwn.call( obj.constructor.prototype || {}, "isPrototypeOf" ) ) {
			return false;
		}

		// Own properties are enumerated firstly, so to speed up,
		// if last one is own, then all properties are own
		for ( key in obj ) {}

		return key === undefined || hasOwn.call( obj, key );
	},

	isEmptyObject: function( obj ) {
		var name;
		for ( name in obj ) {
			return false;
		}
		return true;
	},

	type: function( obj ) {
		if ( obj == null ) {
			return obj + "";
		}

		// Support: Android<4.0, iOS<6 (functionish RegExp)
		return typeof obj === "object" || typeof obj === "function" ?
			class2type[ toString.call( obj ) ] || "object" :
			typeof obj;
	},

	// Evaluates a script in a global context
	globalEval: function( code ) {
		var script,
			indirect = eval;

		code = jQuery.trim( code );

		if ( code ) {

			// If the code includes a valid, prologue position
			// strict mode pragma, execute code by injecting a
			// script tag into the document.
			if ( code.indexOf( "use strict" ) === 1 ) {
				script = document.createElement( "script" );
				script.text = code;
				document.head.appendChild( script ).parentNode.removeChild( script );
			} else {

				// Otherwise, avoid the DOM node creation, insertion
				// and removal by using an indirect global eval

				indirect( code );
			}
		}
	},

	// Convert dashed to camelCase; used by the css and data modules
	// Support: IE9-11+
	// Microsoft forgot to hump their vendor prefix (#9572)
	camelCase: function( string ) {
		return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
	},

	nodeName: function( elem, name ) {
		return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
	},

	each: function( obj, callback ) {
		var length, i = 0;

		if ( isArrayLike( obj ) ) {
			length = obj.length;
			for ( ; i < length; i++ ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		} else {
			for ( i in obj ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		}

		return obj;
	},

	// Support: Android<4.1
	trim: function( text ) {
		return text == null ?
			"" :
			( text + "" ).replace( rtrim, "" );
	},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArrayLike( Object( arr ) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
				);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		return arr == null ? -1 : indexOf.call( arr, elem, i );
	},

	merge: function( first, second ) {
		var len = +second.length,
			j = 0,
			i = first.length;

		for ( ; j < len; j++ ) {
			first[ i++ ] = second[ j ];
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
			matches = [],
			i = 0,
			length = elems.length,
			callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var length, value,
			i = 0,
			ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArrayLike( elems ) ) {
			length = elems.length;
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
		} else {
			for ( i in elems ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}
		}

		// Flatten any nested arrays
		return concat.apply( [], ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// Bind a function to a context, optionally partially applying any
	// arguments.
	proxy: function( fn, context ) {
		var tmp, args, proxy;

		if ( typeof context === "string" ) {
			tmp = fn[ context ];
			context = fn;
			fn = tmp;
		}

		// Quick check to determine if target is callable, in the spec
		// this throws a TypeError, but we will just return undefined.
		if ( !jQuery.isFunction( fn ) ) {
			return undefined;
		}

		// Simulated bind
		args = slice.call( arguments, 2 );
		proxy = function() {
			return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
		};

		// Set the guid of unique handler to the same of original handler, so it can be removed
		proxy.guid = fn.guid = fn.guid || jQuery.guid++;

		return proxy;
	},

	now: Date.now,

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
} );

// JSHint would error on this code due to the Symbol not being defined in ES5.
// Defining this global in .jshintrc would create a danger of using the global
// unguarded in another place, it seems safer to just disable JSHint for these
// three lines.
/* jshint ignore: start */
if ( typeof Symbol === "function" ) {
	jQuery.fn[ Symbol.iterator ] = arr[ Symbol.iterator ];
}
/* jshint ignore: end */

// Populate the class2type map
jQuery.each( "Boolean Number String Function Array Date RegExp Object Error Symbol".split( " " ),
function( i, name ) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
} );

function isArrayLike( obj ) {

	// Support: iOS 8.2 (not reproducible in simulator)
	// `in` check used to prevent JIT error (gh-2145)
	// hasOwn isn't used here due to false negatives
	// regarding Nodelist length in IE
	var length = !!obj && "length" in obj && obj.length,
		type = jQuery.type( obj );

	if ( type === "function" || jQuery.isWindow( obj ) ) {
		return false;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v2.2.1
 * http://sizzlejs.com/
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2015-10-17
 */
(function( window ) {

var i,
	support,
	Expr,
	getText,
	isXML,
	tokenize,
	compile,
	select,
	outermostContext,
	sortInput,
	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + 1 * new Date(),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// General-purpose constants
	MAX_NEGATIVE = 1 << 31,

	// Instance methods
	hasOwn = ({}).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	push_native = arr.push,
	push = arr.push,
	slice = arr.slice,
	// Use a stripped-down indexOf as it's faster than native
	// http://jsperf.com/thor-indexof-vs-for/5
	indexOf = function( list, elem ) {
		var i = 0,
			len = list.length;
		for ( ; i < len; i++ ) {
			if ( list[i] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",

	// http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
	identifier = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",

	// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +
		// Operator (capture 2)
		"*([*^$|!~]?=)" + whitespace +
		// "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
		"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
		"*\\]",

	pseudos = ":(" + identifier + ")(?:\\((" +
		// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
		// 1. quoted (capture 3; capture 4 or capture 5)
		"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
		// 2. simple (capture 6)
		"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
		// 3. anything else (capture 2)
		".*" +
		")\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rwhitespace = new RegExp( whitespace + "+", "g" ),
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),

	rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + identifier + ")" ),
		"CLASS": new RegExp( "^\\.(" + identifier + ")" ),
		"TAG": new RegExp( "^(" + identifier + "|[*])" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
			"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
			"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
			whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,
	rescape = /'|\\/g,

	// CSS escapes http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
	funescape = function( _, escaped, escapedWhitespace ) {
		var high = "0x" + escaped - 0x10000;
		// NaN means non-codepoint
		// Support: Firefox<24
		// Workaround erroneous numeric interpretation of +"0x"
		return high !== high || escapedWhitespace ?
			escaped :
			high < 0 ?
				// BMP codepoint
				String.fromCharCode( high + 0x10000 ) :
				// Supplemental Plane codepoint (surrogate pair)
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
	},

	// Used for iframes
	// See setDocument()
	// Removing the function wrapper causes a "Permission Denied"
	// error in IE
	unloadHandler = function() {
		setDocument();
	};

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		(arr = slice.call( preferredDoc.childNodes )),
		preferredDoc.childNodes
	);
	// Support: Android<4.0
	// Detect silently failing push.apply
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			push_native.apply( target, slice.call(els) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
				i = 0;
			// Can't trust NodeList.length
			while ( (target[j++] = els[i++]) ) {}
			target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var m, i, elem, nid, nidselect, match, groups, newSelector,
		newContext = context && context.ownerDocument,

		// nodeType defaults to 9, since context defaults to document
		nodeType = context ? context.nodeType : 9;

	results = results || [];

	// Return early from calls with invalid selector or context
	if ( typeof selector !== "string" || !selector ||
		nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

		return results;
	}

	// Try to shortcut find operations (as opposed to filters) in HTML documents
	if ( !seed ) {

		if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
			setDocument( context );
		}
		context = context || document;

		if ( documentIsHTML ) {

			// If the selector is sufficiently simple, try using a "get*By*" DOM method
			// (excepting DocumentFragment context, where the methods don't exist)
			if ( nodeType !== 11 && (match = rquickExpr.exec( selector )) ) {

				// ID selector
				if ( (m = match[1]) ) {

					// Document context
					if ( nodeType === 9 ) {
						if ( (elem = context.getElementById( m )) ) {

							// Support: IE, Opera, Webkit
							// TODO: identify versions
							// getElementById can match elements by name instead of ID
							if ( elem.id === m ) {
								results.push( elem );
								return results;
							}
						} else {
							return results;
						}

					// Element context
					} else {

						// Support: IE, Opera, Webkit
						// TODO: identify versions
						// getElementById can match elements by name instead of ID
						if ( newContext && (elem = newContext.getElementById( m )) &&
							contains( context, elem ) &&
							elem.id === m ) {

							results.push( elem );
							return results;
						}
					}

				// Type selector
				} else if ( match[2] ) {
					push.apply( results, context.getElementsByTagName( selector ) );
					return results;

				// Class selector
				} else if ( (m = match[3]) && support.getElementsByClassName &&
					context.getElementsByClassName ) {

					push.apply( results, context.getElementsByClassName( m ) );
					return results;
				}
			}

			// Take advantage of querySelectorAll
			if ( support.qsa &&
				!compilerCache[ selector + " " ] &&
				(!rbuggyQSA || !rbuggyQSA.test( selector )) ) {

				if ( nodeType !== 1 ) {
					newContext = context;
					newSelector = selector;

				// qSA looks outside Element context, which is not what we want
				// Thanks to Andrew Dupont for this workaround technique
				// Support: IE <=8
				// Exclude object elements
				} else if ( context.nodeName.toLowerCase() !== "object" ) {

					// Capture the context ID, setting it first if necessary
					if ( (nid = context.getAttribute( "id" )) ) {
						nid = nid.replace( rescape, "\\$&" );
					} else {
						context.setAttribute( "id", (nid = expando) );
					}

					// Prefix every selector in the list
					groups = tokenize( selector );
					i = groups.length;
					nidselect = ridentifier.test( nid ) ? "#" + nid : "[id='" + nid + "']";
					while ( i-- ) {
						groups[i] = nidselect + " " + toSelector( groups[i] );
					}
					newSelector = groups.join( "," );

					// Expand context for sibling selectors
					newContext = rsibling.test( selector ) && testContext( context.parentNode ) ||
						context;
				}

				if ( newSelector ) {
					try {
						push.apply( results,
							newContext.querySelectorAll( newSelector )
						);
						return results;
					} catch ( qsaError ) {
					} finally {
						if ( nid === expando ) {
							context.removeAttribute( "id" );
						}
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {function(string, object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
	var keys = [];

	function cache( key, value ) {
		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {
			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return (cache[ key + " " ] = value);
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
	fn[ expando ] = true;
	return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created div and expects a boolean result
 */
function assert( fn ) {
	var div = document.createElement("div");

	try {
		return !!fn( div );
	} catch (e) {
		return false;
	} finally {
		// Remove from its parent by default
		if ( div.parentNode ) {
			div.parentNode.removeChild( div );
		}
		// release memory in IE
		div = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
	var arr = attrs.split("|"),
		i = arr.length;

	while ( i-- ) {
		Expr.attrHandle[ arr[i] ] = handler;
	}
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
	var cur = b && a,
		diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
			( ~b.sourceIndex || MAX_NEGATIVE ) -
			( ~a.sourceIndex || MAX_NEGATIVE );

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( (cur = cur.nextSibling) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return name === "input" && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return (name === "input" || name === "button") && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
	return markFunction(function( argument ) {
		argument = +argument;
		return markFunction(function( seed, matches ) {
			var j,
				matchIndexes = fn( [], seed.length, argument ),
				i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ (j = matchIndexes[i]) ] ) {
					seed[j] = !(matches[j] = seed[j]);
				}
			}
		});
	});
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
	return context && typeof context.getElementsByTagName !== "undefined" && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
	// documentElement is verified for cases where it doesn't yet exist
	// (such as loading iframes in IE - #4833)
	var documentElement = elem && (elem.ownerDocument || elem).documentElement;
	return documentElement ? documentElement.nodeName !== "HTML" : false;
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
	var hasCompare, parent,
		doc = node ? node.ownerDocument || node : preferredDoc;

	// Return early if doc is invalid or already selected
	if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Update global variables
	document = doc;
	docElem = document.documentElement;
	documentIsHTML = !isXML( document );

	// Support: IE 9-11, Edge
	// Accessing iframe documents after unload throws "permission denied" errors (jQuery #13936)
	if ( (parent = document.defaultView) && parent.top !== parent ) {
		// Support: IE 11
		if ( parent.addEventListener ) {
			parent.addEventListener( "unload", unloadHandler, false );

		// Support: IE 9 - 10 only
		} else if ( parent.attachEvent ) {
			parent.attachEvent( "onunload", unloadHandler );
		}
	}

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties
	// (excepting IE8 booleans)
	support.attributes = assert(function( div ) {
		div.className = "i";
		return !div.getAttribute("className");
	});

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert(function( div ) {
		div.appendChild( document.createComment("") );
		return !div.getElementsByTagName("*").length;
	});

	// Support: IE<9
	support.getElementsByClassName = rnative.test( document.getElementsByClassName );

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert(function( div ) {
		docElem.appendChild( div ).id = expando;
		return !document.getElementsByName || !document.getElementsByName( expando ).length;
	});

	// ID find and filter
	if ( support.getById ) {
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var m = context.getElementById( id );
				return m ? [ m ] : [];
			}
		};
		Expr.filter["ID"] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute("id") === attrId;
			};
		};
	} else {
		// Support: IE6/7
		// getElementById is not reliable as a find shortcut
		delete Expr.find["ID"];

		Expr.filter["ID"] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== "undefined" &&
					elem.getAttributeNode("id");
				return node && node.value === attrId;
			};
		};
	}

	// Tag
	Expr.find["TAG"] = support.getElementsByTagName ?
		function( tag, context ) {
			if ( typeof context.getElementsByTagName !== "undefined" ) {
				return context.getElementsByTagName( tag );

			// DocumentFragment nodes don't have gEBTN
			} else if ( support.qsa ) {
				return context.querySelectorAll( tag );
			}
		} :

		function( tag, context ) {
			var elem,
				tmp = [],
				i = 0,
				// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( (elem = results[i++]) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
		if ( typeof context.getElementsByClassName !== "undefined" && documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See http://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( (support.qsa = rnative.test( document.querySelectorAll )) ) {
		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert(function( div ) {
			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// http://bugs.jquery.com/ticket/12359
			docElem.appendChild( div ).innerHTML = "<a id='" + expando + "'></a>" +
				"<select id='" + expando + "-\r\\' msallowcapture=''>" +
				"<option selected=''></option></select>";

			// Support: IE8, Opera 11-12.16
			// Nothing should be selected when empty strings follow ^= or $= or *=
			// The test attribute must be unknown in Opera but "safe" for WinRT
			// http://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
			if ( div.querySelectorAll("[msallowcapture^='']").length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !div.querySelectorAll("[selected]").length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Support: Chrome<29, Android<4.4, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.8+
			if ( !div.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
				rbuggyQSA.push("~=");
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":checked").length ) {
				rbuggyQSA.push(":checked");
			}

			// Support: Safari 8+, iOS 8+
			// https://bugs.webkit.org/show_bug.cgi?id=136851
			// In-page `selector#id sibing-combinator selector` fails
			if ( !div.querySelectorAll( "a#" + expando + "+*" ).length ) {
				rbuggyQSA.push(".#.+[+~]");
			}
		});

		assert(function( div ) {
			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = document.createElement("input");
			input.setAttribute( "type", "hidden" );
			div.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( div.querySelectorAll("[name=d]").length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( !div.querySelectorAll(":enabled").length ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Opera 10-11 does not throw on post-comma invalid pseudos
			div.querySelectorAll("*,:x");
			rbuggyQSA.push(",.*:");
		});
	}

	if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||
		docElem.webkitMatchesSelector ||
		docElem.mozMatchesSelector ||
		docElem.oMatchesSelector ||
		docElem.msMatchesSelector) )) ) {

		assert(function( div ) {
			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( div, "div" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( div, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		});
	}

	rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
	rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully self-exclusive
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
		function( a, b ) {
			var adown = a.nodeType === 9 ? a.documentElement : a,
				bup = b && b.parentNode;
			return a === bup || !!( bup && bup.nodeType === 1 && (
				adown.contains ?
					adown.contains( bup ) :
					a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			));
		} :
		function( a, b ) {
			if ( b ) {
				while ( (b = b.parentNode) ) {
					if ( b === a ) {
						return true;
					}
				}
			}
			return false;
		};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
			a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

			// Choose the first element that is related to our preferred document
			if ( a === document || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
				return -1;
			}
			if ( b === document || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
				return 1;
			}

			// Maintain original order
			return sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {
		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
			i = 0,
			aup = a.parentNode,
			bup = b.parentNode,
			ap = [ a ],
			bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {
			return a === document ? -1 :
				b === document ? 1 :
				aup ? -1 :
				bup ? 1 :
				sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;

		// If the nodes are siblings, we can do a quick check
		} else if ( aup === bup ) {
			return siblingCheck( a, b );
		}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( (cur = cur.parentNode) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( (cur = cur.parentNode) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[i] === bp[i] ) {
			i++;
		}

		return i ?
			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[i], bp[i] ) :

			// Otherwise nodes in our document sort first
			ap[i] === preferredDoc ? -1 :
			bp[i] === preferredDoc ? 1 :
			0;
	};

	return document;
};

Sizzle.matches = function( expr, elements ) {
	return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	// Make sure that attribute selectors are quoted
	expr = expr.replace( rattributeQuotes, "='$1']" );

	if ( support.matchesSelector && documentIsHTML &&
		!compilerCache[ expr + " " ] &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||
					// As well, disconnected nodes are said to be in a document
					// fragment in IE 9
					elem.document && elem.document.nodeType !== 11 ) {
				return ret;
			}
		} catch (e) {}
	}

	return Sizzle( expr, document, null, [ elem ] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
	// Set document vars if needed
	if ( ( context.ownerDocument || context ) !== document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],
		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
			fn( elem, name, !documentIsHTML ) :
			undefined;

	return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
			elem.getAttribute( name ) :
			(val = elem.getAttributeNode(name)) && val.specified ?
				val.value :
				null;
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
	var elem,
		duplicates = [],
		j = 0,
		i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( (elem = results[i++]) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
	var node,
		ret = "",
		i = 0,
		nodeType = elem.nodeType;

	if ( !nodeType ) {
		// If no nodeType, this is expected to be an array
		while ( (node = elem[i++]) ) {
			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {
			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}
	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[1] = match[1].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[3] = ( match[3] || match[4] || match[5] || "" ).replace( runescape, funescape );

			if ( match[2] === "~=" ) {
				match[3] = " " + match[3] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {
			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
			match[1] = match[1].toLowerCase();

			if ( match[1].slice( 0, 3 ) === "nth" ) {
				// nth-* requires argument
				if ( !match[3] ) {
					Sizzle.error( match[0] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
				match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

			// other types prohibit arguments
			} else if ( match[3] ) {
				Sizzle.error( match[0] );
			}

			return match;
		},

		"PSEUDO": function( match ) {
			var excess,
				unquoted = !match[6] && match[2];

			if ( matchExpr["CHILD"].test( match[0] ) ) {
				return null;
			}

			// Accept quoted arguments as-is
			if ( match[3] ) {
				match[2] = match[4] || match[5] || "";

			// Strip excess characters from unquoted arguments
			} else if ( unquoted && rpseudo.test( unquoted ) &&
				// Get excess from tokenize (recursively)
				(excess = tokenize( unquoted, true )) &&
				// advance to the next closing parenthesis
				(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

				// excess is a negative index
				match[0] = match[0].slice( 0, excess );
				match[2] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
				function() { return true; } :
				function( elem ) {
					return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
				};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
				(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
				classCache( className, function( elem ) {
					return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "" );
				});
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				return operator === "=" ? result === check :
					operator === "!=" ? result !== check :
					operator === "^=" ? check && result.indexOf( check ) === 0 :
					operator === "*=" ? check && result.indexOf( check ) > -1 :
					operator === "$=" ? check && result.slice( -check.length ) === check :
					operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
					operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
					false;
			};
		},

		"CHILD": function( type, what, argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
				forward = type.slice( -4 ) !== "last",
				ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, context, xml ) {
					var cache, uniqueCache, outerCache, node, nodeIndex, start,
						dir = simple !== forward ? "nextSibling" : "previousSibling",
						parent = elem.parentNode,
						name = ofType && elem.nodeName.toLowerCase(),
						useCache = !xml && !ofType,
						diff = false;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( (node = node[ dir ]) ) {
									if ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) {

										return false;
									}
								}
								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {

							// Seek `elem` from a previously-cached index

							// ...in a gzip-friendly way
							node = parent;
							outerCache = node[ expando ] || (node[ expando ] = {});

							// Support: IE <9 only
							// Defend against cloned attroperties (jQuery gh-1709)
							uniqueCache = outerCache[ node.uniqueID ] ||
								(outerCache[ node.uniqueID ] = {});

							cache = uniqueCache[ type ] || [];
							nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
							diff = nodeIndex && cache[ 2 ];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( (node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								(diff = nodeIndex = 0) || start.pop()) ) {

								// When found, cache indexes on `parent` and break
								if ( node.nodeType === 1 && ++diff && node === elem ) {
									uniqueCache[ type ] = [ dirruns, nodeIndex, diff ];
									break;
								}
							}

						} else {
							// Use previously-cached element index if available
							if ( useCache ) {
								// ...in a gzip-friendly way
								node = elem;
								outerCache = node[ expando ] || (node[ expando ] = {});

								// Support: IE <9 only
								// Defend against cloned attroperties (jQuery gh-1709)
								uniqueCache = outerCache[ node.uniqueID ] ||
									(outerCache[ node.uniqueID ] = {});

								cache = uniqueCache[ type ] || [];
								nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
								diff = nodeIndex;
							}

							// xml :nth-child(...)
							// or :nth-last-child(...) or :nth(-last)?-of-type(...)
							if ( diff === false ) {
								// Use the same loop as above to seek `elem` from the start
								while ( (node = ++nodeIndex && node && node[ dir ] ||
									(diff = nodeIndex = 0) || start.pop()) ) {

									if ( ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) &&
										++diff ) {

										// Cache the index of each encountered element
										if ( useCache ) {
											outerCache = node[ expando ] || (node[ expando ] = {});

											// Support: IE <9 only
											// Defend against cloned attroperties (jQuery gh-1709)
											uniqueCache = outerCache[ node.uniqueID ] ||
												(outerCache[ node.uniqueID ] = {});

											uniqueCache[ type ] = [ dirruns, diff ];
										}

										if ( node === elem ) {
											break;
										}
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
		},

		"PSEUDO": function( pseudo, argument ) {
			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
				fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
					Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
					markFunction(function( seed, matches ) {
						var idx,
							matched = fn( seed, argument ),
							i = matched.length;
						while ( i-- ) {
							idx = indexOf( seed, matched[i] );
							seed[ idx ] = !( matches[ idx ] = matched[i] );
						}
					}) :
					function( elem ) {
						return fn( elem, 0, args );
					};
			}

			return fn;
		}
	},

	pseudos: {
		// Potentially complex pseudos
		"not": markFunction(function( selector ) {
			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
				results = [],
				matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
				markFunction(function( seed, matches, context, xml ) {
					var elem,
						unmatched = matcher( seed, null, xml, [] ),
						i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( (elem = unmatched[i]) ) {
							seed[i] = !(matches[i] = elem);
						}
					}
				}) :
				function( elem, context, xml ) {
					input[0] = elem;
					matcher( input, null, xml, results );
					// Don't keep the element (issue #299)
					input[0] = null;
					return !results.pop();
				};
		}),

		"has": markFunction(function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		}),

		"contains": markFunction(function( text ) {
			text = text.replace( runescape, funescape );
			return function( elem ) {
				return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
			};
		}),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {
			// lang value must be a valid identifier
			if ( !ridentifier.test(lang || "") ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( (elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

						elemLang = elemLang.toLowerCase();
						return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
					}
				} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
				return false;
			};
		}),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
		},

		// Boolean properties
		"enabled": function( elem ) {
			return elem.disabled === false;
		},

		"disabled": function( elem ) {
			return elem.disabled === true;
		},

		"checked": function( elem ) {
			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
		},

		"selected": function( elem ) {
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {
			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos["empty"]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
				elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
		},

		// Position-in-collection
		"first": createPositionalPseudo(function() {
			return [ 0 ];
		}),

		"last": createPositionalPseudo(function( matchIndexes, length ) {
			return [ length - 1 ];
		}),

		"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		}),

		"even": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"odd": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		})
	}
};

Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
	var matched, match, tokens, type,
		soFar, groups, preFilters,
		cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || (match = rcomma.exec( soFar )) ) {
			if ( match ) {
				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[0].length ) || soFar;
			}
			groups.push( (tokens = []) );
		}

		matched = false;

		// Combinators
		if ( (match = rcombinators.exec( soFar )) ) {
			matched = match.shift();
			tokens.push({
				value: matched,
				// Cast descendant combinators to space
				type: match[0].replace( rtrim, " " )
			});
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
				(match = preFilters[ type ]( match ))) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					type: type,
					matches: match
				});
				soFar = soFar.slice( matched.length );
			}
		}

		if ( !matched ) {
			break;
		}
	}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
		soFar.length :
		soFar ?
			Sizzle.error( selector ) :
			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
};

function toSelector( tokens ) {
	var i = 0,
		len = tokens.length,
		selector = "";
	for ( ; i < len; i++ ) {
		selector += tokens[i].value;
	}
	return selector;
}

function addCombinator( matcher, combinator, base ) {
	var dir = combinator.dir,
		checkNonElements = base && dir === "parentNode",
		doneName = done++;

	return combinator.first ?
		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( (elem = elem[ dir ]) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, uniqueCache, outerCache,
				newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from combinator caching
			if ( xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || (elem[ expando ] = {});

						// Support: IE <9 only
						// Defend against cloned attroperties (jQuery gh-1709)
						uniqueCache = outerCache[ elem.uniqueID ] || (outerCache[ elem.uniqueID ] = {});

						if ( (oldCache = uniqueCache[ dir ]) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
							return (newCache[ 2 ] = oldCache[ 2 ]);
						} else {
							// Reuse newcache so results back-propagate to previous elements
							uniqueCache[ dir ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
								return true;
							}
						}
					}
				}
			}
		};
}

function elementMatcher( matchers ) {
	return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[i]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[0];
}

function multipleContexts( selector, contexts, results ) {
	var i = 0,
		len = contexts.length;
	for ( ; i < len; i++ ) {
		Sizzle( selector, contexts[i], results );
	}
	return results;
}

function condense( unmatched, map, filter, context, xml ) {
	var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

	for ( ; i < len; i++ ) {
		if ( (elem = unmatched[i]) ) {
			if ( !filter || filter( elem, context, xml ) ) {
				newUnmatched.push( elem );
				if ( mapped ) {
					map.push( i );
				}
			}
		}
	}

	return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
	if ( postFilter && !postFilter[ expando ] ) {
		postFilter = setMatcher( postFilter );
	}
	if ( postFinder && !postFinder[ expando ] ) {
		postFinder = setMatcher( postFinder, postSelector );
	}
	return markFunction(function( seed, results, context, xml ) {
		var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
				condense( elems, preMap, preFilter, context, xml ) :
				elems,

			matcherOut = matcher ?
				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
				matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( (elem = temp[i]) ) {
					matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {
					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) ) {
							// Restore matcherIn since elem is not yet a final match
							temp.push( (matcherIn[i] = elem) );
						}
					}
					postFinder( null, (matcherOut = []), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( (elem = matcherOut[i]) &&
						(temp = postFinder ? indexOf( seed, elem ) : preMap[i]) > -1 ) {

						seed[temp] = !(results[temp] = elem);
					}
				}
			}

		// Add elements to results, through postFinder if defined
		} else {
			matcherOut = condense(
				matcherOut === results ?
					matcherOut.splice( preexisting, matcherOut.length ) :
					matcherOut
			);
			if ( postFinder ) {
				postFinder( null, results, matcherOut, xml );
			} else {
				push.apply( results, matcherOut );
			}
		}
	});
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
		len = tokens.length,
		leadingRelative = Expr.relative[ tokens[0].type ],
		implicitRelative = leadingRelative || Expr.relative[" "],
		i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				(checkContext = context).nodeType ?
					matchContext( elem, context, xml ) :
					matchAnyContext( elem, context, xml ) );
			// Avoid hanging onto element (issue #299)
			checkContext = null;
			return ret;
		} ];

	for ( ; i < len; i++ ) {
		if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
			matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
		} else {
			matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {
				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[j].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(
						// If the preceding token was a descendant combinator, insert an implicit any-element `*`
						tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
					).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
					j < len && toSelector( tokens )
				);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
		byElement = elementMatchers.length > 0,
		superMatcher = function( seed, context, xml, results, outermost ) {
			var elem, j, matcher,
				matchedCount = 0,
				i = "0",
				unmatched = seed && [],
				setMatched = [],
				contextBackup = outermostContext,
				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
				len = elems.length;

			if ( outermost ) {
				outermostContext = context === document || context || outermost;
			}

			// Add elements passing elementMatchers directly to results
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;
					if ( !context && elem.ownerDocument !== document ) {
						setDocument( elem );
						xml = !documentIsHTML;
					}
					while ( (matcher = elementMatchers[j++]) ) {
						if ( matcher( elem, context || document, xml) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {
					// They will have gone through all possible matchers
					if ( (elem = !matcher && elem) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// `i` is now the count of elements visited above, and adding it to `matchedCount`
			// makes the latter nonnegative.
			matchedCount += i;

			// Apply set filters to unmatched elements
			// NOTE: This can be skipped if there are no unmatched elements (i.e., `matchedCount`
			// equals `i`), unless we didn't visit _any_ elements in the above loop because we have
			// no element matchers and no seed.
			// Incrementing an initially-string "0" `i` allows `i` to remain a string only in that
			// case, which will result in a "00" `matchedCount` that differs from `i` but is also
			// numerically zero.
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( (matcher = setMatchers[j++]) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {
					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !(unmatched[i] || setMatched[i]) ) {
								setMatched[i] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
				}
			}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

	return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
}

compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
	var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

	if ( !cached ) {
		// Generate a function of recursive functions that can be used to check each element
		if ( !match ) {
			match = tokenize( selector );
		}
		i = match.length;
		while ( i-- ) {
			cached = matcherFromTokens( match[i] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );

		// Save selector and tokenization
		cached.selector = selector;
	}
	return cached;
};

/**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
select = Sizzle.select = function( selector, context, results, seed ) {
	var i, tokens, token, type, find,
		compiled = typeof selector === "function" && selector,
		match = !seed && tokenize( (selector = compiled.selector || selector) );

	results = results || [];

	// Try to minimize operations if there is only one selector in the list and no seed
	// (the latter of which guarantees us context)
	if ( match.length === 1 ) {

		// Reduce context if the leading compound selector is an ID
		tokens = match[0] = match[0].slice( 0 );
		if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
				support.getById && context.nodeType === 9 && documentIsHTML &&
				Expr.relative[ tokens[1].type ] ) {

			context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
			if ( !context ) {
				return results;

			// Precompiled matchers will still verify ancestry, so step up a level
			} else if ( compiled ) {
				context = context.parentNode;
			}

			selector = selector.slice( tokens.shift().value.length );
		}

		// Fetch a seed set for right-to-left matching
		i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
		while ( i-- ) {
			token = tokens[i];

			// Abort if we hit a combinator
			if ( Expr.relative[ (type = token.type) ] ) {
				break;
			}
			if ( (find = Expr.find[ type ]) ) {
				// Search, expanding context for leading sibling combinators
				if ( (seed = find(
					token.matches[0].replace( runescape, funescape ),
					rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
				)) ) {

					// If seed is empty or no tokens remain, we can return early
					tokens.splice( i, 1 );
					selector = seed.length && toSelector( tokens );
					if ( !selector ) {
						push.apply( results, seed );
						return results;
					}

					break;
				}
			}
		}
	}

	// Compile and execute a filtering function if one is not provided
	// Provide `match` to avoid retokenization if we modified the selector above
	( compiled || compile( selector, match ) )(
		seed,
		context,
		!documentIsHTML,
		results,
		!context || rsibling.test( selector ) && testContext( context.parentNode ) || context
	);
	return results;
};

// One-time assignments

// Sort stability
support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

// Support: Chrome 14-35+
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert(function( div1 ) {
	// Should return 1, but returns 4 (following)
	return div1.compareDocumentPosition( document.createElement("div") ) & 1;
});

// Support: IE<8
// Prevent attribute/property "interpolation"
// http://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert(function( div ) {
	div.innerHTML = "<a href='#'></a>";
	return div.firstChild.getAttribute("href") === "#" ;
}) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	});
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert(function( div ) {
	div.innerHTML = "<input/>";
	div.firstChild.setAttribute( "value", "" );
	return div.firstChild.getAttribute( "value" ) === "";
}) ) {
	addHandle( "value", function( elem, name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	});
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert(function( div ) {
	return div.getAttribute("disabled") == null;
}) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
					(val = elem.getAttributeNode( name )) && val.specified ?
					val.value :
				null;
		}
	});
}

return Sizzle;

})( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;
jQuery.expr[ ":" ] = jQuery.expr.pseudos;
jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;



var dir = function( elem, dir, until ) {
	var matched = [],
		truncate = until !== undefined;

	while ( ( elem = elem[ dir ] ) && elem.nodeType !== 9 ) {
		if ( elem.nodeType === 1 ) {
			if ( truncate && jQuery( elem ).is( until ) ) {
				break;
			}
			matched.push( elem );
		}
	}
	return matched;
};


var siblings = function( n, elem ) {
	var matched = [];

	for ( ; n; n = n.nextSibling ) {
		if ( n.nodeType === 1 && n !== elem ) {
			matched.push( n );
		}
	}

	return matched;
};


var rneedsContext = jQuery.expr.match.needsContext;

var rsingleTag = ( /^<([\w-]+)\s*\/?>(?:<\/\1>|)$/ );



var risSimple = /^.[^:#\[\.,]*$/;

// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( jQuery.isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			/* jshint -W018 */
			return !!qualifier.call( elem, i, elem ) !== not;
		} );

	}

	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		} );

	}

	if ( typeof qualifier === "string" ) {
		if ( risSimple.test( qualifier ) ) {
			return jQuery.filter( qualifier, elements, not );
		}

		qualifier = jQuery.filter( qualifier, elements );
	}

	return jQuery.grep( elements, function( elem ) {
		return ( indexOf.call( qualifier, elem ) > -1 ) !== not;
	} );
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	return elems.length === 1 && elem.nodeType === 1 ?
		jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [] :
		jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
			return elem.nodeType === 1;
		} ) );
};

jQuery.fn.extend( {
	find: function( selector ) {
		var i,
			len = this.length,
			ret = [],
			self = this;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter( function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			} ) );
		}

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		// Needed because $( selector, context ) becomes $( context ).find( selector )
		ret = this.pushStack( len > 1 ? jQuery.unique( ret ) : ret );
		ret.selector = this.selector ? this.selector + " " + selector : selector;
		return ret;
	},
	filter: function( selector ) {
		return this.pushStack( winnow( this, selector || [], false ) );
	},
	not: function( selector ) {
		return this.pushStack( winnow( this, selector || [], true ) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
				jQuery( selector ) :
				selector || [],
			false
		).length;
	}
} );


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,

	init = jQuery.fn.init = function( selector, context, root ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// Method init() accepts an alternate rootjQuery
		// so migrate can support jQuery.sub (gh-2101)
		root = root || rootjQuery;

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector[ 0 ] === "<" &&
				selector[ selector.length - 1 ] === ">" &&
				selector.length >= 3 ) {

				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = rquickExpr.exec( selector );
			}

			// Match html or make sure no context is specified for #id
			if ( match && ( match[ 1 ] || !context ) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[ 1 ] ) {
					context = context instanceof jQuery ? context[ 0 ] : context;

					// Option to run scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[ 1 ],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
					) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[ 1 ] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {

							// Properties of context are called as methods if possible
							if ( jQuery.isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
							} else {
								this.attr( match, context[ match ] );
							}
						}
					}

					return this;

				// HANDLE: $(#id)
				} else {
					elem = document.getElementById( match[ 2 ] );

					// Support: Blackberry 4.6
					// gEBID returns nodes no longer in the document (#6963)
					if ( elem && elem.parentNode ) {

						// Inject the element directly into the jQuery object
						this.length = 1;
						this[ 0 ] = elem;
					}

					this.context = document;
					this.selector = selector;
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || root ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(DOMElement)
		} else if ( selector.nodeType ) {
			this.context = this[ 0 ] = selector;
			this.length = 1;
			return this;

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( jQuery.isFunction( selector ) ) {
			return root.ready !== undefined ?
				root.ready( selector ) :

				// Execute immediately if ready is not present
				selector( jQuery );
		}

		if ( selector.selector !== undefined ) {
			this.selector = selector.selector;
			this.context = selector.context;
		}

		return jQuery.makeArray( selector, this );
	};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,

	// Methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.fn.extend( {
	has: function( target ) {
		var targets = jQuery( target, this ),
			l = targets.length;

		return this.filter( function() {
			var i = 0;
			for ( ; i < l; i++ ) {
				if ( jQuery.contains( this, targets[ i ] ) ) {
					return true;
				}
			}
		} );
	},

	closest: function( selectors, context ) {
		var cur,
			i = 0,
			l = this.length,
			matched = [],
			pos = rneedsContext.test( selectors ) || typeof selectors !== "string" ?
				jQuery( selectors, context || this.context ) :
				0;

		for ( ; i < l; i++ ) {
			for ( cur = this[ i ]; cur && cur !== context; cur = cur.parentNode ) {

				// Always skip document fragments
				if ( cur.nodeType < 11 && ( pos ?
					pos.index( cur ) > -1 :

					// Don't pass non-elements to Sizzle
					cur.nodeType === 1 &&
						jQuery.find.matchesSelector( cur, selectors ) ) ) {

					matched.push( cur );
					break;
				}
			}
		}

		return this.pushStack( matched.length > 1 ? jQuery.uniqueSort( matched ) : matched );
	},

	// Determine the position of an element within the set
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
		}

		// Index in selector
		if ( typeof elem === "string" ) {
			return indexOf.call( jQuery( elem ), this[ 0 ] );
		}

		// Locate the position of the desired element
		return indexOf.call( this,

			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[ 0 ] : elem
		);
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.uniqueSort(
				jQuery.merge( this.get(), jQuery( selector, context ) )
			)
		);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter( selector )
		);
	}
} );

function sibling( cur, dir ) {
	while ( ( cur = cur[ dir ] ) && cur.nodeType !== 1 ) {}
	return cur;
}

jQuery.each( {
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, i, until ) {
		return dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, i, until ) {
		return dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, i, until ) {
		return dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return siblings( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return siblings( elem.firstChild );
	},
	contents: function( elem ) {
		return elem.contentDocument || jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var matched = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			matched = jQuery.filter( selector, matched );
		}

		if ( this.length > 1 ) {

			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				jQuery.uniqueSort( matched );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				matched.reverse();
			}
		}

		return this.pushStack( matched );
	};
} );
var rnotwhite = ( /\S+/g );



// Convert String-formatted options into Object-formatted ones
function createOptions( options ) {
	var object = {};
	jQuery.each( options.match( rnotwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	} );
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
		createOptions( options ) :
		jQuery.extend( {}, options );

	var // Flag to know if list is currently firing
		firing,

		// Last fire value for non-forgettable lists
		memory,

		// Flag to know if list was already fired
		fired,

		// Flag to prevent firing
		locked,

		// Actual callback list
		list = [],

		// Queue of execution data for repeatable lists
		queue = [],

		// Index of currently firing callback (modified by add/remove as needed)
		firingIndex = -1,

		// Fire callbacks
		fire = function() {

			// Enforce single-firing
			locked = options.once;

			// Execute callbacks for all pending executions,
			// respecting firingIndex overrides and runtime changes
			fired = firing = true;
			for ( ; queue.length; firingIndex = -1 ) {
				memory = queue.shift();
				while ( ++firingIndex < list.length ) {

					// Run callback and check for early termination
					if ( list[ firingIndex ].apply( memory[ 0 ], memory[ 1 ] ) === false &&
						options.stopOnFalse ) {

						// Jump to end and forget the data so .add doesn't re-fire
						firingIndex = list.length;
						memory = false;
					}
				}
			}

			// Forget the data if we're done with it
			if ( !options.memory ) {
				memory = false;
			}

			firing = false;

			// Clean up if we're done firing for good
			if ( locked ) {

				// Keep an empty list if we have data for future add calls
				if ( memory ) {
					list = [];

				// Otherwise, this object is spent
				} else {
					list = "";
				}
			}
		},

		// Actual Callbacks object
		self = {

			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {

					// If we have memory from a past run, we should fire after adding
					if ( memory && !firing ) {
						firingIndex = list.length - 1;
						queue.push( memory );
					}

					( function add( args ) {
						jQuery.each( args, function( _, arg ) {
							if ( jQuery.isFunction( arg ) ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && jQuery.type( arg ) !== "string" ) {

								// Inspect recursively
								add( arg );
							}
						} );
					} )( arguments );

					if ( memory && !firing ) {
						fire();
					}
				}
				return this;
			},

			// Remove a callback from the list
			remove: function() {
				jQuery.each( arguments, function( _, arg ) {
					var index;
					while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
						list.splice( index, 1 );

						// Handle firing indexes
						if ( index <= firingIndex ) {
							firingIndex--;
						}
					}
				} );
				return this;
			},

			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ?
					jQuery.inArray( fn, list ) > -1 :
					list.length > 0;
			},

			// Remove all callbacks from the list
			empty: function() {
				if ( list ) {
					list = [];
				}
				return this;
			},

			// Disable .fire and .add
			// Abort any current/pending executions
			// Clear all callbacks and values
			disable: function() {
				locked = queue = [];
				list = memory = "";
				return this;
			},
			disabled: function() {
				return !list;
			},

			// Disable .fire
			// Also disable .add unless we have memory (since it would have no effect)
			// Abort any pending executions
			lock: function() {
				locked = queue = [];
				if ( !memory ) {
					list = memory = "";
				}
				return this;
			},
			locked: function() {
				return !!locked;
			},

			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( !locked ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					queue.push( args );
					if ( !firing ) {
						fire();
					}
				}
				return this;
			},

			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},

			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};


jQuery.extend( {

	Deferred: function( func ) {
		var tuples = [

				// action, add listener, listener list, final state
				[ "resolve", "done", jQuery.Callbacks( "once memory" ), "resolved" ],
				[ "reject", "fail", jQuery.Callbacks( "once memory" ), "rejected" ],
				[ "notify", "progress", jQuery.Callbacks( "memory" ) ]
			],
			state = "pending",
			promise = {
				state: function() {
					return state;
				},
				always: function() {
					deferred.done( arguments ).fail( arguments );
					return this;
				},
				then: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;
					return jQuery.Deferred( function( newDefer ) {
						jQuery.each( tuples, function( i, tuple ) {
							var fn = jQuery.isFunction( fns[ i ] ) && fns[ i ];

							// deferred[ done | fail | progress ] for forwarding actions to newDefer
							deferred[ tuple[ 1 ] ]( function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && jQuery.isFunction( returned.promise ) ) {
									returned.promise()
										.progress( newDefer.notify )
										.done( newDefer.resolve )
										.fail( newDefer.reject );
								} else {
									newDefer[ tuple[ 0 ] + "With" ](
										this === promise ? newDefer.promise() : this,
										fn ? [ returned ] : arguments
									);
								}
							} );
						} );
						fns = null;
					} ).promise();
				},

				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Keep pipe for back-compat
		promise.pipe = promise.then;

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
				stateString = tuple[ 3 ];

			// promise[ done | fail | progress ] = list.add
			promise[ tuple[ 1 ] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add( function() {

					// state = [ resolved | rejected ]
					state = stateString;

				// [ reject_list | resolve_list ].disable; progress_list.lock
				}, tuples[ i ^ 1 ][ 2 ].disable, tuples[ 2 ][ 2 ].lock );
			}

			// deferred[ resolve | reject | notify ]
			deferred[ tuple[ 0 ] ] = function() {
				deferred[ tuple[ 0 ] + "With" ]( this === deferred ? promise : this, arguments );
				return this;
			};
			deferred[ tuple[ 0 ] + "With" ] = list.fireWith;
		} );

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( subordinate /* , ..., subordinateN */ ) {
		var i = 0,
			resolveValues = slice.call( arguments ),
			length = resolveValues.length,

			// the count of uncompleted subordinates
			remaining = length !== 1 ||
				( subordinate && jQuery.isFunction( subordinate.promise ) ) ? length : 0,

			// the master Deferred.
			// If resolveValues consist of only a single Deferred, just use that.
			deferred = remaining === 1 ? subordinate : jQuery.Deferred(),

			// Update function for both resolve and progress values
			updateFunc = function( i, contexts, values ) {
				return function( value ) {
					contexts[ i ] = this;
					values[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( values === progressValues ) {
						deferred.notifyWith( contexts, values );
					} else if ( !( --remaining ) ) {
						deferred.resolveWith( contexts, values );
					}
				};
			},

			progressValues, progressContexts, resolveContexts;

		// Add listeners to Deferred subordinates; treat others as resolved
		if ( length > 1 ) {
			progressValues = new Array( length );
			progressContexts = new Array( length );
			resolveContexts = new Array( length );
			for ( ; i < length; i++ ) {
				if ( resolveValues[ i ] && jQuery.isFunction( resolveValues[ i ].promise ) ) {
					resolveValues[ i ].promise()
						.progress( updateFunc( i, progressContexts, progressValues ) )
						.done( updateFunc( i, resolveContexts, resolveValues ) )
						.fail( deferred.reject );
				} else {
					--remaining;
				}
			}
		}

		// If we're not waiting on anything, resolve the master
		if ( !remaining ) {
			deferred.resolveWith( resolveContexts, resolveValues );
		}

		return deferred.promise();
	}
} );


// The deferred used on DOM ready
var readyList;

jQuery.fn.ready = function( fn ) {

	// Add the callback
	jQuery.ready.promise().done( fn );

	return this;
};

jQuery.extend( {

	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Hold (or release) the ready event
	holdReady: function( hold ) {
		if ( hold ) {
			jQuery.readyWait++;
		} else {
			jQuery.ready( true );
		}
	},

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );

		// Trigger any bound ready events
		if ( jQuery.fn.triggerHandler ) {
			jQuery( document ).triggerHandler( "ready" );
			jQuery( document ).off( "ready" );
		}
	}
} );

/**
 * The ready event handler and self cleanup method
 */
function completed() {
	document.removeEventListener( "DOMContentLoaded", completed );
	window.removeEventListener( "load", completed );
	jQuery.ready();
}

jQuery.ready.promise = function( obj ) {
	if ( !readyList ) {

		readyList = jQuery.Deferred();

		// Catch cases where $(document).ready() is called
		// after the browser event has already occurred.
		// Support: IE9-10 only
		// Older IE sometimes signals "interactive" too soon
		if ( document.readyState === "complete" ||
			( document.readyState !== "loading" && !document.documentElement.doScroll ) ) {

			// Handle it asynchronously to allow scripts the opportunity to delay ready
			window.setTimeout( jQuery.ready );

		} else {

			// Use the handy event callback
			document.addEventListener( "DOMContentLoaded", completed );

			// A fallback to window.onload, that will always work
			window.addEventListener( "load", completed );
		}
	}
	return readyList.promise( obj );
};

// Kick off the DOM ready check even if the user does not
jQuery.ready.promise();




// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
		len = elems.length,
		bulk = key == null;

	// Sets many values
	if ( jQuery.type( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			access( elems, fn, i, key[ i ], true, emptyGet, raw );
		}

	// Sets one value
	} else if ( value !== undefined ) {
		chainable = true;

		if ( !jQuery.isFunction( value ) ) {
			raw = true;
		}

		if ( bulk ) {

			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
			} else {
				bulk = fn;
				fn = function( elem, key, value ) {
					return bulk.call( jQuery( elem ), value );
				};
			}
		}

		if ( fn ) {
			for ( ; i < len; i++ ) {
				fn(
					elems[ i ], key, raw ?
					value :
					value.call( elems[ i ], i, fn( elems[ i ], key ) )
				);
			}
		}
	}

	return chainable ?
		elems :

		// Gets
		bulk ?
			fn.call( elems ) :
			len ? fn( elems[ 0 ], key ) : emptyGet;
};
var acceptData = function( owner ) {

	// Accepts only:
	//  - Node
	//    - Node.ELEMENT_NODE
	//    - Node.DOCUMENT_NODE
	//  - Object
	//    - Any
	/* jshint -W018 */
	return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};




function Data() {
	this.expando = jQuery.expando + Data.uid++;
}

Data.uid = 1;

Data.prototype = {

	register: function( owner, initial ) {
		var value = initial || {};

		// If it is a node unlikely to be stringify-ed or looped over
		// use plain assignment
		if ( owner.nodeType ) {
			owner[ this.expando ] = value;

		// Otherwise secure it in a non-enumerable, non-writable property
		// configurability must be true to allow the property to be
		// deleted with the delete operator
		} else {
			Object.defineProperty( owner, this.expando, {
				value: value,
				writable: true,
				configurable: true
			} );
		}
		return owner[ this.expando ];
	},
	cache: function( owner ) {

		// We can accept data for non-element nodes in modern browsers,
		// but we should not, see #8335.
		// Always return an empty object.
		if ( !acceptData( owner ) ) {
			return {};
		}

		// Check if the owner object already has a cache
		var value = owner[ this.expando ];

		// If not, create one
		if ( !value ) {
			value = {};

			// We can accept data for non-element nodes in modern browsers,
			// but we should not, see #8335.
			// Always return an empty object.
			if ( acceptData( owner ) ) {

				// If it is a node unlikely to be stringify-ed or looped over
				// use plain assignment
				if ( owner.nodeType ) {
					owner[ this.expando ] = value;

				// Otherwise secure it in a non-enumerable property
				// configurable must be true to allow the property to be
				// deleted when data is removed
				} else {
					Object.defineProperty( owner, this.expando, {
						value: value,
						configurable: true
					} );
				}
			}
		}

		return value;
	},
	set: function( owner, data, value ) {
		var prop,
			cache = this.cache( owner );

		// Handle: [ owner, key, value ] args
		if ( typeof data === "string" ) {
			cache[ data ] = value;

		// Handle: [ owner, { properties } ] args
		} else {

			// Copy the properties one-by-one to the cache object
			for ( prop in data ) {
				cache[ prop ] = data[ prop ];
			}
		}
		return cache;
	},
	get: function( owner, key ) {
		return key === undefined ?
			this.cache( owner ) :
			owner[ this.expando ] && owner[ this.expando ][ key ];
	},
	access: function( owner, key, value ) {
		var stored;

		// In cases where either:
		//
		//   1. No key was specified
		//   2. A string key was specified, but no value provided
		//
		// Take the "read" path and allow the get method to determine
		// which value to return, respectively either:
		//
		//   1. The entire cache object
		//   2. The data stored at the key
		//
		if ( key === undefined ||
				( ( key && typeof key === "string" ) && value === undefined ) ) {

			stored = this.get( owner, key );

			return stored !== undefined ?
				stored : this.get( owner, jQuery.camelCase( key ) );
		}

		// When the key is not a string, or both a key and value
		// are specified, set or extend (existing objects) with either:
		//
		//   1. An object of properties
		//   2. A key and value
		//
		this.set( owner, key, value );

		// Since the "set" path can have two possible entry points
		// return the expected data based on which path was taken[*]
		return value !== undefined ? value : key;
	},
	remove: function( owner, key ) {
		var i, name, camel,
			cache = owner[ this.expando ];

		if ( cache === undefined ) {
			return;
		}

		if ( key === undefined ) {
			this.register( owner );

		} else {

			// Support array or space separated string of keys
			if ( jQuery.isArray( key ) ) {

				// If "name" is an array of keys...
				// When data is initially created, via ("key", "val") signature,
				// keys will be converted to camelCase.
				// Since there is no way to tell _how_ a key was added, remove
				// both plain key and camelCase key. #12786
				// This will only penalize the array argument path.
				name = key.concat( key.map( jQuery.camelCase ) );
			} else {
				camel = jQuery.camelCase( key );

				// Try the string as a key before any manipulation
				if ( key in cache ) {
					name = [ key, camel ];
				} else {

					// If a key with the spaces exists, use it.
					// Otherwise, create an array by matching non-whitespace
					name = camel;
					name = name in cache ?
						[ name ] : ( name.match( rnotwhite ) || [] );
				}
			}

			i = name.length;

			while ( i-- ) {
				delete cache[ name[ i ] ];
			}
		}

		// Remove the expando if there's no more data
		if ( key === undefined || jQuery.isEmptyObject( cache ) ) {

			// Support: Chrome <= 35-45+
			// Webkit & Blink performance suffers when deleting properties
			// from DOM nodes, so set to undefined instead
			// https://code.google.com/p/chromium/issues/detail?id=378607
			if ( owner.nodeType ) {
				owner[ this.expando ] = undefined;
			} else {
				delete owner[ this.expando ];
			}
		}
	},
	hasData: function( owner ) {
		var cache = owner[ this.expando ];
		return cache !== undefined && !jQuery.isEmptyObject( cache );
	}
};
var dataPriv = new Data();

var dataUser = new Data();



//	Implementation Summary
//
//	1. Enforce API surface and semantic compatibility with 1.9.x branch
//	2. Improve the module's maintainability by reducing the storage
//		paths to a single mechanism.
//	3. Use the same single mechanism to support "private" and "user" data.
//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
//	5. Avoid exposing implementation details on user objects (eg. expando properties)
//	6. Provide a clear path for implementation upgrade to WeakMap in 2014

var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	rmultiDash = /[A-Z]/g;

function dataAttr( elem, key, data ) {
	var name;

	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {
		name = "data-" + key.replace( rmultiDash, "-$&" ).toLowerCase();
		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = data === "true" ? true :
					data === "false" ? false :
					data === "null" ? null :

					// Only convert to a number if it doesn't change the string
					+data + "" === data ? +data :
					rbrace.test( data ) ? jQuery.parseJSON( data ) :
					data;
			} catch ( e ) {}

			// Make sure we set the data so it isn't changed later
			dataUser.set( elem, key, data );
		} else {
			data = undefined;
		}
	}
	return data;
}

jQuery.extend( {
	hasData: function( elem ) {
		return dataUser.hasData( elem ) || dataPriv.hasData( elem );
	},

	data: function( elem, name, data ) {
		return dataUser.access( elem, name, data );
	},

	removeData: function( elem, name ) {
		dataUser.remove( elem, name );
	},

	// TODO: Now that all calls to _data and _removeData have been replaced
	// with direct calls to dataPriv methods, these can be deprecated.
	_data: function( elem, name, data ) {
		return dataPriv.access( elem, name, data );
	},

	_removeData: function( elem, name ) {
		dataPriv.remove( elem, name );
	}
} );

jQuery.fn.extend( {
	data: function( key, value ) {
		var i, name, data,
			elem = this[ 0 ],
			attrs = elem && elem.attributes;

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = dataUser.get( elem );

				if ( elem.nodeType === 1 && !dataPriv.get( elem, "hasDataAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {

						// Support: IE11+
						// The attrs elements can be null (#14894)
						if ( attrs[ i ] ) {
							name = attrs[ i ].name;
							if ( name.indexOf( "data-" ) === 0 ) {
								name = jQuery.camelCase( name.slice( 5 ) );
								dataAttr( elem, name, data[ name ] );
							}
						}
					}
					dataPriv.set( elem, "hasDataAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each( function() {
				dataUser.set( this, key );
			} );
		}

		return access( this, function( value ) {
			var data, camelKey;

			// The calling jQuery object (element matches) is not empty
			// (and therefore has an element appears at this[ 0 ]) and the
			// `value` parameter was not undefined. An empty jQuery object
			// will result in `undefined` for elem = this[ 0 ] which will
			// throw an exception if an attempt to read a data cache is made.
			if ( elem && value === undefined ) {

				// Attempt to get data from the cache
				// with the key as-is
				data = dataUser.get( elem, key ) ||

					// Try to find dashed key if it exists (gh-2779)
					// This is for 2.2.x only
					dataUser.get( elem, key.replace( rmultiDash, "-$&" ).toLowerCase() );

				if ( data !== undefined ) {
					return data;
				}

				camelKey = jQuery.camelCase( key );

				// Attempt to get data from the cache
				// with the key camelized
				data = dataUser.get( elem, camelKey );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to "discover" the data in
				// HTML5 custom data-* attrs
				data = dataAttr( elem, camelKey, undefined );
				if ( data !== undefined ) {
					return data;
				}

				// We tried really hard, but the data doesn't exist.
				return;
			}

			// Set the data...
			camelKey = jQuery.camelCase( key );
			this.each( function() {

				// First, attempt to store a copy or reference of any
				// data that might've been store with a camelCased key.
				var data = dataUser.get( this, camelKey );

				// For HTML5 data-* attribute interop, we have to
				// store property names with dashes in a camelCase form.
				// This might not apply to all properties...*
				dataUser.set( this, camelKey, value );

				// *... In the case of properties that might _actually_
				// have dashes, we need to also store a copy of that
				// unchanged property.
				if ( key.indexOf( "-" ) > -1 && data !== undefined ) {
					dataUser.set( this, key, value );
				}
			} );
		}, null, value, arguments.length > 1, null, true );
	},

	removeData: function( key ) {
		return this.each( function() {
			dataUser.remove( this, key );
		} );
	}
} );


jQuery.extend( {
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = dataPriv.get( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || jQuery.isArray( data ) ) {
					queue = dataPriv.access( elem, type, jQuery.makeArray( data ) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			startLength = queue.length,
			fn = queue.shift(),
			hooks = jQuery._queueHooks( elem, type ),
			next = function() {
				jQuery.dequeue( elem, type );
			};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// Clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// Not public - generate a queueHooks object, or return the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return dataPriv.get( elem, key ) || dataPriv.access( elem, key, {
			empty: jQuery.Callbacks( "once memory" ).add( function() {
				dataPriv.remove( elem, [ type + "queue", key ] );
			} )
		} );
	}
} );

jQuery.fn.extend( {
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[ 0 ], type );
		}

		return data === undefined ?
			this :
			this.each( function() {
				var queue = jQuery.queue( this, type, data );

				// Ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[ 0 ] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			} );
	},
	dequeue: function( type ) {
		return this.each( function() {
			jQuery.dequeue( this, type );
		} );
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},

	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
			count = 1,
			defer = jQuery.Deferred(),
			elements = this,
			i = this.length,
			resolve = function() {
				if ( !( --count ) ) {
					defer.resolveWith( elements, [ elements ] );
				}
			};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = dataPriv.get( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
} );
var pnum = ( /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/ ).source;

var rcssNum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" );


var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var isHidden = function( elem, el ) {

		// isHidden might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;
		return jQuery.css( elem, "display" ) === "none" ||
			!jQuery.contains( elem.ownerDocument, elem );
	};



function adjustCSS( elem, prop, valueParts, tween ) {
	var adjusted,
		scale = 1,
		maxIterations = 20,
		currentValue = tween ?
			function() { return tween.cur(); } :
			function() { return jQuery.css( elem, prop, "" ); },
		initial = currentValue(),
		unit = valueParts && valueParts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

		// Starting value computation is required for potential unit mismatches
		initialInUnit = ( jQuery.cssNumber[ prop ] || unit !== "px" && +initial ) &&
			rcssNum.exec( jQuery.css( elem, prop ) );

	if ( initialInUnit && initialInUnit[ 3 ] !== unit ) {

		// Trust units reported by jQuery.css
		unit = unit || initialInUnit[ 3 ];

		// Make sure we update the tween properties later on
		valueParts = valueParts || [];

		// Iteratively approximate from a nonzero starting point
		initialInUnit = +initial || 1;

		do {

			// If previous iteration zeroed out, double until we get *something*.
			// Use string for doubling so we don't accidentally see scale as unchanged below
			scale = scale || ".5";

			// Adjust and apply
			initialInUnit = initialInUnit / scale;
			jQuery.style( elem, prop, initialInUnit + unit );

		// Update scale, tolerating zero or NaN from tween.cur()
		// Break the loop if scale is unchanged or perfect, or if we've just had enough.
		} while (
			scale !== ( scale = currentValue() / initial ) && scale !== 1 && --maxIterations
		);
	}

	if ( valueParts ) {
		initialInUnit = +initialInUnit || +initial || 0;

		// Apply relative offset (+=/-=) if specified
		adjusted = valueParts[ 1 ] ?
			initialInUnit + ( valueParts[ 1 ] + 1 ) * valueParts[ 2 ] :
			+valueParts[ 2 ];
		if ( tween ) {
			tween.unit = unit;
			tween.start = initialInUnit;
			tween.end = adjusted;
		}
	}
	return adjusted;
}
var rcheckableType = ( /^(?:checkbox|radio)$/i );

var rtagName = ( /<([\w:-]+)/ );

var rscriptType = ( /^$|\/(?:java|ecma)script/i );



// We have to close these tags to support XHTML (#13200)
var wrapMap = {

	// Support: IE9
	option: [ 1, "<select multiple='multiple'>", "</select>" ],

	// XHTML parsers do not magically insert elements in the
	// same way that tag soup parsers do. So we cannot shorten
	// this by omitting <tbody> or other required elements.
	thead: [ 1, "<table>", "</table>" ],
	col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
	tr: [ 2, "<table><tbody>", "</tbody></table>" ],
	td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

	_default: [ 0, "", "" ]
};

// Support: IE9
wrapMap.optgroup = wrapMap.option;

wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;


function getAll( context, tag ) {

	// Support: IE9-11+
	// Use typeof to avoid zero-argument method invocation on host objects (#15151)
	var ret = typeof context.getElementsByTagName !== "undefined" ?
			context.getElementsByTagName( tag || "*" ) :
			typeof context.querySelectorAll !== "undefined" ?
				context.querySelectorAll( tag || "*" ) :
			[];

	return tag === undefined || tag && jQuery.nodeName( context, tag ) ?
		jQuery.merge( [ context ], ret ) :
		ret;
}


// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		dataPriv.set(
			elems[ i ],
			"globalEval",
			!refElements || dataPriv.get( refElements[ i ], "globalEval" )
		);
	}
}


var rhtml = /<|&#?\w+;/;

function buildFragment( elems, context, scripts, selection, ignored ) {
	var elem, tmp, tag, wrap, contains, j,
		fragment = context.createDocumentFragment(),
		nodes = [],
		i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		elem = elems[ i ];

		if ( elem || elem === 0 ) {

			// Add nodes directly
			if ( jQuery.type( elem ) === "object" ) {

				// Support: Android<4.1, PhantomJS<2
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

			// Convert non-html into a text node
			} else if ( !rhtml.test( elem ) ) {
				nodes.push( context.createTextNode( elem ) );

			// Convert html into DOM nodes
			} else {
				tmp = tmp || fragment.appendChild( context.createElement( "div" ) );

				// Deserialize a standard representation
				tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
				wrap = wrapMap[ tag ] || wrapMap._default;
				tmp.innerHTML = wrap[ 1 ] + jQuery.htmlPrefilter( elem ) + wrap[ 2 ];

				// Descend through wrappers to the right content
				j = wrap[ 0 ];
				while ( j-- ) {
					tmp = tmp.lastChild;
				}

				// Support: Android<4.1, PhantomJS<2
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, tmp.childNodes );

				// Remember the top-level container
				tmp = fragment.firstChild;

				// Ensure the created nodes are orphaned (#12392)
				tmp.textContent = "";
			}
		}
	}

	// Remove wrapper from fragment
	fragment.textContent = "";

	i = 0;
	while ( ( elem = nodes[ i++ ] ) ) {

		// Skip elements already in the context collection (trac-4087)
		if ( selection && jQuery.inArray( elem, selection ) > -1 ) {
			if ( ignored ) {
				ignored.push( elem );
			}
			continue;
		}

		contains = jQuery.contains( elem.ownerDocument, elem );

		// Append to fragment
		tmp = getAll( fragment.appendChild( elem ), "script" );

		// Preserve script evaluation history
		if ( contains ) {
			setGlobalEval( tmp );
		}

		// Capture executables
		if ( scripts ) {
			j = 0;
			while ( ( elem = tmp[ j++ ] ) ) {
				if ( rscriptType.test( elem.type || "" ) ) {
					scripts.push( elem );
				}
			}
		}
	}

	return fragment;
}


( function() {
	var fragment = document.createDocumentFragment(),
		div = fragment.appendChild( document.createElement( "div" ) ),
		input = document.createElement( "input" );

	// Support: Android 4.0-4.3, Safari<=5.1
	// Check state lost if the name is set (#11217)
	// Support: Windows Web Apps (WWA)
	// `name` and `type` must use .setAttribute for WWA (#14901)
	input.setAttribute( "type", "radio" );
	input.setAttribute( "checked", "checked" );
	input.setAttribute( "name", "t" );

	div.appendChild( input );

	// Support: Safari<=5.1, Android<4.2
	// Older WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE<=11+
	// Make sure textarea (and checkbox) defaultValue is properly cloned
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;
} )();


var
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
	rtypenamespace = /^([^.]*)(?:\.(.+)|)/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

// Support: IE9
// See #13393 for more info
function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

function on( elem, types, selector, data, fn, one ) {
	var origFn, type;

	// Types can be a map of types/handlers
	if ( typeof types === "object" ) {

		// ( types-Object, selector, data )
		if ( typeof selector !== "string" ) {

			// ( types-Object, data )
			data = data || selector;
			selector = undefined;
		}
		for ( type in types ) {
			on( elem, type, selector, data, types[ type ], one );
		}
		return elem;
	}

	if ( data == null && fn == null ) {

		// ( types, fn )
		fn = selector;
		data = selector = undefined;
	} else if ( fn == null ) {
		if ( typeof selector === "string" ) {

			// ( types, selector, fn )
			fn = data;
			data = undefined;
		} else {

			// ( types, data, fn )
			fn = data;
			data = selector;
			selector = undefined;
		}
	}
	if ( fn === false ) {
		fn = returnFalse;
	} else if ( !fn ) {
		return elem;
	}

	if ( one === 1 ) {
		origFn = fn;
		fn = function( event ) {

			// Can use an empty set, since event contains the info
			jQuery().off( event );
			return origFn.apply( this, arguments );
		};

		// Use same guid so caller can remove using origFn
		fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
	}
	return elem.each( function() {
		jQuery.event.add( this, types, fn, data, selector );
	} );
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	global: {},

	add: function( elem, types, handler, data, selector ) {

		var handleObjIn, eventHandle, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.get( elem );

		// Don't attach events to noData or text/comment nodes (but allow plain objects)
		if ( !elemData ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !( events = elemData.events ) ) {
			events = elemData.events = {};
		}
		if ( !( eventHandle = elemData.handle ) ) {
			eventHandle = elemData.handle = function( e ) {

				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ?
					jQuery.event.dispatch.apply( elem, arguments ) : undefined;
			};
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend( {
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join( "." )
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !( handlers = events[ type ] ) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener if the special events handler returns false
				if ( !special.setup ||
					special.setup.call( elem, data, namespaces, eventHandle ) === false ) {

					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {

		var j, origCount, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.hasData( elem ) && dataPriv.get( elem );

		if ( !elemData || !( events = elemData.events ) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnotwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[ 2 ] &&
				new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector ||
						selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

					if ( handleObj.selector ) {
						handlers.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown ||
					special.teardown.call( elem, namespaces, elemData.handle ) === false ) {

					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove data and the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			dataPriv.remove( elem, "handle events" );
		}
	},

	dispatch: function( event ) {

		// Make a writable jQuery.Event from the native event object
		event = jQuery.event.fix( event );

		var i, j, ret, matched, handleObj,
			handlerQueue = [],
			args = slice.call( arguments ),
			handlers = ( dataPriv.get( this, "events" ) || {} )[ event.type ] || [],
			special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[ 0 ] = event;
		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( ( matched = handlerQueue[ i++ ] ) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( ( handleObj = matched.handlers[ j++ ] ) &&
				!event.isImmediatePropagationStopped() ) {

				// Triggered event must either 1) have no namespace, or 2) have namespace(s)
				// a subset or equal to those in the bound event (both can have no namespace).
				if ( !event.rnamespace || event.rnamespace.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( ( jQuery.event.special[ handleObj.origType ] || {} ).handle ||
						handleObj.handler ).apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( ( event.result = ret ) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var i, matches, sel, handleObj,
			handlerQueue = [],
			delegateCount = handlers.delegateCount,
			cur = event.target;

		// Support (at least): Chrome, IE9
		// Find delegate handlers
		// Black-hole SVG <use> instance trees (#13180)
		//
		// Support: Firefox<=42+
		// Avoid non-left-click in FF but don't block IE radio events (#3861, gh-2343)
		if ( delegateCount && cur.nodeType &&
			( event.type !== "click" || isNaN( event.button ) || event.button < 1 ) ) {

			for ( ; cur !== this; cur = cur.parentNode || this ) {

				// Don't check non-elements (#13208)
				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.nodeType === 1 && ( cur.disabled !== true || event.type !== "click" ) ) {
					matches = [];
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matches[ sel ] === undefined ) {
							matches[ sel ] = handleObj.needsContext ?
								jQuery( sel, this ).index( cur ) > -1 :
								jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matches[ sel ] ) {
							matches.push( handleObj );
						}
					}
					if ( matches.length ) {
						handlerQueue.push( { elem: cur, handlers: matches } );
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		if ( delegateCount < handlers.length ) {
			handlerQueue.push( { elem: this, handlers: handlers.slice( delegateCount ) } );
		}

		return handlerQueue;
	},

	// Includes some event props shared by KeyEvent and MouseEvent
	props: ( "altKey bubbles cancelable ctrlKey currentTarget detail eventPhase " +
		"metaKey relatedTarget shiftKey target timeStamp view which" ).split( " " ),

	fixHooks: {},

	keyHooks: {
		props: "char charCode key keyCode".split( " " ),
		filter: function( event, original ) {

			// Add which for key events
			if ( event.which == null ) {
				event.which = original.charCode != null ? original.charCode : original.keyCode;
			}

			return event;
		}
	},

	mouseHooks: {
		props: ( "button buttons clientX clientY offsetX offsetY pageX pageY " +
			"screenX screenY toElement" ).split( " " ),
		filter: function( event, original ) {
			var eventDoc, doc, body,
				button = original.button;

			// Calculate pageX/Y if missing and clientX/Y available
			if ( event.pageX == null && original.clientX != null ) {
				eventDoc = event.target.ownerDocument || document;
				doc = eventDoc.documentElement;
				body = eventDoc.body;

				event.pageX = original.clientX +
					( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) -
					( doc && doc.clientLeft || body && body.clientLeft || 0 );
				event.pageY = original.clientY +
					( doc && doc.scrollTop  || body && body.scrollTop  || 0 ) -
					( doc && doc.clientTop  || body && body.clientTop  || 0 );
			}

			// Add which for click: 1 === left; 2 === middle; 3 === right
			// Note: button is not normalized, so don't use it
			if ( !event.which && button !== undefined ) {
				event.which = ( button & 1 ? 1 : ( button & 2 ? 3 : ( button & 4 ? 2 : 0 ) ) );
			}

			return event;
		}
	},

	fix: function( event ) {
		if ( event[ jQuery.expando ] ) {
			return event;
		}

		// Create a writable copy of the event object and normalize some properties
		var i, prop, copy,
			type = event.type,
			originalEvent = event,
			fixHook = this.fixHooks[ type ];

		if ( !fixHook ) {
			this.fixHooks[ type ] = fixHook =
				rmouseEvent.test( type ) ? this.mouseHooks :
				rkeyEvent.test( type ) ? this.keyHooks :
				{};
		}
		copy = fixHook.props ? this.props.concat( fixHook.props ) : this.props;

		event = new jQuery.Event( originalEvent );

		i = copy.length;
		while ( i-- ) {
			prop = copy[ i ];
			event[ prop ] = originalEvent[ prop ];
		}

		// Support: Cordova 2.5 (WebKit) (#13255)
		// All events should have a target; Cordova deviceready doesn't
		if ( !event.target ) {
			event.target = document;
		}

		// Support: Safari 6.0+, Chrome<28
		// Target should not be a text node (#504, #13143)
		if ( event.target.nodeType === 3 ) {
			event.target = event.target.parentNode;
		}

		return fixHook.filter ? fixHook.filter( event, originalEvent ) : event;
	},

	special: {
		load: {

			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		focus: {

			// Fire native event if possible so blur/focus sequence is correct
			trigger: function() {
				if ( this !== safeActiveElement() && this.focus ) {
					this.focus();
					return false;
				}
			},
			delegateType: "focusin"
		},
		blur: {
			trigger: function() {
				if ( this === safeActiveElement() && this.blur ) {
					this.blur();
					return false;
				}
			},
			delegateType: "focusout"
		},
		click: {

			// For checkbox, fire native event so checked state will be right
			trigger: function() {
				if ( this.type === "checkbox" && this.click && jQuery.nodeName( this, "input" ) ) {
					this.click();
					return false;
				}
			},

			// For cross-browser consistency, don't fire native .click() on links
			_default: function( event ) {
				return jQuery.nodeName( event.target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Support: Firefox 20+
				// Firefox doesn't alert if the returnValue field is not set.
				if ( event.result !== undefined && event.originalEvent ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	}
};

jQuery.removeEvent = function( elem, type, handle ) {

	// This "if" is needed for plain objects
	if ( elem.removeEventListener ) {
		elem.removeEventListener( type, handle );
	}
};

jQuery.Event = function( src, props ) {

	// Allow instantiation without the 'new' keyword
	if ( !( this instanceof jQuery.Event ) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
				src.defaultPrevented === undefined &&

				// Support: Android<4.0
				src.returnValue === false ?
			returnTrue :
			returnFalse;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || jQuery.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	constructor: jQuery.Event,
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,
	isSimulated: false,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;

		if ( e && !this.isSimulated ) {
			e.preventDefault();
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopPropagation();
		}
	},
	stopImmediatePropagation: function() {
		var e = this.originalEvent;

		this.isImmediatePropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopImmediatePropagation();
		}

		this.stopPropagation();
	}
};

// Create mouseenter/leave events using mouseover/out and event-time checks
// so that event delegation works in jQuery.
// Do the same for pointerenter/pointerleave and pointerover/pointerout
//
// Support: Safari 7 only
// Safari sends mouseenter too often; see:
// https://code.google.com/p/chromium/issues/detail?id=470258
// for the description of the bug (it existed in older Chrome versions as well).
jQuery.each( {
	mouseenter: "mouseover",
	mouseleave: "mouseout",
	pointerenter: "pointerover",
	pointerleave: "pointerout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
				target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj;

			// For mouseenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || ( related !== target && !jQuery.contains( target, related ) ) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
} );

jQuery.fn.extend( {
	on: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn );
	},
	one: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {

			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ?
					handleObj.origType + "." + handleObj.namespace :
					handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {

			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {

			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each( function() {
			jQuery.event.remove( this, types, fn, selector );
		} );
	}
} );


var
	rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,

	// Support: IE 10-11, Edge 10240+
	// In IE/Edge using regex groups here causes severe slowdowns.
	// See https://connect.microsoft.com/IE/feedback/details/1736512/
	rnoInnerhtml = /<script|<style|<link/i,

	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rscriptTypeMasked = /^true\/(.*)/,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

// Manipulating tables requires a tbody
function manipulationTarget( elem, content ) {
	return jQuery.nodeName( elem, "table" ) &&
		jQuery.nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ?

		elem.getElementsByTagName( "tbody" )[ 0 ] ||
			elem.appendChild( elem.ownerDocument.createElement( "tbody" ) ) :
		elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = ( elem.getAttribute( "type" ) !== null ) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	var match = rscriptTypeMasked.exec( elem.type );

	if ( match ) {
		elem.type = match[ 1 ];
	} else {
		elem.removeAttribute( "type" );
	}

	return elem;
}

function cloneCopyEvent( src, dest ) {
	var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;

	if ( dest.nodeType !== 1 ) {
		return;
	}

	// 1. Copy private data: events, handlers, etc.
	if ( dataPriv.hasData( src ) ) {
		pdataOld = dataPriv.access( src );
		pdataCur = dataPriv.set( dest, pdataOld );
		events = pdataOld.events;

		if ( events ) {
			delete pdataCur.handle;
			pdataCur.events = {};

			for ( type in events ) {
				for ( i = 0, l = events[ type ].length; i < l; i++ ) {
					jQuery.event.add( dest, type, events[ type ][ i ] );
				}
			}
		}
	}

	// 2. Copy user data
	if ( dataUser.hasData( src ) ) {
		udataOld = dataUser.access( src );
		udataCur = jQuery.extend( {}, udataOld );

		dataUser.set( dest, udataCur );
	}
}

// Fix IE bugs, see support tests
function fixInput( src, dest ) {
	var nodeName = dest.nodeName.toLowerCase();

	// Fails to persist the checked state of a cloned checkbox or radio button.
	if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		dest.checked = src.checked;

	// Fails to return the selected option to the default selected state when cloning options
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;
	}
}

function domManip( collection, args, callback, ignored ) {

	// Flatten any nested arrays
	args = concat.apply( [], args );

	var fragment, first, scripts, hasScripts, node, doc,
		i = 0,
		l = collection.length,
		iNoClone = l - 1,
		value = args[ 0 ],
		isFunction = jQuery.isFunction( value );

	// We can't cloneNode fragments that contain checked, in WebKit
	if ( isFunction ||
			( l > 1 && typeof value === "string" &&
				!support.checkClone && rchecked.test( value ) ) ) {
		return collection.each( function( index ) {
			var self = collection.eq( index );
			if ( isFunction ) {
				args[ 0 ] = value.call( this, index, self.html() );
			}
			domManip( self, args, callback, ignored );
		} );
	}

	if ( l ) {
		fragment = buildFragment( args, collection[ 0 ].ownerDocument, false, collection, ignored );
		first = fragment.firstChild;

		if ( fragment.childNodes.length === 1 ) {
			fragment = first;
		}

		// Require either new content or an interest in ignored elements to invoke the callback
		if ( first || ignored ) {
			scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
			hasScripts = scripts.length;

			// Use the original fragment for the last item
			// instead of the first because it can end up
			// being emptied incorrectly in certain situations (#8070).
			for ( ; i < l; i++ ) {
				node = fragment;

				if ( i !== iNoClone ) {
					node = jQuery.clone( node, true, true );

					// Keep references to cloned scripts for later restoration
					if ( hasScripts ) {

						// Support: Android<4.1, PhantomJS<2
						// push.apply(_, arraylike) throws on ancient WebKit
						jQuery.merge( scripts, getAll( node, "script" ) );
					}
				}

				callback.call( collection[ i ], node, i );
			}

			if ( hasScripts ) {
				doc = scripts[ scripts.length - 1 ].ownerDocument;

				// Reenable scripts
				jQuery.map( scripts, restoreScript );

				// Evaluate executable scripts on first document insertion
				for ( i = 0; i < hasScripts; i++ ) {
					node = scripts[ i ];
					if ( rscriptType.test( node.type || "" ) &&
						!dataPriv.access( node, "globalEval" ) &&
						jQuery.contains( doc, node ) ) {

						if ( node.src ) {

							// Optional AJAX dependency, but won't run scripts if not present
							if ( jQuery._evalUrl ) {
								jQuery._evalUrl( node.src );
							}
						} else {
							jQuery.globalEval( node.textContent.replace( rcleanScript, "" ) );
						}
					}
				}
			}
		}
	}

	return collection;
}

function remove( elem, selector, keepData ) {
	var node,
		nodes = selector ? jQuery.filter( selector, elem ) : elem,
		i = 0;

	for ( ; ( node = nodes[ i ] ) != null; i++ ) {
		if ( !keepData && node.nodeType === 1 ) {
			jQuery.cleanData( getAll( node ) );
		}

		if ( node.parentNode ) {
			if ( keepData && jQuery.contains( node.ownerDocument, node ) ) {
				setGlobalEval( getAll( node, "script" ) );
			}
			node.parentNode.removeChild( node );
		}
	}

	return elem;
}

jQuery.extend( {
	htmlPrefilter: function( html ) {
		return html.replace( rxhtmlTag, "<$1></$2>" );
	},

	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var i, l, srcElements, destElements,
			clone = elem.cloneNode( true ),
			inPage = jQuery.contains( elem.ownerDocument, elem );

		// Fix IE cloning issues
		if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
				!jQuery.isXMLDoc( elem ) ) {

			// We eschew Sizzle here for performance reasons: http://jsperf.com/getall-vs-sizzle/2
			destElements = getAll( clone );
			srcElements = getAll( elem );

			for ( i = 0, l = srcElements.length; i < l; i++ ) {
				fixInput( srcElements[ i ], destElements[ i ] );
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					cloneCopyEvent( srcElements[ i ], destElements[ i ] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		// Return the cloned set
		return clone;
	},

	cleanData: function( elems ) {
		var data, elem, type,
			special = jQuery.event.special,
			i = 0;

		for ( ; ( elem = elems[ i ] ) !== undefined; i++ ) {
			if ( acceptData( elem ) ) {
				if ( ( data = elem[ dataPriv.expando ] ) ) {
					if ( data.events ) {
						for ( type in data.events ) {
							if ( special[ type ] ) {
								jQuery.event.remove( elem, type );

							// This is a shortcut to avoid jQuery.event.remove's overhead
							} else {
								jQuery.removeEvent( elem, type, data.handle );
							}
						}
					}

					// Support: Chrome <= 35-45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataPriv.expando ] = undefined;
				}
				if ( elem[ dataUser.expando ] ) {

					// Support: Chrome <= 35-45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataUser.expando ] = undefined;
				}
			}
		}
	}
} );

jQuery.fn.extend( {

	// Keep domManip exposed until 3.0 (gh-2225)
	domManip: domManip,

	detach: function( selector ) {
		return remove( this, selector, true );
	},

	remove: function( selector ) {
		return remove( this, selector );
	},

	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().each( function() {
					if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
						this.textContent = value;
					}
				} );
		}, null, value, arguments.length );
	},

	append: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		} );
	},

	prepend: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		} );
	},

	before: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		} );
	},

	after: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		} );
	},

	empty: function() {
		var elem,
			i = 0;

		for ( ; ( elem = this[ i ] ) != null; i++ ) {
			if ( elem.nodeType === 1 ) {

				// Prevent memory leaks
				jQuery.cleanData( getAll( elem, false ) );

				// Remove any remaining nodes
				elem.textContent = "";
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map( function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		} );
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
				i = 0,
				l = this.length;

			if ( value === undefined && elem.nodeType === 1 ) {
				return elem.innerHTML;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

				value = jQuery.htmlPrefilter( value );

				try {
					for ( ; i < l; i++ ) {
						elem = this[ i ] || {};

						// Remove element nodes and prevent memory leaks
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch ( e ) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function() {
		var ignored = [];

		// Make the changes, replacing each non-ignored context element with the new content
		return domManip( this, arguments, function( elem ) {
			var parent = this.parentNode;

			if ( jQuery.inArray( this, ignored ) < 0 ) {
				jQuery.cleanData( getAll( this ) );
				if ( parent ) {
					parent.replaceChild( elem, this );
				}
			}

		// Force callback invocation
		}, ignored );
	}
} );

jQuery.each( {
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
			ret = [],
			insert = jQuery( selector ),
			last = insert.length - 1,
			i = 0;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone( true );
			jQuery( insert[ i ] )[ original ]( elems );

			// Support: QtWebKit
			// .get() because push.apply(_, arraylike) throws
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
} );


var iframe,
	elemdisplay = {

		// Support: Firefox
		// We have to pre-define these values for FF (#10227)
		HTML: "block",
		BODY: "block"
	};

/**
 * Retrieve the actual display of a element
 * @param {String} name nodeName of the element
 * @param {Object} doc Document object
 */

// Called only from within defaultDisplay
function actualDisplay( name, doc ) {
	var elem = jQuery( doc.createElement( name ) ).appendTo( doc.body ),

		display = jQuery.css( elem[ 0 ], "display" );

	// We don't have any data stored on the element,
	// so use "detach" method as fast way to get rid of the element
	elem.detach();

	return display;
}

/**
 * Try to determine the default display value of an element
 * @param {String} nodeName
 */
function defaultDisplay( nodeName ) {
	var doc = document,
		display = elemdisplay[ nodeName ];

	if ( !display ) {
		display = actualDisplay( nodeName, doc );

		// If the simple way fails, read from inside an iframe
		if ( display === "none" || !display ) {

			// Use the already-created iframe if possible
			iframe = ( iframe || jQuery( "<iframe frameborder='0' width='0' height='0'/>" ) )
				.appendTo( doc.documentElement );

			// Always write a new HTML skeleton so Webkit and Firefox don't choke on reuse
			doc = iframe[ 0 ].contentDocument;

			// Support: IE
			doc.write();
			doc.close();

			display = actualDisplay( nodeName, doc );
			iframe.detach();
		}

		// Store the correct default display
		elemdisplay[ nodeName ] = display;
	}

	return display;
}
var rmargin = ( /^margin/ );

var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

var getStyles = function( elem ) {

		// Support: IE<=11+, Firefox<=30+ (#15098, #14150)
		// IE throws on elements created in popups
		// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
		var view = elem.ownerDocument.defaultView;

		if ( !view || !view.opener ) {
			view = window;
		}

		return view.getComputedStyle( elem );
	};

var swap = function( elem, options, callback, args ) {
	var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.apply( elem, args || [] );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};


var documentElement = document.documentElement;



( function() {
	var pixelPositionVal, boxSizingReliableVal, pixelMarginRightVal, reliableMarginLeftVal,
		container = document.createElement( "div" ),
		div = document.createElement( "div" );

	// Finish early in limited (non-browser) environments
	if ( !div.style ) {
		return;
	}

	// Support: IE9-11+
	// Style of cloned element affects source element cloned (#8908)
	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	container.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;" +
		"padding:0;margin-top:1px;position:absolute";
	container.appendChild( div );

	// Executing both pixelPosition & boxSizingReliable tests require only one layout
	// so they're executed at the same time to save the second computation.
	function computeStyleTests() {
		div.style.cssText =

			// Support: Firefox<29, Android 2.3
			// Vendor-prefix box-sizing
			"-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;" +
			"position:relative;display:block;" +
			"margin:auto;border:1px;padding:1px;" +
			"top:1%;width:50%";
		div.innerHTML = "";
		documentElement.appendChild( container );

		var divStyle = window.getComputedStyle( div );
		pixelPositionVal = divStyle.top !== "1%";
		reliableMarginLeftVal = divStyle.marginLeft === "2px";
		boxSizingReliableVal = divStyle.width === "4px";

		// Support: Android 4.0 - 4.3 only
		// Some styles come back with percentage values, even though they shouldn't
		div.style.marginRight = "50%";
		pixelMarginRightVal = divStyle.marginRight === "4px";

		documentElement.removeChild( container );
	}

	jQuery.extend( support, {
		pixelPosition: function() {

			// This test is executed only once but we still do memoizing
			// since we can use the boxSizingReliable pre-computing.
			// No need to check if the test was already performed, though.
			computeStyleTests();
			return pixelPositionVal;
		},
		boxSizingReliable: function() {
			if ( boxSizingReliableVal == null ) {
				computeStyleTests();
			}
			return boxSizingReliableVal;
		},
		pixelMarginRight: function() {

			// Support: Android 4.0-4.3
			// We're checking for boxSizingReliableVal here instead of pixelMarginRightVal
			// since that compresses better and they're computed together anyway.
			if ( boxSizingReliableVal == null ) {
				computeStyleTests();
			}
			return pixelMarginRightVal;
		},
		reliableMarginLeft: function() {

			// Support: IE <=8 only, Android 4.0 - 4.3 only, Firefox <=3 - 37
			if ( boxSizingReliableVal == null ) {
				computeStyleTests();
			}
			return reliableMarginLeftVal;
		},
		reliableMarginRight: function() {

			// Support: Android 2.3
			// Check if div with explicit width and no margin-right incorrectly
			// gets computed margin-right based on width of container. (#3333)
			// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
			// This support function is only executed once so no memoizing is needed.
			var ret,
				marginDiv = div.appendChild( document.createElement( "div" ) );

			// Reset CSS: box-sizing; display; margin; border; padding
			marginDiv.style.cssText = div.style.cssText =

				// Support: Android 2.3
				// Vendor-prefix box-sizing
				"-webkit-box-sizing:content-box;box-sizing:content-box;" +
				"display:block;margin:0;border:0;padding:0";
			marginDiv.style.marginRight = marginDiv.style.width = "0";
			div.style.width = "1px";
			documentElement.appendChild( container );

			ret = !parseFloat( window.getComputedStyle( marginDiv ).marginRight );

			documentElement.removeChild( container );
			div.removeChild( marginDiv );

			return ret;
		}
	} );
} )();


function curCSS( elem, name, computed ) {
	var width, minWidth, maxWidth, ret,
		style = elem.style;

	computed = computed || getStyles( elem );
	ret = computed ? computed.getPropertyValue( name ) || computed[ name ] : undefined;

	// Support: Opera 12.1x only
	// Fall back to style even without computed
	// computed is undefined for elems on document fragments
	if ( ( ret === "" || ret === undefined ) && !jQuery.contains( elem.ownerDocument, elem ) ) {
		ret = jQuery.style( elem, name );
	}

	// Support: IE9
	// getPropertyValue is only needed for .css('filter') (#12537)
	if ( computed ) {

		// A tribute to the "awesome hack by Dean Edwards"
		// Android Browser returns percentage for some values,
		// but width seems to be reliably pixels.
		// This is against the CSSOM draft spec:
		// http://dev.w3.org/csswg/cssom/#resolved-values
		if ( !support.pixelMarginRight() && rnumnonpx.test( ret ) && rmargin.test( name ) ) {

			// Remember the original values
			width = style.width;
			minWidth = style.minWidth;
			maxWidth = style.maxWidth;

			// Put in the new values to get a computed value out
			style.minWidth = style.maxWidth = style.width = ret;
			ret = computed.width;

			// Revert the changed values
			style.width = width;
			style.minWidth = minWidth;
			style.maxWidth = maxWidth;
		}
	}

	return ret !== undefined ?

		// Support: IE9-11+
		// IE returns zIndex value as an integer.
		ret + "" :
		ret;
}


function addGetHookIf( conditionFn, hookFn ) {

	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			if ( conditionFn() ) {

				// Hook not needed (or it's not possible to use it due
				// to missing dependency), remove it.
				delete this.get;
				return;
			}

			// Hook needed; redefine it so that the support test is not executed again.
			return ( this.get = hookFn ).apply( this, arguments );
		}
	};
}


var

	// Swappable if display is none or starts with table
	// except "table", "table-cell", or "table-caption"
	// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,

	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: "0",
		fontWeight: "400"
	},

	cssPrefixes = [ "Webkit", "O", "Moz", "ms" ],
	emptyStyle = document.createElement( "div" ).style;

// Return a css property mapped to a potentially vendor prefixed property
function vendorPropName( name ) {

	// Shortcut for names that are not vendor prefixed
	if ( name in emptyStyle ) {
		return name;
	}

	// Check for vendor prefixed names
	var capName = name[ 0 ].toUpperCase() + name.slice( 1 ),
		i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in emptyStyle ) {
			return name;
		}
	}
}

function setPositiveNumber( elem, value, subtract ) {

	// Any relative (+/-) values have already been
	// normalized at this point
	var matches = rcssNum.exec( value );
	return matches ?

		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 2 ] - ( subtract || 0 ) ) + ( matches[ 3 ] || "px" ) :
		value;
}

function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
	var i = extra === ( isBorderBox ? "border" : "content" ) ?

		// If we already have the right measurement, avoid augmentation
		4 :

		// Otherwise initialize for horizontal or vertical properties
		name === "width" ? 1 : 0,

		val = 0;

	for ( ; i < 4; i += 2 ) {

		// Both box models exclude margin, so add it if we want it
		if ( extra === "margin" ) {
			val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
		}

		if ( isBorderBox ) {

			// border-box includes padding, so remove it if we want content
			if ( extra === "content" ) {
				val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// At this point, extra isn't border nor margin, so remove border
			if ( extra !== "margin" ) {
				val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		} else {

			// At this point, extra isn't content, so add padding
			val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// At this point, extra isn't content nor padding, so add border
			if ( extra !== "padding" ) {
				val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	return val;
}

function getWidthOrHeight( elem, name, extra ) {

	// Start with offset property, which is equivalent to the border-box value
	var valueIsBorderBox = true,
		val = name === "width" ? elem.offsetWidth : elem.offsetHeight,
		styles = getStyles( elem ),
		isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

	// Some non-html elements return undefined for offsetWidth, so check for null/undefined
	// svg - https://bugzilla.mozilla.org/show_bug.cgi?id=649285
	// MathML - https://bugzilla.mozilla.org/show_bug.cgi?id=491668
	if ( val <= 0 || val == null ) {

		// Fall back to computed then uncomputed css if necessary
		val = curCSS( elem, name, styles );
		if ( val < 0 || val == null ) {
			val = elem.style[ name ];
		}

		// Computed unit is not pixels. Stop here and return.
		if ( rnumnonpx.test( val ) ) {
			return val;
		}

		// Check for style in case a browser which returns unreliable values
		// for getComputedStyle silently falls back to the reliable elem.style
		valueIsBorderBox = isBorderBox &&
			( support.boxSizingReliable() || val === elem.style[ name ] );

		// Normalize "", auto, and prepare for extra
		val = parseFloat( val ) || 0;
	}

	// Use the active box-sizing model to add/subtract irrelevant styles
	return ( val +
		augmentWidthOrHeight(
			elem,
			name,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles
		)
	) + "px";
}

function showHide( elements, show ) {
	var display, elem, hidden,
		values = [],
		index = 0,
		length = elements.length;

	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		values[ index ] = dataPriv.get( elem, "olddisplay" );
		display = elem.style.display;
		if ( show ) {

			// Reset the inline display of this element to learn if it is
			// being hidden by cascaded rules or not
			if ( !values[ index ] && display === "none" ) {
				elem.style.display = "";
			}

			// Set elements which have been overridden with display: none
			// in a stylesheet to whatever the default browser style is
			// for such an element
			if ( elem.style.display === "" && isHidden( elem ) ) {
				values[ index ] = dataPriv.access(
					elem,
					"olddisplay",
					defaultDisplay( elem.nodeName )
				);
			}
		} else {
			hidden = isHidden( elem );

			if ( display !== "none" || !hidden ) {
				dataPriv.set(
					elem,
					"olddisplay",
					hidden ? display : jQuery.css( elem, "display" )
				);
			}
		}
	}

	// Set the display of most of the elements in a second loop
	// to avoid the constant reflow
	for ( index = 0; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}
		if ( !show || elem.style.display === "none" || elem.style.display === "" ) {
			elem.style.display = show ? values[ index ] || "" : "none";
		}
	}

	return elements;
}

jQuery.extend( {

	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {

					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"animationIterationCount": true,
		"columnCount": true,
		"fillOpacity": true,
		"flexGrow": true,
		"flexShrink": true,
		"fontWeight": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {
		"float": "cssFloat"
	},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {

		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
			origName = jQuery.camelCase( name ),
			style = elem.style;

		name = jQuery.cssProps[ origName ] ||
			( jQuery.cssProps[ origName ] = vendorPropName( origName ) || origName );

		// Gets hook for the prefixed version, then unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// Convert "+=" or "-=" to relative numbers (#7345)
			if ( type === "string" && ( ret = rcssNum.exec( value ) ) && ret[ 1 ] ) {
				value = adjustCSS( elem, name, ret );

				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set (#7116)
			if ( value == null || value !== value ) {
				return;
			}

			// If a number was passed in, add the unit (except for certain CSS properties)
			if ( type === "number" ) {
				value += ret && ret[ 3 ] || ( jQuery.cssNumber[ origName ] ? "" : "px" );
			}

			// Support: IE9-11+
			// background-* props affect original clone's values
			if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !( "set" in hooks ) ||
				( value = hooks.set( elem, value, extra ) ) !== undefined ) {

				style[ name ] = value;
			}

		} else {

			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks &&
				( ret = hooks.get( elem, false, extra ) ) !== undefined ) {

				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var val, num, hooks,
			origName = jQuery.camelCase( name );

		// Make sure that we're working with the right name
		name = jQuery.cssProps[ origName ] ||
			( jQuery.cssProps[ origName ] = vendorPropName( origName ) || origName );

		// Try prefixed name followed by the unprefixed name
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		// Convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Make numeric if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || isFinite( num ) ? num || 0 : val;
		}
		return val;
	}
} );

jQuery.each( [ "height", "width" ], function( i, name ) {
	jQuery.cssHooks[ name ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {

				// Certain elements can have dimension info if we invisibly show them
				// but it must have a current display style that would benefit
				return rdisplayswap.test( jQuery.css( elem, "display" ) ) &&
					elem.offsetWidth === 0 ?
						swap( elem, cssShow, function() {
							return getWidthOrHeight( elem, name, extra );
						} ) :
						getWidthOrHeight( elem, name, extra );
			}
		},

		set: function( elem, value, extra ) {
			var matches,
				styles = extra && getStyles( elem ),
				subtract = extra && augmentWidthOrHeight(
					elem,
					name,
					extra,
					jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
					styles
				);

			// Convert to pixels if value adjustment is needed
			if ( subtract && ( matches = rcssNum.exec( value ) ) &&
				( matches[ 3 ] || "px" ) !== "px" ) {

				elem.style[ name ] = value;
				value = jQuery.css( elem, name );
			}

			return setPositiveNumber( elem, value, subtract );
		}
	};
} );

jQuery.cssHooks.marginLeft = addGetHookIf( support.reliableMarginLeft,
	function( elem, computed ) {
		if ( computed ) {
			return ( parseFloat( curCSS( elem, "marginLeft" ) ) ||
				elem.getBoundingClientRect().left -
					swap( elem, { marginLeft: 0 }, function() {
						return elem.getBoundingClientRect().left;
					} )
				) + "px";
		}
	}
);

// Support: Android 2.3
jQuery.cssHooks.marginRight = addGetHookIf( support.reliableMarginRight,
	function( elem, computed ) {
		if ( computed ) {
			return swap( elem, { "display": "inline-block" },
				curCSS, [ elem, "marginRight" ] );
		}
	}
);

// These hooks are used by animate to expand properties
jQuery.each( {
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
				expanded = {},

				// Assumes a single number if not a string
				parts = typeof value === "string" ? value.split( " " ) : [ value ];

			for ( ; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};

	if ( !rmargin.test( prefix ) ) {
		jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
	}
} );

jQuery.fn.extend( {
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
				map = {},
				i = 0;

			if ( jQuery.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
				jQuery.style( elem, name, value ) :
				jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	},
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each( function() {
			if ( isHidden( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		} );
	}
} );


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || jQuery.easing._default;
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
			hooks.get( this ) :
			Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
			hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
			);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			// Use a property on the element directly when it is not a DOM element,
			// or when there is no matching style property that exists.
			if ( tween.elem.nodeType !== 1 ||
				tween.elem[ tween.prop ] != null && tween.elem.style[ tween.prop ] == null ) {
				return tween.elem[ tween.prop ];
			}

			// Passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails.
			// Simple values such as "10px" are parsed to Float;
			// complex values such as "rotate(1rad)" are returned as-is.
			result = jQuery.css( tween.elem, tween.prop, "" );

			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {

			// Use step hook for back compat.
			// Use cssHook if its there.
			// Use .style if available and use plain properties where available.
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.nodeType === 1 &&
				( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null ||
					jQuery.cssHooks[ tween.prop ] ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE9
// Panic based approach to setting things on disconnected nodes
Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	},
	_default: "swing"
};

jQuery.fx = Tween.prototype.init;

// Back Compat <1.8 extension point
jQuery.fx.step = {};




var
	fxNow, timerId,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rrun = /queueHooks$/;

// Animations created synchronously will run synchronously
function createFxNow() {
	window.setTimeout( function() {
		fxNow = undefined;
	} );
	return ( fxNow = jQuery.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
		i = 0,
		attrs = { height: type };

	// If we include width, step value is 1 to do all cssExpand values,
	// otherwise step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4 ; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
		collection = ( Animation.tweeners[ prop ] || [] ).concat( Animation.tweeners[ "*" ] ),
		index = 0,
		length = collection.length;
	for ( ; index < length; index++ ) {
		if ( ( tween = collection[ index ].call( animation, prop, value ) ) ) {

			// We're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	/* jshint validthis: true */
	var prop, value, toggle, tween, hooks, oldfire, display, checkDisplay,
		anim = this,
		orig = {},
		style = elem.style,
		hidden = elem.nodeType && isHidden( elem ),
		dataShow = dataPriv.get( elem, "fxshow" );

	// Handle queue: false promises
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always( function() {

			// Ensure the complete handler is called before this completes
			anim.always( function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			} );
		} );
	}

	// Height/width overflow pass
	if ( elem.nodeType === 1 && ( "height" in props || "width" in props ) ) {

		// Make sure that nothing sneaks out
		// Record all 3 overflow attributes because IE9-10 do not
		// change the overflow attribute when overflowX and
		// overflowY are set to the same value
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Set display property to inline-block for height/width
		// animations on inline elements that are having width/height animated
		display = jQuery.css( elem, "display" );

		// Test default display if display is currently "none"
		checkDisplay = display === "none" ?
			dataPriv.get( elem, "olddisplay" ) || defaultDisplay( elem.nodeName ) : display;

		if ( checkDisplay === "inline" && jQuery.css( elem, "float" ) === "none" ) {
			style.display = "inline-block";
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		anim.always( function() {
			style.overflow = opts.overflow[ 0 ];
			style.overflowX = opts.overflow[ 1 ];
			style.overflowY = opts.overflow[ 2 ];
		} );
	}

	// show/hide pass
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.exec( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// If there is dataShow left over from a stopped hide or show
				// and we are going to proceed with show, we should pretend to be hidden
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;
				} else {
					continue;
				}
			}
			orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );

		// Any non-fx value stops us from restoring the original display value
		} else {
			display = undefined;
		}
	}

	if ( !jQuery.isEmptyObject( orig ) ) {
		if ( dataShow ) {
			if ( "hidden" in dataShow ) {
				hidden = dataShow.hidden;
			}
		} else {
			dataShow = dataPriv.access( elem, "fxshow", {} );
		}

		// Store state if its toggle - enables .stop().toggle() to "reverse"
		if ( toggle ) {
			dataShow.hidden = !hidden;
		}
		if ( hidden ) {
			jQuery( elem ).show();
		} else {
			anim.done( function() {
				jQuery( elem ).hide();
			} );
		}
		anim.done( function() {
			var prop;

			dataPriv.remove( elem, "fxshow" );
			for ( prop in orig ) {
				jQuery.style( elem, prop, orig[ prop ] );
			}
		} );
		for ( prop in orig ) {
			tween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );

			if ( !( prop in dataShow ) ) {
				dataShow[ prop ] = tween.start;
				if ( hidden ) {
					tween.end = tween.start;
					tween.start = prop === "width" || prop === "height" ? 1 : 0;
				}
			}
		}

	// If this is a noop like .hide().hide(), restore an overwritten display value
	} else if ( ( display === "none" ? defaultDisplay( elem.nodeName ) : display ) === "inline" ) {
		style.display = display;
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = jQuery.camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( jQuery.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// Not quite $.extend, this won't overwrite existing keys.
			// Reusing 'index' because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
		stopped,
		index = 0,
		length = Animation.prefilters.length,
		deferred = jQuery.Deferred().always( function() {

			// Don't match elem in the :animated selector
			delete tick.elem;
		} ),
		tick = function() {
			if ( stopped ) {
				return false;
			}
			var currentTime = fxNow || createFxNow(),
				remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),

				// Support: Android 2.3
				// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

			for ( ; index < length ; index++ ) {
				animation.tweens[ index ].run( percent );
			}

			deferred.notifyWith( elem, [ animation, percent, remaining ] );

			if ( percent < 1 && length ) {
				return remaining;
			} else {
				deferred.resolveWith( elem, [ animation ] );
				return false;
			}
		},
		animation = deferred.promise( {
			elem: elem,
			props: jQuery.extend( {}, properties ),
			opts: jQuery.extend( true, {
				specialEasing: {},
				easing: jQuery.easing._default
			}, options ),
			originalProperties: properties,
			originalOptions: options,
			startTime: fxNow || createFxNow(),
			duration: options.duration,
			tweens: [],
			createTween: function( prop, end ) {
				var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
				animation.tweens.push( tween );
				return tween;
			},
			stop: function( gotoEnd ) {
				var index = 0,

					// If we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
				if ( stopped ) {
					return this;
				}
				stopped = true;
				for ( ; index < length ; index++ ) {
					animation.tweens[ index ].run( 1 );
				}

				// Resolve when we played the last frame; otherwise, reject
				if ( gotoEnd ) {
					deferred.notifyWith( elem, [ animation, 1, 0 ] );
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		} ),
		props = animation.props;

	propFilter( props, animation.opts.specialEasing );

	for ( ; index < length ; index++ ) {
		result = Animation.prefilters[ index ].call( animation, elem, props, animation.opts );
		if ( result ) {
			if ( jQuery.isFunction( result.stop ) ) {
				jQuery._queueHooks( animation.elem, animation.opts.queue ).stop =
					jQuery.proxy( result.stop, result );
			}
			return result;
		}
	}

	jQuery.map( props, createTween, animation );

	if ( jQuery.isFunction( animation.opts.start ) ) {
		animation.opts.start.call( elem, animation );
	}

	jQuery.fx.timer(
		jQuery.extend( tick, {
			elem: elem,
			anim: animation,
			queue: animation.opts.queue
		} )
	);

	// attach callbacks from options
	return animation.progress( animation.opts.progress )
		.done( animation.opts.done, animation.opts.complete )
		.fail( animation.opts.fail )
		.always( animation.opts.always );
}

jQuery.Animation = jQuery.extend( Animation, {
	tweeners: {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value );
			adjustCSS( tween.elem, prop, rcssNum.exec( value ), tween );
			return tween;
		} ]
	},

	tweener: function( props, callback ) {
		if ( jQuery.isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.match( rnotwhite );
		}

		var prop,
			index = 0,
			length = props.length;

		for ( ; index < length ; index++ ) {
			prop = props[ index ];
			Animation.tweeners[ prop ] = Animation.tweeners[ prop ] || [];
			Animation.tweeners[ prop ].unshift( callback );
		}
	},

	prefilters: [ defaultPrefilter ],

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			Animation.prefilters.unshift( callback );
		} else {
			Animation.prefilters.push( callback );
		}
	}
} );

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
			jQuery.isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
	};

	opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ?
		opt.duration : opt.duration in jQuery.fx.speeds ?
			jQuery.fx.speeds[ opt.duration ] : jQuery.fx.speeds._default;

	// Normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( jQuery.isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend( {
	fadeTo: function( speed, to, easing, callback ) {

		// Show any hidden elements after setting opacity to 0
		return this.filter( isHidden ).css( "opacity", 0 ).show()

			// Animate to the value specified
			.end().animate( { opacity: to }, speed, easing, callback );
	},
	animate: function( prop, speed, easing, callback ) {
		var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {

				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || dataPriv.get( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

		return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},
	stop: function( type, clearQueue, gotoEnd ) {
		var stopQueue = function( hooks ) {
			var stop = hooks.stop;
			delete hooks.stop;
			stop( gotoEnd );
		};

		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue && type !== false ) {
			this.queue( type || "fx", [] );
		}

		return this.each( function() {
			var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = dataPriv.get( this );

			if ( index ) {
				if ( data[ index ] && data[ index ].stop ) {
					stopQueue( data[ index ] );
				}
			} else {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
						stopQueue( data[ index ] );
					}
				}
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this &&
					( type == null || timers[ index ].queue === type ) ) {

					timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// Start the next in the queue if the last step wasn't forced.
			// Timers currently will call their complete callbacks, which
			// will dequeue but only if they were gotoEnd.
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		} );
	},
	finish: function( type ) {
		if ( type !== false ) {
			type = type || "fx";
		}
		return this.each( function() {
			var index,
				data = dataPriv.get( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// Enable finishing flag on private data
			data.finish = true;

			// Empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// Look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// Look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// Turn off finishing flag
			delete data.finish;
		} );
	}
} );

jQuery.each( [ "toggle", "show", "hide" ], function( i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
			cssFn.apply( this, arguments ) :
			this.animate( genFx( name, true ), speed, easing, callback );
	};
} );

// Generate shortcuts for custom animations
jQuery.each( {
	slideDown: genFx( "show" ),
	slideUp: genFx( "hide" ),
	slideToggle: genFx( "toggle" ),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
} );

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
		i = 0,
		timers = jQuery.timers;

	fxNow = jQuery.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];

		// Checks the timer has not already been removed
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	if ( timer() ) {
		jQuery.fx.start();
	} else {
		jQuery.timers.pop();
	}
};

jQuery.fx.interval = 13;
jQuery.fx.start = function() {
	if ( !timerId ) {
		timerId = window.setInterval( jQuery.fx.tick, jQuery.fx.interval );
	}
};

jQuery.fx.stop = function() {
	window.clearInterval( timerId );

	timerId = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,

	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// http://web.archive.org/web/20100324014747/http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = window.setTimeout( next, time );
		hooks.stop = function() {
			window.clearTimeout( timeout );
		};
	} );
};


( function() {
	var input = document.createElement( "input" ),
		select = document.createElement( "select" ),
		opt = select.appendChild( document.createElement( "option" ) );

	input.type = "checkbox";

	// Support: iOS<=5.1, Android<=4.2+
	// Default value for a checkbox should be "on"
	support.checkOn = input.value !== "";

	// Support: IE<=11+
	// Must access selectedIndex to make default options select
	support.optSelected = opt.selected;

	// Support: Android<=2.3
	// Options inside disabled selects are incorrectly marked as disabled
	select.disabled = true;
	support.optDisabled = !opt.disabled;

	// Support: IE<=11+
	// An input loses its value after becoming a radio
	input = document.createElement( "input" );
	input.value = "t";
	input.type = "radio";
	support.radioValue = input.value === "t";
} )();


var boolHook,
	attrHandle = jQuery.expr.attrHandle;

jQuery.fn.extend( {
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each( function() {
			jQuery.removeAttr( this, name );
		} );
	}
} );

jQuery.extend( {
	attr: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set attributes on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === "undefined" ) {
			return jQuery.prop( elem, name, value );
		}

		// All attributes are lowercase
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			name = name.toLowerCase();
			hooks = jQuery.attrHooks[ name ] ||
				( jQuery.expr.match.bool.test( name ) ? boolHook : undefined );
		}

		if ( value !== undefined ) {
			if ( value === null ) {
				jQuery.removeAttr( elem, name );
				return;
			}

			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			elem.setAttribute( name, value + "" );
			return value;
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		ret = jQuery.find.attr( elem, name );

		// Non-existent attributes return null, we normalize to undefined
		return ret == null ? undefined : ret;
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" &&
					jQuery.nodeName( elem, "input" ) ) {
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		}
	},

	removeAttr: function( elem, value ) {
		var name, propName,
			i = 0,
			attrNames = value && value.match( rnotwhite );

		if ( attrNames && elem.nodeType === 1 ) {
			while ( ( name = attrNames[ i++ ] ) ) {
				propName = jQuery.propFix[ name ] || name;

				// Boolean attributes get special treatment (#10870)
				if ( jQuery.expr.match.bool.test( name ) ) {

					// Set corresponding property to false
					elem[ propName ] = false;
				}

				elem.removeAttribute( name );
			}
		}
	}
} );

// Hooks for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {

			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else {
			elem.setAttribute( name, name );
		}
		return name;
	}
};
jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {
	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = function( elem, name, isXML ) {
		var ret, handle;
		if ( !isXML ) {

			// Avoid an infinite loop by temporarily removing this function from the getter
			handle = attrHandle[ name ];
			attrHandle[ name ] = ret;
			ret = getter( elem, name, isXML ) != null ?
				name.toLowerCase() :
				null;
			attrHandle[ name ] = handle;
		}
		return ret;
	};
} );




var rfocusable = /^(?:input|select|textarea|button)$/i,
	rclickable = /^(?:a|area)$/i;

jQuery.fn.extend( {
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		return this.each( function() {
			delete this[ jQuery.propFix[ name ] || name ];
		} );
	}
} );

jQuery.extend( {
	prop: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set properties on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {

			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			return ( elem[ name ] = value );
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		return elem[ name ];
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {

				// elem.tabIndex doesn't always return the
				// correct value when it hasn't been explicitly set
				// http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
				// Use proper attribute retrieval(#12072)
				var tabindex = jQuery.find.attr( elem, "tabindex" );

				return tabindex ?
					parseInt( tabindex, 10 ) :
					rfocusable.test( elem.nodeName ) ||
						rclickable.test( elem.nodeName ) && elem.href ?
							0 :
							-1;
			}
		}
	},

	propFix: {
		"for": "htmlFor",
		"class": "className"
	}
} );

// Support: IE <=11 only
// Accessing the selectedIndex property
// forces the browser to respect setting selected
// on the option
// The getter ensures a default option is selected
// when in an optgroup
if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {
			var parent = elem.parentNode;
			if ( parent && parent.parentNode ) {
				parent.parentNode.selectedIndex;
			}
			return null;
		},
		set: function( elem ) {
			var parent = elem.parentNode;
			if ( parent ) {
				parent.selectedIndex;

				if ( parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
			}
		}
	};
}

jQuery.each( [
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
], function() {
	jQuery.propFix[ this.toLowerCase() ] = this;
} );




var rclass = /[\t\r\n\f]/g;

function getClass( elem ) {
	return elem.getAttribute && elem.getAttribute( "class" ) || "";
}

jQuery.fn.extend( {
	addClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( jQuery.isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).addClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		if ( typeof value === "string" && value ) {
			classes = value.match( rnotwhite ) || [];

			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );
				cur = elem.nodeType === 1 &&
					( " " + curValue + " " ).replace( rclass, " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = jQuery.trim( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( jQuery.isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).removeClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		if ( !arguments.length ) {
			return this.attr( "class", "" );
		}

		if ( typeof value === "string" && value ) {
			classes = value.match( rnotwhite ) || [];

			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );

				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 &&
					( " " + curValue + " " ).replace( rclass, " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {

						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) > -1 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = jQuery.trim( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value;

		if ( typeof stateVal === "boolean" && type === "string" ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( jQuery.isFunction( value ) ) {
			return this.each( function( i ) {
				jQuery( this ).toggleClass(
					value.call( this, i, getClass( this ), stateVal ),
					stateVal
				);
			} );
		}

		return this.each( function() {
			var className, i, self, classNames;

			if ( type === "string" ) {

				// Toggle individual class names
				i = 0;
				self = jQuery( this );
				classNames = value.match( rnotwhite ) || [];

				while ( ( className = classNames[ i++ ] ) ) {

					// Check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
			} else if ( value === undefined || type === "boolean" ) {
				className = getClass( this );
				if ( className ) {

					// Store className if set
					dataPriv.set( this, "__className__", className );
				}

				// If the element has a class name or if we're passed `false`,
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				if ( this.setAttribute ) {
					this.setAttribute( "class",
						className || value === false ?
						"" :
						dataPriv.get( this, "__className__" ) || ""
					);
				}
			}
		} );
	},

	hasClass: function( selector ) {
		var className, elem,
			i = 0;

		className = " " + selector + " ";
		while ( ( elem = this[ i++ ] ) ) {
			if ( elem.nodeType === 1 &&
				( " " + getClass( elem ) + " " ).replace( rclass, " " )
					.indexOf( className ) > -1
			) {
				return true;
			}
		}

		return false;
	}
} );




var rreturn = /\r/g,
	rspaces = /[\x20\t\r\n\f]+/g;

jQuery.fn.extend( {
	val: function( value ) {
		var hooks, ret, isFunction,
			elem = this[ 0 ];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] ||
					jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks &&
					"get" in hooks &&
					( ret = hooks.get( elem, "value" ) ) !== undefined
				) {
					return ret;
				}

				ret = elem.value;

				return typeof ret === "string" ?

					// Handle most common string cases
					ret.replace( rreturn, "" ) :

					// Handle cases where value is null/undef or number
					ret == null ? "" : ret;
			}

			return;
		}

		isFunction = jQuery.isFunction( value );

		return this.each( function( i ) {
			var val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( isFunction ) {
				val = value.call( this, i, jQuery( this ).val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";

			} else if ( typeof val === "number" ) {
				val += "";

			} else if ( jQuery.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				} );
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !( "set" in hooks ) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		} );
	}
} );

jQuery.extend( {
	valHooks: {
		option: {
			get: function( elem ) {

				var val = jQuery.find.attr( elem, "value" );
				return val != null ?
					val :

					// Support: IE10-11+
					// option.text throws exceptions (#14686, #14858)
					// Strip and collapse whitespace
					// https://html.spec.whatwg.org/#strip-and-collapse-whitespace
					jQuery.trim( jQuery.text( elem ) ).replace( rspaces, " " );
			}
		},
		select: {
			get: function( elem ) {
				var value, option,
					options = elem.options,
					index = elem.selectedIndex,
					one = elem.type === "select-one" || index < 0,
					values = one ? null : [],
					max = one ? index + 1 : options.length,
					i = index < 0 ?
						max :
						one ? index : 0;

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// IE8-9 doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&

							// Don't return options that are disabled or in a disabled optgroup
							( support.optDisabled ?
								!option.disabled : option.getAttribute( "disabled" ) === null ) &&
							( !option.parentNode.disabled ||
								!jQuery.nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
					options = elem.options,
					values = jQuery.makeArray( value ),
					i = options.length;

				while ( i-- ) {
					option = options[ i ];
					if ( option.selected =
						jQuery.inArray( jQuery.valHooks.option.get( option ), values ) > -1
					) {
						optionSet = true;
					}
				}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}
				return values;
			}
		}
	}
} );

// Radios and checkboxes getter/setter
jQuery.each( [ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( jQuery.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery( elem ).val(), value ) > -1 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			return elem.getAttribute( "value" ) === null ? "on" : elem.value;
		};
	}
} );




// Return jQuery for attributes-only inclusion


var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/;

jQuery.extend( jQuery.event, {

	trigger: function( event, data, elem, onlyHandlers ) {

		var i, cur, tmp, bubbleType, ontype, handle, special,
			eventPath = [ elem || document ],
			type = hasOwn.call( event, "type" ) ? event.type : event,
			namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split( "." ) : [];

		cur = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf( "." ) > -1 ) {

			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split( "." );
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf( ":" ) < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
			event :
			new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join( "." );
		event.rnamespace = event.namespace ?
			new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" ) :
			null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
			[ event ] :
			jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === ( elem.ownerDocument || document ) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( ( cur = eventPath[ i++ ] ) && !event.isPropagationStopped() ) {

			event.type = i > 1 ?
				bubbleType :
				special.bindType || type;

			// jQuery handler
			handle = ( dataPriv.get( cur, "events" ) || {} )[ event.type ] &&
				dataPriv.get( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( ( !special._default ||
				special._default.apply( eventPath.pop(), data ) === false ) &&
				acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name name as the event.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && jQuery.isFunction( elem[ type ] ) && !jQuery.isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;
					elem[ type ]();
					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	// Piggyback on a donor event to simulate a different one
	// Used only for `focus(in | out)` events
	simulate: function( type, elem, event ) {
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true
			}
		);

		jQuery.event.trigger( e, null, elem );
	}

} );

jQuery.fn.extend( {

	trigger: function( type, data ) {
		return this.each( function() {
			jQuery.event.trigger( type, data, this );
		} );
	},
	triggerHandler: function( type, data ) {
		var elem = this[ 0 ];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
} );


jQuery.each( ( "blur focus focusin focusout load resize scroll unload click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup error contextmenu" ).split( " " ),
	function( i, name ) {

	// Handle event binding
	jQuery.fn[ name ] = function( data, fn ) {
		return arguments.length > 0 ?
			this.on( name, null, data, fn ) :
			this.trigger( name );
	};
} );

jQuery.fn.extend( {
	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	}
} );




support.focusin = "onfocusin" in window;


// Support: Firefox
// Firefox doesn't have focus(in | out) events
// Related ticket - https://bugzilla.mozilla.org/show_bug.cgi?id=687787
//
// Support: Chrome, Safari
// focus(in | out) events fire after focus & blur events,
// which is spec violation - http://www.w3.org/TR/DOM-Level-3-Events/#events-focusevent-event-order
// Related ticket - https://code.google.com/p/chromium/issues/detail?id=449857
if ( !support.focusin ) {
	jQuery.each( { focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
			jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ) );
		};

		jQuery.event.special[ fix ] = {
			setup: function() {
				var doc = this.ownerDocument || this,
					attaches = dataPriv.access( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				dataPriv.access( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this,
					attaches = dataPriv.access( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					dataPriv.remove( doc, fix );

				} else {
					dataPriv.access( doc, fix, attaches );
				}
			}
		};
	} );
}
var location = window.location;

var nonce = jQuery.now();

var rquery = ( /\?/ );



// Support: Android 2.3
// Workaround failure to string-cast null input
jQuery.parseJSON = function( data ) {
	return JSON.parse( data + "" );
};


// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml;
	if ( !data || typeof data !== "string" ) {
		return null;
	}

	// Support: IE9
	try {
		xml = ( new window.DOMParser() ).parseFromString( data, "text/xml" );
	} catch ( e ) {
		xml = undefined;
	}

	if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
	rhash = /#.*$/,
	rts = /([?&])_=[^&]*/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,

	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat( "*" ),

	// Anchor tag for parsing the document origin
	originAnchor = document.createElement( "a" );
	originAnchor.href = location.href;

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
			i = 0,
			dataTypes = dataTypeExpression.toLowerCase().match( rnotwhite ) || [];

		if ( jQuery.isFunction( func ) ) {

			// For each dataType in the dataTypeExpression
			while ( ( dataType = dataTypes[ i++ ] ) ) {

				// Prepend if requested
				if ( dataType[ 0 ] === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					( structure[ dataType ] = structure[ dataType ] || [] ).unshift( func );

				// Otherwise append
				} else {
					( structure[ dataType ] = structure[ dataType ] || [] ).push( func );
				}
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
		seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" &&
				!seekingTransport && !inspected[ dataTypeOrTransport ] ) {

				options.dataTypes.unshift( dataTypeOrTransport );
				inspect( dataTypeOrTransport );
				return false;
			} else if ( seekingTransport ) {
				return !( selected = dataTypeOrTransport );
			}
		} );
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var key, deep,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || ( deep = {} ) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {

	var ct, type, finalDataType, firstDataType,
		contents = s.contents,
		dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader( "Content-Type" );
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {

		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[ 0 ] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}

		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
	var conv2, current, conv, tmp, prev,
		converters = {},

		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

		// There's only work to do if current dataType is non-auto
			if ( current === "*" ) {

				current = prev;

			// Convert response if prev dataType is non-auto and differs from current
			} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
								converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {

								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
								} else if ( converters[ conv2 ] !== true ) {
									current = tmp[ 0 ];
									dataTypes.unshift( tmp[ 1 ] );
								}
								break;
							}
						}
					}
				}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s.throws ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return {
								state: "parsererror",
								error: conv ? e : "No conversion from " + prev + " to " + current
							};
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend( {

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: location.href,
		type: "GET",
		isLocal: rlocalProtocol.test( location.protocol ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /\bxml\b/,
			html: /\bhtml/,
			json: /\bjson\b/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": jQuery.parseJSON,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var transport,

			// URL without anti-cache param
			cacheURL,

			// Response headers
			responseHeadersString,
			responseHeaders,

			// timeout handle
			timeoutTimer,

			// Url cleanup var
			urlAnchor,

			// To know if global events are to be dispatched
			fireGlobals,

			// Loop variable
			i,

			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),

			// Callbacks context
			callbackContext = s.context || s,

			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context &&
				( callbackContext.nodeType || callbackContext.jquery ) ?
					jQuery( callbackContext ) :
					jQuery.event,

			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks( "once memory" ),

			// Status-dependent callbacks
			statusCode = s.statusCode || {},

			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},

			// The jqXHR state
			state = 0,

			// Default abort message
			strAbort = "canceled",

			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( state === 2 ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( ( match = rheaders.exec( responseHeadersString ) ) ) {
								responseHeaders[ match[ 1 ].toLowerCase() ] = match[ 2 ];
							}
						}
						match = responseHeaders[ key.toLowerCase() ];
					}
					return match == null ? null : match;
				},

				// Raw string
				getAllResponseHeaders: function() {
					return state === 2 ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					var lname = name.toLowerCase();
					if ( !state ) {
						name = requestHeadersNames[ lname ] = requestHeadersNames[ lname ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( !state ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( state < 2 ) {
							for ( code in map ) {

								// Lazy-add the new callback in a way that preserves old ones
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						} else {

							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR ).complete = completeDeferred.add;
		jqXHR.success = jqXHR.done;
		jqXHR.error = jqXHR.fail;

		// Remove hash character (#7531: and string promotion)
		// Add protocol if not provided (prefilters might expect it)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || location.href ) + "" ).replace( rhash, "" )
			.replace( rprotocol, location.protocol + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = jQuery.trim( s.dataType || "*" ).toLowerCase().match( rnotwhite ) || [ "" ];

		// A cross-domain request is in order when the origin doesn't match the current origin.
		if ( s.crossDomain == null ) {
			urlAnchor = document.createElement( "a" );

			// Support: IE8-11+
			// IE throws exception if url is malformed, e.g. http://example.com:80x/
			try {
				urlAnchor.href = s.url;

				// Support: IE8-11+
				// Anchor's host property isn't correctly set when s.url is relative
				urlAnchor.href = urlAnchor.href;
				s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !==
					urlAnchor.protocol + "//" + urlAnchor.host;
			} catch ( e ) {

				// If there is an error parsing the URL, assume it is crossDomain,
				// it can be rejected by the transport if it is invalid
				s.crossDomain = true;
			}
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( state === 2 ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
		fireGlobals = jQuery.event && s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger( "ajaxStart" );
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		cacheURL = s.url;

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// If data is available, append data to url
			if ( s.data ) {
				cacheURL = ( s.url += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data );

				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add anti-cache in url if needed
			if ( s.cache === false ) {
				s.url = rts.test( cacheURL ) ?

					// If there is already a '_' parameter, set its value
					cacheURL.replace( rts, "$1_=" + nonce++ ) :

					// Otherwise add one to the end
					cacheURL + ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + nonce++;
			}
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[ 0 ] ] ?
				s.accepts[ s.dataTypes[ 0 ] ] +
					( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend &&
			( s.beforeSend.call( callbackContext, jqXHR, s ) === false || state === 2 ) ) {

			// Abort if not done already and return
			return jqXHR.abort();
		}

		// Aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		for ( i in { success: 1, error: 1, complete: 1 } ) {
			jqXHR[ i ]( s[ i ] );
		}

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}

			// If request was aborted inside ajaxSend, stop there
			if ( state === 2 ) {
				return jqXHR;
			}

			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = window.setTimeout( function() {
					jqXHR.abort( "timeout" );
				}, s.timeout );
			}

			try {
				state = 1;
				transport.send( requestHeaders, done );
			} catch ( e ) {

				// Propagate exception as error if not done
				if ( state < 2 ) {
					done( -1, e );

				// Simply rethrow otherwise
				} else {
					throw e;
				}
			}
		}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
				statusText = nativeStatusText;

			// Called once
			if ( state === 2 ) {
				return;
			}

			// State is "done" now
			state = 2;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				window.clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader( "Last-Modified" );
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader( "etag" );
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
				} else if ( status === 304 ) {
					statusText = "notmodified";

				// If we have data, let's convert it
				} else {
					statusText = response.state;
					success = response.data;
					error = response.error;
					isSuccess = !error;
				}
			} else {

				// Extract error from statusText and normalize for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );

				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger( "ajaxStop" );
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
} );

jQuery.each( [ "get", "post" ], function( i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {

		// Shift arguments if data argument was omitted
		if ( jQuery.isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		// The url can be an options object (which then must have .url)
		return jQuery.ajax( jQuery.extend( {
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		}, jQuery.isPlainObject( url ) && url ) );
	};
} );


jQuery._evalUrl = function( url ) {
	return jQuery.ajax( {
		url: url,

		// Make this explicit, since user can override this through ajaxSetup (#11264)
		type: "GET",
		dataType: "script",
		async: false,
		global: false,
		"throws": true
	} );
};


jQuery.fn.extend( {
	wrapAll: function( html ) {
		var wrap;

		if ( jQuery.isFunction( html ) ) {
			return this.each( function( i ) {
				jQuery( this ).wrapAll( html.call( this, i ) );
			} );
		}

		if ( this[ 0 ] ) {

			// The elements to wrap the target around
			wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

			if ( this[ 0 ].parentNode ) {
				wrap.insertBefore( this[ 0 ] );
			}

			wrap.map( function() {
				var elem = this;

				while ( elem.firstElementChild ) {
					elem = elem.firstElementChild;
				}

				return elem;
			} ).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each( function( i ) {
				jQuery( this ).wrapInner( html.call( this, i ) );
			} );
		}

		return this.each( function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		} );
	},

	wrap: function( html ) {
		var isFunction = jQuery.isFunction( html );

		return this.each( function( i ) {
			jQuery( this ).wrapAll( isFunction ? html.call( this, i ) : html );
		} );
	},

	unwrap: function() {
		return this.parent().each( function() {
			if ( !jQuery.nodeName( this, "body" ) ) {
				jQuery( this ).replaceWith( this.childNodes );
			}
		} ).end();
	}
} );


jQuery.expr.filters.hidden = function( elem ) {
	return !jQuery.expr.filters.visible( elem );
};
jQuery.expr.filters.visible = function( elem ) {

	// Support: Opera <= 12.12
	// Opera reports offsetWidths and offsetHeights less than zero on some elements
	// Use OR instead of AND as the element is not visible if either is true
	// See tickets #10406 and #13132
	return elem.offsetWidth > 0 || elem.offsetHeight > 0 || elem.getClientRects().length > 0;
};




var r20 = /%20/g,
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( jQuery.isArray( obj ) ) {

		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {

				// Treat each array item as a scalar.
				add( prefix, v );

			} else {

				// Item is non-scalar (array or object), encode its numeric index.
				buildParams(
					prefix + "[" + ( typeof v === "object" && v != null ? i : "" ) + "]",
					v,
					traditional,
					add
				);
			}
		} );

	} else if ( !traditional && jQuery.type( obj ) === "object" ) {

		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {

		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
		s = [],
		add = function( key, value ) {

			// If value is a function, invoke it and return its value
			value = jQuery.isFunction( value ) ? value() : ( value == null ? "" : value );
			s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
		};

	// Set traditional to true for jQuery <= 1.3.2 behavior.
	if ( traditional === undefined ) {
		traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
	}

	// If an array was passed in, assume that it is an array of form elements.
	if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {

		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		} );

	} else {

		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" ).replace( r20, "+" );
};

jQuery.fn.extend( {
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map( function() {

			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		} )
		.filter( function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		} )
		.map( function( i, elem ) {
			var val = jQuery( this ).val();

			return val == null ?
				null :
				jQuery.isArray( val ) ?
					jQuery.map( val, function( val ) {
						return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
					} ) :
					{ name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		} ).get();
	}
} );


jQuery.ajaxSettings.xhr = function() {
	try {
		return new window.XMLHttpRequest();
	} catch ( e ) {}
};

var xhrSuccessStatus = {

		// File protocol always yields status code 0, assume 200
		0: 200,

		// Support: IE9
		// #1450: sometimes IE returns 1223 when it should be 204
		1223: 204
	},
	xhrSupported = jQuery.ajaxSettings.xhr();

support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
support.ajax = xhrSupported = !!xhrSupported;

jQuery.ajaxTransport( function( options ) {
	var callback, errorCallback;

	// Cross domain only allowed if supported through XMLHttpRequest
	if ( support.cors || xhrSupported && !options.crossDomain ) {
		return {
			send: function( headers, complete ) {
				var i,
					xhr = options.xhr();

				xhr.open(
					options.type,
					options.url,
					options.async,
					options.username,
					options.password
				);

				// Apply custom fields if provided
				if ( options.xhrFields ) {
					for ( i in options.xhrFields ) {
						xhr[ i ] = options.xhrFields[ i ];
					}
				}

				// Override mime type if needed
				if ( options.mimeType && xhr.overrideMimeType ) {
					xhr.overrideMimeType( options.mimeType );
				}

				// X-Requested-With header
				// For cross-domain requests, seeing as conditions for a preflight are
				// akin to a jigsaw puzzle, we simply never set it to be sure.
				// (it can always be set on a per-request basis or even using ajaxSetup)
				// For same-domain requests, won't change header if already provided.
				if ( !options.crossDomain && !headers[ "X-Requested-With" ] ) {
					headers[ "X-Requested-With" ] = "XMLHttpRequest";
				}

				// Set headers
				for ( i in headers ) {
					xhr.setRequestHeader( i, headers[ i ] );
				}

				// Callback
				callback = function( type ) {
					return function() {
						if ( callback ) {
							callback = errorCallback = xhr.onload =
								xhr.onerror = xhr.onabort = xhr.onreadystatechange = null;

							if ( type === "abort" ) {
								xhr.abort();
							} else if ( type === "error" ) {

								// Support: IE9
								// On a manual native abort, IE9 throws
								// errors on any property access that is not readyState
								if ( typeof xhr.status !== "number" ) {
									complete( 0, "error" );
								} else {
									complete(

										// File: protocol always yields status 0; see #8605, #14207
										xhr.status,
										xhr.statusText
									);
								}
							} else {
								complete(
									xhrSuccessStatus[ xhr.status ] || xhr.status,
									xhr.statusText,

									// Support: IE9 only
									// IE9 has no XHR2 but throws on binary (trac-11426)
									// For XHR2 non-text, let the caller handle it (gh-2498)
									( xhr.responseType || "text" ) !== "text"  ||
									typeof xhr.responseText !== "string" ?
										{ binary: xhr.response } :
										{ text: xhr.responseText },
									xhr.getAllResponseHeaders()
								);
							}
						}
					};
				};

				// Listen to events
				xhr.onload = callback();
				errorCallback = xhr.onerror = callback( "error" );

				// Support: IE9
				// Use onreadystatechange to replace onabort
				// to handle uncaught aborts
				if ( xhr.onabort !== undefined ) {
					xhr.onabort = errorCallback;
				} else {
					xhr.onreadystatechange = function() {

						// Check readyState before timeout as it changes
						if ( xhr.readyState === 4 ) {

							// Allow onerror to be called first,
							// but that will not handle a native abort
							// Also, save errorCallback to a variable
							// as xhr.onerror cannot be accessed
							window.setTimeout( function() {
								if ( callback ) {
									errorCallback();
								}
							} );
						}
					};
				}

				// Create the abort callback
				callback = callback( "abort" );

				try {

					// Do send the request (this may raise an exception)
					xhr.send( options.hasContent && options.data || null );
				} catch ( e ) {

					// #14683: Only rethrow if this hasn't been notified as an error yet
					if ( callback ) {
						throw e;
					}
				}
			},

			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




// Install script dataType
jQuery.ajaxSetup( {
	accepts: {
		script: "text/javascript, application/javascript, " +
			"application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /\b(?:java|ecma)script\b/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
} );

// Handle cache's special case and crossDomain
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
	}
} );

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function( s ) {

	// This transport only deals with cross domain requests
	if ( s.crossDomain ) {
		var script, callback;
		return {
			send: function( _, complete ) {
				script = jQuery( "<script>" ).prop( {
					charset: s.scriptCharset,
					src: s.url
				} ).on(
					"load error",
					callback = function( evt ) {
						script.remove();
						callback = null;
						if ( evt ) {
							complete( evt.type === "error" ? 404 : 200, evt.type );
						}
					}
				);

				// Use native DOM manipulation to avoid our domManip AJAX trickery
				document.head.appendChild( script[ 0 ] );
			},
			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




var oldCallbacks = [],
	rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup( {
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
		this[ callback ] = true;
		return callback;
	}
} );

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
		jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
			"url" :
			typeof s.data === "string" &&
				( s.contentType || "" )
					.indexOf( "application/x-www-form-urlencoded" ) === 0 &&
				rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
			s.jsonpCallback() :
			s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters[ "script json" ] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// Force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always( function() {

			// If previous value didn't exist - remove it
			if ( overwritten === undefined ) {
				jQuery( window ).removeProp( callbackName );

			// Otherwise restore preexisting value
			} else {
				window[ callbackName ] = overwritten;
			}

			// Save back as free
			if ( s[ callbackName ] ) {

				// Make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// Save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && jQuery.isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		} );

		// Delegate to script
		return "script";
	}
} );




// Argument "data" should be string of html
// context (optional): If specified, the fragment will be created in this context,
// defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( !data || typeof data !== "string" ) {
		return null;
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}
	context = context || document;

	var parsed = rsingleTag.exec( data ),
		scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[ 1 ] ) ];
	}

	parsed = buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


// Keep a copy of the old load method
var _load = jQuery.fn.load;

/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
	if ( typeof url !== "string" && _load ) {
		return _load.apply( this, arguments );
	}

	var selector, type, response,
		self = this,
		off = url.indexOf( " " );

	if ( off > -1 ) {
		selector = jQuery.trim( url.slice( off ) );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( jQuery.isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax( {
			url: url,

			// If "type" variable is undefined, then "GET" method will be used.
			// Make value of this field explicit since
			// user can override it through ajaxSetup method
			type: type || "GET",
			dataType: "html",
			data: params
		} ).done( function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery( "<div>" ).append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		// If the request succeeds, this function gets "data", "status", "jqXHR"
		// but they are ignored because response was set above.
		// If it fails, this function gets "jqXHR", "status", "error"
		} ).always( callback && function( jqXHR, status ) {
			self.each( function() {
				callback.apply( this, response || [ jqXHR.responseText, status, jqXHR ] );
			} );
		} );
	}

	return this;
};




// Attach a bunch of functions for handling common AJAX events
jQuery.each( [
	"ajaxStart",
	"ajaxStop",
	"ajaxComplete",
	"ajaxError",
	"ajaxSuccess",
	"ajaxSend"
], function( i, type ) {
	jQuery.fn[ type ] = function( fn ) {
		return this.on( type, fn );
	};
} );




jQuery.expr.filters.animated = function( elem ) {
	return jQuery.grep( jQuery.timers, function( fn ) {
		return elem === fn.elem;
	} ).length;
};




/**
 * Gets a window from an element
 */
function getWindow( elem ) {
	return jQuery.isWindow( elem ) ? elem : elem.nodeType === 9 && elem.defaultView;
}

jQuery.offset = {
	setOffset: function( elem, options, i ) {
		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
			position = jQuery.css( elem, "position" ),
			curElem = jQuery( elem ),
			props = {};

		// Set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
			( curCSSTop + curCSSLeft ).indexOf( "auto" ) > -1;

		// Need to be able to calculate position if either
		// top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;

		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( jQuery.isFunction( options ) ) {

			// Use jQuery.extend here to allow modification of coordinates argument (gh-1848)
			options = options.call( elem, i, jQuery.extend( {}, curOffset ) );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );

		} else {
			curElem.css( props );
		}
	}
};

jQuery.fn.extend( {
	offset: function( options ) {
		if ( arguments.length ) {
			return options === undefined ?
				this :
				this.each( function( i ) {
					jQuery.offset.setOffset( this, options, i );
				} );
		}

		var docElem, win,
			elem = this[ 0 ],
			box = { top: 0, left: 0 },
			doc = elem && elem.ownerDocument;

		if ( !doc ) {
			return;
		}

		docElem = doc.documentElement;

		// Make sure it's not a disconnected DOM node
		if ( !jQuery.contains( docElem, elem ) ) {
			return box;
		}

		box = elem.getBoundingClientRect();
		win = getWindow( doc );
		return {
			top: box.top + win.pageYOffset - docElem.clientTop,
			left: box.left + win.pageXOffset - docElem.clientLeft
		};
	},

	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset,
			elem = this[ 0 ],
			parentOffset = { top: 0, left: 0 };

		// Fixed elements are offset from window (parentOffset = {top:0, left: 0},
		// because it is its only offset parent
		if ( jQuery.css( elem, "position" ) === "fixed" ) {

			// Assume getBoundingClientRect is there when computed position is fixed
			offset = elem.getBoundingClientRect();

		} else {

			// Get *real* offsetParent
			offsetParent = this.offsetParent();

			// Get correct offsets
			offset = this.offset();
			if ( !jQuery.nodeName( offsetParent[ 0 ], "html" ) ) {
				parentOffset = offsetParent.offset();
			}

			// Add offsetParent borders
			parentOffset.top += jQuery.css( offsetParent[ 0 ], "borderTopWidth", true );
			parentOffset.left += jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true );
		}

		// Subtract parent offsets and element margins
		return {
			top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
		};
	},

	// This method will return documentElement in the following cases:
	// 1) For the element inside the iframe without offsetParent, this method will return
	//    documentElement of the parent window
	// 2) For the hidden or detached element
	// 3) For body or html element, i.e. in case of the html node - it will return itself
	//
	// but those exceptions were never presented as a real life use-cases
	// and might be considered as more preferable results.
	//
	// This logic, however, is not guaranteed and can change at any point in the future
	offsetParent: function() {
		return this.map( function() {
			var offsetParent = this.offsetParent;

			while ( offsetParent && jQuery.css( offsetParent, "position" ) === "static" ) {
				offsetParent = offsetParent.offsetParent;
			}

			return offsetParent || documentElement;
		} );
	}
} );

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = "pageYOffset" === prop;

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {
			var win = getWindow( elem );

			if ( val === undefined ) {
				return win ? win[ prop ] : elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : win.pageXOffset,
					top ? val : win.pageYOffset
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length );
	};
} );

// Support: Safari<7-8+, Chrome<37-44+
// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// Blink bug: https://code.google.com/p/chromium/issues/detail?id=229280
// getComputedStyle returns percent when specified for top/left/bottom/right;
// rather than make the css module depend on the offset module, just check for it here
jQuery.each( [ "top", "left" ], function( i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );

				// If curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
					jQuery( elem ).position()[ prop ] + "px" :
					computed;
			}
		}
	);
} );


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name },
		function( defaultExtra, funcName ) {

		// Margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
				extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( jQuery.isWindow( elem ) ) {

					// As of 5/8/2012 this will yield incorrect results for Mobile Safari, but there
					// isn't a whole lot we can do. See pull request at this URL for discussion:
					// https://github.com/jquery/jquery/pull/764
					return elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
					// whichever is greatest
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
					);
				}

				return value === undefined ?

					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
			}, type, chainable ? margin : undefined, chainable, null );
		};
	} );
} );


jQuery.fn.extend( {

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {

		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ?
			this.off( selector, "**" ) :
			this.off( types, selector || "**", fn );
	},
	size: function() {
		return this.length;
	}
} );

jQuery.fn.andSelf = jQuery.fn.addBack;




// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.

// Note that for maximum portability, libraries that are not jQuery should
// declare themselves as anonymous modules, and avoid setting a global if an
// AMD loader is present. jQuery is a special case. For more information, see
// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

if ( typeof define === "function" && define.amd ) {
	define( "jquery", [], function() {
		return jQuery;
	} );
}



var

	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

jQuery.noConflict = function( deep ) {
	if ( window.$ === jQuery ) {
		window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
		window.jQuery = _jQuery;
	}

	return jQuery;
};

// Expose jQuery and $ identifiers, even in AMD
// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( !noGlobal ) {
	window.jQuery = window.$ = jQuery;
}

return jQuery;
}));

/**
 * Framework7 1.4.2
 * Full Featured Mobile HTML Framework For Building iOS & Android Apps
 * 
 * http://www.idangero.us/framework7
 * 
 * Copyright 2016, Vladimir Kharlampidi
 * The iDangero.us
 * http://www.idangero.us/
 * 
 * Licensed under MIT
 * 
 * Released on: February 27, 2016
 */
!function(){"use strict";window.Framework7=function(a){function t(e){var a=e.replace(/^./,function(e){return e.toUpperCase()});i["onPage"+a]=function(a,t){return i.onPage(e,a,t)}}function n(){var e,a=o(this),t=a[0].scrollTop,n=a[0].scrollHeight,r=a[0].offsetHeight,i=a[0].getAttribute("data-distance"),s=a.find(".virtual-list"),l=a.hasClass("infinite-scroll-top");if(i||(i=50),"string"==typeof i&&i.indexOf("%")>=0&&(i=parseInt(i,10)/100*r),i>r&&(i=r),l)i>t&&a.trigger("infinite");else if(t+r>=n-i){if(s.length>0&&(e=s[0].f7VirtualList,e&&!e.reachEnd))return;a.trigger("infinite")}}function r(){i.device.ipad&&(document.body.scrollLeft=0,setTimeout(function(){document.body.scrollLeft=0},0))}var i=this;i.version="1.4.2",i.params={cache:!0,cacheIgnore:[],cacheIgnoreGetParameters:!1,cacheDuration:6e5,preloadPreviousPage:!0,uniqueHistory:!1,uniqueHistoryIgnoreGetParameters:!1,dynamicPageUrl:"content-{{index}}",allowDuplicateUrls:!1,router:!0,pushState:!1,pushStateRoot:void 0,pushStateNoAnimation:!1,pushStateSeparator:"#!/",pushStateOnLoad:!0,fastClicks:!0,fastClicksDistanceThreshold:10,fastClicksDelayBetweenClicks:50,tapHold:!1,tapHoldDelay:750,tapHoldPreventClicks:!0,activeState:!0,activeStateElements:"a, button, label, span",animateNavBackIcon:!1,swipeBackPage:!0,swipeBackPageThreshold:0,swipeBackPageActiveArea:30,swipeBackPageAnimateShadow:!0,swipeBackPageAnimateOpacity:!0,ajaxLinks:void 0,externalLinks:".external",sortable:!0,hideNavbarOnPageScroll:!1,hideToolbarOnPageScroll:!1,hideTabbarOnPageScroll:!1,showBarsOnPageScrollEnd:!0,showBarsOnPageScrollTop:!0,swipeout:!0,swipeoutActionsNoFold:!1,swipeoutNoFollow:!1,smartSelectOpenIn:"page",smartSelectBackText:"Back",smartSelectPopupCloseText:"Close",smartSelectPickerCloseText:"Done",smartSelectSearchbar:!1,smartSelectBackOnSelect:!1,scrollTopOnNavbarClick:!1,scrollTopOnStatusbarClick:!1,swipePanel:!1,swipePanelActiveArea:0,swipePanelCloseOpposite:!0,swipePanelOnlyClose:!1,swipePanelNoFollow:!1,swipePanelThreshold:0,panelsCloseByOutside:!0,modalButtonOk:"OK",modalButtonCancel:"Cancel",modalUsernamePlaceholder:"Username",modalPasswordPlaceholder:"Password",modalTitle:"Framework7",modalCloseByOutside:!1,actionsCloseByOutside:!0,popupCloseByOutside:!0,modalPreloaderTitle:"Loading... ",modalStack:!0,imagesLazyLoadThreshold:0,imagesLazyLoadSequential:!0,viewClass:"view",viewMainClass:"view-main",viewsClass:"views",notificationCloseOnClick:!1,notificationCloseIcon:!0,notificationCloseButtonText:"Close",animatePages:!0,templates:{},template7Data:{},template7Pages:!1,precompileTemplates:!1,material:!1,materialPageLoadDelay:0,materialPreloaderSvg:'<svg xmlns="http://www.w3.org/2000/svg" height="75" width="75" viewbox="0 0 75 75"><circle cx="37.5" cy="37.5" r="33.5" stroke-width="8"/></svg>',materialPreloaderHtml:'<span class="preloader-inner"><span class="preloader-inner-gap"></span><span class="preloader-inner-left"><span class="preloader-inner-half-circle"></span></span><span class="preloader-inner-right"><span class="preloader-inner-half-circle"></span></span></span>',materialRipple:!0,materialRippleElements:".ripple, a.link, a.item-link, .button, .modal-button, .tab-link, .label-radio, .label-checkbox, .actions-modal-button, a.searchbar-clear, a.floating-button, .floating-button > a, .speed-dial-buttons a",init:!0};for(var s in a)i.params[s]=a[s];var o=e,l=Template7;i._compiledTemplates={},i.touchEvents={start:i.support.touch?"touchstart":"mousedown",move:i.support.touch?"touchmove":"mousemove",end:i.support.touch?"touchend":"mouseup"},i.ls=window.localStorage,i.rtl="rtl"===o("body").css("direction"),i.rtl&&o("html").attr("dir","rtl"),"undefined"!=typeof i.params.statusbarOverlay&&(i.params.statusbarOverlay?o("html").addClass("with-statusbar-overlay"):o("html").removeClass("with-statusbar-overlay")),i.views=[];var p=function(e,a){var t,n={dynamicNavbar:!1,domCache:!1,linksView:void 0,reloadPages:!1,uniqueHistory:i.params.uniqueHistory,uniqueHistoryIgnoreGetParameters:i.params.uniqueHistoryIgnoreGetParameters,allowDuplicateUrls:i.params.allowDuplicateUrls,swipeBackPage:i.params.swipeBackPage,swipeBackPageAnimateShadow:i.params.swipeBackPageAnimateShadow,swipeBackPageAnimateOpacity:i.params.swipeBackPageAnimateOpacity,swipeBackPageActiveArea:i.params.swipeBackPageActiveArea,swipeBackPageThreshold:i.params.swipeBackPageThreshold,animatePages:i.params.animatePages,preloadPreviousPage:i.params.preloadPreviousPage};a=a||{},a.dynamicNavbar&&i.params.material&&(a.dynamicNavbar=!1);for(var r in n)"undefined"==typeof a[r]&&(a[r]=n[r]);var s=this;s.params=a,s.selector=e;var l=o(e);if(s.container=l[0],"string"!=typeof e&&(e=(l.attr("id")?"#"+l.attr("id"):"")+(l.attr("class")?"."+l.attr("class").replace(/ /g,".").replace(".active",""):""),s.selector=e),s.main=l.hasClass(i.params.viewMainClass),s.contentCache={},s.pagesCache={},l[0].f7View=s,s.pagesContainer=l.find(".pages")[0],s.initialPages=[],s.initialPagesUrl=[],s.initialNavbars=[],s.params.domCache){var p=l.find(".page");for(t=0;t<p.length;t++)s.initialPages.push(p[t]),s.initialPagesUrl.push("#"+p.eq(t).attr("data-page"));if(s.params.dynamicNavbar){var d=l.find(".navbar-inner");for(t=0;t<d.length;t++)s.initialNavbars.push(d[t])}}s.allowPageChange=!0;var c=document.location.href;s.history=[];var u=c,m=i.params.pushStateSeparator,f=i.params.pushStateRoot;i.params.pushState&&s.main&&(f?u=f:u.indexOf(m)>=0&&u.indexOf(m+"#")<0&&(u=u.split(m)[0]));var h,g;s.activePage||(h=o(s.pagesContainer).find(".page-on-center"),0===h.length&&(h=o(s.pagesContainer).find(".page:not(.cached)"),h=h.eq(h.length-1)),h.length>0&&(g=h[0].f7PageData)),s.params.domCache&&h?(s.url=l.attr("data-url")||s.params.url||"#"+h.attr("data-page"),s.pagesCache[s.url]=h.attr("data-page")):s.url=l.attr("data-url")||s.params.url||u,g&&(g.view=s,g.url=s.url,s.params.domCache&&s.params.dynamicNavbar&&!g.navbarInnerContainer&&(g.navbarInnerContainer=s.initialNavbars[s.initialPages.indexOf(g.container)]),s.activePage=g,h[0].f7PageData=g),s.url&&s.history.push(s.url);var v,b,w,C,y,x,T,k,P,S,M,I=!1,O=!1,E={},D=[],L=[],B=!0,z=[],N=[];if(s.handleTouchStart=function(e){B&&s.params.swipeBackPage&&!I&&!i.swipeoutOpenedEl&&s.allowPageChange&&(O=!1,I=!0,v=void 0,E.x="touchstart"===e.type?e.targetTouches[0].pageX:e.pageX,E.y="touchstart"===e.type?e.targetTouches[0].pageY:e.pageY,C=(new Date).getTime(),P=s.params.dynamicNavbar&&l.find(".navbar-inner").length>1)},s.handleTouchMove=function(e){if(I){var a="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,t="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY;if("undefined"==typeof v&&(v=!!(v||Math.abs(t-E.y)>Math.abs(a-E.x))),v||e.f7PreventSwipeBack||i.preventSwipeBack)return void(I=!1);if(!O){var n=!1;b=l.width();var r=o(e.target),p=r.hasClass("swipeout")?r:r.parents(".swipeout");p.length>0&&(!i.rtl&&p.find(".swipeout-actions-left").length>0&&(n=!0),i.rtl&&p.find(".swipeout-actions-right").length>0&&(n=!0)),D=r.is(".page")?r:r.parents(".page"),D.hasClass("no-swipeback")&&(n=!0),L=l.find(".page-on-left:not(.cached)");var d=E.x-l.offset().left>s.params.swipeBackPageActiveArea;if(d=i.rtl?E.x<l.offset().left-l[0].scrollLeft+b-s.params.swipeBackPageActiveArea:E.x-l.offset().left>s.params.swipeBackPageActiveArea,d&&(n=!0),(0===L.length||0===D.length)&&(n=!0),n)return void(I=!1);s.params.swipeBackPageAnimateShadow&&!i.device.android&&(S=D.find(".swipeback-page-shadow"),0===S.length&&(S=o('<div class="swipeback-page-shadow"></div>'),D.append(S))),P&&(z=l.find(".navbar-on-center:not(.cached)"),N=l.find(".navbar-on-left:not(.cached)"),y=z.find(".left, .center, .right, .subnavbar, .fading"),x=N.find(".left, .center, .right, .subnavbar, .fading"),i.params.animateNavBackIcon&&(T=z.find(".left.sliding .back .icon"),k=N.find(".left.sliding .back .icon"))),o(".picker-modal.modal-in").length>0&&i.closeModal(o(".picker-modal.modal-in"))}e.f7PreventPanelSwipe=!0,O=!0,e.preventDefault();var c=i.rtl?-1:1;w=(a-E.x-s.params.swipeBackPageThreshold)*c,0>w&&(w=0);var u=w/b,m={percentage:u,activePage:D[0],previousPage:L[0],activeNavbar:z[0],previousNavbar:N[0]};s.params.onSwipeBackMove&&s.params.onSwipeBackMove(m),l.trigger("swipeBackMove",m);var f=w*c,h=(w/5-b/5)*c;if(1===i.device.pixelRatio&&(f=Math.round(f),h=Math.round(h)),D.transform("translate3d("+f+"px,0,0)"),s.params.swipeBackPageAnimateShadow&&!i.device.android&&(S[0].style.opacity=1-1*u),L.transform("translate3d("+h+"px,0,0)"),s.params.swipeBackPageAnimateOpacity&&(L[0].style.opacity=.9+.1*u),P){var g;for(g=0;g<y.length;g++)if(M=o(y[g]),M.is(".subnavbar.sliding")||(M[0].style.opacity=1-1.3*u),M[0].className.indexOf("sliding")>=0){var C=u*M[0].f7NavbarRightOffset;1===i.device.pixelRatio&&(C=Math.round(C)),M.transform("translate3d("+C+"px,0,0)"),i.params.animateNavBackIcon&&M[0].className.indexOf("left")>=0&&T.length>0&&T.transform("translate3d("+-C+"px,0,0)")}for(g=0;g<x.length;g++)if(M=o(x[g]),M.is(".subnavbar.sliding")||(M[0].style.opacity=1.3*u-.3),M[0].className.indexOf("sliding")>=0){var B=M[0].f7NavbarLeftOffset*(1-u);1===i.device.pixelRatio&&(B=Math.round(B)),M.transform("translate3d("+B+"px,0,0)"),i.params.animateNavBackIcon&&M[0].className.indexOf("left")>=0&&k.length>0&&k.transform("translate3d("+-B+"px,0,0)")}}}},s.handleTouchEnd=function(e){if(!I||!O)return I=!1,void(O=!1);if(I=!1,O=!1,0===w)return o([D[0],L[0]]).transform("").css({opacity:"",boxShadow:""}),void(P&&(y.transform("").css({opacity:""}),x.transform("").css({opacity:""}),T&&T.length>0&&T.transform(""),k&&T.length>0&&k.transform("")));var a=(new Date).getTime()-C,t=!1;(300>a&&w>10||a>=300&&w>b/2)&&(D.removeClass("page-on-center").addClass("page-on-right"),L.removeClass("page-on-left").addClass("page-on-center"),P&&(z.removeClass("navbar-on-center").addClass("navbar-on-right"),N.removeClass("navbar-on-left").addClass("navbar-on-center")),t=!0),o([D[0],L[0]]).transform("").css({opacity:"",boxShadow:""}).addClass("page-transitioning"),P&&(y.css({opacity:""}).each(function(){var e=t?this.f7NavbarRightOffset:0,a=o(this);a.transform("translate3d("+e+"px,0,0)"),i.params.animateNavBackIcon&&a.hasClass("left")&&T.length>0&&T.addClass("page-transitioning").transform("translate3d("+-e+"px,0,0)")}).addClass("page-transitioning"),x.transform("").css({opacity:""}).each(function(){var e=t?0:this.f7NavbarLeftOffset,a=o(this);a.transform("translate3d("+e+"px,0,0)"),i.params.animateNavBackIcon&&a.hasClass("left")&&k.length>0&&k.addClass("page-transitioning").transform("translate3d("+-e+"px,0,0)")}).addClass("page-transitioning")),B=!1,s.allowPageChange=!1;var n={activePage:D[0],previousPage:L[0],activeNavbar:z[0],previousNavbar:N[0]};if(t){var r=s.history[s.history.length-2];s.url=r,i.pageBackCallback("before",s,{pageContainer:D[0],url:r,position:"center",newPage:L,oldPage:D,swipeBack:!0}),i.pageAnimCallback("before",s,{pageContainer:L[0],url:r,position:"left",newPage:L,oldPage:D,swipeBack:!0}),s.params.onSwipeBackBeforeChange&&s.params.onSwipeBackBeforeChange(n),l.trigger("swipeBackBeforeChange",n)}else s.params.onSwipeBackBeforeReset&&s.params.onSwipeBackBeforeReset(n),l.trigger("swipeBackBeforeReset",n);D.transitionEnd(function(){o([D[0],L[0]]).removeClass("page-transitioning"),P&&(y.removeClass("page-transitioning").css({opacity:""}),x.removeClass("page-transitioning").css({opacity:""}),T&&T.length>0&&T.removeClass("page-transitioning"),k&&k.length>0&&k.removeClass("page-transitioning")),B=!0,s.allowPageChange=!0,t?(i.params.pushState&&s.main&&history.back(),i.pageBackCallback("after",s,{pageContainer:D[0],url:r,position:"center",newPage:L,oldPage:D,swipeBack:!0}),i.pageAnimCallback("after",s,{pageContainer:L[0],url:r,position:"left",newPage:L,oldPage:D,swipeBack:!0}),i.router.afterBack(s,D,L),s.params.onSwipeBackAfterChange&&s.params.onSwipeBackAfterChange(n),l.trigger("swipeBackAfterChange",n)):(s.params.onSwipeBackAfterReset&&s.params.onSwipeBackAfterReset(n),l.trigger("swipeBackAfterReset",n)),S&&S.length>0&&S.remove()})},s.attachEvents=function(e){var a=e?"off":"on";l[a](i.touchEvents.start,s.handleTouchStart),l[a](i.touchEvents.move,s.handleTouchMove),l[a](i.touchEvents.end,s.handleTouchEnd)},s.detachEvents=function(){s.attachEvents(!0)},s.params.swipeBackPage&&!i.params.material&&s.attachEvents(),i.views.push(s),s.main&&(i.mainView=s),s.router={load:function(e){return i.router.load(s,e)},back:function(e){return i.router.back(s,e)},loadPage:function(e){if(e=e||{},"string"==typeof e){var a=e;e={},a&&0===a.indexOf("#")&&s.params.domCache?e.pageName=a.split("#")[1]:e.url=a}return i.router.load(s,e)},loadContent:function(e){return i.router.load(s,{content:e})},reloadPage:function(e){return i.router.load(s,{url:e,reload:!0})},reloadContent:function(e){return i.router.load(s,{content:e,reload:!0})},reloadPreviousPage:function(e){return i.router.load(s,{url:e,reloadPrevious:!0,reload:!0})},reloadPreviousContent:function(e){return i.router.load(s,{content:e,reloadPrevious:!0,reload:!0})},refreshPage:function(){var e={url:s.url,reload:!0,ignoreCache:!0};return e.url&&0===e.url.indexOf("#")&&(s.params.domCache&&s.pagesCache[e.url]?(e.pageName=s.pagesCache[e.url],e.url=void 0,delete e.url):s.contentCache[e.url]&&(e.content=s.contentCache[e.url],e.url=void 0,delete e.url)),i.router.load(s,e)},refreshPreviousPage:function(){var e={url:s.history[s.history.length-2],reload:!0,reloadPrevious:!0,ignoreCache:!0};return e.url&&0===e.url.indexOf("#")&&s.params.domCache&&s.pagesCache[e.url]&&(e.pageName=s.pagesCache[e.url],e.url=void 0,delete e.url),i.router.load(s,e)}},s.loadPage=s.router.loadPage,s.loadContent=s.router.loadContent,s.reloadPage=s.router.reloadPage,s.reloadContent=s.router.reloadContent,s.reloadPreviousPage=s.router.reloadPreviousPage,s.reloadPreviousContent=s.router.reloadPreviousContent,s.refreshPage=s.router.refreshPage,s.refreshPreviousPage=s.router.refreshPreviousPage,s.back=s.router.back,s.hideNavbar=function(){return i.hideNavbar(l.find(".navbar"))},s.showNavbar=function(){return i.showNavbar(l.find(".navbar"))},s.hideToolbar=function(){return i.hideToolbar(l.find(".toolbar"))},s.showToolbar=function(){return i.showToolbar(l.find(".toolbar"))},i.params.pushState&&i.params.pushStateOnLoad&&s.main){var H,A=c.split(m)[1];f?H=c.split(i.params.pushStateRoot+m)[1]:m&&c.indexOf(m)>=0&&c.indexOf(m+"#")<0&&(H=A);var R=i.params.pushStateNoAnimation?!1:void 0,V=history.state;H?H.indexOf("#")>=0&&s.params.domCache&&V&&V.pageName&&"viewIndex"in V?i.router.load(s,{pageName:V.pageName,url:V.url,animatePages:R,pushState:!1}):H.indexOf("#")>=0&&s.params.domCache&&s.initialPagesUrl.indexOf(H)>=0?i.router.load(s,{pageName:H.replace("#",""),animatePages:R,pushState:!1}):i.router.load(s,{url:H,animatePages:R,pushState:!1}):s.params.domCache&&c.indexOf(m+"#")>=0&&(V&&V.pageName&&"viewIndex"in V?i.router.load(s,{pageName:V.pageName,url:V.url,animatePages:R,pushState:!1}):m&&0===A.indexOf("#")&&s.initialPagesUrl.indexOf(A)&&i.router.load(s,{pageName:A.replace("#",""),animatePages:R,pushState:!1}))}return s.destroy=function(){s.detachEvents(),s=void 0},i.pluginHook("addView",s),s};i.addView=function(e,a){return new p(e,a)},i.getCurrentView=function(e){var a=o(".popover.modal-in .view"),t=o(".popup.modal-in .view"),n=o(".panel.active .view"),r=o(".views"),i=r.children(".view");if(i.length>1&&i.hasClass("tab")&&(i=r.children(".view.active")),a.length>0&&a[0].f7View)return a[0].f7View;if(t.length>0&&t[0].f7View)return t[0].f7View;if(n.length>0&&n[0].f7View)return n[0].f7View;if(i.length>0){if(1===i.length&&i[0].f7View)return i[0].f7View;if(i.length>1){for(var s=[],l=0;l<i.length;l++)i[l].f7View&&s.push(i[l].f7View);if(s.length>0&&"undefined"!=typeof e)return s[e];if(s.length>1)return s;if(1===s.length)return s[0];return}}},i.navbarInitCallback=function(e,a,t,n){if(!t&&n&&(t=o(n).parent(".navbar")[0]),!n.f7NavbarInitialized||!e||e.params.domCache){var r={container:t,innerContainer:n},s=a&&a.f7PageData,l={page:s,navbar:r};if(n.f7NavbarInitialized&&(e&&e.params.domCache||!e&&o(t).parents(".popup, .popover, .login-screen, .modal, .actions-modal, .picker-modal").length>0))return i.reinitNavbar(t,n),i.pluginHook("navbarReinit",l),void o(n).trigger("navbarReinit",l);n.f7NavbarInitialized=!0,i.pluginHook("navbarBeforeInit",r,s),o(n).trigger("navbarBeforeInit",l),i.initNavbar(t,n),i.pluginHook("navbarInit",r,s),o(n).trigger("navbarInit",l)}},i.navbarRemoveCallback=function(e,a,t,n){!t&&n&&(t=o(n).parent(".navbar")[0]);var r={container:t,innerContainer:n},s=a.f7PageData,l={page:s,navbar:r};i.pluginHook("navbarBeforeRemove",r,s),o(n).trigger("navbarBeforeRemove",l)},i.initNavbar=function(e,a){i.initSearchbar&&i.initSearchbar(a)},i.reinitNavbar=function(e,a){},i.initNavbarWithCallback=function(e){e=o(e);var a,t=e.parents("."+i.params.viewClass);0!==t.length&&(0!==e.parents(".navbar-through").length||0!==t.find(".navbar-through").length)&&(a=t[0].f7View||void 0,e.find(".navbar-inner").each(function(){var n,r=this;if(o(r).attr("data-page")&&(n=t.find('.page[data-page="'+o(r).attr("data-page")+'"]')[0]),!n){var s=t.find(".page");1===s.length?n=s[0]:t.find(".page").each(function(){this.f7PageData&&this.f7PageData.navbarInnerContainer===r&&(n=this)})}i.navbarInitCallback(a,n,e[0],r)}))},i.sizeNavbars=function(e){if(!i.params.material){var a=e?o(e).find(".navbar .navbar-inner:not(.cached)"):o(".navbar .navbar-inner:not(.cached)");a.each(function(){var e=o(this);if(!e.hasClass("cached")){var a,t,n=i.rtl?e.find(".right"):e.find(".left"),r=i.rtl?e.find(".left"):e.find(".right"),s=e.find(".center"),l=e.find(".subnavbar"),p=0===n.length,d=0===r.length,c=p?0:n.outerWidth(!0),u=d?0:r.outerWidth(!0),m=s.outerWidth(!0),f=e.styles(),h=e[0].offsetWidth-parseInt(f.paddingLeft,10)-parseInt(f.paddingRight,10),g=e.hasClass("navbar-on-left");d&&(a=h-m),p&&(a=0),p||d||(a=(h-u-m+c)/2);var v=(h-m)/2;h-c-u>m?(c>v&&(v=c),v+m>h-u&&(v=h-u-m),t=v-a):t=0;var b=i.rtl?-1:1;if(s.hasClass("sliding")&&(s[0].f7NavbarLeftOffset=-(a+t)*b,s[0].f7NavbarRightOffset=(h-a-t-m)*b,g)){if(i.params.animateNavBackIcon){var w=e.parent().find(".navbar-on-center").find(".left.sliding .back .icon ~ span");w.length>0&&(s[0].f7NavbarLeftOffset+=w[0].offsetLeft)}s.transform("translate3d("+s[0].f7NavbarLeftOffset+"px, 0, 0)")}!p&&n.hasClass("sliding")&&(i.rtl?(n[0].f7NavbarLeftOffset=-(h-n[0].offsetWidth)/2*b,n[0].f7NavbarRightOffset=c*b):(n[0].f7NavbarLeftOffset=-c,n[0].f7NavbarRightOffset=(h-n[0].offsetWidth)/2,i.params.animateNavBackIcon&&n.find(".back .icon").length>0&&(n[0].f7NavbarRightOffset-=n.find(".back .icon")[0].offsetWidth)),g&&n.transform("translate3d("+n[0].f7NavbarLeftOffset+"px, 0, 0)")),!d&&r.hasClass("sliding")&&(i.rtl?(r[0].f7NavbarLeftOffset=-u*b,r[0].f7NavbarRightOffset=(h-r[0].offsetWidth)/2*b):(r[0].f7NavbarLeftOffset=-(h-r[0].offsetWidth)/2,r[0].f7NavbarRightOffset=u),g&&r.transform("translate3d("+r[0].f7NavbarLeftOffset+"px, 0, 0)")),l.length&&l.hasClass("sliding")&&(l[0].f7NavbarLeftOffset=i.rtl?l[0].offsetWidth:-l[0].offsetWidth,l[0].f7NavbarRightOffset=-l[0].f7NavbarLeftOffset);var C=t;i.rtl&&p&&d&&s.length>0&&(C=-C),s.css({left:C+"px"})}})}},i.hideNavbar=function(e){return o(e).addClass("navbar-hidden"),!0},i.showNavbar=function(e){var a=o(e);return a.addClass("navbar-hiding").removeClass("navbar-hidden").transitionEnd(function(){a.removeClass("navbar-hiding")}),!0},i.hideToolbar=function(e){return o(e).addClass("toolbar-hidden"),!0},i.showToolbar=function(e){var a=o(e);a.addClass("toolbar-hiding").removeClass("toolbar-hidden").transitionEnd(function(){a.removeClass("toolbar-hiding")})};var d=function(e,a){function t(e){return e.replace(/[^\u0000-\u007E]/g,function(e){return d[e]||e})}function n(e){e.preventDefault()}var r={input:null,clearButton:null,cancelButton:null,searchList:null,searchIn:".item-title",searchBy:"",found:null,notFound:null,overlay:null,ignore:".searchbar-ignore",customSearch:!1,removeDiacritics:!1,hideDividers:!0,hideGroups:!0};a=a||{};for(var s in r)("undefined"==typeof a[s]||null===a[s])&&(a[s]=r[s]);var l=this;l.material=i.params.material,l.params=a,e=o(e),l.container=e,l.active=!1,l.input=l.params.input?o(l.params.input):l.container.find('input[type="search"]'),l.clearButton=l.params.clearButton?o(l.params.clearButton):l.container.find(".searchbar-clear"),l.cancelButton=l.params.cancelButton?o(l.params.cancelButton):l.container.find(".searchbar-cancel"),l.searchList=o(l.params.searchList),l.isVirtualList=l.searchList.hasClass("virtual-list"),l.pageContainer=l.container.parents(".page").eq(0),l.params.overlay?l.overlay=o(l.params.overlay):l.overlay=l.pageContainer.length>0?l.pageContainer.find(".searchbar-overlay"):o(".searchbar-overlay"),l.params.found?l.found=o(l.params.found):l.found=l.pageContainer.length>0?l.pageContainer.find(".searchbar-found"):o(".searchbar-found"),l.params.notFound?l.notFound=o(l.params.notFound):l.notFound=l.pageContainer.length>0?l.pageContainer.find(".searchbar-not-found"):o(".searchbar-not-found");for(var p=[{base:"A",letters:"AⒶＡÀÁÂẦẤẪẨÃĀĂẰẮẴẲȦǠÄǞẢÅǺǍȀȂẠẬẶḀĄȺⱯ"},{base:"AA",letters:"Ꜳ"},{base:"AE",letters:"ÆǼǢ"},{base:"AO",letters:"Ꜵ"},{base:"AU",letters:"Ꜷ"},{base:"AV",letters:"ꜸꜺ"},{base:"AY",letters:"Ꜽ"},{base:"B",letters:"BⒷＢḂḄḆɃƂƁ"},{base:"C",letters:"CⒸＣĆĈĊČÇḈƇȻꜾ"},{base:"D",letters:"DⒹＤḊĎḌḐḒḎĐƋƊƉꝹ"},{base:"DZ",letters:"ǱǄ"},{base:"Dz",letters:"ǲǅ"},{base:"E",letters:"EⒺＥÈÉÊỀẾỄỂẼĒḔḖĔĖËẺĚȄȆẸỆȨḜĘḘḚƐƎ"},{base:"F",letters:"FⒻＦḞƑꝻ"},{base:"G",letters:"GⒼＧǴĜḠĞĠǦĢǤƓꞠꝽꝾ"},{base:"H",letters:"HⒽＨĤḢḦȞḤḨḪĦⱧⱵꞍ"},{base:"I",letters:"IⒾＩÌÍÎĨĪĬİÏḮỈǏȈȊỊĮḬƗ"},{base:"J",letters:"JⒿＪĴɈ"},{base:"K",letters:"KⓀＫḰǨḲĶḴƘⱩꝀꝂꝄꞢ"},{base:"L",letters:"LⓁＬĿĹĽḶḸĻḼḺŁȽⱢⱠꝈꝆꞀ"},{base:"LJ",letters:"Ǉ"},{base:"Lj",letters:"ǈ"},{base:"M",letters:"MⓂＭḾṀṂⱮƜ"},{base:"N",letters:"NⓃＮǸŃÑṄŇṆŅṊṈȠƝꞐꞤ"},{base:"NJ",letters:"Ǌ"},{base:"Nj",letters:"ǋ"},{base:"O",letters:"OⓄＯÒÓÔỒỐỖỔÕṌȬṎŌṐṒŎȮȰÖȪỎŐǑȌȎƠỜỚỠỞỢỌỘǪǬØǾƆƟꝊꝌ"},{base:"OI",letters:"Ƣ"},{base:"OO",letters:"Ꝏ"},{base:"OU",letters:"Ȣ"},{base:"OE",letters:"Œ"},{base:"oe",letters:"œ"},{base:"P",letters:"PⓅＰṔṖƤⱣꝐꝒꝔ"},{base:"Q",letters:"QⓆＱꝖꝘɊ"},{base:"R",letters:"RⓇＲŔṘŘȐȒṚṜŖṞɌⱤꝚꞦꞂ"},{base:"S",letters:"SⓈＳẞŚṤŜṠŠṦṢṨȘŞⱾꞨꞄ"},{base:"T",letters:"TⓉＴṪŤṬȚŢṰṮŦƬƮȾꞆ"},{base:"TZ",letters:"Ꜩ"},{base:"U",letters:"UⓊＵÙÚÛŨṸŪṺŬÜǛǗǕǙỦŮŰǓȔȖƯỪỨỮỬỰỤṲŲṶṴɄ"},{base:"V",letters:"VⓋＶṼṾƲꝞɅ"},{base:"VY",letters:"Ꝡ"},{base:"W",letters:"WⓌＷẀẂŴẆẄẈⱲ"},{base:"X",letters:"XⓍＸẊẌ"},{base:"Y",letters:"YⓎＹỲÝŶỸȲẎŸỶỴƳɎỾ"},{base:"Z",letters:"ZⓏＺŹẐŻŽẒẔƵȤⱿⱫꝢ"},{base:"a",letters:"aⓐａẚàáâầấẫẩãāăằắẵẳȧǡäǟảåǻǎȁȃạậặḁąⱥɐ"},{base:"aa",letters:"ꜳ"},{base:"ae",letters:"æǽǣ"},{base:"ao",letters:"ꜵ"},{base:"au",letters:"ꜷ"},{base:"av",letters:"ꜹꜻ"},{base:"ay",letters:"ꜽ"},{base:"b",letters:"bⓑｂḃḅḇƀƃɓ"},{base:"c",letters:"cⓒｃćĉċčçḉƈȼꜿↄ"},{base:"d",letters:"dⓓｄḋďḍḑḓḏđƌɖɗꝺ"},{base:"dz",letters:"ǳǆ"},{base:"e",letters:"eⓔｅèéêềếễểẽēḕḗĕėëẻěȅȇẹệȩḝęḙḛɇɛǝ"},{base:"f",letters:"fⓕｆḟƒꝼ"},{base:"g",letters:"gⓖｇǵĝḡğġǧģǥɠꞡᵹꝿ"},{base:"h",letters:"hⓗｈĥḣḧȟḥḩḫẖħⱨⱶɥ"},{base:"hv",letters:"ƕ"},{base:"i",letters:"iⓘｉìíîĩīĭïḯỉǐȉȋịįḭɨı"},{base:"j",letters:"jⓙｊĵǰɉ"},{base:"k",letters:"kⓚｋḱǩḳķḵƙⱪꝁꝃꝅꞣ"},{base:"l",letters:"lⓛｌŀĺľḷḹļḽḻſłƚɫⱡꝉꞁꝇ"},{base:"lj",letters:"ǉ"},{base:"m",letters:"mⓜｍḿṁṃɱɯ"},{base:"n",letters:"nⓝｎǹńñṅňṇņṋṉƞɲŉꞑꞥ"},{base:"nj",letters:"ǌ"},{base:"o",letters:"oⓞｏòóôồốỗổõṍȭṏōṑṓŏȯȱöȫỏőǒȍȏơờớỡởợọộǫǭøǿɔꝋꝍɵ"},{base:"oi",letters:"ƣ"},{base:"ou",letters:"ȣ"},{base:"oo",letters:"ꝏ"},{base:"p",letters:"pⓟｐṕṗƥᵽꝑꝓꝕ"},{base:"q",letters:"qⓠｑɋꝗꝙ"},{base:"r",letters:"rⓡｒŕṙřȑȓṛṝŗṟɍɽꝛꞧꞃ"},{base:"s",letters:"sⓢｓßśṥŝṡšṧṣṩșşȿꞩꞅẛ"},{base:"t",letters:"tⓣｔṫẗťṭțţṱṯŧƭʈⱦꞇ"},{base:"tz",letters:"ꜩ"},{base:"u",letters:"uⓤｕùúûũṹūṻŭüǜǘǖǚủůűǔȕȗưừứữửựụṳųṷṵʉ"},{base:"v",letters:"vⓥｖṽṿʋꝟʌ"},{base:"vy",letters:"ꝡ"},{base:"w",letters:"wⓦｗẁẃŵẇẅẘẉⱳ"},{base:"x",letters:"xⓧｘẋẍ"},{base:"y",letters:"yⓨｙỳýŷỹȳẏÿỷẙỵƴɏỿ"},{base:"z",letters:"zⓩｚźẑżžẓẕƶȥɀⱬꝣ"}],d={},c=0;c<p.length;c++)for(var u=p[c].letters,m=0;m<u.length;m++)d[u[m]]=p[c].base;var f=i.rtl?"margin-left":"margin-right",h=!1;l.setCancelButtonMargin=function(){l.cancelButton.transition(0).show(),l.cancelButton.css(f,-l.cancelButton[0].offsetWidth+"px");l.cancelButton[0].clientLeft;l.cancelButton.transition(""),h=!0},l.triggerEvent=function(e,a,t){l.container.trigger(e,t),l.searchList.length>0&&l.searchList.trigger(e,t),a&&l.params[a]&&l.params[a](l,t)},l.enable=function(e){function a(){!l.searchList.length&&!l.params.customSearch||l.container.hasClass("searchbar-active")||l.query||l.overlay.addClass("searchbar-overlay-active"),l.container.addClass("searchbar-active"),l.cancelButton.length>0&&!l.material&&(h||l.setCancelButtonMargin(),l.cancelButton.css(f,"0px")),l.triggerEvent("enableSearch","onEnable"),l.active=!0}i.device.ios&&!i.params.material&&e&&"focus"===e.type?setTimeout(function(){a()},400):a()},l.disable=function(){function e(){l.input.blur()}l.input.val("").trigger("change"),l.container.removeClass("searchbar-active searchbar-not-empty"),l.cancelButton.length>0&&!l.material&&l.cancelButton.css(f,-l.cancelButton[0].offsetWidth+"px"),(l.searchList.length||l.params.customSearch)&&l.overlay.removeClass("searchbar-overlay-active"),l.active=!1,i.device.ios?setTimeout(function(){e()},400):e(),l.triggerEvent("disableSearch","onDisable")},l.clear=function(e){return!l.query&&e&&o(e.target).hasClass("searchbar-clear")?void l.disable():(l.input.val("").trigger("change").focus(),void l.triggerEvent("clearSearch","onClear"))},l.handleInput=function(){setTimeout(function(){var e=l.input.val().trim();(l.searchList.length>0||l.params.customSearch)&&(l.params.searchIn||l.isVirtualList)&&l.search(e,!0)},0)};var g,v="";return l.search=function(e,a){if(e.trim()!==v){if(v=e.trim(),a||(l.active||l.enable(),l.input.val(e)),l.query=l.value=e,0===e.length?(l.container.removeClass("searchbar-not-empty"),l.searchList.length&&l.container.hasClass("searchbar-active")&&l.overlay.addClass("searchbar-overlay-active")):(l.container.addClass("searchbar-not-empty"),l.searchList.length&&l.container.hasClass("searchbar-active")&&l.overlay.removeClass("searchbar-overlay-active")),l.params.customSearch)return void l.triggerEvent("search","onSearch",{query:e});var n=[];if(l.isVirtualList){if(g=l.searchList[0].f7VirtualList,""===e.trim())return g.resetFilter(),l.notFound.hide(),void l.found.show();if(g.params.searchAll)n=g.params.searchAll(e,g.items)||[];else if(g.params.searchByItem)for(var r=0;r<g.items.length;r++)g.params.searchByItem(e,r,g.params.items[r])&&n.push(r)}else{var i;i=l.params.removeDiacritics?t(e.trim().toLowerCase()).split(" "):e.trim().toLowerCase().split(" "),l.searchList.find("li").removeClass("hidden-by-searchbar").each(function(e,a){a=o(a);var r=[];a.find(l.params.searchIn).each(function(){var e=o(this).text().trim().toLowerCase();l.params.removeDiacritics&&(e=t(e)),r.push(e)}),r=r.join(" ");for(var s=0,p=0;p<i.length;p++)r.indexOf(i[p])>=0&&s++;s===i.length||l.params.ignore&&a.is(l.params.ignore)?n.push(a[0]):a.addClass("hidden-by-searchbar")}),l.params.hideDividers&&l.searchList.find(".item-divider, .list-group-title").each(function(){for(var e=o(this),a=e.nextAll("li"),t=!0,n=0;n<a.length;n++){var r=o(a[n]);if(r.hasClass("list-group-title")||r.hasClass("item-divider"))break;r.hasClass("hidden-by-searchbar")||(t=!1)}var i=l.params.ignore&&e.is(l.params.ignore);t&&!i?e.addClass("hidden-by-searchbar"):e.removeClass("hidden-by-searchbar")}),l.params.hideGroups&&l.searchList.find(".list-group").each(function(){var e=o(this),a=l.params.ignore&&e.is(l.params.ignore),t=e.find("li:not(.hidden-by-searchbar)");0!==t.length||a?e.removeClass("hidden-by-searchbar"):e.addClass("hidden-by-searchbar")})}l.triggerEvent("search","onSearch",{query:e,foundItems:n}),0===n.length?(l.notFound.show(),l.found.hide()):(l.notFound.hide(),l.found.show()),l.isVirtualList&&g.filterItems(n)}},l.attachEvents=function(e){var a=e?"off":"on";l.container[a]("submit",n),l.material||l.cancelButton[a]("click",l.disable),l.overlay[a]("click",l.disable),l.input[a]("focus",l.enable),l.input[a]("change keydown keypress keyup",l.handleInput),l.clearButton[a]("click",l.clear)},l.detachEvents=function(){l.attachEvents(!0)},l.init=function(){l.attachEvents()},l.destroy=function(){l&&(l.detachEvents(),l=null)},l.init(),l.container[0].f7Searchbar=l,l};i.searchbar=function(e,a){return new d(e,a)},i.initSearchbar=function(e){function a(){n&&n.destroy()}e=o(e);var t=e.hasClass("searchbar")?e:e.find(".searchbar");if(0!==t.length&&t.hasClass("searchbar-init")){var n=i.searchbar(t,t.dataset());e.hasClass("page")?e.once("pageBeforeRemove",a):e.hasClass("navbar-inner")&&e.once("navbarBeforeRemove",a)}};var c=function(e,a){function t(e){e.preventDefault()}var n={textarea:null,maxHeight:null};a=a||{};for(var r in n)("undefined"==typeof a[r]||null===a[r])&&(a[r]=n[r]);var i=this;return i.params=a,i.container=o(e),0!==i.container.length?(i.textarea=i.params.textarea?o(i.params.textarea):i.container.find("textarea"),i.pageContainer=i.container.parents(".page").eq(0),i.pageContent=i.pageContainer.find(".page-content"),i.pageContentPadding=parseInt(i.pageContent.css("padding-bottom")),i.initialBarHeight=i.container[0].offsetHeight,i.initialAreaHeight=i.textarea[0].offsetHeight,i.sizeTextarea=function(){i.textarea.css({height:""});var e=i.textarea[0].offsetHeight,a=e-i.textarea[0].clientHeight,t=i.textarea[0].scrollHeight;if(t+a>e){var n=t+a,r=i.initialBarHeight+(n-i.initialAreaHeight),s=i.params.maxHeight||i.container.parents(".view")[0].offsetHeight-88;r>s&&(r=parseInt(s,10),n=r-i.initialBarHeight+i.initialAreaHeight),i.textarea.css("height",n+"px"),i.container.css("height",r+"px");var o=i.pageContent[0].scrollTop===i.pageContent[0].scrollHeight-i.pageContent[0].offsetHeight;i.pageContent.length>0&&(i.pageContent.css("padding-bottom",r+"px"),0===i.pageContent.find(".messages-new-first").length&&o&&i.pageContent.scrollTop(i.pageContent[0].scrollHeight-i.pageContent[0].offsetHeight))}else i.pageContent.length>0&&(i.container.css({height:"",bottom:""}),i.pageContent.css({"padding-bottom":""}))},i.clear=function(){i.textarea.val("").trigger("change")},i.value=function(e){return"undefined"==typeof e?i.textarea.val():void i.textarea.val(e).trigger("change")},i.textareaTimeout=void 0,i.handleTextarea=function(e){clearTimeout(i.textareaTimeout),i.textareaTimeout=setTimeout(function(){i.sizeTextarea()},0)},i.attachEvents=function(e){var a=e?"off":"on";i.container[a]("submit",t),i.textarea[a]("change keydown keypress keyup paste cut",i.handleTextarea)},i.detachEvents=function(){i.attachEvents(!0)},i.init=function(){i.attachEvents()},i.destroy=function(){i.detachEvents(),i=null},i.init(),i.container[0].f7Messagebar=i,i):void 0};i.messagebar=function(e,a){return new c(e,a)},i.initPageMessagebar=function(e){function a(){n.destroy(),e.off("pageBeforeRemove",a)}e=o(e);var t=e.hasClass("messagebar")?e:e.find(".messagebar");if(0!==t.length&&t.hasClass("messagebar-init")){var n=i.messagebar(t,t.dataset());e.hasClass("page")&&e.on("pageBeforeRemove",a)}},i.cache=[],i.removeFromCache=function(e){for(var a=!1,t=0;t<i.cache.length;t++)i.cache[t].url===e&&(a=t);a!==!1&&i.cache.splice(a,1)},i.xhr=!1,i.get=function(e,a,t,n){var r=e;if(i.params.cacheIgnoreGetParameters&&e.indexOf("?")>=0&&(r=e.split("?")[0]),i.params.cache&&!t&&e.indexOf("nocache")<0&&i.params.cacheIgnore.indexOf(r)<0)for(var s=0;s<i.cache.length;s++)if(i.cache[s].url===r&&(new Date).getTime()-i.cache[s].time<i.params.cacheDuration)return n(i.cache[s].content),!1;return i.xhr=o.ajax({url:e,method:"GET",beforeSend:i.params.onAjaxStart,complete:function(e){e.status>=200&&e.status<300||0===e.status?(i.params.cache&&(i.removeFromCache(r),i.cache.push({url:r,time:(new Date).getTime(),content:e.responseText})),n(e.responseText,!1)):n(e.responseText,!0),i.params.onAjaxComplete&&i.params.onAjaxComplete(e)},error:function(e){n(e.responseText,!0),i.params.onAjaxError&&i.params.onAjaxError(e)}}),a&&(a.xhr=i.xhr),i.xhr},i.pageCallbacks={},i.onPage=function(e,a,t){if(a&&a.split(" ").length>1){for(var n=a.split(" "),r=[],s=0;s<n.length;s++)r.push(i.onPage(e,n[s],t));return r.remove=function(){for(var e=0;e<r.length;e++)r[e].remove()},r.trigger=function(){for(var e=0;e<r.length;e++)r[e].trigger()},r}var o=i.pageCallbacks[e][a];
return o||(o=i.pageCallbacks[e][a]=[]),i.pageCallbacks[e][a].push(t),{remove:function(){for(var e,a=0;a<o.length;a++)o[a].toString()===t.toString()&&(e=a);"undefined"!=typeof e&&o.splice(e,1)},trigger:t}};for(var u="beforeInit init reinit beforeAnimation afterAnimation back afterBack beforeRemove".split(" "),m=0;m<u.length;m++)i.pageCallbacks[u[m]]={},t(u[m]);i.triggerPageCallbacks=function(e,a,t){var n=i.pageCallbacks[e]["*"];if(n)for(var r=0;r<n.length;r++)n[r](t);var s=i.pageCallbacks[e][a];if(s&&0!==s.length)for(var o=0;o<s.length;o++)s[o](t)},i.pageInitCallback=function(e,a){var t=a.pageContainer;if(!t.f7PageInitialized||!e||e.params.domCache){var n=a.query;n||(n=a.url&&a.url.indexOf("?")>0?o.parseUrlQuery(a.url||""):t.f7PageData&&t.f7PageData.query?t.f7PageData.query:{});var r={container:t,url:a.url,query:n,name:o(t).attr("data-page"),view:e,from:a.position,context:a.context,navbarInnerContainer:a.navbarInnerContainer,fromPage:a.fromPage};if(a.fromPage&&!a.fromPage.navbarInnerContainer&&a.oldNavbarInnerContainer&&(a.fromPage.navbarInnerContainer=a.oldNavbarInnerContainer),t.f7PageInitialized&&(e&&e.params.domCache||!e&&o(t).parents(".popup, .popover, .login-screen, .modal, .actions-modal, .picker-modal").length>0))return i.reinitPage(t),i.pluginHook("pageReinit",r),i.params.onPageReinit&&i.params.onPageReinit(i,r),i.triggerPageCallbacks("reinit",r.name,r),void o(r.container).trigger("pageReinit",{page:r});t.f7PageInitialized=!0,t.f7PageData=r,!e||a.preloadOnly||a.reloadPrevious||(o(e.container).attr("data-page",r.name),e.activePage=r),i.pluginHook("pageBeforeInit",r),i.params.onPageBeforeInit&&i.params.onPageBeforeInit(i,r),i.triggerPageCallbacks("beforeInit",r.name,r),o(r.container).trigger("pageBeforeInit",{page:r}),i.initPage(t),i.pluginHook("pageInit",r),i.params.onPageInit&&i.params.onPageInit(i,r),i.triggerPageCallbacks("init",r.name,r),o(r.container).trigger("pageInit",{page:r})}},i.pageRemoveCallback=function(e,a,t){var n;a.f7PageData&&(n=a.f7PageData.context);var r={container:a,name:o(a).attr("data-page"),view:e,url:a.f7PageData&&a.f7PageData.url,query:a.f7PageData&&a.f7PageData.query,navbarInnerContainer:a.f7PageData&&a.f7PageData.navbarInnerContainer,from:t,context:n};i.pluginHook("pageBeforeRemove",r),i.params.onPageBeforeRemove&&i.params.onPageBeforeRemove(i,r),i.triggerPageCallbacks("beforeRemove",r.name,r),o(r.container).trigger("pageBeforeRemove",{page:r})},i.pageBackCallback=function(e,a,t){var n,r=t.pageContainer;r.f7PageData&&(n=r.f7PageData.context);var s={container:r,name:o(r).attr("data-page"),url:r.f7PageData&&r.f7PageData.url,query:r.f7PageData&&r.f7PageData.query,view:a,from:t.position,context:n,navbarInnerContainer:r.f7PageData&&r.f7PageData.navbarInnerContainer,swipeBack:t.swipeBack};"after"===e&&(i.pluginHook("pageAfterBack",s),i.params.onPageAfterBack&&i.params.onPageAfterBack(i,s),i.triggerPageCallbacks("afterBack",s.name,s),o(r).trigger("pageAfterBack",{page:s})),"before"===e&&(i.pluginHook("pageBack",s),i.params.onPageBack&&i.params.onPageBack(i,s),i.triggerPageCallbacks("back",s.name,s),o(s.container).trigger("pageBack",{page:s}))},i.pageAnimCallback=function(e,a,t){var n,r=t.pageContainer;r.f7PageData&&(n=r.f7PageData.context);var s=t.query;s||(s=t.url&&t.url.indexOf("?")>0?o.parseUrlQuery(t.url||""):r.f7PageData&&r.f7PageData.query?r.f7PageData.query:{});var l={container:r,url:t.url,query:s,name:o(r).attr("data-page"),view:a,from:t.position,context:n,swipeBack:t.swipeBack,navbarInnerContainer:r.f7PageData&&r.f7PageData.navbarInnerContainer,fromPage:t.fromPage},p=t.oldPage,d=t.newPage;if(r.f7PageData=l,"after"===e&&(i.pluginHook("pageAfterAnimation",l),i.params.onPageAfterAnimation&&i.params.onPageAfterAnimation(i,l),i.triggerPageCallbacks("afterAnimation",l.name,l),o(l.container).trigger("pageAfterAnimation",{page:l})),"before"===e){o(a.container).attr("data-page",l.name),a&&(a.activePage=l),d.hasClass("no-navbar")&&!p.hasClass("no-navbar")&&a.hideNavbar(),d.hasClass("no-navbar")||!p.hasClass("no-navbar")&&!p.hasClass("no-navbar-by-scroll")||a.showNavbar(),d.hasClass("no-toolbar")&&!p.hasClass("no-toolbar")&&a.hideToolbar(),d.hasClass("no-toolbar")||!p.hasClass("no-toolbar")&&!p.hasClass("no-toolbar-by-scroll")||a.showToolbar();var c;d.hasClass("no-tabbar")&&!p.hasClass("no-tabbar")&&(c=o(a.container).find(".tabbar"),0===c.length&&(c=o(a.container).parents("."+i.params.viewsClass).find(".tabbar")),i.hideToolbar(c)),d.hasClass("no-tabbar")||!p.hasClass("no-tabbar")&&!p.hasClass("no-tabbar-by-scroll")||(c=o(a.container).find(".tabbar"),0===c.length&&(c=o(a.container).parents("."+i.params.viewsClass).find(".tabbar")),i.showToolbar(c)),p.removeClass("no-navbar-by-scroll no-toolbar-by-scroll"),i.pluginHook("pageBeforeAnimation",l),i.params.onPageBeforeAnimation&&i.params.onPageBeforeAnimation(i,l),i.triggerPageCallbacks("beforeAnimation",l.name,l),o(l.container).trigger("pageBeforeAnimation",{page:l})}},i.initPage=function(e){e=o(e),0!==e.length&&(i.sizeNavbars&&i.sizeNavbars(e.parents("."+i.params.viewClass)[0]),i.initPageMessages&&i.initPageMessages(e),i.initFormsStorage&&i.initFormsStorage(e),i.initSmartSelects&&i.initSmartSelects(e),i.initPageSwiper&&i.initPageSwiper(e),i.initPullToRefresh&&i.initPullToRefresh(e),i.initPageInfiniteScroll&&i.initPageInfiniteScroll(e),i.initSearchbar&&i.initSearchbar(e),i.initPageMessagebar&&i.initPageMessagebar(e),i.initPageScrollToolbars&&i.initPageScrollToolbars(e),i.initImagesLazyLoad&&i.initImagesLazyLoad(e),i.initPageProgressbar&&i.initPageProgressbar(e),i.initPageResizableTextarea&&i.initPageResizableTextarea(e),i.params.material&&i.initPageMaterialPreloader&&i.initPageMaterialPreloader(e),i.params.material&&i.initPageMaterialInputs&&i.initPageMaterialInputs(e),i.params.material&&i.initPageMaterialTabbar&&i.initPageMaterialTabbar(e))},i.reinitPage=function(e){e=o(e),0!==e.length&&(i.sizeNavbars&&i.sizeNavbars(e.parents("."+i.params.viewClass)[0]),i.reinitPageSwiper&&i.reinitPageSwiper(e),i.reinitLazyLoad&&i.reinitLazyLoad(e))},i.initPageWithCallback=function(e){e=o(e);var a=e.parents("."+i.params.viewClass);if(0!==a.length){var t=a[0].f7View||void 0,n=t&&t.url?t.url:void 0;a&&e.attr("data-page")&&a.attr("data-page",e.attr("data-page")),i.pageInitCallback(t,{pageContainer:e[0],url:n,position:"center"})}},i.router={temporaryDom:document.createElement("div"),findElement:function(e,a,t,n){a=o(a),n&&(e+=":not(.cached)");var r=a.find(e);return r.length>1&&("string"==typeof t.selector&&(r=a.find(t.selector+" "+e)),r.length>1&&(r=a.find("."+i.params.viewMainClass+" "+e))),1===r.length?r:(n||(r=i.router.findElement(e,a,t,!0)),r&&1===r.length?r:void 0)},animatePages:function(e,a,t,n){var r="page-on-center page-on-right page-on-left";"to-left"===t&&(e.removeClass(r).addClass("page-from-center-to-left"),a.removeClass(r).addClass("page-from-right-to-center")),"to-right"===t&&(e.removeClass(r).addClass("page-from-left-to-center"),a.removeClass(r).addClass("page-from-center-to-right"))},prepareNavbar:function(e,a,t){o(e).find(".sliding").each(function(){var e=o(this),a="right"===t?this.f7NavbarRightOffset:this.f7NavbarLeftOffset;i.params.animateNavBackIcon&&e.hasClass("left")&&e.find(".back .icon").length>0&&e.find(".back .icon").transform("translate3d("+-a+"px,0,0)"),e.transform("translate3d("+a+"px,0,0)")})},animateNavbars:function(e,a,t,n){var r="navbar-on-right navbar-on-center navbar-on-left";"to-left"===t&&(a.removeClass(r).addClass("navbar-from-right-to-center"),a.find(".sliding").each(function(){var e=o(this);e.transform("translate3d(0px,0,0)"),i.params.animateNavBackIcon&&e.hasClass("left")&&e.find(".back .icon").length>0&&e.find(".back .icon").transform("translate3d(0px,0,0)")}),e.removeClass(r).addClass("navbar-from-center-to-left"),e.find(".sliding").each(function(){var e,t=o(this);i.params.animateNavBackIcon&&(t.hasClass("center")&&a.find(".sliding.left .back .icon").length>0&&(e=a.find(".sliding.left .back span"),e.length>0&&(this.f7NavbarLeftOffset+=e[0].offsetLeft)),t.hasClass("left")&&t.find(".back .icon").length>0&&t.find(".back .icon").transform("translate3d("+-this.f7NavbarLeftOffset+"px,0,0)")),t.transform("translate3d("+this.f7NavbarLeftOffset+"px,0,0)")})),"to-right"===t&&(e.removeClass(r).addClass("navbar-from-left-to-center"),e.find(".sliding").each(function(){var e=o(this);e.transform("translate3d(0px,0,0)"),i.params.animateNavBackIcon&&e.hasClass("left")&&e.find(".back .icon").length>0&&e.find(".back .icon").transform("translate3d(0px,0,0)")}),a.removeClass(r).addClass("navbar-from-center-to-right"),a.find(".sliding").each(function(){var e=o(this);i.params.animateNavBackIcon&&e.hasClass("left")&&e.find(".back .icon").length>0&&e.find(".back .icon").transform("translate3d("+-this.f7NavbarRightOffset+"px,0,0)"),e.transform("translate3d("+this.f7NavbarRightOffset+"px,0,0)")}))},preprocess:function(e,a,t,n){i.pluginHook("routerPreprocess",e,a,t,n),a=i.pluginProcess("preprocess",a),e&&e.params&&e.params.preprocess?(a=e.params.preprocess(a,t,n),"undefined"!=typeof a&&n(a)):i.params.preprocess?(a=i.params.preprocess(a,t,n),"undefined"!=typeof a&&n(a)):n(a)},preroute:function(e,a){return i.pluginHook("routerPreroute",e,a),i.params.preroute&&i.params.preroute(e,a)===!1||e&&e.params.preroute&&e.params.preroute(e,a)===!1?!0:!1},template7Render:function(e,a){var t,n,r=a.url,s=a.content,p=a.content,d=a.context,c=a.contextName,u=a.template;a.pageName;if("string"==typeof s?r?i.template7Cache[r]&&!a.ignoreCache?n=l.cache[r]:(n=l.compile(s),l.cache[r]=n):n=l.compile(s):u&&(n=u),d)t=d;else{if(c)if(c.indexOf(".")>=0){for(var m=c.split("."),f=l.data[m[0]],h=1;h<m.length;h++)m[h]&&(f=f[m[h]]);t=f}else t=l.data[c];if(!t&&r&&(t=l.data["url:"+r]),!t&&"string"==typeof s&&!u){var g=s.match(/(data-page=["'][^"^']*["'])/);if(g){var v=g[0].split("data-page=")[1].replace(/['"]/g,"");v&&(t=l.data["page:"+v])}}if(!t&&u&&l.templates)for(var b in l.templates)l.templates[b]===u&&(t=l.data[b]);t||(t={})}if(n&&t){if("function"==typeof t&&(t=t()),r){var w=o.parseUrlQuery(r);t.url_query={};for(var C in w)t.url_query[C]=w[C]}p=n(t)}return{content:p,context:t}}},i.router._load=function(e,a){function t(){e.allowPageChange=!0,n.removeClass("page-from-right-to-center page-on-right page-on-left").addClass("page-on-center"),r.removeClass("page-from-center-to-left page-on-center page-on-right").addClass("page-on-left"),u&&(d.removeClass("navbar-from-right-to-center navbar-on-left navbar-on-right").addClass("navbar-on-center"),p.removeClass("navbar-from-center-to-left navbar-on-center navbar-on-right").addClass("navbar-on-left")),i.pageAnimCallback("after",e,{pageContainer:n[0],url:f,position:"right",oldPage:r,newPage:n,query:a.query,fromPage:r&&r.length&&r[0].f7PageData}),i.params.pushState&&e.main&&i.pushStateClearQueue(),e.params.swipeBackPage||e.params.preloadPreviousPage||(e.params.domCache?(r.addClass("cached"),u&&p.addClass("cached")):(0!==f.indexOf("#")||0!==n.attr("data-page").indexOf("smart-select-"))&&(i.pageRemoveCallback(e,r[0],"left"),u&&i.navbarRemoveCallback(e,r[0],c[0],p[0]),r.remove(),u&&p.remove())),e.params.uniqueHistory&&O&&e.refreshPreviousPage()}a=a||{};var n,r,s,l,p,d,c,u,m,f=a.url,h=a.content,g={content:a.content},v=a.template,b=a.pageName,w=o(e.container),C=o(e.pagesContainer),y=a.animatePages,x="undefined"==typeof f&&h||v,T=a.pushState;if("undefined"==typeof y&&(y=e.params.animatePages),i.pluginHook("routerLoad",e,a),(i.params.template7Pages&&"string"==typeof h||v)&&(g=i.router.template7Render(e,a),g.content&&!h&&(h=g.content)),i.router.temporaryDom.innerHTML="",!b)if("string"==typeof h||f&&"string"==typeof h)i.router.temporaryDom.innerHTML=g.content;else if("length"in h&&h.length>1)for(var k=0;k<h.length;k++)o(i.router.temporaryDom).append(h[k]);else o(i.router.temporaryDom).append(h);if(m=a.reload&&(a.reloadPrevious?"left":"center"),n=b?C.find('.page[data-page="'+b+'"]'):i.router.findElement(".page",i.router.temporaryDom,e),!n||0===n.length||b&&e.activePage&&e.activePage.name===b)return void(e.allowPageChange=!0);if(n.addClass(a.reload?"page-on-"+m:"page-on-right"),s=C.children(".page:not(.cached)"),a.reload&&a.reloadPrevious&&1===s.length)return void(e.allowPageChange=!0);if(a.reload)r=s.eq(s.length-1);else{if(s.length>1){for(l=0;l<s.length-2;l++)e.params.domCache?o(s[l]).addClass("cached"):(i.pageRemoveCallback(e,s[l],"left"),o(s[l]).remove());e.params.domCache?o(s[l]).addClass("cached"):(i.pageRemoveCallback(e,s[l],"left"),o(s[l]).remove())}r=C.children(".page:not(.cached)")}if(e.params.domCache&&n.removeClass("cached"),e.params.dynamicNavbar)if(u=!0,d=b?w.find('.navbar-inner[data-page="'+b+'"]'):i.router.findElement(".navbar-inner",i.router.temporaryDom,e),d&&0!==d.length||(u=!1),c=w.find(".navbar"),a.reload)p=c.find(".navbar-inner:not(.cached):last-child");else if(p=c.find(".navbar-inner:not(.cached)"),p.length>0){for(l=0;l<p.length-1;l++)e.params.domCache?o(p[l]).addClass("cached"):(i.navbarRemoveCallback(e,s[l],c[0],p[l]),o(p[l]).remove());d||1!==p.length||(e.params.domCache?o(p[0]).addClass("cached"):(i.navbarRemoveCallback(e,s[0],c[0],p[0]),o(p[0]).remove())),p=c.find(".navbar-inner:not(.cached)")}if(u&&(d.addClass(a.reload?"navbar-on-"+m:"navbar-on-right"),e.params.domCache&&d.removeClass("cached"),n[0].f7RelatedNavbar=d[0],d[0].f7RelatedPage=n[0]),!f){var P=b||n.attr("data-page");f=x?"#"+i.params.dynamicPageUrl.replace(/{{name}}/g,P).replace(/{{index}}/g,e.history.length-(a.reload?1:0)):"#"+P,e.params.domCache||(e.contentCache[f]=h),e.params.domCache&&b&&(e.pagesCache[f]=b)}if(i.params.pushState&&!a.reloadPrevious&&e.main){"undefined"==typeof T&&(T=!0);var S=i.params.pushStateRoot||"",M=a.reload?"replaceState":"pushState";T&&(x||b?x&&h?history[M]({content:"string"==typeof h?h:"",url:f,viewIndex:i.views.indexOf(e)},"",S+i.params.pushStateSeparator+f):b&&history[M]({pageName:b,url:f,viewIndex:i.views.indexOf(e)},"",S+i.params.pushStateSeparator+f):history[M]({url:f,viewIndex:i.views.indexOf(e)},"",S+i.params.pushStateSeparator+f))}if(e.url=f,a.reload){var I=e.history[e.history.length-(a.reloadPrevious?2:1)];I&&0===I.indexOf("#")&&I in e.contentCache&&I!==f&&-1===e.history.indexOf(I)&&(e.contentCache[I]=null,delete e.contentCache[I]),e.history[e.history.length-(a.reloadPrevious?2:1)]=f}else e.history.push(f);var O=!1;if(e.params.uniqueHistory){var E=e.history,D=f;if(e.params.uniqueHistoryIgnoreGetParameters)for(E=[],D=f.split("?")[0],l=0;l<e.history.length;l++)E.push(e.history[l].split("?")[0]);E.indexOf(D)!==E.lastIndexOf(D)&&(e.history=e.history.slice(0,E.indexOf(D)),e.history.push(f),O=!0)}if(a.reloadPrevious?(r=r.prev(".page"),n.insertBefore(r),u&&(p=p.prev(".navbar-inner"),d.insertAfter(p))):(C.append(n[0]),u&&c.append(d[0])),a.reload&&(e.params.domCache&&e.initialPages.indexOf(r[0])>=0?(r.addClass("cached"),u&&p.addClass("cached")):(i.pageRemoveCallback(e,r[0],m),u&&i.navbarRemoveCallback(e,r[0],c[0],p[0]),r.remove(),u&&p.remove())),i.pageInitCallback(e,{pageContainer:n[0],url:f,position:a.reload?m:"right",navbarInnerContainer:u?d&&d[0]:void 0,oldNavbarInnerContainer:u?p&&p[0]:void 0,context:g.context,query:a.query,fromPage:r&&r.length&&r[0].f7PageData,reload:a.reload,reloadPrevious:a.reloadPrevious}),u&&i.navbarInitCallback(e,n[0],c[0],d[0],f,a.reload?m:"right"),a.reload)return e.allowPageChange=!0,void(O&&e.refreshPreviousPage());u&&y&&i.router.prepareNavbar(d,p,"right");n[0].clientLeft;i.pageAnimCallback("before",e,{pageContainer:n[0],url:f,position:"right",oldPage:r,newPage:n,query:a.query,fromPage:r&&r.length&&r[0].f7PageData}),y?(i.params.material&&i.params.materialPageLoadDelay?setTimeout(function(){i.router.animatePages(r,n,"to-left",e)},i.params.materialPageLoadDelay):i.router.animatePages(r,n,"to-left",e),u&&setTimeout(function(){i.router.animateNavbars(p,d,"to-left",e)},0),n.animationEnd(function(e){t()})):(u&&d.find(".sliding, .sliding .back .icon").transform(""),t())},i.router.load=function(e,a){function t(t){i.router.preprocess(e,t,n,function(t){a.content=t,i.router._load(e,a)})}if(i.router.preroute(e,a))return!1;a=a||{};var n=a.url,r=a.content,s=a.pageName;s&&s.indexOf("?")>0&&(a.query=o.parseUrlQuery(s),a.pageName=s=s.split("?")[0]);var l=a.template;return e.params.reloadPages===!0&&(a.reload=!0),e.allowPageChange&&(!n||e.url!==n||a.reload||e.params.allowDuplicateUrls)?(e.allowPageChange=!1,i.xhr&&e.xhr&&e.xhr===i.xhr&&(i.xhr.abort(),i.xhr=!1),r||s?void t(r):l?void i.router._load(e,a):a.url&&"#"!==a.url?void i.get(a.url,e,a.ignoreCache,function(a,n){return n?void(e.allowPageChange=!0):void t(a)}):void(e.allowPageChange=!0)):!1},i.router._back=function(e,a){function t(){i.pageBackCallback("after",e,{pageContainer:l[0],url:h,position:"center",oldPage:l,newPage:p}),i.pageAnimCallback("after",e,{pageContainer:p[0],url:h,position:"left",oldPage:l,newPage:p,query:a.query,fromPage:l&&l.length&&l[0].f7PageData}),i.router.afterBack(e,l[0],p[0])}function n(){i.pageBackCallback("before",e,{pageContainer:l[0],url:h,position:"center",oldPage:l,newPage:p}),i.pageAnimCallback("before",e,{pageContainer:p[0],url:h,position:"left",oldPage:l,newPage:p,query:a.query,fromPage:l&&l.length&&l[0].f7PageData}),w?(i.router.animatePages(p,l,"to-right",e),f&&setTimeout(function(){i.router.animateNavbars(c,d,"to-right",e)},0),p.animationEnd(function(){t()})):(f&&c.find(".sliding, .sliding .back .icon").transform(""),t())}function r(){if(i.router.temporaryDom.innerHTML="","string"==typeof g||h&&"string"==typeof g)i.router.temporaryDom.innerHTML=v.content;else if("length"in g&&g.length>1)for(var a=0;a<g.length;a++)o(i.router.temporaryDom).append(g[a]);else o(i.router.temporaryDom).append(g);p=i.router.findElement(".page",i.router.temporaryDom,e),e.params.dynamicNavbar&&(c=i.router.findElement(".navbar-inner",i.router.temporaryDom,e))}function s(){if(!p||0===p.length)return void(e.allowPageChange=!0);if(e.params.dynamicNavbar&&"undefined"==typeof f&&(f=c&&0!==c.length?!0:!1),p.addClass("page-on-left").removeClass("cached"),f&&(u=k.find(".navbar"),m=k.find(".navbar-inner:not(.cached)"),c.addClass("navbar-on-left").removeClass("cached")),x){var t,r;if(t=o(S[S.length-2]),f&&(r=o(t[0]&&t[0].f7RelatedNavbar||m[m.length-2])),e.params.domCache&&e.initialPages.indexOf(t[0])>=0)t.length&&t[0]!==p[0]&&t.addClass("cached"),f&&r.length&&r[0]!==c[0]&&r.addClass("cached");else{var s=f&&r.length;t.length?(i.pageRemoveCallback(e,t[0],"right"),s&&i.navbarRemoveCallback(e,t[0],u[0],r[0]),t.remove(),s&&r.remove()):s&&(i.navbarRemoveCallback(e,t[0],u[0],r[0]),r.remove())}S=P.children(".page:not(.cached)"),f&&(m=k.find(".navbar-inner:not(.cached)")),e.history.indexOf(h)>=0?e.history=e.history.slice(0,e.history.indexOf(h)+2):e.history[[e.history.length-2]]?e.history[e.history.length-2]=h:e.history.unshift(h)}if(l=o(S[S.length-1]),e.params.domCache&&l[0]===p[0]&&(l=P.children(".page.page-on-center"),0===l.length&&e.activePage&&(l=o(e.activePage.container))),f&&!d&&(d=o(m[m.length-1]),e.params.domCache&&(d[0]===c[0]&&(d=u.children(".navbar-inner.navbar-on-center:not(.cached)")),0===d.length&&(d=u.children('.navbar-inner[data-page="'+l.attr("data-page")+'"]'))),(0===d.length||c[0]===d[0])&&(f=!1)),f&&(M&&c.insertBefore(d),c[0].f7RelatedPage=p[0],p[0].f7RelatedNavbar=c[0]),M&&p.insertBefore(l),i.pageInitCallback(e,{pageContainer:p[0],url:h,position:"left",navbarInnerContainer:f?c[0]:void 0,oldNavbarInnerContainer:f?d&&d[0]:void 0,context:v.context,query:a.query,fromPage:l&&l.length&&l[0].f7PageData,preloadOnly:C}),f&&i.navbarInitCallback(e,p[0],u[0],c[0],h,"right"),f&&c.hasClass("navbar-on-left")&&w&&i.router.prepareNavbar(c,d,"left"),C)return void(e.allowPageChange=!0);e.url=h;p[0].clientLeft;n(),i.params.pushState&&e.main&&("undefined"==typeof y&&(y=!0),!C&&history.state&&y&&history.back())}a=a||{};var l,p,d,c,u,m,f,h=a.url,g=a.content,v={content:a.content},b=a.template,w=a.animatePages,C=a.preloadOnly,y=a.pushState,x=(a.ignoreCache,a.force),T=a.pageName,k=o(e.container),P=o(e.pagesContainer),S=P.children(".page:not(.cached)"),M=!0;return"undefined"==typeof w&&(w=e.params.animatePages),i.pluginHook("routerBack",e,a),(i.params.template7Pages&&"string"==typeof g||b)&&(v=i.router.template7Render(e,a),v.content&&!g&&(g=v.content)),S.length>1&&!x?C?void(e.allowPageChange=!0):(e.url=e.history[e.history.length-2],h=e.url,p=o(S[S.length-2]),l=o(S[S.length-1]),e.params.dynamicNavbar&&(f=!0,m=k.find(".navbar-inner:not(.cached)"),c=o(m[0]),d=o(m[1]),(0===c.length||0===d.length||d[0]===c[0])&&(f=!1)),M=!1,void s()):x?h&&h===e.url||T&&e.activePage&&e.activePage.name===T?void(e.allowPageChange=!0):g?(r(),void s()):T&&e.params.domCache?(T&&(h="#"+T),p=o(k).find('.page[data-page="'+T+'"]'),p[0].f7PageData&&p[0].f7PageData.url&&(h=p[0].f7PageData.url),e.params.dynamicNavbar&&(c=o(k).find('.navbar-inner[data-page="'+T+'"]'),0===c.length&&p[0].f7RelatedNavbar&&(c=o(p[0].f7RelatedNavbar)),0===c.length&&p[0].f7PageData&&(c=o(p[0].f7PageData.navbarInnerContainer))),void s()):void(e.allowPageChange=!0):(C||(e.url=e.history[e.history.length-2],h=e.url),g?(r(),void s()):T?(p=o(k).find('.page[data-page="'+T+'"]'),e.params.dynamicNavbar&&(c=o(k).find('.navbar-inner[data-page="'+T+'"]'),0===c.length&&p[0].f7RelatedNavbar&&(c=o(p[0].f7RelatedNavbar)),0===c.length&&p[0].f7PageData&&(c=o(p[0].f7PageData.navbarInnerContainer))),void s()):void(e.allowPageChange=!0))},i.router.back=function(e,a){function t(t){i.router.preprocess(e,t,n,function(t){a.content=t,i.router._back(e,a)})}if(i.router.preroute(e,a))return!1;a=a||{};var n=a.url,r=a.content,s=a.pageName;s&&s.indexOf("?")>0&&(a.query=o.parseUrlQuery(s),a.pageName=s=s.split("?")[0]);var l=a.force;if(!e.allowPageChange)return!1;e.allowPageChange=!1,i.xhr&&e.xhr&&e.xhr===i.xhr&&(i.xhr.abort(),i.xhr=!1);var p=o(e.pagesContainer).find(".page:not(.cached)");if(p.length>1&&!l)return void i.router._back(e,a);if(l){if(!n&&r)return void t(r);if(!n&&s)return s&&(n="#"+s),void t();if(n)return void i.get(a.url,e,a.ignoreCache,function(a,n){return n?void(e.allowPageChange=!0):void t(a)})}else{if(n=a.url=e.history[e.history.length-2],!n)return void(e.allowPageChange=!0);if(0===n.indexOf("#")&&e.contentCache[n])return void t(e.contentCache[n]);if(0===n.indexOf("#")&&e.params.domCache)return s||(a.pageName=n.split("#")[1]),void t();if(0!==n.indexOf("#"))return void i.get(a.url,e,a.ignoreCache,function(a,n){return n?void(e.allowPageChange=!0):void t(a)})}e.allowPageChange=!0},i.router.afterBack=function(e,a,t){a=o(a),t=o(t),e.params.domCache&&e.initialPages.indexOf(a[0])>=0?a.removeClass("page-from-center-to-right").addClass("cached"):(i.pageRemoveCallback(e,a[0],"right"),a.remove()),t.removeClass("page-from-left-to-center page-on-left").addClass("page-on-center"),e.allowPageChange=!0;var n,r=e.history.pop();if(e.params.dynamicNavbar){var s=o(e.container).find(".navbar-inner:not(.cached)"),l=o(a[0].f7RelatedNavbar||s[1]);e.params.domCache&&e.initialNavbars.indexOf(l[0])>=0?l.removeClass("navbar-from-center-to-right").addClass("cached"):(i.navbarRemoveCallback(e,a[0],void 0,l[0]),l.remove()),n=o(s[0]).removeClass("navbar-on-left navbar-from-left-to-center").addClass("navbar-on-center")}if(e.params.domCache&&o(e.container).find(".page.cached").each(function(){var a=o(this),t=(a.index(),a[0].f7PageData&&a[0].f7PageData.url);t&&e.history.indexOf(t)<0&&e.initialPages.indexOf(this)<0&&(i.pageRemoveCallback(e,a[0],"right"),a[0].f7RelatedNavbar&&e.params.dynamicNavbar&&i.navbarRemoveCallback(e,a[0],void 0,a[0].f7RelatedNavbar),a.remove(),a[0].f7RelatedNavbar&&e.params.dynamicNavbar&&o(a[0].f7RelatedNavbar).remove())}),!e.params.domCache&&r&&r.indexOf("#")>-1&&r in e.contentCache&&-1===e.history.indexOf(r)&&(e.contentCache[r]=null,delete e.contentCache[r]),i.params.pushState&&e.main&&i.pushStateClearQueue(),e.params.preloadPreviousPage)if(e.params.domCache&&e.history.length>1){var p,d,c=e.history[e.history.length-2];c&&e.pagesCache[c]?(p=o(e.container).find('.page[data-page="'+e.pagesCache[c]+'"]'),p.next(".page")[0]!==t[0]&&p.insertBefore(t),n&&(d=o(e.container).find('.navbar-inner[data-page="'+e.pagesCache[c]+'"]'),d&&0!==d.length||(d=n.prev(".navbar-inner.cached")),d.next(".navbar-inner")[0]!==n[0]&&d.insertBefore(n))):(p=t.prev(".page.cached"),n&&(d=n.prev(".navbar-inner.cached"))),p&&p.length>0&&p.removeClass("cached page-on-right page-on-center").addClass("page-on-left"),d&&d.length>0&&d.removeClass("cached navbar-on-right navbar-on-center").addClass("navbar-on-left")}else i.router.back(e,{preloadOnly:!0})};var f=document.createElement("div");i.modalStack=[],i.modalStackClearQueue=function(){i.modalStack.length&&i.modalStack.shift()()},i.modal=function(e){e=e||{};var a="";if(i.params.modalTemplate)i._compiledTemplates.modal||(i._compiledTemplates.modal=l.compile(i.params.modalTemplate)),a=i._compiledTemplates.modal(e);else{var t="";if(e.buttons&&e.buttons.length>0)for(var n=0;n<e.buttons.length;n++)t+='<span class="modal-button'+(e.buttons[n].bold?" modal-button-bold":"")+'">'+e.buttons[n].text+"</span>";var r=e.title?'<div class="modal-title">'+e.title+"</div>":"",s=e.text?'<div class="modal-text">'+e.text+"</div>":"",p=e.afterText?e.afterText:"",d=e.buttons&&0!==e.buttons.length?"":"modal-no-buttons",c=e.verticalButtons?"modal-buttons-vertical":"",u=e.buttons&&e.buttons.length>0?'<div class="modal-buttons modal-buttons-'+e.buttons.length+" "+c+'">'+t+"</div>":"";a='<div class="modal '+d+" "+(e.cssClass||"")+'"><div class="modal-inner">'+(r+s+p)+"</div>"+u+"</div>"}f.innerHTML=a;var m=o(f).children();return o("body").append(m[0]),m.find(".modal-button").each(function(a,t){o(t).on("click",function(t){e.buttons[a].close!==!1&&i.closeModal(m),e.buttons[a].onClick&&e.buttons[a].onClick(m,t),e.onClick&&e.onClick(m,a)})}),i.openModal(m),m[0]},i.alert=function(e,a,t){return"function"==typeof a&&(t=arguments[1],a=void 0),i.modal({text:e||"",title:"undefined"==typeof a?i.params.modalTitle:a,buttons:[{text:i.params.modalButtonOk,bold:!0,onClick:t}]})},i.confirm=function(e,a,t,n){return"function"==typeof a&&(n=arguments[2],t=arguments[1],a=void 0),i.modal({text:e||"",title:"undefined"==typeof a?i.params.modalTitle:a,buttons:[{text:i.params.modalButtonCancel,onClick:n},{text:i.params.modalButtonOk,bold:!0,onClick:t}]})},i.prompt=function(e,a,t,n){return"function"==typeof a&&(n=arguments[2],t=arguments[1],a=void 0),i.modal({text:e||"",title:"undefined"==typeof a?i.params.modalTitle:a,afterText:'<div class="input-field"><input type="text" class="modal-text-input"></div>',buttons:[{text:i.params.modalButtonCancel},{text:i.params.modalButtonOk,bold:!0}],onClick:function(e,a){0===a&&n&&n(o(e).find(".modal-text-input").val()),1===a&&t&&t(o(e).find(".modal-text-input").val())}})},i.modalLogin=function(e,a,t,n){return"function"==typeof a&&(n=arguments[2],t=arguments[1],a=void 0),i.modal({text:e||"",title:"undefined"==typeof a?i.params.modalTitle:a,afterText:'<div class="input-field modal-input-double"><input type="text" name="modal-username" placeholder="'+i.params.modalUsernamePlaceholder+'" class="modal-text-input"></div><div class="input-field modal-input-double"><input type="password" name="modal-password" placeholder="'+i.params.modalPasswordPlaceholder+'" class="modal-text-input"></div>',buttons:[{text:i.params.modalButtonCancel},{text:i.params.modalButtonOk,bold:!0}],onClick:function(e,a){var r=o(e).find('.modal-text-input[name="modal-username"]').val(),i=o(e).find('.modal-text-input[name="modal-password"]').val();0===a&&n&&n(r,i),1===a&&t&&t(r,i)}})},i.modalPassword=function(e,a,t,n){return"function"==typeof a&&(n=arguments[2],t=arguments[1],a=void 0),i.modal({text:e||"",title:"undefined"==typeof a?i.params.modalTitle:a,afterText:'<div class="input-field"><input type="password" name="modal-password" placeholder="'+i.params.modalPasswordPlaceholder+'" class="modal-text-input"></div>',buttons:[{text:i.params.modalButtonCancel},{text:i.params.modalButtonOk,bold:!0}],onClick:function(e,a){var r=o(e).find('.modal-text-input[name="modal-password"]').val();0===a&&n&&n(r),1===a&&t&&t(r)}})},i.showPreloader=function(e){return i.modal({title:e||i.params.modalPreloaderTitle,text:'<div class="preloader">'+(i.params.material?i.params.materialPreloaderHtml:"")+"</div>",cssClass:"modal-preloader"})},i.hidePreloader=function(){i.closeModal(".modal.modal-in")},i.showIndicator=function(){o("body").append('<div class="preloader-indicator-overlay"></div><div class="preloader-indicator-modal"><span class="preloader preloader-white">'+(i.params.material?i.params.materialPreloaderHtml:"")+"</span></div>")},i.hideIndicator=function(){o(".preloader-indicator-overlay, .preloader-indicator-modal").remove()},i.actions=function(e,a){var t,n,r,s=!1;1===arguments.length?a=e:i.device.ios?i.device.ipad&&(s=!0):o(window).width()>=768&&(s=!0),a=a||[],a.length>0&&!o.isArray(a[0])&&(a=[a]);var p;if(s){var d=i.params.modalActionsToPopoverTemplate||'<div class="popover actions-popover"><div class="popover-inner">{{#each this}}<div class="list-block"><ul>{{#each this}}{{#if label}}<li class="actions-popover-label {{#if color}}color-{{color}}{{/if}} {{#if bold}}actions-popover-bold{{/if}}">{{text}}</li>{{else}}<li><a href="#" class="item-link list-button {{#if color}}color-{{color}}{{/if}} {{#if bg}}bg-{{bg}}{{/if}} {{#if bold}}actions-popover-bold{{/if}} {{#if disabled}}disabled{{/if}}">{{text}}</a></li>{{/if}}{{/each}}</ul></div>{{/each}}</div></div>';i._compiledTemplates.actionsToPopover||(i._compiledTemplates.actionsToPopover=l.compile(d));var c=i._compiledTemplates.actionsToPopover(a);t=o(i.popover(c,e,!0)),n=".list-block ul",r=".list-button"}else{if(i.params.modalActionsTemplate)i._compiledTemplates.actions||(i._compiledTemplates.actions=l.compile(i.params.modalActionsTemplate)),p=i._compiledTemplates.actions(a);else{for(var u="",m=0;m<a.length;m++)for(var h=0;h<a[m].length;h++){0===h&&(u+='<div class="actions-modal-group">');var g=a[m][h],v=g.label?"actions-modal-label":"actions-modal-button";g.bold&&(v+=" actions-modal-button-bold"),g.color&&(v+=" color-"+g.color),g.bg&&(v+=" bg-"+g.bg),g.disabled&&(v+=" disabled"),u+='<div class="'+v+'">'+g.text+"</div>",h===a[m].length-1&&(u+="</div>")}p='<div class="actions-modal">'+u+"</div>"}f.innerHTML=p,t=o(f).children(),o("body").append(t[0]),n=".actions-modal-group",r=".actions-modal-button"}var b=t.find(n);return b.each(function(e,n){var l=e;o(n).children().each(function(e,n){var p,d=e,c=a[l][d];!s&&o(n).is(r)&&(p=o(n)),s&&o(n).find(r).length>0&&(p=o(n).find(r)),p&&p.on("click",function(e){c.close!==!1&&i.closeModal(t),c.onClick&&c.onClick(t,e)})})}),s||i.openModal(t),t[0]},i.popover=function(e,a,t){function n(){e.css({left:"",top:""});var t,n,r,i=e.width(),l=e.height(),p=0;s?e.removeClass("popover-on-left popover-on-right popover-on-top popover-on-bottom").css({left:"",top:""}):(t=e.find(".popover-angle"),p=t.width()/2,t.removeClass("on-left on-right on-top on-bottom").css({left:"",top:""}));var d=a.outerWidth(),c=a.outerHeight(),u=a.offset(),m=a.parents(".page");m.length>0&&(u.top=u.top-m[0].scrollTop);var f=o(window).height(),h=o(window).width(),g=0,v=0,b=0,w=s?"bottom":"top";if(s){if(l<f-u.top-c?(w="bottom",g=u.top):l<u.top?(g=u.top-l+c,w="top"):(w="bottom",g=u.top),0>=g?g=8:g+l>=f&&(g=f-l-8),v=u.left,v+i>=h-8&&(v=u.left+d-i-8),8>v&&(v=8),"top"===w&&e.addClass("popover-on-top"),"bottom"===w&&e.addClass("popover-on-bottom"),a.hasClass("floating-button-to-popover")&&!e.hasClass("modal-in")){e.addClass("popover-floating-button");var C=v+i/2-(u.left+d/2),y=g+l/2-(u.top+c/2);a.addClass("floating-button-to-popover-in").transform("translate3d("+C+"px, "+y+"px,0)").transitionEnd(function(e){a.hasClass("floating-button-to-popover-in")&&a.addClass("floating-button-to-popover-scale").transform("translate3d("+C+"px, "+y+"px,0) scale("+i/d+", "+l/c+")")}),e.once("close",function(){a.removeClass("floating-button-to-popover-in floating-button-to-popover-scale").addClass("floating-button-to-popover-out").transform("").transitionEnd(function(e){a.removeClass("floating-button-to-popover-out")})}),e.once("closed",function(){e.removeClass("popover-floating-button");
})}}else l+p<u.top?g=u.top-l-p:l+p<f-u.top-c?(w="bottom",g=u.top+c+p):(w="middle",g=c/2+u.top-l/2,b=g,0>=g?g=5:g+l>=f&&(g=f-l-5),b-=g),"top"===w||"bottom"===w?(v=d/2+u.left-i/2,b=v,5>v&&(v=5),v+i>h&&(v=h-i-5),"top"===w&&t.addClass("on-bottom"),"bottom"===w&&t.addClass("on-top"),b-=v,n=i/2-p+b,n=Math.max(Math.min(n,i-2*p-13),13),t.css({left:n+"px"})):"middle"===w&&(v=u.left-i-p,t.addClass("on-right"),(5>v||v+i>h)&&(5>v&&(v=u.left+d+p),v+i>h&&(v=h-i-5),t.removeClass("on-right").addClass("on-left")),r=l/2-p+b,r=Math.max(Math.min(r,l-2*p-13),13),t.css({top:r+"px"}));e.css({top:g+"px",left:v+"px"})}if("undefined"==typeof t&&(t=!0),"string"==typeof e&&e.indexOf("<")>=0){var r=document.createElement("div");if(r.innerHTML=e.trim(),!(r.childNodes.length>0))return!1;e=r.childNodes[0],t&&e.classList.add("remove-on-close"),o("body").append(e)}if(e=o(e),a=o(a),0===e.length||0===a.length)return!1;0===e.parents("body").length&&(t&&e.addClass("remove-on-close"),o("body").append(e[0])),0!==e.find(".popover-angle").length||i.params.material||e.append('<div class="popover-angle"></div>'),e.show();var s=i.params.material;return n(),o(window).on("resize",n),e.on("close",function(){o(window).off("resize",n)}),i.openModal(e),e[0]},i.popup=function(e,a){if("undefined"==typeof a&&(a=!0),"string"==typeof e&&e.indexOf("<")>=0){var t=document.createElement("div");if(t.innerHTML=e.trim(),!(t.childNodes.length>0))return!1;e=t.childNodes[0],a&&e.classList.add("remove-on-close"),o("body").append(e)}return e=o(e),0===e.length?!1:(0===e.parents("body").length&&(a&&e.addClass("remove-on-close"),o("body").append(e[0])),e.show(),i.openModal(e),e[0])},i.pickerModal=function(e,a){if("undefined"==typeof a&&(a=!0),"string"==typeof e&&e.indexOf("<")>=0){if(e=o(e),!(e.length>0))return!1;a&&e.addClass("remove-on-close"),o("body").append(e[0])}return e=o(e),0===e.length?!1:(0===e.parents("body").length&&(a&&e.addClass("remove-on-close"),o("body").append(e[0])),o(".picker-modal.modal-in:not(.modal-out)").length>0&&!e.hasClass("modal-in")&&i.closeModal(".picker-modal.modal-in:not(.modal-out)"),e.show(),i.openModal(e),e[0])},i.loginScreen=function(e){return e||(e=".login-screen"),e=o(e),0===e.length?!1:(o(".login-screen.modal-in:not(.modal-out)").length>0&&!e.hasClass("modal-in")&&i.closeModal(".login-screen.modal-in:not(.modal-out)"),e.show(),i.openModal(e),e[0])},i.openModal=function(e){e=o(e);var a=e.hasClass("modal");if(o(".modal.modal-in:not(.modal-out)").length&&i.params.modalStack&&a)return void i.modalStack.push(function(){i.openModal(e)});if(!0!==e.data("f7-modal-shown")){e.data("f7-modal-shown",!0),e.once("close",function(){e.removeData("f7-modal-shown")});var t=(e.hasClass("popover"),e.hasClass("popup")),n=e.hasClass("login-screen"),r=e.hasClass("picker-modal");a&&(e.show(),e.css({marginTop:-Math.round(e.outerHeight()/2)+"px"}));var s;n||r||(0!==o(".modal-overlay").length||t||o("body").append('<div class="modal-overlay"></div>'),0===o(".popup-overlay").length&&t&&o("body").append('<div class="popup-overlay"></div>'),s=o(t?".popup-overlay":".modal-overlay")),i.params.material&&r&&e.hasClass("picker-calendar")&&(0!==o(".picker-modal-overlay").length||t||o("body").append('<div class="picker-modal-overlay"></div>'),s=o(".picker-modal-overlay"));e[0].clientLeft;return e.trigger("open"),r&&o("body").addClass("with-picker-modal"),e.find("."+i.params.viewClass).length>0&&(e.find(".page").each(function(){i.initPageWithCallback(this)}),e.find(".navbar").each(function(){i.initNavbarWithCallback(this)})),n||r||s.addClass("modal-overlay-visible"),i.params.material&&r&&s&&s.addClass("modal-overlay-visible"),e.removeClass("modal-out").addClass("modal-in").transitionEnd(function(a){e.hasClass("modal-out")?e.trigger("closed"):e.trigger("opened")}),!0}},i.closeModal=function(e){if(e=o(e||".modal-in"),"undefined"==typeof e||0!==e.length){var a,t=e.hasClass("modal"),n=e.hasClass("popover"),r=e.hasClass("popup"),s=e.hasClass("login-screen"),l=e.hasClass("picker-modal"),p=e.hasClass("remove-on-close");return r?a=o(".popup-overlay"):l&&i.params.material?a=o(".picker-modal-overlay"):l||(a=o(".modal-overlay")),r?e.length===o(".popup.modal-in").length&&a.removeClass("modal-overlay-visible"):a&&a.length>0&&a.removeClass("modal-overlay-visible"),e.trigger("close"),l&&(o("body").removeClass("with-picker-modal"),o("body").addClass("picker-modal-closing")),!n||i.params.material?(e.removeClass("modal-in").addClass("modal-out").transitionEnd(function(a){if(e.hasClass("modal-out"))e.trigger("closed");else if(e.trigger("opened"),n)return;l&&o("body").removeClass("picker-modal-closing"),r||s||l||n?(e.removeClass("modal-out").hide(),p&&e.length>0&&e.remove()):e.remove()}),t&&i.params.modalStack&&i.modalStackClearQueue()):(e.removeClass("modal-in modal-out").trigger("closed").hide(),p&&e.remove()),!0}},i.setProgressbar=function(e,a,t){if(e=o(e||"body"),0!==e.length){a&&(a=Math.min(Math.max(a,0),100));var n;if(n=e.hasClass("progressbar")?e:e.children(".progressbar"),0!==n.length&&!n.hasClass("progressbar-infinite")){n[0].clientLeft;return n.children("span").transform("translate3d("+(-100+a)+"%,0,0)"),"undefined"!=typeof t?n.children("span").transition(t):n.children("span").transition(""),n[0]}}},i.showProgressbar=function(e,a,t){if("number"==typeof e&&(e="body",a=arguments[0],t=arguments[1]),a&&"string"==typeof a&&parseFloat(a)!==1*a&&(t=a,a=void 0),e=o(e||"body"),0!==e.length){var n;return e.hasClass("progressbar")?n=e:(n=e.children(".progressbar:not(.progressbar-out), .progressbar-infinite:not(.progressbar-out)"),0===n.length&&(n=o("undefined"!=typeof a?'<span class="progressbar progressbar-in'+(t?" color-"+t:"")+'"><span></span></span>':'<span class="progressbar-infinite progressbar-in'+(t?" color-"+t:"")+'"></span>'),e.append(n))),a&&i.setProgressbar(e,a),n[0]}},i.hideProgressbar=function(e){if(e=o(e||"body"),0!==e.length){var a;a=e.hasClass("progressbar")?e:e.children(".progressbar, .progressbar-infinite"),0!==a.length&&a.hasClass("progressbar-in")&&!a.hasClass("progressbar-out")&&a.removeClass("progressbar-in").addClass("progressbar-out").animationEnd(function(){a.remove(),a=null})}},i.initPageProgressbar=function(e){e=o(e),e.find(".progressbar").each(function(){var e=o(this);0===e.children("span").length&&e.append("<span></span>"),e.attr("data-progress")&&i.setProgressbar(e,e.attr("data-progress"))})},i.allowPanelOpen=!0,i.openPanel=function(e){function a(){r.transitionEnd(function(e){o(e.target).is(r)?(t.hasClass("active")?t.trigger("opened"):t.trigger("closed"),i.params.material&&o(".panel-overlay").css({display:""}),i.allowPanelOpen=!0):a()})}if(!i.allowPanelOpen)return!1;var t=o(".panel-"+e);if(0===t.length||t.hasClass("active"))return!1;i.closePanel(),i.allowPanelOpen=!1;var n=t.hasClass("panel-reveal")?"reveal":"cover";t.css({display:"block"}).addClass("active"),t.trigger("open"),i.params.material&&o(".panel-overlay").show(),t.find("."+i.params.viewClass).length>0&&i.sizeNavbars&&i.sizeNavbars(t.find("."+i.params.viewClass)[0]);var r=(t[0].clientLeft,"reveal"===n?o("."+i.params.viewsClass):t);return a(),o("body").addClass("with-panel-"+e+"-"+n),!0},i.closePanel=function(){var e=o(".panel.active");if(0===e.length)return!1;var a=e.hasClass("panel-reveal")?"reveal":"cover",t=e.hasClass("panel-left")?"left":"right";e.removeClass("active");var n="reveal"===a?o("."+i.params.viewsClass):e;e.trigger("close"),i.allowPanelOpen=!1,n.transitionEnd(function(){e.hasClass("active")||(e.css({display:""}),e.trigger("closed"),o("body").removeClass("panel-closing"),i.allowPanelOpen=!0)}),o("body").addClass("panel-closing").removeClass("with-panel-"+t+"-"+a)},i.initSwipePanels=function(){function e(e){if(i.allowPanelOpen&&(i.params.swipePanel||i.params.swipePanelOnlyClose)&&!s&&!(o(".modal-in, .photo-browser-in").length>0)&&(i.params.swipePanelCloseOpposite||i.params.swipePanelOnlyClose||!(o(".panel.active").length>0)||n.hasClass("active"))){if(w.x="touchstart"===e.type?e.targetTouches[0].pageX:e.pageX,w.y="touchstart"===e.type?e.targetTouches[0].pageY:e.pageY,i.params.swipePanelCloseOpposite||i.params.swipePanelOnlyClose){if(o(".panel.active").length>0)r=o(".panel.active").hasClass("panel-left")?"left":"right";else{if(i.params.swipePanelOnlyClose)return;r=i.params.swipePanel}if(!r)return}if(n=o(".panel.panel-"+r),f=n.hasClass("active"),i.params.swipePanelActiveArea&&!f){if("left"===r&&w.x>i.params.swipePanelActiveArea)return;if("right"===r&&w.x<window.innerWidth-i.params.swipePanelActiveArea)return}l=!1,s=!0,p=void 0,d=(new Date).getTime(),v=void 0}}function a(e){if(s&&!e.f7PreventPanelSwipe){var a="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,t="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY;if("undefined"==typeof p&&(p=!!(p||Math.abs(t-w.y)>Math.abs(a-w.x))),p)return void(s=!1);if(!v&&(v=a>w.x?"to-right":"to-left","left"===r&&"to-left"===v&&!n.hasClass("active")||"right"===r&&"to-right"===v&&!n.hasClass("active")))return void(s=!1);if(i.params.swipePanelNoFollow){var o=(new Date).getTime()-d;return 300>o&&("to-left"===v&&("right"===r&&i.openPanel(r),"left"===r&&n.hasClass("active")&&i.closePanel()),"to-right"===v&&("left"===r&&i.openPanel(r),"right"===r&&n.hasClass("active")&&i.closePanel())),s=!1,void(l=!1)}l||(g=n.hasClass("panel-cover")?"cover":"reveal",f||(n.show(),b.show()),h=n[0].offsetWidth,n.transition(0),n.find("."+i.params.viewClass).length>0&&i.sizeNavbars&&i.sizeNavbars(n.find("."+i.params.viewClass)[0])),l=!0,e.preventDefault();var y=f?0:-i.params.swipePanelThreshold;"right"===r&&(y=-y),c=a-w.x+y,"right"===r?(u=c-(f?h:0),u>0&&(u=0),-h>u&&(u=-h)):(u=c+(f?h:0),0>u&&(u=0),u>h&&(u=h)),"reveal"===g?(C.transform("translate3d("+u+"px,0,0)").transition(0),b.transform("translate3d("+u+"px,0,0)").transition(0),i.pluginHook("swipePanelSetTransform",C[0],n[0],Math.abs(u/h))):(n.transform("translate3d("+u+"px,0,0)").transition(0),i.params.material&&(b.transition(0),m=Math.abs(u/h),b.css({opacity:m})),i.pluginHook("swipePanelSetTransform",C[0],n[0],Math.abs(u/h)))}}function t(e){if(!s||!l)return s=!1,void(l=!1);s=!1,l=!1;var a,t=(new Date).getTime()-d,p=0===u||Math.abs(u)===h;if(a=f?u===-h?"reset":300>t&&Math.abs(u)>=0||t>=300&&Math.abs(u)<=h/2?"left"===r&&u===h?"reset":"swap":"reset":0===u?"reset":300>t&&Math.abs(u)>0||t>=300&&Math.abs(u)>=h/2?"swap":"reset","swap"===a&&(i.allowPanelOpen=!0,f?(i.closePanel(),p&&(n.css({display:""}),o("body").removeClass("panel-closing"))):i.openPanel(r),p&&(i.allowPanelOpen=!0)),"reset"===a)if(f)i.allowPanelOpen=!0,i.openPanel(r);else if(i.closePanel(),p)i.allowPanelOpen=!0,n.css({display:""});else{var c="reveal"===g?C:n;n.trigger("close"),o("body").addClass("panel-closing"),c.transitionEnd(function(){n.trigger("closed"),n.css({display:""}),o("body").removeClass("panel-closing"),i.allowPanelOpen=!0})}"reveal"===g&&(C.transition(""),C.transform("")),n.transition("").transform(""),b.css({display:""}).transform("").transition("").css("opacity","")}var n,r;if(i.params.swipePanel){if(n=o(".panel.panel-"+i.params.swipePanel),r=i.params.swipePanel,0===n.length)return}else{if(!i.params.swipePanelOnlyClose)return;if(0===o(".panel").length)return}var s,l,p,d,c,u,m,f,h,g,v,b=o(".panel-overlay"),w={},C=o("."+i.params.viewsClass);o(document).on(i.touchEvents.start,e),o(document).on(i.touchEvents.move,a),o(document).on(i.touchEvents.end,t)},i.initImagesLazyLoad=function(e){function a(e){function t(){e.removeClass("lazy").addClass("lazy-loaded"),n?e.css("background-image","url("+r+")"):e.attr("src",r),i.params.imagesLazyLoadSequential&&(u=!1,c.length>0&&a(c.shift()))}e=o(e);var n=e.attr("data-background"),r=n?n:e.attr("data-src");if(r){if(i.params.imagesLazyLoadSequential&&u)return void(c.indexOf(e[0])<0&&c.push(e[0]));u=!0;var s=new Image;s.onload=t,s.onerror=t,s.src=r}}function t(){l=e.find(".lazy"),l.each(function(e,t){t=o(t),t.parents(".tab:not(.active)").length>0||n(t[0])&&a(t)})}function n(e){var a=e.getBoundingClientRect(),t=i.params.imagesLazyLoadThreshold||0;return a.top>=0-t&&a.left>=0-t&&a.top<=window.innerHeight+t&&a.left<=window.innerWidth+t}function r(a){var n=a?"off":"on";l[n]("lazy",t),l.parents(".tab")[n]("show",t),e[n]("lazy",t),p[n]("lazy",t),p[n]("scroll",t),o(window)[n]("resize",t)}function s(){r(!0)}e=o(e);var l;if(e.hasClass("lazy")?(l=e,e=l.parents(".page")):l=e.find(".lazy"),0!==l.length){var p;if(e.hasClass("page-content")?(p=e,e=e.parents(".page")):p=e.find(".page-content"),0!==p.length){var d="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEXCwsK592mkAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==";"string"==typeof i.params.imagesLazyLoadPlaceholder&&(d=i.params.imagesLazyLoadPlaceholder),i.params.imagesLazyLoadPlaceholder!==!1&&l.each(function(){o(this).attr("data-src")&&o(this).attr("src",d)});var c=[],u=!1;e[0].f7DestroyImagesLazyLoad=s,r(),e.hasClass("page")&&e.once("pageBeforeRemove",s),t(),e.once("pageAfterAnimation",t)}}},i.destroyImagesLazyLoad=function(e){e=o(e),e.length>0&&e[0].f7DestroyImagesLazyLoad&&e[0].f7DestroyImagesLazyLoad()},i.reinitImagesLazyLoad=function(e){e=o(e),e.length>0&&e.trigger("lazy")},i.initPageMaterialPreloader=function(e){o(e).find(".preloader").each(function(){0===o(this).children().length&&o(this).html(i.params.materialPreloaderHtml)})};var h=function(e,a){var t={autoLayout:!0,newMessagesFirst:!1,messageTemplate:'{{#if day}}<div class="messages-date">{{day}} {{#if time}}, <span>{{time}}</span>{{/if}}</div>{{/if}}<div class="message message-{{type}} {{#if hasImage}}message-pic{{/if}} {{#if avatar}}message-with-avatar{{/if}} {{#if position}}message-appear-from-{{position}}{{/if}}">{{#if name}}<div class="message-name">{{name}}</div>{{/if}}<div class="message-text">{{text}}{{#if date}}<div class="message-date">{{date}}</div>{{/if}}</div>{{#if avatar}}<div class="message-avatar" style="background-image:url({{avatar}})"></div>{{/if}}{{#if label}}<div class="message-label">{{label}}</div>{{/if}}</div>'};a=a||{};for(var n in t)("undefined"==typeof a[n]||null===a[n])&&(a[n]=t[n]);var r=this;return r.params=a,r.container=o(e),0!==r.container.length?(r.params.autoLayout&&r.container.addClass("messages-auto-layout"),r.params.newMessagesFirst&&r.container.addClass("messages-new-first"),r.pageContainer=r.container.parents(".page").eq(0),r.pageContent=r.pageContainer.find(".page-content"),r.template=Template7.compile(r.params.messageTemplate),r.layout=function(){r.container.hasClass("messages-auto-layout")||r.container.addClass("messages-auto-layout"),r.container.find(".message").each(function(){var e=o(this);e.find(".message-text img").length>0&&e.addClass("message-pic"),e.find(".message-avatar").length>0&&e.addClass("message-with-avatar")}),r.container.find(".message").each(function(){var e=o(this),a=e.hasClass("message-sent"),t=e.next(".message-"+(a?"sent":"received")),n=e.prev(".message-"+(a?"sent":"received"));0===t.length?e.addClass("message-last message-with-tail"):e.removeClass("message-last message-with-tail"),0===n.length?e.addClass("message-first"):e.removeClass("message-first"),n.length>0&&n.find(".message-name").length>0&&e.find(".message-name").length>0&&n.find(".message-name").text()!==e.find(".message-name").text()&&(n.addClass("message-last message-with-tail"),e.addClass("message-first"))})},r.appendMessage=function(e,a){return r.addMessage(e,"append",a)},r.prependMessage=function(e,a){return r.addMessage(e,"prepend",a)},r.addMessage=function(e,a,t){return r.addMessages([e],a,t)},r.addMessages=function(e,a,t){"undefined"==typeof t&&(t=!0),"undefined"==typeof a&&(a=r.params.newMessagesFirst?"prepend":"append");var n,i="";for(n=0;n<e.length;n++){var s=e[n]||{};s.type=s.type||"sent",s.text&&(s.hasImage=s.text.indexOf("<img")>=0,t&&(s.position="append"===a?"bottom":"top"),i+=r.template(s))}var o,l;"prepend"===a&&(o=r.pageContent[0].scrollHeight,l=r.pageContent[0].scrollTop),r.container[a](i),r.params.autoLayout&&r.layout(),"prepend"===a&&(r.pageContent[0].scrollTop=l+(r.pageContent[0].scrollHeight-o)),("append"===a&&!r.params.newMessagesFirst||"prepend"===a&&r.params.newMessagesFirst)&&r.scrollMessages(t?void 0:0);var p=r.container.find(".message");if(1===e.length)return"append"===a?p[p.length-1]:p[0];var d=[];if("append"===a)for(n=p.length-e.length;n<p.length;n++)d.push(p[n]);else for(n=0;n<e.length;n++)d.push(p[n]);return d},r.removeMessage=function(e){return e=o(e),0===e.length?!1:(e.remove(),r.params.autoLayout&&r.layout(),!0)},r.removeMessages=function(e){r.removeMessage(e)},r.clean=function(){r.container.html("")},r.scrollMessages=function(e,a){"undefined"==typeof e&&(e=400);var t,n=r.pageContent[0].scrollTop;if("undefined"!=typeof a)t=a;else if(t=r.params.newMessagesFirst?0:r.pageContent[0].scrollHeight-r.pageContent[0].offsetHeight,t===n)return;r.pageContent.scrollTop(t,e)},r.init=function(){r.params.messages?r.addMessages(r.params.messages,void 0,!1):(r.params.autoLayout&&r.layout(),r.scrollMessages(0))},r.destroy=function(){r=null},r.init(),r.container[0].f7Messages=r,r):void 0};i.messages=function(e,a){return new h(e,a)},i.initPageMessages=function(e){function a(){n.destroy(),e.off("pageBeforeRemove",a)}e=o(e);var t=e.find(".messages");if(0!==t.length&&t.hasClass("messages-init")){var n=i.messages(t,t.dataset());e.hasClass("page")&&e.on("pageBeforeRemove",a)}},i.swipeoutOpenedEl=void 0,i.allowSwipeout=!0,i.initSwipeout=function(e){function a(e){i.allowSwipeout&&(s=!1,r=!0,l=void 0,O.x="touchstart"===e.type?e.targetTouches[0].pageX:e.pageX,O.y="touchstart"===e.type?e.targetTouches[0].pageY:e.pageY,p=(new Date).getTime())}function t(e){if(r){var a="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,t="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY;if("undefined"==typeof l&&(l=!!(l||Math.abs(t-O.y)>Math.abs(a-O.x))),l)return void(r=!1);if(!s){if(o(".list-block.sortable-opened").length>0)return;c=o(this),u=c.find(".swipeout-content"),m=c.find(".swipeout-actions-right"),f=c.find(".swipeout-actions-left"),h=g=C=y=k=T=null,M=f.hasClass("swipeout-actions-no-fold")||i.params.swipeoutActionsNoFold,I=m.hasClass("swipeout-actions-no-fold")||i.params.swipeoutActionsNoFold,f.length>0&&(h=f.outerWidth(),C=f.children("a"),T=f.find(".swipeout-overswipe")),m.length>0&&(g=m.outerWidth(),y=m.children("a"),k=m.find(".swipeout-overswipe")),b=c.hasClass("swipeout-opened"),b&&(w=c.find(".swipeout-actions-left.swipeout-actions-opened").length>0?"left":"right"),c.removeClass("transitioning"),i.params.swipeoutNoFollow||(c.find(".swipeout-actions-opened").removeClass("swipeout-actions-opened"),c.removeClass("swipeout-opened"))}if(s=!0,e.preventDefault(),d=a-O.x,v=d,b&&("right"===w?v-=g:v+=h),v>0&&0===f.length||0>v&&0===m.length){if(!b)return r=s=!1,u.transform(""),y&&y.length>0&&y.transform(""),void(C&&C.length>0&&C.transform(""));v=0}x=0>v?"to-left":v>0?"to-right":x?x:"to-left";var n,p,E;if(e.f7PreventPanelSwipe=!0,i.params.swipeoutNoFollow)return b?("right"===w&&d>0&&i.swipeoutClose(c),"left"===w&&0>d&&i.swipeoutClose(c)):(0>d&&m.length>0&&i.swipeoutOpen(c,"right"),d>0&&f.length>0&&i.swipeoutOpen(c,"left")),r=!1,void(s=!1);P=!1,S=!1;var D;if(m.length>0)for(E=v/g,-g>v&&(v=-g-Math.pow(-v-g,.8),k.length>0&&(S=!0)),n=0;n<y.length;n++)"undefined"==typeof y[n]._buttonOffset&&(y[n]._buttonOffset=y[n].offsetLeft),p=y[n]._buttonOffset,D=o(y[n]),k.length>0&&D.hasClass("swipeout-overswipe")&&(D.css({left:(S?-p:0)+"px"}),S?D.addClass("swipeout-overswipe-active"):D.removeClass("swipeout-overswipe-active")),D.transform("translate3d("+(v-p*(1+Math.max(E,-1)))+"px,0,0)");if(f.length>0)for(E=v/h,v>h&&(v=h+Math.pow(v-h,.8),T.length>0&&(P=!0)),n=0;n<C.length;n++)"undefined"==typeof C[n]._buttonOffset&&(C[n]._buttonOffset=h-C[n].offsetLeft-C[n].offsetWidth),p=C[n]._buttonOffset,D=o(C[n]),T.length>0&&D.hasClass("swipeout-overswipe")&&(D.css({left:(P?p:0)+"px"}),P?D.addClass("swipeout-overswipe-active"):D.removeClass("swipeout-overswipe-active")),C.length>1&&D.css("z-index",C.length-n),D.transform("translate3d("+(v+p*(1-Math.min(E,1)))+"px,0,0)");u.transform("translate3d("+v+"px,0,0)")}}function n(e){if(!r||!s)return r=!1,void(s=!1);r=!1,s=!1;var a,t,n,l,w,T,k=(new Date).getTime()-p;if(T="to-left"===x?I:M,n="to-left"===x?m:f,t="to-left"===x?g:h,a=300>k&&(-10>d&&"to-left"===x||d>10&&"to-right"===x)||k>=300&&Math.abs(v)>t/2?"open":"close",300>k&&(0===Math.abs(v)&&(a="close"),Math.abs(v)===t&&(a="open")),"open"===a){i.swipeoutOpenedEl=c,c.trigger("open"),c.addClass("swipeout-opened transitioning");var O="to-left"===x?-t:t;if(u.transform("translate3d("+O+"px,0,0)"),n.addClass("swipeout-actions-opened"),l="to-left"===x?y:C)for(w=0;w<l.length;w++)o(l[w]).transform("translate3d("+O+"px,0,0)");S&&m.find(".swipeout-overswipe")[0].click(),P&&f.find(".swipeout-overswipe")[0].click()}else c.trigger("close"),i.swipeoutOpenedEl=void 0,c.addClass("transitioning").removeClass("swipeout-opened"),u.transform(""),n.removeClass("swipeout-actions-opened");var E;if(C&&C.length>0&&C!==l)for(w=0;w<C.length;w++)E=C[w]._buttonOffset,"undefined"==typeof E&&(C[w]._buttonOffset=h-C[w].offsetLeft-C[w].offsetWidth),o(C[w]).transform("translate3d("+E+"px,0,0)");if(y&&y.length>0&&y!==l)for(w=0;w<y.length;w++)E=y[w]._buttonOffset,"undefined"==typeof E&&(y[w]._buttonOffset=y[w].offsetLeft),o(y[w]).transform("translate3d("+-E+"px,0,0)");u.transitionEnd(function(e){b&&"open"===a||closed&&"close"===a||(c.trigger("open"===a?"opened":"closed"),b&&"close"===a&&(m.length>0&&y.transform(""),f.length>0&&C.transform("")))})}var r,s,l,p,d,c,u,m,f,h,g,v,b,w,C,y,x,T,k,P,S,M,I,O={};o(document).on(i.touchEvents.start,function(e){if(i.swipeoutOpenedEl){var a=o(e.target);i.swipeoutOpenedEl.is(a[0])||a.parents(".swipeout").is(i.swipeoutOpenedEl)||a.hasClass("modal-in")||a.hasClass("modal-overlay")||a.hasClass("actions-modal")||a.parents(".actions-modal.modal-in, .modal.modal-in").length>0||i.swipeoutClose(i.swipeoutOpenedEl)}}),e?(o(e).on(i.touchEvents.start,a),o(e).on(i.touchEvents.move,t),o(e).on(i.touchEvents.end,n)):(o(document).on(i.touchEvents.start,".list-block li.swipeout",a),o(document).on(i.touchEvents.move,".list-block li.swipeout",t),o(document).on(i.touchEvents.end,".list-block li.swipeout",n))},i.swipeoutOpen=function(e,a,t){if(e=o(e),2===arguments.length&&"function"==typeof arguments[1]&&(t=a),0!==e.length&&(e.length>1&&(e=o(e[0])),e.hasClass("swipeout")&&!e.hasClass("swipeout-opened"))){a||(a=e.find(".swipeout-actions-right").length>0?"right":"left");var n=e.find(".swipeout-actions-"+a);if(0!==n.length){n.hasClass("swipeout-actions-no-fold")||i.params.swipeoutActionsNoFold;e.trigger("open").addClass("swipeout-opened").removeClass("transitioning"),n.addClass("swipeout-actions-opened");var r,s=n.children("a"),l=n.outerWidth(),p="right"===a?-l:l;if(s.length>1){for(r=0;r<s.length;r++)"right"===a?o(s[r]).transform("translate3d("+-s[r].offsetLeft+"px,0,0)"):o(s[r]).css("z-index",s.length-r).transform("translate3d("+(l-s[r].offsetWidth-s[r].offsetLeft)+"px,0,0)");s[1].clientLeft}for(e.addClass("transitioning"),r=0;r<s.length;r++)o(s[r]).transform("translate3d("+p+"px,0,0)");e.find(".swipeout-content").transform("translate3d("+p+"px,0,0)").transitionEnd(function(){e.trigger("opened"),t&&t.call(e[0])}),i.swipeoutOpenedEl=e}}},i.swipeoutClose=function(e,a){function t(){i.allowSwipeout=!0,e.hasClass("swipeout-opened")||(e.removeClass("transitioning"),s.transform(""),e.trigger("closed"),a&&a.call(e[0]),p&&clearTimeout(p))}if(e=o(e),0!==e.length&&e.hasClass("swipeout-opened")){var n=e.find(".swipeout-actions-opened").hasClass("swipeout-actions-right")?"right":"left",r=e.find(".swipeout-actions-opened").removeClass("swipeout-actions-opened"),s=(r.hasClass("swipeout-actions-no-fold")||i.params.swipeoutActionsNoFold,r.children("a")),l=r.outerWidth();i.allowSwipeout=!1,e.trigger("close"),e.removeClass("swipeout-opened").addClass("transitioning");var p;e.find(".swipeout-content").transform("").transitionEnd(t),p=setTimeout(t,500);for(var d=0;d<s.length;d++)"right"===n?o(s[d]).transform("translate3d("+-s[d].offsetLeft+"px,0,0)"):o(s[d]).transform("translate3d("+(l-s[d].offsetWidth-s[d].offsetLeft)+"px,0,0)"),o(s[d]).css({left:"0px"}).removeClass("swipeout-overswipe-active");i.swipeoutOpenedEl&&i.swipeoutOpenedEl[0]===e[0]&&(i.swipeoutOpenedEl=void 0)}},i.swipeoutDelete=function(e,a){if(e=o(e),0!==e.length){e.length>1&&(e=o(e[0])),i.swipeoutOpenedEl=void 0,e.trigger("delete"),e.css({height:e.outerHeight()+"px"});e[0].clientLeft;e.css({height:"0px"}).addClass("deleting transitioning").transitionEnd(function(){if(e.trigger("deleted"),a&&a.call(e[0]),e.parents(".virtual-list").length>0){var t=e.parents(".virtual-list")[0].f7VirtualList,n=e[0].f7VirtualListIndex;t&&"undefined"!=typeof n&&t.deleteItem(n)}else e.remove()});var t="-100%";e.find(".swipeout-content").transform("translate3d("+t+",0,0)")}},i.sortableToggle=function(e){return e=o(e),0===e.length&&(e=o(".list-block.sortable")),e.toggleClass("sortable-opened"),e.hasClass("sortable-opened")?e.trigger("open"):e.trigger("close"),e},i.sortableOpen=function(e){return e=o(e),0===e.length&&(e=o(".list-block.sortable")),e.addClass("sortable-opened"),e.trigger("open"),e},i.sortableClose=function(e){return e=o(e),0===e.length&&(e=o(".list-block.sortable")),e.removeClass("sortable-opened"),e.trigger("close"),e},i.initSortable=function(){function e(e){r=!1,n=!0,s="touchstart"===e.type?e.targetTouches[0].pageY:e.pageY,p=o(this).parent(),c=p.parent().find("li"),g=p.parents(".sortable"),e.preventDefault(),i.allowPanelOpen=i.allowSwipeout=!1}function a(e){if(n&&p){var a=("touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,"touchmove"===e.type?e.targetTouches[0].pageY:e.pageY);r||(p.addClass("sorting"),g.addClass("sortable-sorting"),u=p[0].offsetTop,m=p.parent().height()-p[0].offsetTop-p.height(),d=p[0].offsetHeight),r=!0,e.preventDefault(),e.f7PreventPanelSwipe=!0,l=a-s;var t=l;-u>t&&(t=-u),t>m&&(t=m),p.transform("translate3d(0,"+t+"px,0)"),h=f=void 0,c.each(function(){var e=o(this);if(e[0]!==p[0]){var a=e[0].offsetTop,n=e.height(),r=p[0].offsetTop+t;r>=a-n/2&&p.index()<e.index()?(e.transform("translate3d(0, "+-d+"px,0)"),f=e,h=void 0):a+n/2>=r&&p.index()>e.index()?(e.transform("translate3d(0, "+d+"px,0)"),f=void 0,h||(h=e)):o(this).transform("translate3d(0, 0%,0)")}})}}function t(e){if(i.allowPanelOpen=i.allowSwipeout=!0,!n||!r)return n=!1,void(r=!1);e.preventDefault(),c.transform(""),p.removeClass("sorting"),g.removeClass("sortable-sorting");var a,t,s;f&&(p.insertAfter(f),p.trigger("sort")),h&&(p.insertBefore(h),p.trigger("sort")),(f||h)&&g.hasClass("virtual-list")&&(a=g[0].f7VirtualList,t=p[0].f7VirtualListIndex,s=h?h[0].f7VirtualListIndex:f[0].f7VirtualListIndex,a&&a.moveItem(t,s)),f=h=void 0,n=!1,r=!1}var n,r,s,l,p,d,c,u,m,f,h,g;o(document).on(i.touchEvents.start,".list-block.sortable .sortable-handler",e),i.support.touch?(o(document).on(i.touchEvents.move,".list-block.sortable .sortable-handler",a),o(document).on(i.touchEvents.end,".list-block.sortable .sortable-handler",t)):(o(document).on(i.touchEvents.move,a),o(document).on(i.touchEvents.end,t))},i.initSmartSelects=function(e){e=o(e);var a;a=e.is(".smart-select")?e:e.find(".smart-select"),0!==a.length&&a.each(function(){var e=o(this),a=e.find("select");if(0!==a.length){var t=a[0];if(0!==t.length){for(var n=[],r=0;r<t.length;r++)t[r].selected&&n.push(t[r].textContent.trim());var i=e.find(".item-after");if(0===i.length)e.find(".item-inner").append('<div class="item-after">'+n.join(", ")+"</div>");else{var s=i.text();if(i.hasClass("smart-select-value"))for(r=0;r<t.length;r++)t[r].selected=t[r].textContent.trim()===s.trim();else i.text(n.join(", "))}}}})},i.smartSelectAddOption=function(e,a,t){e=o(e);var n=e.parents(".smart-select");"undefined"==typeof t?e.append(a):o(a).insertBefore(e.find("option").eq(t)),i.initSmartSelects(n);var r=n.find("select").attr("name"),s=o('.page.smart-select-page[data-select-name="'+r+'"]').length>0;s&&i.smartSelectOpen(n,!0)},i.smartSelectOpen=function(e,a){function t(){var a=e.parents(".page-content");if(0!==a.length){var t,n=parseInt(a.css("padding-top"),10),r=parseInt(a.css("padding-bottom"),10),i=a[0].offsetHeight-n-ie.height(),s=a[0].scrollHeight-n-ie.height(),o=e.offset().top-n+e[0].offsetHeight;if(o>i){var l=a.scrollTop()+o-i;l+i>s&&(t=l+i-s+r,i===s&&(t=ie.height()),a.css({"padding-bottom":t+"px"})),a.scrollTop(l,300)}}}function n(a){var t=!0;(a.target===e[0]||o(a.target).parents(e[0]).length>0)&&(t=!1),o(a.target).parents(".picker-modal").length>0&&(t=!1),t&&i.closeModal(".smart-select-picker.modal-in")}function r(e){k.selectedOptions.length>=Y?e.find('input[type="checkbox"]').each(function(){this.checked?o(this).parents("li").removeClass("disabled"):o(this).parents("li").addClass("disabled")}):e.find(".disabled").removeClass("disabled")}function s(a){if(a=o(a),C){var t=i.virtualList(a.find(".virtual-list"),{items:R,template:W,height:y||void 0,searchByItem:function(e,a,t){return t.text.toLowerCase().indexOf(e.trim().toLowerCase())>=0?!0:!1}});a.once("popup"===c||"picker"===c?"closed":"pageBeforeRemove",function(){t&&t.destroy&&t.destroy()})}Y&&r(a),a.on("change",'input[name="'+F+'"]',function(){var t=this,n=t.value,s=[];if("checkbox"===t.type){for(var o=0;o<k.options.length;o++){var l=k.options[o];l.value===n&&(l.selected=t.checked),l.selected&&s.push(l.textContent.trim())}Y&&r(a)}else s=[e.find('option[value="'+n+'"]').text()],k.value=n;P.trigger("change"),e.find(".item-after").text(s.join(", ")),g&&"radio"===q&&("popup"===c?i.closeModal(re):"picker"===c?i.closeModal(ie):d.router.back())})}function p(e){var a=e.detail.page;a.name===te&&s(a.container)}if(e=o(e),0!==e.length){var d=e.parents("."+i.params.viewClass);if(0!==d.length){d=d[0].f7View;var c=e.attr("data-open-in")||i.params.smartSelectOpenIn;if("popup"===c){if(o(".popup.smart-select-popup").length>0)return}else if("picker"===c){if(o(".picker-modal.modal-in").length>0&&!a){if(e[0].f7SmartSelectPicker===o(".picker-modal.modal-in:not(.modal-out)")[0])return;i.closeModal(o(".picker-modal.modal-in:not(.modal-out)"))}}else if(!d)return;var u,m=e.dataset(),f=m.pageTitle||e.find(".item-title").text(),h=m.backText||i.params.smartSelectBackText;u="picker"===c?m.pickerCloseText||m.backText||i.params.smartSelectPickerCloseText:m.popupCloseText||m.backText||i.params.smartSelectPopupCloseText;var g=void 0!==m.backOnSelect?m.backOnSelect:i.params.smartSelectBackOnSelect,v=m.formTheme||i.params.smartSelectFormTheme,b=m.navbarTheme||i.params.smartSelectNavbarTheme,w=m.toolbarTheme||i.params.smartSelectToolbarTheme,C=m.virtualList,y=m.virtualListHeight,x=i.params.material,T=m.pickerHeight||i.params.smartSelectPickerHeight,k=e.find("select")[0],P=o(k),S=P.dataset();if(!(k.disabled||e.hasClass("disabled")||P.hasClass("disabled"))){for(var M,I,O,E,D,L,B,z,N,H,A,R=[],V=(new Date).getTime(),q=k.multiple?"checkbox":"radio",F=q+"-"+V,Y=P.attr("maxlength"),G=k.name,j=0;j<k.length;j++)M=o(k[j]),A=M.dataset(),O=A.optionImage||S.optionImage||m.optionImage,E=A.optionIcon||S.optionIcon||m.optionIcon,I=O||E||"checkbox"===q,x&&(I=O||E),N=A.optionColor,H=A.optionClass,M[0].disabled&&(H+=" disabled"),D=M.parent("optgroup")[0],L=D&&D.label,B=!1,D&&D!==z&&(B=!0,z=D,R.push({groupLabel:L,isLabel:B})),R.push({value:M[0].value,text:M[0].textContent.trim(),selected:M[0].selected,group:D,groupLabel:L,image:O,icon:E,color:N,className:H,disabled:M[0].disabled,inputType:q,id:V,hasMedia:I,checkbox:"checkbox"===q,inputName:F,material:i.params.material});i._compiledTemplates.smartSelectItem||(i._compiledTemplates.smartSelectItem=l.compile(i.params.smartSelectItemTemplate||'{{#if isLabel}}<li class="item-divider">{{groupLabel}}</li>{{else}}<li{{#if className}} class="{{className}}"{{/if}}><label class="label-{{inputType}} item-content"><input type="{{inputType}}" name="{{inputName}}" value="{{value}}" {{#if selected}}checked{{/if}}>{{#if material}}{{#if hasMedia}}<div class="item-media">{{#if icon}}<i class="icon {{icon}}"></i>{{/if}}{{#if image}}<img src="{{image}}">{{/if}}</div><div class="item-inner"><div class="item-title{{#if color}} color-{{color}}{{/if}}">{{text}}</div></div><div class="item-after"><i class="icon icon-form-{{inputType}}"></i></div>{{else}}<div class="item-media"><i class="icon icon-form-{{inputType}}"></i></div><div class="item-inner"><div class="item-title{{#if color}} color-{{color}}{{/if}}">{{text}}</div></div>{{/if}}{{else}}{{#if hasMedia}}<div class="item-media">{{#if checkbox}}<i class="icon icon-form-checkbox"></i>{{/if}}{{#if icon}}<i class="icon {{icon}}"></i>{{/if}}{{#if image}}<img src="{{image}}">{{/if}}</div>{{/if}}<div class="item-inner"><div class="item-title{{#if color}} color-{{color}}{{/if}}">{{text}}</div></div>{{/if}}</label></li>{{/if}}'));
var W=i._compiledTemplates.smartSelectItem,X="";if(!C)for(var _=0;_<R.length;_++)X+=W(R[_]);var U,J,Q="",K="",Z="",$="";"picker"===c?(i._compiledTemplates.smartSelectToolbar||(i._compiledTemplates.smartSelectToolbar=l.compile(i.params.smartSelectToolbarTemplate||'<div class="toolbar {{#if toolbarTheme}}theme-{{toolbarTheme}}{{/if}}"><div class="toolbar-inner"><div class="left"></div><div class="right"><a href="#" class="link close-picker"><span>{{closeText}}</span></a></div></div></div>')),Q=i._compiledTemplates.smartSelectToolbar({pageTitle:f,closeText:u,openIn:c,toolbarTheme:w,inPicker:"picker"===c})):(i._compiledTemplates.smartSelectNavbar||(i._compiledTemplates.smartSelectNavbar=l.compile(i.params.smartSelectNavbarTemplate||'<div class="navbar {{#if navbarTheme}}theme-{{navbarTheme}}{{/if}}"><div class="navbar-inner">{{leftTemplate}}<div class="center sliding">{{pageTitle}}</div></div></div>')),U=i._compiledTemplates.smartSelectNavbar({pageTitle:f,backText:h,closeText:u,openIn:c,navbarTheme:b,inPopup:"popup"===c,inPage:"page"===c,leftTemplate:"popup"===c?(i.params.smartSelectPopupCloseTemplate||(x?'<div class="left"><a href="#" class="link close-popup icon-only"><i class="icon icon-back"></i></a></div>':'<div class="left"><a href="#" class="link close-popup"><i class="icon icon-back"></i><span>{{closeText}}</span></a></div>')).replace(/{{closeText}}/g,u):(i.params.smartSelectBackTemplate||(x?'<div class="left"><a href="#" class="back link icon-only"><i class="icon icon-back"></i></a></div>':'<div class="left sliding"><a href="#" class="back link"><i class="icon icon-back"></i><span>{{backText}}</span></a></div>')).replace(/{{backText}}/g,h)}),"page"===c?(J="static",e.parents(".navbar-through").length>0&&(J="through"),e.parents(".navbar-fixed").length>0&&(J="fixed"),Z=e.parents(".page").hasClass("no-toolbar")?"no-toolbar":"",K=e.parents(".page").hasClass("no-navbar")?"no-navbar":"navbar-"+J,$=e.parents(".page").hasClass("no-tabbar")?"no-tabbar":""):J="fixed");var ee,ae,te="smart-select-"+F,ne="undefined"==typeof e.data("searchbar")?i.params.smartSelectSearchbar:"true"===e.data("searchbar")?!0:!1;ne&&(ee=e.data("searchbar-placeholder")||"Search",ae=e.data("searchbar-cancel")||"Cancel");var re,ie,se='<form class="searchbar searchbar-init" data-search-list=".smart-select-list-'+V+'" data-search-in=".item-title"><div class="searchbar-input"><input type="search" placeholder="'+ee+'"><a href="#" class="searchbar-clear"></a></div>'+(x?"":'<a href="#" class="searchbar-cancel">'+ae+"</a>")+'</form><div class="searchbar-overlay"></div>',oe=("picker"!==c&&"through"===J?U:"")+'<div class="pages">  <div data-page="'+te+'" data-select-name="'+G+'" class="page smart-select-page '+K+" "+Z+" "+$+'">'+("picker"!==c&&"fixed"===J?U:"")+(ne?se:"")+'    <div class="page-content">'+("picker"!==c&&"static"===J?U:"")+'      <div class="list-block '+(C?"virtual-list":"")+" smart-select-list-"+V+" "+(v?"theme-"+v:"")+'">        <ul>'+(C?"":X)+"        </ul>      </div>    </div>  </div></div>";"popup"===c?(a?(re=o(".popup.smart-select-popup .view"),re.html(oe)):(re=i.popup('<div class="popup smart-select-popup smart-select-popup-'+F+'"><div class="view navbar-fixed">'+oe+"</div></div>"),re=o(re)),i.initPage(re.find(".page")),s(re)):"picker"===c?(a?(ie=o(".picker-modal.smart-select-picker .view"),ie.html(oe)):(ie=i.pickerModal('<div class="picker-modal smart-select-picker smart-select-picker-'+F+'"'+(T?' style="height:'+T+'"':"")+">"+Q+'<div class="picker-modal-inner"><div class="view">'+oe+"</div></div></div>"),ie=o(ie),t(),o("html").on("click",n),ie.once("close",function(){e[0].f7SmartSelectPicker=void 0,o("html").off("click",n),e.parents(".page-content").css({paddingBottom:""})}),e[0].f7SmartSelectPicker=ie[0]),i.initPage(ie.find(".page")),s(ie)):(o(document).once("pageInit",".smart-select-page",p),d.router.load({content:oe,reload:a?!0:void 0}))}}}};var g=function(e,a){var t={cols:1,height:i.params.material?48:44,cache:!0,dynamicHeightBufferSize:1,showFilteredItemsOnly:!1};a=a||{};for(var n in t)"undefined"==typeof a[n]&&(a[n]=t[n]);var r=this;r.listBlock=o(e),r.params=a,r.items=r.params.items,r.params.showFilteredItemsOnly&&(r.filteredItems=[]),r.params.template&&("string"==typeof r.params.template?r.template=l.compile(r.params.template):"function"==typeof r.params.template&&(r.template=r.params.template)),r.pageContent=r.listBlock.parents(".page-content");var s;"undefined"!=typeof r.params.updatableScroll?s=r.params.updatableScroll:(s=!0,i.device.ios&&i.device.osVersion.split(".")[0]<8&&(s=!1)),r.ul=r.params.ul?o(r.params.ul):r.listBlock.children("ul"),0===r.ul.length&&(r.listBlock.append("<ul></ul>"),r.ul=r.listBlock.children("ul")),r.domCache={},r.displayDomCache={},r.tempDomElement=document.createElement("ul"),r.lastRepaintY=null,r.fragment=document.createDocumentFragment(),r.filterItems=function(e,a){r.filteredItems=[];for(var t=(e[0],e[e.length-1],0);t<e.length;t++)r.filteredItems.push(r.items[e[t]]);"undefined"==typeof a&&(a=!0),a&&(r.pageContent[0].scrollTop=0),r.update()},r.resetFilter=function(){r.params.showFilteredItemsOnly?r.filteredItems=[]:(r.filteredItems=null,delete r.filteredItems),r.update()};var p,d,c,u,m,f,h=0,g="function"==typeof r.params.height;return r.setListSize=function(){var e=r.filteredItems||r.items;if(p=r.pageContent[0].offsetHeight,g){f=0,r.heights=[];for(var a=0;a<e.length;a++){var t=r.params.height(e[a]);f+=t,r.heights.push(t)}}else f=e.length*r.params.height/r.params.cols,d=Math.ceil(p/r.params.height),c=r.params.rowsBefore||2*d,u=r.params.rowsAfter||d,m=d+c+u,h=c/2*r.params.height;s&&r.ul.css({height:f+"px"})},r.render=function(e,a){e&&(r.lastRepaintY=null);var t=-(r.listBlock[0].getBoundingClientRect().top-r.pageContent[0].getBoundingClientRect().top);if("undefined"!=typeof a&&(t=a),null===r.lastRepaintY||Math.abs(t-r.lastRepaintY)>h||!s&&r.pageContent[0].scrollTop+p>=r.pageContent[0].scrollHeight){r.lastRepaintY=t;var n,i,o=r.filteredItems||r.items,l=0,d=0;if(g){var u,f,v=0;for(h=p,u=0;u<r.heights.length;u++)f=r.heights[u],"undefined"==typeof n&&(v+f>=t-2*p*r.params.dynamicHeightBufferSize?n=u:l+=f),"undefined"==typeof i&&((v+f>=t+2*p*r.params.dynamicHeightBufferSize||u===r.heights.length-1)&&(i=u+1),d+=f),v+=f;i=Math.min(i,o.length)}else n=(parseInt(t/r.params.height)-c)*r.params.cols,0>n&&(n=0),i=Math.min(n+m*r.params.cols,o.length);var b;r.reachEnd=!1;for(var w=n;i>w;w++){var C,y;y=r.items.indexOf(o[w]),w===n&&(r.currentFromIndex=y),w===i-1&&(r.currentToIndex=y),y===r.items.length-1&&(r.reachEnd=!0),r.domCache[y]?C=r.domCache[y]:(r.template?r.tempDomElement.innerHTML=r.template(o[w],{index:y}).trim():r.params.renderItem?r.tempDomElement.innerHTML=r.params.renderItem(y,o[w]).trim():r.tempDomElement.innerHTML=o[w].trim(),C=r.tempDomElement.childNodes[0],r.params.cache&&(r.domCache[y]=C)),C.f7VirtualListIndex=y,w===n&&(b=g?l:w*r.params.height/r.params.cols),C.style.top=b+"px",r.params.onItemBeforeInsert&&r.params.onItemBeforeInsert(r,C),r.fragment.appendChild(C)}s||(g?r.ul[0].style.height=d+"px":r.ul[0].style.height=w*r.params.height/r.params.cols+"px"),r.params.onBeforeClear&&r.params.onBeforeClear(r,r.fragment),r.ul[0].innerHTML="",r.params.onItemsBeforeInsert&&r.params.onItemsBeforeInsert(r,r.fragment),r.ul[0].appendChild(r.fragment),r.params.onItemsAfterInsert&&r.params.onItemsAfterInsert(r,r.fragment),"undefined"!=typeof a&&e&&r.pageContent.scrollTop(a,0)}},r.scrollToItem=function(e){if(e>r.items.length)return!1;var a,t=0;if(g)for(var n=0;e>n;n++)t+=r.heights[n];else t=e*r.params.height;return a=r.listBlock[0].offsetTop,r.render(!0,a+t-parseInt(r.pageContent.css("padding-top"),10)),!0},r.handleScroll=function(e){r.render()},r.handleResize=function(e){r.setListSize(),r.render(!0)},r.attachEvents=function(e){var a=e?"off":"on";r.pageContent[a]("scroll",r.handleScroll),r.listBlock.parents(".tab").eq(0)[a]("show",r.handleResize),o(window)[a]("resize",r.handleResize)},r.init=function(){r.attachEvents(),r.setListSize(),r.render()},r.appendItems=function(e){for(var a=0;a<e.length;a++)r.items.push(e[a]);r.update()},r.appendItem=function(e){r.appendItems([e])},r.replaceAllItems=function(e){r.items=e,delete r.filteredItems,r.domCache={},r.update()},r.replaceItem=function(e,a){r.items[e]=a,r.params.cache&&delete r.domCache[e],r.update()},r.prependItems=function(e){for(var a=e.length-1;a>=0;a--)r.items.unshift(e[a]);if(r.params.cache){var t={};for(var n in r.domCache)t[parseInt(n,10)+e.length]=r.domCache[n];r.domCache=t}r.update()},r.prependItem=function(e){r.prependItems([e])},r.moveItem=function(e,a){if(e!==a){var t=r.items.splice(e,1)[0];if(a>=r.items.length?(r.items.push(t),a=r.items.length-1):r.items.splice(a,0,t),r.params.cache){var n={};for(var i in r.domCache){var s=parseInt(i,10),o=a>e?e:a,l=a>e?a:e,p=a>e?-1:1;(o>s||s>l)&&(n[s]=r.domCache[s]),s===o&&(n[l]=r.domCache[s]),s>o&&l>=s&&(n[s+p]=r.domCache[s])}r.domCache=n}r.update()}},r.insertItemBefore=function(e,a){if(0===e)return void r.prependItem(a);if(e>=r.items.length)return void r.appendItem(a);if(r.items.splice(e,0,a),r.params.cache){var t={};for(var n in r.domCache){var i=parseInt(n,10);i>=e&&(t[i+1]=r.domCache[i])}r.domCache=t}r.update()},r.deleteItems=function(e){for(var a,t=0,n=0;n<e.length;n++){var i=e[n];"undefined"!=typeof a&&i>a&&(t=-n),i+=t,a=e[n];var s=r.items.splice(i,1)[0];if(r.filteredItems&&r.filteredItems.indexOf(s)>=0&&r.filteredItems.splice(r.filteredItems.indexOf(s),1),r.params.cache){var o={};for(var l in r.domCache){var p=parseInt(l,10);p===i?delete r.domCache[i]:parseInt(l,10)>i?o[p-1]=r.domCache[l]:o[p]=r.domCache[l]}r.domCache=o}}r.update()},r.deleteAllItems=function(){r.items=[],delete r.filteredItems,r.params.cache&&(r.domCache={}),r.update()},r.deleteItem=function(e){r.deleteItems([e])},r.clearCache=function(){r.domCache={}},r.update=function(){r.setListSize(),r.render(!0)},r.destroy=function(){r.attachEvents(!0),delete r.items,delete r.domCache},r.init(),r.listBlock[0].f7VirtualList=r,r};i.virtualList=function(e,a){return new g(e,a)},i.reinitVirtualList=function(e){var a=o(e),t=a.find(".virtual-list");if(0!==t.length)for(var n=0;n<t.length;n++){var r=t[n].f7VirtualList;r&&r.update()}},i.initPullToRefresh=function(e){function a(e){if(d){if("android"!==i.device.os)return;if("targetTouches"in e&&e.targetTouches.length>1)return}h=o(this),h.hasClass("refreshing")||(c=!1,y=!1,d=!0,u=void 0,b=void 0,"touchstart"===e.type&&(p=e.targetTouches[0].identifier),x.x="touchstart"===e.type?e.targetTouches[0].pageX:e.pageX,x.y="touchstart"===e.type?e.targetTouches[0].pageY:e.pageY,f=(new Date).getTime())}function t(e){if(d){var a,t,n;if("touchmove"===e.type){if(p&&e.touches)for(var r=0;r<e.touches.length;r++)e.touches[r].identifier===p&&(n=e.touches[r]);n||(n=e.targetTouches[0]),a=n.pageX,t=n.pageY}else a=e.pageX,t=e.pageY;if(a&&t){if("undefined"==typeof u&&(u=!!(u||Math.abs(t-x.y)>Math.abs(a-x.x))),!u)return void(d=!1);if(v=h[0].scrollTop,"undefined"==typeof b&&0!==v&&(b=!0),!c){if(h.removeClass("transitioning"),v>h[0].offsetHeight)return void(d=!1);C&&(w=h.attr("data-ptr-distance"),w.indexOf("%")>=0&&(w=h[0].offsetHeight*parseInt(w,10)/100)),P=h.hasClass("refreshing")?w:0,k=h[0].scrollHeight===h[0].offsetHeight||"ios"!==i.device.os?!0:!1}return c=!0,m=t-x.y,m>0&&0>=v||0>v?("ios"===i.device.os&&parseInt(i.device.osVersion.split(".")[0],10)>7&&0===v&&!b&&(k=!0),k&&(e.preventDefault(),g=Math.pow(m,.85)+P,h.transform("translate3d(0,"+g+"px,0)")),k&&Math.pow(m,.85)>w||!k&&m>=2*w?(T=!0,h.addClass("pull-up").removeClass("pull-down")):(T=!1,h.removeClass("pull-up").addClass("pull-down")),y||(h.trigger("pullstart"),y=!0),h.trigger("pullmove",{event:e,scrollTop:v,translate:g,touchesDiff:m}),void 0):(y=!1,h.removeClass("pull-up pull-down"),void(T=!1))}}}function n(e){if(!("touchend"===e.type&&e.changedTouches&&e.changedTouches.length>0&&p&&e.changedTouches[0].identifier!==p)){if(!d||!c)return d=!1,void(c=!1);g&&(h.addClass("transitioning"),g=0),h.transform(""),T?(h.addClass("refreshing"),h.trigger("refresh",{done:function(){i.pullToRefreshDone(h)}})):h.removeClass("pull-down"),d=!1,c=!1,y&&h.trigger("pullend")}}function r(){l.off(i.touchEvents.start,a),l.off(i.touchEvents.move,t),l.off(i.touchEvents.end,n)}function s(){r(),S.off("pageBeforeRemove",s)}var l=o(e);if(l.hasClass("pull-to-refresh-content")||(l=l.find(".pull-to-refresh-content")),l&&0!==l.length){var p,d,c,u,m,f,h,g,v,b,w,C,y,x={},T=!1,k=!1,P=0,S=l.hasClass("page")?l:l.parents(".page"),M=!1;(S.find(".navbar").length>0||S.parents(".navbar-fixed, .navbar-through").length>0||S.hasClass("navbar-fixed")||S.hasClass("navbar-through"))&&(M=!0),S.hasClass("no-navbar")&&(M=!1),M||l.addClass("pull-to-refresh-no-navbar"),h=l,h.attr("data-ptr-distance")?C=!0:w=44,l.on(i.touchEvents.start,a),l.on(i.touchEvents.move,t),l.on(i.touchEvents.end,n),0!==S.length&&(l[0].f7DestroyPullToRefresh=r,S.on("pageBeforeRemove",s))}},i.pullToRefreshDone=function(e){e=o(e),0===e.length&&(e=o(".pull-to-refresh-content.refreshing")),e.removeClass("refreshing").addClass("transitioning"),e.transitionEnd(function(){e.removeClass("transitioning pull-up pull-down"),e.trigger("refreshdone")})},i.pullToRefreshTrigger=function(e){e=o(e),0===e.length&&(e=o(".pull-to-refresh-content")),e.hasClass("refreshing")||(e.addClass("transitioning refreshing"),e.trigger("refresh",{done:function(){i.pullToRefreshDone(e)}}))},i.destroyPullToRefresh=function(e){e=o(e);var a=e.hasClass("pull-to-refresh-content")?e:e.find(".pull-to-refresh-content");0!==a.length&&a[0].f7DestroyPullToRefresh&&a[0].f7DestroyPullToRefresh()},i.attachInfiniteScroll=function(e){o(e).on("scroll",n)},i.detachInfiniteScroll=function(e){o(e).off("scroll",n)},i.initPageInfiniteScroll=function(e){function a(){i.detachInfiniteScroll(t),e.off("pageBeforeRemove",a)}e=o(e);var t=e.find(".infinite-scroll");0!==t.length&&(i.attachInfiniteScroll(t),e.on("pageBeforeRemove",a))},i.initPageScrollToolbars=function(e){function a(a){e.hasClass("page-on-left")||(m=t[0].scrollTop,v=t[0].scrollHeight,b=t[0].offsetHeight,w=m+b>=v-S,y=d.hasClass("navbar-hidden"),x=c.hasClass("toolbar-hidden"),T=p&&p.hasClass("toolbar-hidden"),w?i.params.showBarsOnPageScrollEnd&&(C="show"):C=u>m?i.params.showBarsOnPageScrollTop||44>=m?"show":"hide":m>44?"hide":"show","show"===C?(f&&n&&y&&(i.showNavbar(d),e.removeClass("no-navbar-by-scroll"),y=!1),h&&r&&x&&(i.showToolbar(c),e.removeClass("no-toolbar-by-scroll"),x=!1),g&&s&&T&&(i.showToolbar(p),e.removeClass("no-tabbar-by-scroll"),T=!1)):(f&&n&&!y&&(i.hideNavbar(d),e.addClass("no-navbar-by-scroll"),y=!0),h&&r&&!x&&(i.hideToolbar(c),e.addClass("no-toolbar-by-scroll"),x=!0),g&&s&&!T&&(i.hideToolbar(p),e.addClass("no-tabbar-by-scroll"),T=!0)),u=m)}e=o(e);var t=e.find(".page-content");if(0!==t.length){var n=(i.params.hideNavbarOnPageScroll||t.hasClass("hide-navbar-on-scroll")||t.hasClass("hide-bars-on-scroll"))&&!(t.hasClass("keep-navbar-on-scroll")||t.hasClass("keep-bars-on-scroll")),r=(i.params.hideToolbarOnPageScroll||t.hasClass("hide-toolbar-on-scroll")||t.hasClass("hide-bars-on-scroll"))&&!(t.hasClass("keep-toolbar-on-scroll")||t.hasClass("keep-bars-on-scroll")),s=(i.params.hideTabbarOnPageScroll||t.hasClass("hide-tabbar-on-scroll"))&&!t.hasClass("keep-tabbar-on-scroll");if(n||r||s){var l=t.parents("."+i.params.viewClass);if(0!==l.length){var p,d=l.find(".navbar"),c=l.find(".toolbar");s&&(p=l.find(".tabbar"),0===p.length&&(p=l.parents("."+i.params.viewsClass).find(".tabbar")));var u,m,f=d.length>0,h=c.length>0,g=p&&p.length>0;u=m=t[0].scrollTop;var v,b,w,C,y,x,T,k=h&&r?c[0].offsetHeight:0,P=g&&s?p[0].offsetHeight:0,S=P||k;t.on("scroll",a),t[0].f7ScrollToolbarsHandler=a}}}},i.destroyScrollToolbars=function(e){e=o(e);var a=e.find(".page-content");if(0!==a.length){var t=a[0].f7ScrollToolbarsHandler;t&&a.off("scroll",a[0].f7ScrollToolbarsHandler)}},i.materialTabbarSetHighlight=function(e,a){e=o(e),a=a||e.find(".tab-link.active");var t,n;e.hasClass("tabbar-scrollable")?(t=a[0].offsetWidth+"px",n=(i.rtl?-a[0].offsetLeft:a[0].offsetLeft)+"px"):(t=1/e.find(".tab-link").length*100+"%",n=100*(i.rtl?-a.index():a.index())+"%"),e.find(".tab-link-highlight").css({width:t}).transform("translate3d("+n+",0,0)")},i.initPageMaterialTabbar=function(e){function a(){i.materialTabbarSetHighlight(t)}e=o(e);var t=o(e).find(".tabbar");t.length>0&&(0===t.find(".tab-link-highlight").length&&t.find(".toolbar-inner").append('<span class="tab-link-highlight"></span>'),a(),o(window).on("resize",a),e.once("pageBeforeRemove",function(){o(window).off("resize",a)}))},i.showTab=function(e,a,t){var n=o(e);if(2===arguments.length&&"boolean"==typeof a&&(t=a),0===n.length)return!1;if(n.hasClass("active"))return t&&n.trigger("show"),!1;var r=n.parent(".tabs");if(0===r.length)return!1;i.allowSwipeout=!0;var s=r.parent().hasClass("tabs-animated-wrap");if(s){var l=100*(i.rtl?n.index():-n.index());r.transform("translate3d("+l+"%,0,0)")}var p,d=r.parent().hasClass("tabs-swipeable-wrap");d&&(p=r.parent()[0].swiper,p.activeIndex!==n.index()&&p.slideTo(n.index(),void 0,!1));var c=r.children(".tab.active").removeClass("active");if(n.addClass("active"),n.trigger("show"),!s&&!d&&n.find(".navbar").length>0){var u;u=n.hasClass(i.params.viewClass)?n[0]:n.parents("."+i.params.viewClass)[0],i.sizeNavbars(u)}if(a?a=o(a):(a=o("string"==typeof e?'.tab-link[href="'+e+'"]':'.tab-link[href="#'+n.attr("id")+'"]'),(!a||a&&0===a.length)&&o("[data-tab]").each(function(){n.is(o(this).attr("data-tab"))&&(a=o(this))})),0!==a.length){var m;if(c&&c.length>0){var f=c.attr("id");f&&(m=o('.tab-link[href="#'+f+'"]')),(!m||m&&0===m.length)&&o("[data-tab]").each(function(){c.is(o(this).attr("data-tab"))&&(m=o(this))})}if(a&&a.length>0&&(a.addClass("active"),i.params.material)){var h=a.parents(".tabbar");h.length>0&&(0===h.find(".tab-link-highlight").length&&h.find(".toolbar-inner").append('<span class="tab-link-highlight"></span>'),i.materialTabbarSetHighlight(h,a))}return m&&m.length>0&&m.removeClass("active"),!0}},i.accordionToggle=function(e){e=o(e),0!==e.length&&(e.hasClass("accordion-item-expanded")?i.accordionClose(e):i.accordionOpen(e))},i.accordionOpen=function(e){e=o(e);var a=e.parents(".accordion-list").eq(0),t=e.children(".accordion-item-content");0===t.length&&(t=e.find(".accordion-item-content"));var n=a.length>0&&e.parent().children(".accordion-item-expanded");n.length>0&&i.accordionClose(n),t.css("height",t[0].scrollHeight+"px").transitionEnd(function(){if(e.hasClass("accordion-item-expanded")){t.transition(0),t.css("height","auto");t[0].clientLeft;t.transition(""),e.trigger("opened")}else t.css("height",""),e.trigger("closed")}),e.trigger("open"),e.addClass("accordion-item-expanded")},i.accordionClose=function(e){e=o(e);var a=e.children(".accordion-item-content");0===a.length&&(a=e.find(".accordion-item-content")),e.removeClass("accordion-item-expanded"),a.transition(0),a.css("height",a[0].scrollHeight+"px");a[0].clientLeft;a.transition(""),a.css("height","").transitionEnd(function(){if(e.hasClass("accordion-item-expanded")){a.transition(0),a.css("height","auto");a[0].clientLeft;a.transition(""),e.trigger("opened")}else a.css("height",""),e.trigger("closed")}),e.trigger("close")},i.initFastClicks=function(){function e(e){var a,t=o(e),n=t.parents(i.params.activeStateElements);return t.is(i.params.activeStateElements)&&(a=t),n.length>0&&(a=a?a.add(n):n),a?a:t}function a(e){var a=e.parents(".page-content, .panel");return 0===a.length?!1:("yes"!==a.prop("scrollHandlerSet")&&(a.on("scroll",function(){clearTimeout(R),clearTimeout(j)}),a.prop("scrollHandlerSet","yes")),!0)}function t(){A&&A.addClass("active-state")}function n(e){A&&(A.removeClass("active-state"),A=null)}function r(e){var a="input select textarea label".split(" ");return e.nodeName&&a.indexOf(e.nodeName.toLowerCase())>=0?!0:!1}function s(e){var a="button input textarea select".split(" ");return document.activeElement&&e!==document.activeElement&&document.activeElement!==document.body?a.indexOf(e.nodeName.toLowerCase())>=0?!1:!0:!1}function l(e){var a=o(e);return"input"===e.nodeName.toLowerCase()&&"file"===e.type?!1:a.hasClass("no-fastclick")||a.parents(".no-fastclick").length>0?!1:!0}function p(e){if(document.activeElement===e)return!1;var a=e.nodeName.toLowerCase(),t="button checkbox file image radio submit".split(" ");return e.disabled||e.readOnly?!1:"textarea"===a?!0:"select"===a?i.device.android?!1:!0:"input"===a&&t.indexOf(e.type)<0?!0:void 0}function d(e){e=o(e);var a=!0;return(e.is("label")||e.parents("label").length>0)&&(a=i.device.android?!1:i.device.ios&&e.is("input")?!0:!1),a}function c(a){e(a.target).addClass("active-state"),"which"in a&&3===a.which&&setTimeout(function(){o(".active-state").removeClass("active-state")},0),i.params.material&&i.params.materialRipple&&(S=a.pageX,M=a.pageY,v(a.target,a.pageX,a.pageY))}function u(e){o(".active-state").removeClass("active-state"),i.params.material&&i.params.materialRipple&&b()}function m(e){o(".active-state").removeClass("active-state"),i.params.material&&i.params.materialRipple&&w()}function f(e){var a=i.params.materialRippleElements,t=o(e);if(t.is(a))return t.hasClass("no-ripple")?!1:t;if(t.parents(a).length>0){var n=t.parents(a).eq(0);return n.hasClass("no-ripple")?!1:n}return!1}function h(e,a,t){var n=t[0].getBoundingClientRect(),r={x:e-n.left,y:a-n.top},i=n.height,s=n.width,l=Math.max(Math.pow(Math.pow(i,2)+Math.pow(s,2),.5),48);F=o('<div class="ripple-wave" style="width: '+l+"px; height: "+l+"px; margin-top:-"+l/2+"px; margin-left:-"+l/2+"px; left:"+r.x+"px; top:"+r.y+'px;"></div>'),t.prepend(F);F[0].clientLeft;G="translate3d("+(-r.x+s/2)+"px, "+(-r.y+i/2)+"px, 0) scale(1)",F.transform(G)}function g(){if(F){var e=F,a=setTimeout(function(){e.remove()},400);F.addClass("ripple-wave-fill").transform(G.replace("scale(1)","scale(1.01)")).transitionEnd(function(){clearTimeout(a);var e=o(this).addClass("ripple-wave-out").transform(G.replace("scale(1)","scale(1.01)"));a=setTimeout(function(){e.remove()},700),setTimeout(function(){e.transitionEnd(function(){clearTimeout(a),o(this).remove()})},0)}),F=Y=void 0}}function v(e,t,n){return Y=f(e),Y&&0!==Y.length?void(a(Y)?j=setTimeout(function(){h(S,M,Y)},80):h(S,M,Y)):void(Y=void 0)}function b(){clearTimeout(j),g()}function w(){F?g():Y&&!z?(clearTimeout(j),h(S,M,Y),setTimeout(g,0)):g()}function C(e){var a=e.changedTouches[0],t=document.createEvent("MouseEvents"),n="click";i.device.android&&"select"===O.nodeName.toLowerCase()&&(n="mousedown"),t.initMouseEvent(n,!0,!0,window,1,a.screenX,a.screenY,a.clientX,a.clientY,!1,!1,!1,!1,0,null),t.forwardedTouchEvent=!0,O.dispatchEvent(t)}function y(r){if(z=!1,N=!1,r.targetTouches.length>1)return A&&n(),!0;if(r.touches.length>1&&A&&n(),i.params.tapHold&&(H&&clearTimeout(H),H=setTimeout(function(){r&&r.touches&&r.touches.length>1||(N=!0,r.preventDefault(),o(r.target).trigger("taphold"))},i.params.tapHoldDelay)),q&&clearTimeout(q),V=l(r.target),!V)return E=!1,!0;if(i.device.ios){var p=window.getSelection();if(p.rangeCount&&p.focusNode!==document.body&&(!p.isCollapsed||document.activeElement===p.focusNode))return D=!0,!0;D=!1}i.device.android&&s(r.target)&&document.activeElement.blur(),E=!0,O=r.target,I=(new Date).getTime(),S=r.targetTouches[0].pageX,M=r.targetTouches[0].pageY,i.device.ios&&(L=void 0,o(O).parents().each(function(){var e=this;e.scrollHeight>e.offsetHeight&&!L&&(L=e,L.f7ScrollTop=L.scrollTop)})),r.timeStamp-B<i.params.fastClicksDelayBetweenClicks&&r.preventDefault(),i.params.activeState&&(A=e(O),a(A)?R=setTimeout(t,80):t()),i.params.material&&i.params.materialRipple&&v(O,S,M)}function x(e){if(E){var a=!1,t=i.params.fastClicksDistanceThreshold;if(t){var r=e.targetTouches[0].pageX,s=e.targetTouches[0].pageY;(Math.abs(r-S)>t||Math.abs(s-M)>t)&&(a=!0)}else a=!0;a&&(E=!1,O=null,z=!0,i.params.tapHold&&clearTimeout(H),i.params.activeState&&(clearTimeout(R),n()),i.params.material&&i.params.materialRipple&&b())}}function T(e){if(clearTimeout(R),clearTimeout(H),!E)return!D&&V&&(!i.device.android||e.cancelable)&&e.preventDefault(),!0;if(document.activeElement===e.target)return i.params.activeState&&n(),i.params.material&&i.params.materialRipple&&w(),!0;if(D||e.preventDefault(),e.timeStamp-B<i.params.fastClicksDelayBetweenClicks)return setTimeout(n,0),!0;if(B=e.timeStamp,E=!1,i.device.ios&&L&&L.scrollTop!==L.f7ScrollTop)return!1;if(i.params.activeState&&(t(),setTimeout(n,0)),i.params.material&&i.params.materialRipple&&w(),p(O)){if(i.device.ios&&i.device.webView)return event.timeStamp-I>159?(O=null,!1):(O.focus(),!1);O.focus()}return document.activeElement&&O!==document.activeElement&&document.activeElement!==document.body&&"label"!==O.nodeName.toLowerCase()&&document.activeElement.blur(),e.preventDefault(),C(e),!1}function k(e){E=!1,O=null,clearTimeout(R),clearTimeout(H),i.params.activeState&&n(),i.params.material&&i.params.materialRipple&&w()}function P(e){var a=!1;return E?(O=null,E=!1,!0):"submit"===e.target.type&&0===e.detail?!0:(O||r(e.target)||(a=!0),V||(a=!0),document.activeElement===O&&(a=!0),e.forwardedTouchEvent&&(a=!0),e.cancelable||(a=!0),i.params.tapHold&&i.params.tapHoldPreventClicks&&N&&(a=!1),a||(e.stopImmediatePropagation(),e.stopPropagation(),O?(d(O)||z)&&e.preventDefault():e.preventDefault(),O=null),q=setTimeout(function(){V=!1},i.device.ios||i.device.androidChrome?100:400),i.params.tapHold&&(H=setTimeout(function(){N=!1},i.device.ios||i.device.androidChrome?100:400)),a)}i.params.activeState&&o("html").addClass("watch-active-state"),i.device.ios&&i.device.webView&&window.addEventListener("touchstart",function(){});var S,M,I,O,E,D,L,B,z,N,H,A,R,V,q,F,Y,G,j;i.support.touch?(document.addEventListener("click",P,!0),document.addEventListener("touchstart",y),document.addEventListener("touchmove",x),document.addEventListener("touchend",T),document.addEventListener("touchcancel",k)):i.params.activeState&&(document.addEventListener("mousedown",c),document.addEventListener("mousemove",u),document.addEventListener("mouseup",m)),i.params.material&&i.params.materialRipple&&document.addEventListener("contextmenu",function(e){A&&n(),w()})},i.initClickEvents=function(){function e(e){var a=o(this),t=o(e.target),n="a"===a[0].nodeName.toLowerCase()||a.parents("a").length>0||"a"===t[0].nodeName.toLowerCase()||t.parents("a").length>0;if(!n){var r;if(i.params.scrollTopOnNavbarClick&&a.is(".navbar .center")){var s=a.parents(".navbar");r=s.parents(".page-content"),0===r.length&&(s.parents(".page").length>0&&(r=s.parents(".page").find(".page-content")),0===r.length&&s.nextAll(".pages").length>0&&(r=s.nextAll(".pages").find(".page:not(.page-on-left):not(.page-on-right):not(.cached)").find(".page-content")))}i.params.scrollTopOnStatusbarClick&&a.is(".statusbar-overlay")&&(r=o(".popup.modal-in").length>0?o(".popup.modal-in").find(".page:not(.page-on-left):not(.page-on-right):not(.cached)").find(".page-content"):o(".panel.active").length>0?o(".panel.active").find(".page:not(.page-on-left):not(.page-on-right):not(.cached)").find(".page-content"):o(".views > .view.active").length>0?o(".views > .view.active").find(".page:not(.page-on-left):not(.page-on-right):not(.cached)").find(".page-content"):o(".views").find(".page:not(.page-on-left):not(.page-on-right):not(.cached)").find(".page-content")),r&&r.length>0&&(r.hasClass("tab")&&(r=r.parent(".tabs").children(".page-content.active")),r.length>0&&r.scrollTop(0,300))}}function a(e){var a=o(this),t=a.attr("href"),n="a"===a[0].nodeName.toLowerCase();if(n&&(a.is(i.params.externalLinks)||t&&t.indexOf("javascript:")>=0))return void(t&&"_system"===a.attr("target")&&(e.preventDefault(),window.open(t,"_system")));var r=a.dataset();if(a.hasClass("smart-select")&&i.smartSelectOpen&&i.smartSelectOpen(a),a.hasClass("open-panel")&&(1===o(".panel").length?o(".panel").hasClass("panel-left")?i.openPanel("left"):i.openPanel("right"):"right"===r.panel?i.openPanel("right"):i.openPanel("left")),a.hasClass("close-panel")&&i.closePanel(),a.hasClass("panel-overlay")&&i.params.panelsCloseByOutside&&i.closePanel(),a.hasClass("open-popover")){var s;s=r.popover?r.popover:".popover",i.popover(s,a)}a.hasClass("close-popover")&&i.closeModal(".popover.modal-in");var d;a.hasClass("open-popup")&&(d=r.popup?r.popup:".popup",i.popup(d)),a.hasClass("close-popup")&&(d=r.popup?r.popup:".popup.modal-in",i.closeModal(d));var c;if(a.hasClass("open-login-screen")&&(c=r.loginScreen?r.loginScreen:".login-screen",i.loginScreen(c)),a.hasClass("close-login-screen")&&i.closeModal(".login-screen.modal-in"),a.hasClass("modal-overlay")&&(o(".modal.modal-in").length>0&&i.params.modalCloseByOutside&&i.closeModal(".modal.modal-in"),o(".actions-modal.modal-in").length>0&&i.params.actionsCloseByOutside&&i.closeModal(".actions-modal.modal-in"),o(".popover.modal-in").length>0&&i.closeModal(".popover.modal-in")),a.hasClass("popup-overlay")&&o(".popup.modal-in").length>0&&i.params.popupCloseByOutside&&i.closeModal(".popup.modal-in"),a.hasClass("picker-modal-overlay")&&o(".picker-modal.modal-in").length>0&&i.closeModal(".picker-modal.modal-in"),a.hasClass("close-picker")){var u=o(".picker-modal.modal-in");u.length>0?i.closeModal(u):(u=o(".popover.modal-in .picker-modal"),u.length>0&&i.closeModal(u.parents(".popover")))}if(a.hasClass("open-picker")){var m;m=r.picker?r.picker:".picker-modal",i.pickerModal(m,a)}var f;if(a.hasClass("tab-link")&&(f=!0,i.showTab(r.tab||a.attr("href"),a)),a.hasClass("swipeout-close")&&i.swipeoutClose(a.parents(".swipeout-opened")),a.hasClass("swipeout-delete"))if(r.confirm){var h=r.confirm,g=r.confirmTitle;g?i.confirm(h,g,function(){i.swipeoutDelete(a.parents(".swipeout"))},function(){r.closeOnCancel&&i.swipeoutClose(a.parents(".swipeout"))}):i.confirm(h,function(){i.swipeoutDelete(a.parents(".swipeout"))},function(){r.closeOnCancel&&i.swipeoutClose(a.parents(".swipeout"))})}else i.swipeoutDelete(a.parents(".swipeout"));if(a.hasClass("toggle-sortable")&&i.sortableToggle(r.sortable),a.hasClass("open-sortable")&&i.sortableOpen(r.sortable),a.hasClass("close-sortable")&&i.sortableClose(r.sortable),a.hasClass("accordion-item-toggle")||a.hasClass("item-link")&&a.parent().hasClass("accordion-item")){var v=a.parent(".accordion-item");0===v.length&&(v=a.parents(".accordion-item")),0===v.length&&(v=a.parents("li")),i.accordionToggle(v)}if(i.params.material&&(a.hasClass("floating-button")&&a.parent().hasClass("speed-dial")&&a.parent().toggleClass("speed-dial-opened"),a.hasClass("close-speed-dial")&&o(".speed-dial-opened").removeClass("speed-dial-opened")),(!i.params.ajaxLinks||a.is(i.params.ajaxLinks))&&n&&i.params.router){n&&e.preventDefault();var b=t&&t.length>0&&"#"!==t&&!f,w=r.template;if(b||a.hasClass("back")||w){var C;if(r.view?C=o(r.view)[0].f7View:(C=a.parents("."+i.params.viewClass)[0]&&a.parents("."+i.params.viewClass)[0].f7View,C&&C.params.linksView&&("string"==typeof C.params.linksView?C=o(C.params.linksView)[0].f7View:C.params.linksView instanceof p&&(C=C.params.linksView))),C||i.mainView&&(C=i.mainView),!C)return;var y;if(w)t=void 0;else{if(0===t.indexOf("#")&&"#"!==t){if(!C.params.domCache)return;y=t.split("#")[1]}if("#"===t&&!a.hasClass("back"))return}var x;"undefined"!=typeof r.animatePages?x=r.animatePages:(a.hasClass("with-animation")&&(x=!0),a.hasClass("no-animation")&&(x=!1));var T={animatePages:x,ignoreCache:r.ignoreCache,force:r.force,reload:r.reload,reloadPrevious:r.reloadPrevious,pageName:y,pushState:r.pushState,url:t};if(i.params.template7Pages){T.contextName=r.contextName;var k=r.context;k&&(T.context=JSON.parse(k))}w&&w in l.templates&&(T.template=l.templates[w]),a.hasClass("back")?C.router.back(T):C.router.load(T)}}}function t(e){e.preventDefault()}o(document).on("click","a, .open-panel, .close-panel, .panel-overlay, .modal-overlay, .popup-overlay, .swipeout-delete, .swipeout-close, .close-popup, .open-popup, .open-popover, .open-login-screen, .close-login-screen .smart-select, .toggle-sortable, .open-sortable, .close-sortable, .accordion-item-toggle, .close-picker, .picker-modal-overlay",a),(i.params.scrollTopOnNavbarClick||i.params.scrollTopOnStatusbarClick)&&o(document).on("click",".statusbar-overlay, .navbar .center",e),
i.support.touch&&!i.device.android&&o(document).on(i.params.fastClicks?"touchstart":"touchmove",".panel-overlay, .modal-overlay, .preloader-indicator-overlay, .popup-overlay, .searchbar-overlay",t)},i.initResize=function(){o(window).on("resize",i.resize),o(window).on("orientationchange",i.orientationchange)},i.resize=function(){i.sizeNavbars&&i.sizeNavbars(),r()},i.orientationchange=function(){i.device&&i.device.minimalUi&&(90===window.orientation||-90===window.orientation)&&(document.body.scrollTop=0),r()},i.formsData={},i.formStoreData=function(e,a){i.formsData[e]=a,i.ls["f7form-"+e]=JSON.stringify(a)},i.formDeleteData=function(e){i.formsData[e]&&(i.formsData[e]="",delete i.formsData[e]),i.ls["f7form-"+e]&&(i.ls["f7form-"+e]="",i.ls.removeItem("f7form-"+e))},i.formGetData=function(e){return i.ls["f7form-"+e]?JSON.parse(i.ls["f7form-"+e]):i.formsData[e]?i.formsData[e]:void 0},i.formToJSON=function(e){if(e=o(e),1!==e.length)return!1;var a={},t=["submit","image","button","file"],n=[];return e.find("input, select, textarea").each(function(){var r=o(this),i=r.attr("name"),s=r.attr("type"),l=this.nodeName.toLowerCase();if(!(t.indexOf(s)>=0)&&!(n.indexOf(i)>=0)&&i)if("select"===l&&r.prop("multiple"))n.push(i),a[i]=[],e.find('select[name="'+i+'"] option').each(function(){this.selected&&a[i].push(this.value)});else switch(s){case"checkbox":n.push(i),a[i]=[],e.find('input[name="'+i+'"]').each(function(){this.checked&&a[i].push(this.value)});break;case"radio":n.push(i),e.find('input[name="'+i+'"]').each(function(){this.checked&&(a[i]=this.value)});break;default:a[i]=r.val()}}),e.trigger("formToJSON",{formData:a}),a},i.formFromJSON=function(e,a){if(e=o(e),1!==e.length)return!1;var t=["submit","image","button","file"],n=[];e.find("input, select, textarea").each(function(){var r=o(this),i=r.attr("name"),s=r.attr("type"),l=this.nodeName.toLowerCase();if(a[i]&&!(t.indexOf(s)>=0)&&!(n.indexOf(i)>=0)&&i)if("select"===l&&r.prop("multiple"))n.push(i),e.find('select[name="'+i+'"] option').each(function(){a[i].indexOf(this.value)>=0?this.selected=!0:this.selected=!1});else switch(s){case"checkbox":n.push(i),e.find('input[name="'+i+'"]').each(function(){a[i].indexOf(this.value)>=0?this.checked=!0:this.checked=!1});break;case"radio":n.push(i),e.find('input[name="'+i+'"]').each(function(){a[i]===this.value?this.checked=!0:this.checked=!1});break;default:r.val(a[i])}}),e.trigger("formFromJSON",{formData:a})},i.initFormsStorage=function(e){function a(){var e=o(this),a=e[0].id;if(a){var t=i.formToJSON(e);t&&(i.formStoreData(a,t),e.trigger("store",{data:t}))}}function t(){n.off("change submit",a),e.off("pageBeforeRemove",t)}e=o(e);var n=e.find("form.store-data");0!==n.length&&(n.each(function(){var e=this.getAttribute("id");if(e){var a=i.formGetData(e);a&&i.formFromJSON(this,a)}}),n.on("change submit",a),e.on("pageBeforeRemove",t))},o(document).on("submit change","form.ajax-submit, form.ajax-submit-onchange",function(e){var a=o(this);if("change"!==e.type||a.hasClass("ajax-submit-onchange")){"submit"===e.type&&e.preventDefault();var t=a.attr("method")||"GET",n=a.prop("enctype")||a.attr("enctype"),r=a.attr("action");if(r){var s;s="POST"===t?new FormData(a[0]):o.serializeObject(i.formToJSON(a[0]));var l=o.ajax({method:t,url:r,contentType:n,data:s,beforeSend:function(e){a.trigger("beforeSubmit",{data:s,xhr:e})},error:function(e){a.trigger("submitError",{data:s,xhr:e})},success:function(e){a.trigger("submitted",{data:e,xhr:l})}})}}}),i.resizeTextarea=function(e){if(e=o(e),e.hasClass("resizable")){e.css({height:""});var a=e[0].offsetHeight,t=a-e[0].clientHeight,n=e[0].scrollHeight;if(n+t>a){var r=n+t;e.css("height",r+"px")}}},i.resizableTextarea=function(e){function a(){clearTimeout(t),t=setTimeout(function(){i.resizeTextarea(e)},0)}if(e=o(e),0!==e.length){var t;return e[0].f7DestroyResizableTextarea=function(){e.off("change keydown keypress keyup paste cut",a)},e.on("change keydown keypress keyup paste cut",a)}},i.destroyResizableTextarea=function(e){e=o(e),e.length>0&&e.is("textarea")&&e[0].f7DestroyResizableTextarea?e[0].f7DestroyResizableTextarea():e.length>0&&e.find("textarea.resiable").each(function(){var e=this;e.f7DestroyResizableTextarea&&e.f7DestroyResizableTextarea()})},i.initPageResizableTextarea=function(e){e=o(e);var a=e.find("textarea.resizable");a.each(function(){i.resizableTextarea(this)})},i.initPageMaterialInputs=function(e){e=o(e);e.find("textarea.resizable");e.find(".item-input").each(function(){var e=o(this),a=["checkbox","button","submit","range","radio","image"];e.find("input, select, textarea").each(function(){var t=o(this);a.indexOf(t.attr("type"))<0&&(e.addClass("item-input-field"),""!==t.val().trim()&&t.parents(".item-input, .input-field").add(t.parents(".item-inner")).addClass("not-empty-state"))}),e.parents(".input-item, .inputs-list").length>0||e.parents(".list-block").eq(0).addClass("inputs-list")})},i.initMaterialWatchInputs=function(){function e(e){var a=o(this),t=a.attr("type");if(!(n.indexOf(t)>=0)){var r=a.add(a.parents(".item-input, .input-field")).add(a.parents(".item-inner").eq(0));r.addClass("focus-state")}}function a(e){var a=o(this),t=a.val(),r=a.attr("type");if(!(n.indexOf(r)>=0)){var i=a.add(a.parents(".item-input, .input-field")).add(a.parents(".item-inner").eq(0));i.removeClass("focus-state"),t&&""!==t.trim()?i.addClass("not-empty-state"):i.removeClass("not-empty-state")}}function t(e){var a=o(this),t=a.val(),r=a.attr("type");if(!(n.indexOf(r)>=0)){var i=a.add(a.parents(".item-input, .input-field")).add(a.parents(".item-inner").eq(0));t&&""!==t.trim()?i.addClass("not-empty-state"):i.removeClass("not-empty-state")}}var n=["checkbox","button","submit","range","radio","image"];o(document).on("change",".item-input input, .item-input select, .item-input textarea, input, textarea, select",t,!0),o(document).on("focus",".item-input input, .item-input select, .item-input textarea, input, textarea, select",e,!0),o(document).on("blur",".item-input input, .item-input select, .item-input textarea, input, textarea, select",a,!0)},i.pushStateQueue=[],i.pushStateClearQueue=function(){if(0!==i.pushStateQueue.length){var e,a=i.pushStateQueue.pop();i.params.pushStateNoAnimation===!0&&(e=!1),"back"===a.action&&i.router.back(a.view,{animatePages:e}),"loadPage"===a.action&&i.router.load(a.view,{url:a.stateUrl,animatePages:e,pushState:!1}),"loadContent"===a.action&&i.router.load(a.view,{content:a.stateContent,animatePages:e,pushState:!1}),"loadPageName"===a.action&&i.router.load(a.view,{pageName:a.statePageName,url:a.stateUrl,animatePages:e,pushState:!1})}},i.initPushState=function(){function e(e){if(!a){var t=i.mainView;if(t){var n=e.state;if(n||(n={viewIndex:i.views.indexOf(t),url:t.history[0]}),!(n.viewIndex<0)){var r,s=i.views[n.viewIndex],o=n&&n.url||void 0,l=n&&n.content||void 0,p=n&&n.pageName||void 0;i.params.pushStateNoAnimation===!0&&(r=!1),o!==s.url&&(s.history.indexOf(o)>=0?s.allowPageChange?i.router.back(s,{url:void 0,animatePages:r,pushState:!1,preloadOnly:!1}):i.pushStateQueue.push({action:"back",view:s}):l?s.allowPageChange?i.router.load(s,{content:l,animatePages:r,pushState:!1}):i.pushStateQueue.unshift({action:"loadContent",stateContent:l,view:s}):p?s.allowPageChange?i.router.load(s,{pageName:p,url:o,animatePages:r,pushState:!1}):i.pushStateQueue.unshift({action:"loadPageName",statePageName:p,view:s}):s.allowPageChange?i.router.load(s,{url:o,animatePages:r,pushState:!1}):i.pushStateQueue.unshift({action:"loadPage",stateUrl:o,view:s}))}}}}var a=!0;o(window).on("load",function(){setTimeout(function(){a=!1},0)}),document.readyState&&"complete"===document.readyState&&(a=!1),o(window).on("popstate",e)},i.swiper=function(e,a){return new Swiper(e,a)},i.initPageSwiper=function(e){function a(a){function t(){a.destroy(),e.off("pageBeforeRemove",t)}e.on("pageBeforeRemove",t)}e=o(e);var t=e.find(".swiper-init, .tabs-swipeable-wrap");0!==t.length&&t.each(function(){var e=o(this);e.hasClass("tabs-swipeable-wrap")&&e.addClass("swiper-container").children(".tabs").addClass("swiper-wrapper").children(".tab").addClass("swiper-slide");var t;t=e.data("swiper")?JSON.parse(e.data("swiper")):e.dataset(),e.hasClass("tabs-swipeable-wrap")&&(t.onSlideChangeStart=function(e){i.showTab(e.slides.eq(e.activeIndex))});var n=i.swiper(e[0],t);a(n)})},i.reinitPageSwiper=function(e){e=o(e);var a=e.find(".swiper-init, .tabs-swipeable-wrap");if(0!==a.length)for(var t=0;t<a.length;t++){var n=a[0].swiper;n&&n.update(!0)}};var v=function(e){var a=this,t={photos:[],initialSlide:0,spaceBetween:20,speed:300,zoom:!0,maxZoom:3,minZoom:1,exposition:!0,expositionHideCaptions:!1,type:"standalone",navbar:!0,toolbar:!0,theme:"light",swipeToClose:!0,backLinkText:"Close",ofText:"of",loop:!1,lazyLoading:!1,lazyLoadingInPrevNext:!1,lazyLoadingOnTransitionStart:!1,material:i.params.material,materialPreloaderSvg:i.params.materialPreloaderSvg,materialPreloaderHtml:i.params.materialPreloaderHtml};e=e||{},!e.backLinkText&&i.params.material&&(t.backLinkText="");for(var n in t)"undefined"==typeof e[n]&&(e[n]=t[n]);a.params=e,a.params.iconsColorClass=a.params.iconsColor?"color-"+a.params.iconsColor:"dark"===a.params.theme?"color-white":"",a.params.preloaderColorClass="dark"===a.params.theme?"preloader-white":"";var r=a.params.photoTemplate||'<div class="photo-browser-slide swiper-slide"><span class="photo-browser-zoom-container"><img src="{{js "this.url || this"}}"></span></div>',s=a.params.lazyPhotoTemplate||'<div class="photo-browser-slide photo-browser-slide-lazy swiper-slide"><div class="preloader {{@root.preloaderColorClass}}">{{#if @root.material}}{{@root.materialPreloaderHtml}}{{/if}}</div><span class="photo-browser-zoom-container"><img data-src="{{js "this.url || this"}}" class="swiper-lazy"></span></div>',p=a.params.objectTemplate||'<div class="photo-browser-slide photo-browser-object-slide swiper-slide">{{js "this.html || this"}}</div>',d=a.params.captionTemplate||'<div class="photo-browser-caption" data-caption-index="{{@index}}">{{caption}}</div>',c=a.params.navbarTemplate||'<div class="navbar"><div class="navbar-inner"><div class="left sliding"><a href="#" class="link close-popup photo-browser-close-link {{#unless backLinkText}}icon-only{{/unless}} {{js "this.type === \'page\' ? \'back\' : \'\'"}}"><i class="icon icon-back {{iconsColorClass}}"></i>{{#if backLinkText}}<span>{{backLinkText}}</span>{{/if}}</a></div><div class="center sliding"><span class="photo-browser-current"></span> <span class="photo-browser-of">{{ofText}}</span> <span class="photo-browser-total"></span></div><div class="right"></div></div></div>',u=a.params.toolbarTemplate||'<div class="toolbar tabbar"><div class="toolbar-inner"><a href="#" class="link photo-browser-prev"><i class="icon icon-prev {{iconsColorClass}}"></i></a><a href="#" class="link photo-browser-next"><i class="icon icon-next {{iconsColorClass}}"></i></a></div></div>',m=l.compile('<div class="photo-browser photo-browser-{{theme}}"><div class="view navbar-fixed toolbar-fixed">{{#unless material}}{{#if navbar}}'+c+'{{/if}}{{/unless}}<div class="page no-toolbar {{#unless navbar}}no-navbar{{/unless}} toolbar-fixed navbar-fixed" data-page="photo-browser-slides">{{#if material}}{{#if navbar}}'+c+"{{/if}}{{/if}}{{#if toolbar}}"+u+'{{/if}}<div class="photo-browser-captions photo-browser-captions-{{js "this.captionsTheme || this.theme"}}">{{#each photos}}{{#if caption}}'+d+"{{/if}}{{/each}}</div><div class=\"photo-browser-swiper-container swiper-container\"><div class=\"photo-browser-swiper-wrapper swiper-wrapper\">{{#each photos}}{{#js_compare \"this.html || ((typeof this === 'string' || this instanceof String) && (this.indexOf('<') >= 0 || this.indexOf('>') >= 0))\"}}"+p+"{{else}}{{#if @root.lazyLoading}}"+s+"{{else}}"+r+"{{/if}}{{/js_compare}}{{/each}}</div></div></div></div></div>")(a.params);a.activeIndex=a.params.initialSlide,a.openIndex=a.activeIndex,a.opened=!1,a.open=function(e){return"undefined"==typeof e&&(e=a.activeIndex),e=parseInt(e,10),a.opened&&a.swiper?void a.swiper.slideTo(e):(a.opened=!0,a.openIndex=e,"standalone"===a.params.type&&o("body").append(m),"popup"===a.params.type&&(a.popup=i.popup('<div class="popup photo-browser-popup">'+m+"</div>"),o(a.popup).on("closed",a.onPopupClose)),"page"===a.params.type?(o(document).on("pageBeforeInit",a.onPageBeforeInit),o(document).on("pageBeforeRemove",a.onPageBeforeRemove),a.params.view||(a.params.view=i.mainView),void a.params.view.loadContent(m)):(a.layout(a.openIndex),void(a.params.onOpen&&a.params.onOpen(a))))},a.close=function(){a.opened=!1,a.swiperContainer&&0!==a.swiperContainer.length&&(a.params.onClose&&a.params.onClose(a),a.attachEvents(!0),"standalone"===a.params.type&&a.container.removeClass("photo-browser-in").addClass("photo-browser-out").animationEnd(function(){a.container.remove()}),a.swiper.destroy(),a.swiper=a.swiperContainer=a.swiperWrapper=a.slides=f=h=g=void 0)},a.onPopupClose=function(e){a.close(),o(a.popup).off("pageBeforeInit",a.onPopupClose)},a.onPageBeforeInit=function(e){"photo-browser-slides"===e.detail.page.name&&a.layout(a.openIndex),o(document).off("pageBeforeInit",a.onPageBeforeInit)},a.onPageBeforeRemove=function(e){"photo-browser-slides"===e.detail.page.name&&a.close(),o(document).off("pageBeforeRemove",a.onPageBeforeRemove)},a.onSliderTransitionStart=function(e){a.activeIndex=e.activeIndex;var t=e.activeIndex+1,n=e.slides.length;if(a.params.loop&&(n-=2,t-=e.loopedSlides,1>t&&(t=n+t),t>n&&(t-=n)),a.container.find(".photo-browser-current").text(t),a.container.find(".photo-browser-total").text(n),o(".photo-browser-prev, .photo-browser-next").removeClass("photo-browser-link-inactive"),e.isBeginning&&!a.params.loop&&o(".photo-browser-prev").addClass("photo-browser-link-inactive"),e.isEnd&&!a.params.loop&&o(".photo-browser-next").addClass("photo-browser-link-inactive"),a.captions.length>0){a.captionsContainer.find(".photo-browser-caption-active").removeClass("photo-browser-caption-active");var r=a.params.loop?e.slides.eq(e.activeIndex).attr("data-swiper-slide-index"):a.activeIndex;a.captionsContainer.find('[data-caption-index="'+r+'"]').addClass("photo-browser-caption-active")}var i=e.slides.eq(e.previousIndex).find("video");i.length>0&&"pause"in i[0]&&i[0].pause(),a.params.onTransitionStart&&a.params.onTransitionStart(e)},a.onSliderTransitionEnd=function(e){a.params.zoom&&f&&e.previousIndex!==e.activeIndex&&(h.transform("translate3d(0,0,0) scale(1)"),g.transform("translate3d(0,0,0)"),f=h=g=void 0,v=b=1),a.params.onTransitionEnd&&a.params.onTransitionEnd(e)},a.layout=function(e){"page"===a.params.type?a.container=o(".photo-browser-swiper-container").parents(".view"):a.container=o(".photo-browser"),"standalone"===a.params.type&&(a.container.addClass("photo-browser-in"),i.sizeNavbars(a.container)),a.swiperContainer=a.container.find(".photo-browser-swiper-container"),a.swiperWrapper=a.container.find(".photo-browser-swiper-wrapper"),a.slides=a.container.find(".photo-browser-slide"),a.captionsContainer=a.container.find(".photo-browser-captions"),a.captions=a.container.find(".photo-browser-caption");var t={nextButton:a.params.nextButton||".photo-browser-next",prevButton:a.params.prevButton||".photo-browser-prev",indexButton:a.params.indexButton,initialSlide:e,spaceBetween:a.params.spaceBetween,speed:a.params.speed,loop:a.params.loop,lazyLoading:a.params.lazyLoading,lazyLoadingInPrevNext:a.params.lazyLoadingInPrevNext,lazyLoadingOnTransitionStart:a.params.lazyLoadingOnTransitionStart,preloadImages:a.params.lazyLoading?!1:!0,onTap:function(e,t){a.params.onTap&&a.params.onTap(e,t)},onClick:function(e,t){a.params.exposition&&a.toggleExposition(),a.params.onClick&&a.params.onClick(e,t)},onDoubleTap:function(e,t){a.toggleZoom(t),a.params.onDoubleTap&&a.params.onDoubleTap(e,t)},onTransitionStart:function(e){a.onSliderTransitionStart(e)},onTransitionEnd:function(e){a.onSliderTransitionEnd(e)},onSlideChangeStart:a.params.onSlideChangeStart,onSlideChangeEnd:a.params.onSlideChangeEnd,onLazyImageLoad:function(e,t,n){a.params.onLazyImageLoad&&a.params.onLazyImageLoad(a,t,n)},onLazyImageReady:function(e,t,n){o(t).removeClass("photo-browser-slide-lazy"),a.params.onLazyImageReady&&a.params.onLazyImageReady(a,t,n)}};a.params.swipeToClose&&"page"!==a.params.type&&(t.onTouchStart=a.swipeCloseTouchStart,t.onTouchMoveOpposite=a.swipeCloseTouchMove,t.onTouchEnd=a.swipeCloseTouchEnd),a.swiper=i.swiper(a.swiperContainer,t),0===e&&a.onSliderTransitionStart(a.swiper),a.attachEvents()},a.attachEvents=function(e){var t=e?"off":"on";if(a.params.zoom){var n=a.params.loop?a.swiper.slides:a.slides;n[t]("gesturestart",a.onSlideGestureStart),n[t]("gesturechange",a.onSlideGestureChange),n[t]("gestureend",a.onSlideGestureEnd),n[t](i.touchEvents.start,a.onSlideTouchStart),n[t](i.touchEvents.move,a.onSlideTouchMove),n[t](i.touchEvents.end,a.onSlideTouchEnd)}a.container.find(".photo-browser-close-link")[t]("click",a.close)};a.exposed=!1,a.toggleExposition=function(){a.container&&a.container.toggleClass("photo-browser-exposed"),a.params.expositionHideCaptions&&a.captionsContainer.toggleClass("photo-browser-captions-exposed"),a.exposed=!a.exposed},a.enableExposition=function(){a.container&&a.container.addClass("photo-browser-exposed"),a.params.expositionHideCaptions&&a.captionsContainer.addClass("photo-browser-captions-exposed"),a.exposed=!0},a.disableExposition=function(){a.container&&a.container.removeClass("photo-browser-exposed"),a.params.expositionHideCaptions&&a.captionsContainer.removeClass("photo-browser-captions-exposed"),a.exposed=!1};var f,h,g,v=1,b=1,w=!1;a.onSlideGestureStart=function(e){return f&&f.length||(f=o(this),0===f.length&&(f=a.swiper.slides.eq(a.swiper.activeIndex)),h=f.find("img, svg, canvas"),g=h.parent(".photo-browser-zoom-container"),0!==g.length)?(h.transition(0),void(w=!0)):void(h=void 0)},a.onSlideGestureChange=function(e){h&&0!==h.length&&(v=e.scale*b,v>a.params.maxZoom&&(v=a.params.maxZoom-1+Math.pow(v-a.params.maxZoom+1,.5)),v<a.params.minZoom&&(v=a.params.minZoom+1-Math.pow(a.params.minZoom-v+1,.5)),h.transform("translate3d(0,0,0) scale("+v+")"))},a.onSlideGestureEnd=function(e){h&&0!==h.length&&(v=Math.max(Math.min(v,a.params.maxZoom),a.params.minZoom),h.transition(a.params.speed).transform("translate3d(0,0,0) scale("+v+")"),b=v,w=!1,1===v&&(f=void 0))},a.toggleZoom=function(e){if(f||(f=a.swiper.slides.eq(a.swiper.activeIndex),h=f.find("img, svg, canvas"),g=h.parent(".photo-browser-zoom-container")),h&&0!==h.length){var t,n,r,i,s,o,l,p,d,c,u,m,w,C,y,x;"undefined"==typeof A.x&&e?(t="touchend"===e.type?e.changedTouches[0].pageX:e.pageX,n="touchend"===e.type?e.changedTouches[0].pageY:e.pageY):(t=A.x,n=A.y),v&&1!==v?(v=b=1,g.transition(300).transform("translate3d(0,0,0)"),h.transition(300).transform("translate3d(0,0,0) scale(1)"),f=void 0):(v=b=a.params.maxZoom,e?(r=a.container.offset().left,i=a.container.offset().top,s=r+a.container[0].offsetWidth/2-t,o=i+a.container[0].offsetHeight/2-n,d=h[0].offsetWidth,c=h[0].offsetHeight,u=d*v,m=c*v,w=Math.min(a.swiper.width/2-u/2,0),C=Math.min(a.swiper.height/2-m/2,0),y=-w,x=-C,l=s*v,p=o*v,w>l&&(l=w),l>y&&(l=y),C>p&&(p=C),p>x&&(p=x)):(l=0,p=0),g.transition(300).transform("translate3d("+l+"px, "+p+"px,0)"),h.transition(300).transform("translate3d(0,0,0) scale("+v+")"))}};var C,y,x,T,k,P,S,M,I,O,E,D,L,B,z,N,H,A={},R={};a.onSlideTouchStart=function(e){h&&0!==h.length&&(C||("android"===i.device.os&&e.preventDefault(),C=!0,A.x="touchstart"===e.type?e.targetTouches[0].pageX:e.pageX,A.y="touchstart"===e.type?e.targetTouches[0].pageY:e.pageY))},a.onSlideTouchMove=function(e){if(h&&0!==h.length&&(a.swiper.allowClick=!1,C&&f)){y||(I=h[0].offsetWidth,O=h[0].offsetHeight,E=o.getTranslate(g[0],"x")||0,D=o.getTranslate(g[0],"y")||0,g.transition(0));var t=I*v,n=O*v;if(!(t<a.swiper.width&&n<a.swiper.height)){if(k=Math.min(a.swiper.width/2-t/2,0),S=-k,P=Math.min(a.swiper.height/2-n/2,0),M=-P,R.x="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,R.y="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY,!y&&!w&&(Math.floor(k)===Math.floor(E)&&R.x<A.x||Math.floor(S)===Math.floor(E)&&R.x>A.x))return void(C=!1);e.preventDefault(),e.stopPropagation(),y=!0,x=R.x-A.x+E,T=R.y-A.y+D,k>x&&(x=k+1-Math.pow(k-x+1,.8)),x>S&&(x=S-1+Math.pow(x-S+1,.8)),P>T&&(T=P+1-Math.pow(P-T+1,.8)),T>M&&(T=M-1+Math.pow(T-M+1,.8)),L||(L=R.x),N||(N=R.y),B||(B=Date.now()),z=(R.x-L)/(Date.now()-B)/2,H=(R.y-N)/(Date.now()-B)/2,Math.abs(R.x-L)<2&&(z=0),Math.abs(R.y-N)<2&&(H=0),L=R.x,N=R.y,B=Date.now(),g.transform("translate3d("+x+"px, "+T+"px,0)")}}},a.onSlideTouchEnd=function(e){if(h&&0!==h.length){if(!C||!y)return C=!1,void(y=!1);C=!1,y=!1;var t=300,n=300,r=z*t,i=x+r,s=H*n,o=T+s;0!==z&&(t=Math.abs((i-x)/z)),0!==H&&(n=Math.abs((o-T)/H));var l=Math.max(t,n);x=i,T=o;var p=I*v,d=O*v;k=Math.min(a.swiper.width/2-p/2,0),S=-k,P=Math.min(a.swiper.height/2-d/2,0),M=-P,x=Math.max(Math.min(x,S),k),T=Math.max(Math.min(T,M),P),g.transition(l).transform("translate3d("+x+"px, "+T+"px,0)")}};var V,q,F,Y,G,j=!1,W=!0,X=!1;return a.swipeCloseTouchStart=function(e,a){W&&(j=!0)},a.swipeCloseTouchMove=function(e,t){if(j){X||(X=!0,q="touchmove"===t.type?t.targetTouches[0].pageY:t.pageY,Y=a.swiper.slides.eq(a.swiper.activeIndex),G=(new Date).getTime()),t.preventDefault(),F="touchmove"===t.type?t.targetTouches[0].pageY:t.pageY,V=q-F;var n=1-Math.abs(V)/300;Y.transform("translate3d(0,"+-V+"px,0)"),a.swiper.container.css("opacity",n).transition(0)}},a.swipeCloseTouchEnd=function(e,t){if(j=!1,!X)return void(X=!1);X=!1,W=!1;var n=Math.abs(V),r=(new Date).getTime()-G;return 300>r&&n>20||r>=300&&n>100?void setTimeout(function(){"standalone"===a.params.type&&a.close(),"popup"===a.params.type&&i.closeModal(a.popup),a.params.onSwipeToClose&&a.params.onSwipeToClose(a),W=!0},0):(0!==n?Y.addClass("transitioning").transitionEnd(function(){W=!0,Y.removeClass("transitioning")}):W=!0,a.swiper.container.css("opacity","").transition(""),void Y.transform(""))},a};i.photoBrowser=function(e){return new v(e)};var b=function(e){function a(e){var a=r.input.val();r.params.source&&r.params.source(r,a,function(e){var t="",n=r.params.limit?Math.min(r.params.limit,e.length):e.length;r.items=e;var i,s=new RegExp("("+a+")","i");for(i=0;n>i;i++){var o="object"==typeof e[i]?e[i][r.params.valueProperty]:e[i];t+=r.dropdownItemTemplate({value:o,text:("object"!=typeof e[i]?e[i]:e[i][r.params.textProperty]).replace(s,"<b>$1</b>")})}""===t&&""===a&&r.params.dropdownPlaceholderText&&(t+=r.dropdownPlaceholderTemplate({text:r.params.dropdownPlaceholderText})),r.dropdown.find("ul").html(t)})}function t(e){for(var a,t=o(this),n=0;n<r.items.length;n++){var i="object"==typeof r.items[n]?r.items[n][r.params.valueProperty]:r.items[n],s=t.attr("data-value");(i===s||1*i===1*s)&&(a=r.items[n])}r.params.updateInputValueOnSelect&&(r.input.val("object"==typeof a?a[r.params.textProperty]:a),r.input.trigger("input change")),r.params.onChange&&r.params.onChange(r,a),r.close()}function n(e){var a=o(e.target);a.is(r.input[0])||r.dropdown&&a.parents(r.dropdown[0]).length>0||r.close()}var r=this,s={popupCloseText:"Close",backText:"Back",searchbarPlaceholderText:"Search...",searchbarCancelText:"Cancel",openIn:"page",backOnSelect:!1,notFoundText:"Nothing found",valueProperty:"id",textProperty:"text",updateInputValueOnSelect:!0,expandInput:!1,preloaderColor:!1,preloader:!1};e=e||{};for(var p in s)"undefined"==typeof e[p]&&(e[p]=s[p]);r.params=e,r.params.opener&&(r.opener=o(r.params.opener));var d=r.params.view;if(!r.params.view&&r.opener&&r.opener.length){if(d=r.opener.parents("."+i.params.viewClass),0===d.length)return;d=d[0].f7View}if(!r.params.input||(r.input=o(r.params.input),0!==r.input.length||"dropdown"!==r.params.openIn)){r.value=r.params.value||[],r.id=(new Date).getTime(),r.inputType=r.params.multiple?"checkbox":"radio",r.inputName=r.inputType+"-"+r.id;var c=i.params.material,u=r.params.backOnSelect;if("dropdown"!==r.params.openIn){r.itemTemplate=l.compile(r.params.itemTemplate||'<li><label class="label-{{inputType}} item-content"><input type="{{inputType}}" name="{{inputName}}" value="{{value}}" {{#if selected}}checked{{/if}}>{{#if material}}<div class="item-media"><i class="icon icon-form-{{inputType}}"></i></div><div class="item-inner"><div class="item-title">{{text}}</div></div>{{else}}{{#if checkbox}}<div class="item-media"><i class="icon icon-form-checkbox"></i></div>{{/if}}<div class="item-inner"><div class="item-title">{{text}}</div></div>{{/if}}</label></li>');var m=r.params.pageTitle||"";!m&&r.opener&&r.opener.length&&(m=r.opener.find(".item-title").text());var f,h,g="autocomplete-"+r.inputName,v=r.params.navbarTheme,b=r.params.formTheme,w="",C="";r.navbarTemplate=l.compile(r.params.navbarTemplate||'<div class="navbar {{#if navbarTheme}}theme-{{navbarTheme}}{{/if}}"><div class="navbar-inner"><div class="left sliding">{{#if material}}<a href="#" class="link {{#if inPopup}}close-popup{{else}}back{{/if}} icon-only"><i class="icon icon-back"></i></a>{{else}}<a href="#" class="link {{#if inPopup}}close-popup{{else}}back{{/if}}"><i class="icon icon-back"></i>{{#if inPopup}}<span>{{popupCloseText}}</span>{{else}}<span>{{backText}}</span>{{/if}}</a>{{/if}}</div><div class="center sliding">{{pageTitle}}</div>{{#if preloader}}<div class="right"><div class="autocomplete-preloader preloader {{#if preloaderColor}}preloader-{{preloaderColor}}{{/if}}"></div></div>{{/if}}</div></div>'),f=r.navbarTemplate({pageTitle:m,backText:r.params.backText,popupCloseText:r.params.popupCloseText,openIn:r.params.openIn,navbarTheme:v,inPopup:"popup"===r.params.openIn,inPage:"page"===r.params.openIn,material:c,preloader:r.params.preloader,preloaderColor:r.params.preloaderColor}),"page"===r.params.openIn?(h="static",r.opener?(r.opener.parents(".navbar-through").length>0&&(h="through"),r.opener.parents(".navbar-fixed").length>0&&(h="fixed"),C=r.opener.parents(".page").hasClass("no-toolbar")?"no-toolbar":"",w=r.opener.parents(".page").hasClass("no-navbar")?"no-navbar":"navbar-"+h):d.container&&((o(d.container).hasClass("navbar-through")||o(d.container).find(".navbar-through").length>0)&&(h="through"),(o(d.container).hasClass("navbar-fixed")||o(d.container).find(".navbar-fixed").length>0)&&(h="fixed"),C=o(d.activePage.container).hasClass("no-toolbar")?"no-toolbar":"",w=o(d.activePage.container).hasClass("no-navbar")?"no-navbar":"navbar-"+h)):h="fixed";var y='<form class="searchbar"><div class="searchbar-input"><input type="search" placeholder="'+r.params.searchbarPlaceholderText+'"><a href="#" class="searchbar-clear"></a></div>'+(c?"":'<a href="#" class="searchbar-cancel">'+r.params.searchbarCancelText+"</a>")+'</form><div class="searchbar-overlay"></div>',x=("through"===h?f:"")+'<div class="pages"><div data-page="'+g+'" class="page autocomplete-page '+w+" "+C+'">'+("fixed"===h?f:"")+y+'<div class="page-content">'+("static"===h?f:"")+'<div class="list-block autocomplete-found autocomplete-list-'+r.id+" "+(b?"theme-"+b:"")+'"><ul></ul></div><div class="list-block autocomplete-not-found"><ul><li class="item-content"><div class="item-inner"><div class="item-title">'+r.params.notFoundText+'</div></div></li></ul></div><div class="list-block autocomplete-values"><ul></ul></div></div></div></div>'}else r.dropdownItemTemplate=l.compile(r.params.dropdownItemTemplate||'<li><label class="{{#unless placeholder}}label-radio{{/unless}} item-content" data-value="{{value}}"><div class="item-inner"><div class="item-title">{{text}}</div></div></label></li>'),r.dropdownPlaceholderTemplate=l.compile(r.params.dropdownPlaceholderTemplate||'<li class="autocomplete-dropdown-placeholder"><div class="item-content"><div class="item-inner"><div class="item-title">{{text}}</div></div></label></li>'),r.dropdownTemplate=l.compile(r.params.dropdownTemplate||'<div class="autocomplete-dropdown"><div class="autocomplete-dropdown-inner"><div class="list-block"><ul></ul></div></div>{{#if preloader}}<div class="autocomplete-preloader preloader {{#if preloaderColor}}preloader-{{preloaderColor}}{{/if}}">{{#if material}}{{materialPreloaderHtml}}{{/if}}</div>{{/if}}</div>');return r.popup=void 0,r.dropdown=void 0,r.positionDropDown=function(){var e=r.input.parents(".list-block"),a=r.input.parents(".page-content"),t=(parseInt(a.css("padding-top"),10),parseInt(a.css("padding-top"),10)),n=r.input.offset(),i=e.length>0?e.offset():0,s=a[0].scrollHeight-t-(n.top+a[0].scrollTop)-r.input[0].offsetHeight;r.dropdown.css({left:(e.length>0?i.left:n.left)+"px",top:n.top+a[0].scrollTop+r.input[0].offsetHeight+"px",width:(e.length>0?e[0].offsetWidth:r.input[0].offsetWidth)+"px"}),r.dropdown.children(".autocomplete-dropdown-inner").css({maxHeight:s+"px",paddingLeft:e.length>0&&!r.params.expandInput?n.left-(c?16:15)+"px":""})},r.pageInit=function(e){function a(){var e,a="";for(e=0;e<r.value.length;e++)a+=r.itemTemplate({value:"object"==typeof r.value[e]?r.value[e][r.params.valueProperty]:r.value[e],text:"object"==typeof r.value[e]?r.value[e][r.params.textProperty]:r.value[e],inputType:r.inputType,id:r.id,inputName:r.inputName+"-checked",checkbox:"checkbox"===r.inputType,material:c,selected:!0});n.find(".autocomplete-values ul").html(a)}var t=e.detail.page;if(r.page=o(t.container),r.pageData=t,t.name===g){var n=o(t.container),s=i.searchbar(n.find(".searchbar"),{customSearch:!0,onSearch:function(e,a){0===a.query.length&&e.active?e.overlay.addClass("searchbar-overlay-active"):e.overlay.removeClass("searchbar-overlay-active");var t,i;r.params.source&&r.params.source(r,a.query,function(e){var s="",o=r.params.limit?Math.min(r.params.limit,e.length):e.length;for(r.items=e,t=0;o>t;t++){var l=!1,p="object"==typeof e[t]?e[t][r.params.valueProperty]:e[t];for(i=0;i<r.value.length;i++){var d="object"==typeof r.value[i]?r.value[i][r.params.valueProperty]:r.value[i];(d===p||1*d===1*p)&&(l=!0)}s+=r.itemTemplate({value:p,text:"object"!=typeof e[t]?e[t]:e[t][r.params.textProperty],inputType:r.inputType,id:r.id,inputName:r.inputName,selected:l,checkbox:"checkbox"===r.inputType,material:c})}n.find(".autocomplete-found ul").html(s),0===e.length?0!==a.query.length?(n.find(".autocomplete-not-found").show(),n.find(".autocomplete-found, .autocomplete-values").hide()):(n.find(".autocomplete-values").show(),n.find(".autocomplete-found, .autocomplete-not-found").hide()):(n.find(".autocomplete-found").show(),n.find(".autocomplete-not-found, .autocomplete-values").hide())})}});r.searchbar=s,n.on("change",'input[type="radio"], input[type="checkbox"]',function(){var e,t,n,s,l=this,p=l.value,c=(o(l).parents("li").find(".item-title").text(),o(l).parents(".autocomplete-values").length>0);if(c){if("checkbox"===r.inputType&&!l.checked){for(e=0;e<r.value.length;e++)s="string"==typeof r.value[e]?r.value[e]:r.value[e][r.params.valueProperty],(s===p||1*s===1*p)&&r.value.splice(e,1);a(),r.params.onChange&&r.params.onChange(r,r.value)}}else{for(e=0;e<r.items.length;e++)n="string"==typeof r.items[e]?r.items[e]:r.items[e][r.params.valueProperty],(n===p||1*n===1*p)&&(t=r.items[e]);if("radio"===r.inputType)r.value=[t];else if(l.checked)r.value.push(t);else for(e=0;e<r.value.length;e++)s="string"==typeof r.value[e]?r.value[e]:r.value[e][r.params.valueProperty],(s===p||1*s===1*p)&&r.value.splice(e,1);a(),("radio"===r.inputType&&l.checked||"checkbox"===r.inputType)&&r.params.onChange&&r.params.onChange(r,r.value),u&&"radio"===r.inputType&&("popup"===r.params.openIn?i.closeModal(r.popup):d.router.back())}}),a(),r.params.onOpen&&r.params.onOpen(r)}},r.showPreloader=function(){"dropdown"===r.params.openIn?r.dropdown&&r.dropdown.find(".autocomplete-preloader").addClass("autocomplete-preloader-visible"):o(".autocomplete-preloader").addClass("autocomplete-preloader-visible")},r.hidePreloader=function(){"dropdown"===r.params.openIn?r.dropdown&&r.dropdown.find(".autocomplete-preloader").removeClass("autocomplete-preloader-visible"):o(".autocomplete-preloader").removeClass("autocomplete-preloader-visible")},r.open=function(){if(!r.opened)if(r.opened=!0,"dropdown"===r.params.openIn){r.dropdown||(r.dropdown=o(r.dropdownTemplate({preloader:r.params.preloader,
preloaderColor:r.params.preloaderColor,material:c,materialPreloaderHtml:i.params.materialPreloaderHtml})),r.dropdown.on("click","label",t));var e=r.input.parents(".list-block");e.length&&r.input.parents(".item-content").length>0&&r.params.expandInput&&r.input.parents(".item-content").addClass("item-content-dropdown-expand"),r.positionDropDown(),r.input.parents(".page-content").append(r.dropdown),r.dropdown.addClass("autocomplete-dropdown-in"),r.input.trigger("input"),o(window).on("resize",r.positionDropDown),r.params.onOpen&&r.params.onOpen(r)}else o(document).once("pageInit",".autocomplete-page",r.pageInit),"popup"===r.params.openIn?(r.popup=i.popup('<div class="popup autocomplete-popup autocomplete-popup-'+r.inputName+'"><div class="view navbar-fixed">'+x+"</div></div>"),r.popup=o(r.popup),r.popup.once("closed",function(){r.popup=void 0,r.opened=!1,r.params.onClose&&r.params.onClose(r)})):(d.router.load({content:x}),o(document).once("pageBack",".autocomplete-page",function(){r.opened=!1,r.params.onClose&&r.params.onClose(r)}))},r.close=function(e){if(r.opened){if("dropdown"===r.params.openIn){if(e&&"blur"===e.type&&r.dropdown.find("label.active-state").length>0)return;r.dropdown.removeClass("autocomplete-dropdown-in").remove(),r.input.parents(".item-content-dropdown-expand").removeClass("item-content-dropdown-expand"),r.opened=!1,o(window).off("resize",r.positionDropDown),r.params.onClose&&r.params.onClose(r)}"popup"===r.params.openIn&&r.popup&&i.closeModal(r.popup)}},r.initEvents=function(e){var t=e?"off":"on";"dropdown"!==r.params.openIn&&r.opener&&r.opener[t]("click",r.open),"dropdown"===r.params.openIn&&r.input&&(r.input[t]("focus",r.open),r.input[t]("input",a),i.device.android?o("html")[t]("click",n):r.input[t]("blur",r.close)),e&&r.dropdown&&(r.dropdown=null)},r.init=function(){r.initEvents()},r.destroy=function(){r.initEvents(!0),r=null},r.init(),r}};i.autocomplete=function(e){return new b(e)};var w=function(e){function a(){var e=!1;return p.params.convertToPopover||p.params.onlyInPopover?(!p.inline&&p.params.input&&(p.params.onlyInPopover?e=!0:i.device.ios?e=i.device.ipad?!0:!1:o(window).width()>=768&&(e=!0)),e):e}function t(){return p.opened&&p.container&&p.container.length>0&&p.container.parents(".popover").length>0?!0:!1}function n(){if(p.opened)for(var e=0;e<p.cols.length;e++)p.cols[e].divider||(p.cols[e].calcSize(),p.cols[e].setValue(p.cols[e].value,0,!1))}function r(e){if(e.preventDefault(),!p.opened&&(p.open(),p.params.scrollToInput&&!a())){var t=p.input.parents(".page-content");if(0===t.length)return;var n,r=parseInt(t.css("padding-top"),10),i=parseInt(t.css("padding-bottom"),10),s=t[0].offsetHeight-r-p.container.height(),o=t[0].scrollHeight-r-p.container.height(),l=p.input.offset().top-r+p.input[0].offsetHeight;if(l>s){var d=t.scrollTop()+l-s;d+s>o&&(n=d+s-o+i,s===o&&(n=p.container.height()),t.css({"padding-bottom":n+"px"})),t.scrollTop(d,300)}}}function s(e){t()||(p.input&&p.input.length>0?e.target!==p.input[0]&&0===o(e.target).parents(".picker-modal").length&&p.close():0===o(e.target).parents(".picker-modal").length&&p.close())}function l(){p.opened=!1,p.input&&p.input.length>0&&(p.input.parents(".page-content").css({"padding-bottom":""}),i.params.material&&p.input.trigger("blur")),p.params.onClose&&p.params.onClose(p),p.container.find(".picker-items-col").each(function(){p.destroyPickerCol(this)})}var p=this,d={updateValuesOnMomentum:!1,updateValuesOnTouchmove:!0,rotateEffect:!1,momentumRatio:7,freeMode:!1,closeByOutsideClick:!0,scrollToInput:!0,inputReadOnly:!0,convertToPopover:!0,onlyInPopover:!1,toolbar:!0,toolbarCloseText:"Done",toolbarTemplate:'<div class="toolbar"><div class="toolbar-inner"><div class="left"></div><div class="right"><a href="#" class="link close-picker">{{closeText}}</a></div></div></div>'};e=e||{};for(var c in d)"undefined"==typeof e[c]&&(e[c]=d[c]);p.params=e,p.cols=[],p.initialized=!1,p.inline=p.params.container?!0:!1;var u=i.device.ios||navigator.userAgent.toLowerCase().indexOf("safari")>=0&&navigator.userAgent.toLowerCase().indexOf("chrome")<0&&!i.device.android;return p.setValue=function(e,a){var t=0;if(0===p.cols.length)return p.value=e,void p.updateValue(e);for(var n=0;n<p.cols.length;n++)p.cols[n]&&!p.cols[n].divider&&(p.cols[n].setValue(e[t],a),t++)},p.updateValue=function(e){for(var a=e||[],t=[],n=0;n<p.cols.length;n++)p.cols[n].divider||(a.push(p.cols[n].value),t.push(p.cols[n].displayValue));a.indexOf(void 0)>=0||(p.value=a,p.displayValue=t,p.params.onChange&&p.params.onChange(p,p.value,p.displayValue),p.input&&p.input.length>0&&(o(p.input).val(p.params.formatValue?p.params.formatValue(p,p.value,p.displayValue):p.value.join(" ")),o(p.input).trigger("change")))},p.initPickerCol=function(e,a){function t(){w=o.requestAnimationFrame(function(){m.updateItems(void 0,void 0,0),t()})}function n(e){y||C||(e.preventDefault(),C=!0,x=T="touchstart"===e.type?e.targetTouches[0].pageY:e.pageY,k=(new Date).getTime(),L=!0,S=I=o.getTranslate(m.wrapper[0],"y"))}function r(e){if(C){e.preventDefault(),L=!1,T="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY,y||(o.cancelAnimationFrame(w),y=!0,S=I=o.getTranslate(m.wrapper[0],"y"),m.wrapper.transition(0)),e.preventDefault();var a=T-x;I=S+a,M=void 0,v>I&&(I=v-Math.pow(v-I,.8),M="min"),I>b&&(I=b+Math.pow(I-b,.8),M="max"),m.wrapper.transform("translate3d(0,"+I+"px,0)"),m.updateItems(void 0,I,0,p.params.updateValuesOnTouchmove),E=I-O||I,D=(new Date).getTime(),O=I}}function s(e){if(!C||!y)return void(C=y=!1);C=y=!1,m.wrapper.transition(""),M&&("min"===M?m.wrapper.transform("translate3d(0,"+v+"px,0)"):m.wrapper.transform("translate3d(0,"+b+"px,0)")),P=(new Date).getTime();var a,n;P-k>300?n=I:(a=Math.abs(E/(P-D)),n=I+E*p.params.momentumRatio),n=Math.max(Math.min(n,b),v);var r=-Math.floor((n-b)/h);p.params.freeMode||(n=-r*h+b),m.wrapper.transform("translate3d(0,"+parseInt(n,10)+"px,0)"),m.updateItems(r,n,"",!0),p.params.updateValuesOnMomentum&&(t(),m.wrapper.transitionEnd(function(){o.cancelAnimationFrame(w)})),setTimeout(function(){L=!0},100)}function l(e){if(L){o.cancelAnimationFrame(w);var a=o(this).attr("data-picker-value");m.setValue(a)}}var d=o(e),c=d.index(),m=p.cols[c];if(!m.divider){m.container=d,m.wrapper=m.container.find(".picker-items-col-wrapper"),m.items=m.wrapper.find(".picker-item");var f,h,g,v,b;m.replaceValues=function(e,a){m.destroyEvents(),m.values=e,m.displayValues=a;var t=p.columnHTML(m,!0);m.wrapper.html(t),m.items=m.wrapper.find(".picker-item"),m.calcSize(),m.setValue(m.values[0],0,!0),m.initEvents()},m.calcSize=function(){p.params.rotateEffect&&(m.container.removeClass("picker-items-col-absolute"),m.width||m.container.css({width:""}));var e,a;e=0,a=m.container[0].offsetHeight,f=m.wrapper[0].offsetHeight,h=m.items[0].offsetHeight,g=h*m.items.length,v=a/2-g+h/2,b=a/2-h/2,m.width&&(e=m.width,parseInt(e,10)===e&&(e+="px"),m.container.css({width:e})),p.params.rotateEffect&&(m.width||(m.items.each(function(){var a=o(this);a.css({width:"auto"}),e=Math.max(e,a[0].offsetWidth),a.css({width:""})}),m.container.css({width:e+2+"px"})),m.container.addClass("picker-items-col-absolute"))},m.calcSize(),m.wrapper.transform("translate3d(0,"+b+"px,0)").transition(0);var w;m.setValue=function(e,a,n){"undefined"==typeof a&&(a="");var r=m.wrapper.find('.picker-item[data-picker-value="'+e+'"]').index();if("undefined"!=typeof r&&-1!==r){var i=-r*h+b;m.wrapper.transition(a),m.wrapper.transform("translate3d(0,"+i+"px,0)"),p.params.updateValuesOnMomentum&&m.activeIndex&&m.activeIndex!==r&&(o.cancelAnimationFrame(w),m.wrapper.transitionEnd(function(){o.cancelAnimationFrame(w)}),t()),m.updateItems(r,i,a,n)}},m.updateItems=function(e,a,t,n){"undefined"==typeof a&&(a=o.getTranslate(m.wrapper[0],"y")),"undefined"==typeof e&&(e=-Math.round((a-b)/h)),0>e&&(e=0),e>=m.items.length&&(e=m.items.length-1);var r=m.activeIndex;m.activeIndex=e,m.wrapper.find(".picker-selected").removeClass("picker-selected"),m.items.transition(t);var i=m.items.eq(e).addClass("picker-selected").transform("");if(p.params.rotateEffect){(a-(Math.floor((a-b)/h)*h+b))/h;m.items.each(function(){var e=o(this),t=e.index()*h,n=b-a,r=t-n,i=r/h,s=Math.ceil(m.height/h/2)+1,l=-18*i;l>180&&(l=180),-180>l&&(l=-180),Math.abs(i)>s?e.addClass("picker-item-far"):e.removeClass("picker-item-far"),e.transform("translate3d(0, "+(-a+b)+"px, "+(u?-110:0)+"px) rotateX("+l+"deg)")})}(n||"undefined"==typeof n)&&(m.value=i.attr("data-picker-value"),m.displayValue=m.displayValues?m.displayValues[e]:m.value,r!==e&&(m.onChange&&m.onChange(p,m.value,m.displayValue),p.updateValue()))},a&&m.updateItems(0,b,0);var C,y,x,T,k,P,S,M,I,O,E,D,L=!0;m.initEvents=function(e){var a=e?"off":"on";m.container[a](i.touchEvents.start,n),m.container[a](i.touchEvents.move,r),m.container[a](i.touchEvents.end,s),m.items[a]("click",l)},m.destroyEvents=function(){m.initEvents(!0)},m.container[0].f7DestroyPickerCol=function(){m.destroyEvents()},m.initEvents()}},p.destroyPickerCol=function(e){e=o(e),"f7DestroyPickerCol"in e[0]&&e[0].f7DestroyPickerCol()},o(window).on("resize",n),p.columnHTML=function(e,a){var t="",n="";if(e.divider)n+='<div class="picker-items-col picker-items-col-divider '+(e.textAlign?"picker-items-col-"+e.textAlign:"")+" "+(e.cssClass||"")+'">'+e.content+"</div>";else{for(var r=0;r<e.values.length;r++)t+='<div class="picker-item" data-picker-value="'+e.values[r]+'">'+(e.displayValues?e.displayValues[r]:e.values[r])+"</div>";n+='<div class="picker-items-col '+(e.textAlign?"picker-items-col-"+e.textAlign:"")+" "+(e.cssClass||"")+'"><div class="picker-items-col-wrapper">'+t+"</div></div>"}return a?t:n},p.layout=function(){var e,a="",t="";p.cols=[];var n="";for(e=0;e<p.params.cols.length;e++){var r=p.params.cols[e];n+=p.columnHTML(p.params.cols[e]),p.cols.push(r)}t="picker-modal picker-columns "+(p.params.cssClass||"")+(p.params.rotateEffect?" picker-3d":""),a='<div class="'+t+'">'+(p.params.toolbar?p.params.toolbarTemplate.replace(/{{closeText}}/g,p.params.toolbarCloseText):"")+'<div class="picker-modal-inner picker-items">'+n+'<div class="picker-center-highlight"></div></div></div>',p.pickerHTML=a},p.params.input&&(p.input=o(p.params.input),p.input.length>0&&(p.params.inputReadOnly&&p.input.prop("readOnly",!0),p.inline||p.input.on("click",r),p.params.inputReadOnly&&p.input.on("focus mousedown",function(e){e.preventDefault()}))),!p.inline&&p.params.closeByOutsideClick&&o("html").on("click",s),p.opened=!1,p.open=function(){var e=a();p.opened||(p.layout(),e?(p.pickerHTML='<div class="popover popover-picker-columns"><div class="popover-inner">'+p.pickerHTML+"</div></div>",p.popover=i.popover(p.pickerHTML,p.params.input,!0),p.container=o(p.popover).find(".picker-modal"),o(p.popover).on("close",function(){l()})):p.inline?(p.container=o(p.pickerHTML),p.container.addClass("picker-modal-inline"),o(p.params.container).append(p.container)):(p.container=o(i.pickerModal(p.pickerHTML)),o(p.container).on("close",function(){l()})),p.container[0].f7Picker=p,p.container.find(".picker-items-col").each(function(){var e=!0;(!p.initialized&&p.params.value||p.initialized&&p.value)&&(e=!1),p.initPickerCol(this,e)}),p.initialized?p.value&&p.setValue(p.value,0):p.value?p.setValue(p.value,0):p.params.value&&p.setValue(p.params.value,0),p.input&&p.input.length>0&&i.params.material&&p.input.trigger("focus")),p.opened=!0,p.initialized=!0,p.params.onOpen&&p.params.onOpen(p)},p.close=function(){return p.opened&&!p.inline?t()?void i.closeModal(p.popover):void i.closeModal(p.container):void 0},p.destroy=function(){p.close(),p.params.input&&p.input.length>0&&p.input.off("click focus",r),o("html").off("click",s),o(window).off("resize",n)},p.inline?p.open():!p.initialized&&p.params.value&&p.setValue(p.params.value),p};i.picker=function(e){return new w(e)};var C=function(e){function a(){var e=!1;return p.params.convertToPopover||p.params.onlyInPopover?(!p.inline&&p.params.input&&(p.params.onlyInPopover?e=!0:i.device.ios?e=i.device.ipad?!0:!1:o(window).width()>=768&&(e=!0)),e):e}function t(){return p.opened&&p.container&&p.container.length>0&&p.container.parents(".popover").length>0?!0:!1}function n(e){e=new Date(e);var a=e.getFullYear(),t=e.getMonth(),n=t+1,r=e.getDate(),i=e.getDay();return p.params.dateFormat.replace(/yyyy/g,a).replace(/yy/g,(a+"").substring(2)).replace(/mm/g,10>n?"0"+n:n).replace(/m(\W+)/g,n+"$1").replace(/MM/g,p.params.monthNames[t]).replace(/M(\W+)/g,p.params.monthNamesShort[t]+"$1").replace(/dd/g,10>r?"0"+r:r).replace(/d(\W+)/g,r+"$1").replace(/DD/g,p.params.dayNames[i]).replace(/D(\W+)/g,p.params.dayNamesShort[i]+"$1")}function r(e){if(e.preventDefault(),!p.opened&&(p.open(),p.params.scrollToInput&&!a()&&!i.params.material)){var t=p.input.parents(".page-content");if(0===t.length)return;var n,r=parseInt(t.css("padding-top"),10),s=parseInt(t.css("padding-bottom"),10),o=t[0].offsetHeight-r-p.container.height(),l=t[0].scrollHeight-r-p.container.height(),d=p.input.offset().top-r+p.input[0].offsetHeight;if(d>o){var c=t.scrollTop()+d-o;c+o>l&&(n=c+o-l+s,o===l&&(n=p.container.height()),t.css({"padding-bottom":n+"px"})),t.scrollTop(c,300)}}}function s(e){t()||(p.input&&p.input.length>0?e.target!==p.input[0]&&0===o(e.target).parents(".picker-modal").length&&p.close():0===o(e.target).parents(".picker-modal").length&&p.close())}function l(){p.opened=!1,p.input&&p.input.length>0&&(p.input.parents(".page-content").css({"padding-bottom":""}),i.params.material&&p.input.trigger("blur")),p.params.onClose&&p.params.onClose(p),p.destroyCalendarEvents()}var p=this,d={monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],monthNamesShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],dayNames:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],dayNamesShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],firstDay:1,weekendDays:[0,6],multiple:!1,rangePicker:!1,dateFormat:"yyyy-mm-dd",direction:"horizontal",minDate:null,maxDate:null,disabled:null,events:null,rangesClasses:null,touchMove:!0,animate:!0,closeOnSelect:!1,monthPicker:!0,monthPickerTemplate:'<div class="picker-calendar-month-picker"><a href="#" class="link icon-only picker-calendar-prev-month"><i class="icon icon-prev"></i></a><span class="current-month-value"></span><a href="#" class="link icon-only picker-calendar-next-month"><i class="icon icon-next"></i></a></div>',yearPicker:!0,yearPickerTemplate:'<div class="picker-calendar-year-picker"><a href="#" class="link icon-only picker-calendar-prev-year"><i class="icon icon-prev"></i></a><span class="current-year-value"></span><a href="#" class="link icon-only picker-calendar-next-year"><i class="icon icon-next"></i></a></div>',weekHeader:!0,closeByOutsideClick:!0,scrollToInput:!0,inputReadOnly:!0,convertToPopover:!0,onlyInPopover:!1,toolbar:!0,toolbarCloseText:"Done",headerPlaceholder:"Select date",header:i.params.material,footer:i.params.material,toolbarTemplate:'<div class="toolbar"><div class="toolbar-inner">{{monthPicker}}{{yearPicker}}</div></div>',headerTemplate:'<div class="picker-header"><div class="picker-calendar-selected-date">{{placeholder}}</div></div>',footerTemplate:'<div class="picker-footer"><a href="#" class="button close-picker">{{closeText}}</a></div>'};e=e||{};for(var c in d)"undefined"==typeof e[c]&&(e[c]=d[c]);p.params=e,p.initialized=!1,p.inline=p.params.container?!0:!1,p.isH="horizontal"===p.params.direction;var u=p.isH&&i.rtl?-1:1;return p.animating=!1,p.addValue=function(e){if(p.params.multiple){p.value||(p.value=[]);for(var a,t=0;t<p.value.length;t++)new Date(e).getTime()===new Date(p.value[t]).getTime()&&(a=t);"undefined"==typeof a?p.value.push(e):p.value.splice(a,1),p.updateValue()}else p.params.rangePicker?(p.value||(p.value=[]),(2===p.value.length||0===p.value.length)&&(p.value=[]),p.value[0]!==e?p.value.push(e):p.value=[],p.value.sort(function(e,a){return e-a}),p.updateValue()):(p.value=[e],p.updateValue())},p.setValue=function(e){p.value=e,p.updateValue()},p.updateValue=function(e){var a,t;if(p.container&&p.container.length>0){p.wrapper.find(".picker-calendar-day-selected").removeClass("picker-calendar-day-selected");var r;if(p.params.rangePicker&&2===p.value.length)for(a=p.value[0];a<=p.value[1];a+=864e5)r=new Date(a),p.wrapper.find('.picker-calendar-day[data-date="'+r.getFullYear()+"-"+r.getMonth()+"-"+r.getDate()+'"]').addClass("picker-calendar-day-selected");else for(a=0;a<p.value.length;a++)r=new Date(p.value[a]),p.wrapper.find('.picker-calendar-day[data-date="'+r.getFullYear()+"-"+r.getMonth()+"-"+r.getDate()+'"]').addClass("picker-calendar-day-selected")}if(p.params.onChange&&p.params.onChange(p,p.value),p.input&&p.input.length>0||i.params.material&&p.params.header){if(p.params.formatValue)t=p.params.formatValue(p,p.value);else{for(t=[],a=0;a<p.value.length;a++)t.push(n(p.value[a]));t=t.join(p.params.rangePicker?" - ":", ")}i.params.material&&p.params.header&&p.container&&p.container.length>0&&p.container.find(".picker-calendar-selected-date").text(t),p.input&&p.input.length>0&&!e&&(o(p.input).val(t),o(p.input).trigger("change"))}},p.initCalendarEvents=function(){function e(e){s||r||(r=!0,l=m="touchstart"===e.type?e.targetTouches[0].pageX:e.pageX,d=m="touchstart"===e.type?e.targetTouches[0].pageY:e.pageY,f=(new Date).getTime(),C=0,T=!0,x=void 0,g=v=p.monthsTranslate)}function a(e){if(r){if(c="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,m="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY,"undefined"==typeof x&&(x=!!(x||Math.abs(m-d)>Math.abs(c-l))),p.isH&&x)return void(r=!1);if(e.preventDefault(),p.animating)return void(r=!1);T=!1,s||(s=!0,b=p.wrapper[0].offsetWidth,w=p.wrapper[0].offsetHeight,p.wrapper.transition(0)),e.preventDefault(),y=p.isH?c-l:m-d,C=y/(p.isH?b:w),v=100*(p.monthsTranslate*u+C),p.wrapper.transform("translate3d("+(p.isH?v:0)+"%, "+(p.isH?0:v)+"%, 0)")}}function t(e){return r&&s?(r=s=!1,h=(new Date).getTime(),300>h-f?Math.abs(y)<10?p.resetMonth():y>=10?i.rtl?p.nextMonth():p.prevMonth():i.rtl?p.prevMonth():p.nextMonth():-.5>=C?i.rtl?p.prevMonth():p.nextMonth():C>=.5?i.rtl?p.nextMonth():p.prevMonth():p.resetMonth(),void setTimeout(function(){T=!0},100)):void(r=s=!1)}function n(e){if(T){var a=o(e.target).parents(".picker-calendar-day");if(0===a.length&&o(e.target).hasClass("picker-calendar-day")&&(a=o(e.target)),0!==a.length&&(!a.hasClass("picker-calendar-day-selected")||p.params.multiple||p.params.rangePicker)&&!a.hasClass("picker-calendar-day-disabled")){p.params.rangePicker||(a.hasClass("picker-calendar-day-next")&&p.nextMonth(),a.hasClass("picker-calendar-day-prev")&&p.prevMonth());var t=a.attr("data-year"),n=a.attr("data-month"),r=a.attr("data-day");p.params.onDayClick&&p.params.onDayClick(p,a[0],t,n,r),p.addValue(new Date(t,n,r).getTime()),p.params.closeOnSelect&&(p.params.rangePicker&&2===p.value.length||!p.params.rangePicker)&&p.close()}}}var r,s,l,d,c,m,f,h,g,v,b,w,C,y,x,T=!0;p.container.find(".picker-calendar-prev-month").on("click",p.prevMonth),p.container.find(".picker-calendar-next-month").on("click",p.nextMonth),p.container.find(".picker-calendar-prev-year").on("click",p.prevYear),p.container.find(".picker-calendar-next-year").on("click",p.nextYear),p.wrapper.on("click",n),p.params.touchMove&&(p.wrapper.on(i.touchEvents.start,e),p.wrapper.on(i.touchEvents.move,a),p.wrapper.on(i.touchEvents.end,t)),p.container[0].f7DestroyCalendarEvents=function(){p.container.find(".picker-calendar-prev-month").off("click",p.prevMonth),p.container.find(".picker-calendar-next-month").off("click",p.nextMonth),p.container.find(".picker-calendar-prev-year").off("click",p.prevYear),p.container.find(".picker-calendar-next-year").off("click",p.nextYear),p.wrapper.off("click",n),p.params.touchMove&&(p.wrapper.off(i.touchEvents.start,e),p.wrapper.off(i.touchEvents.move,a),p.wrapper.off(i.touchEvents.end,t))}},p.destroyCalendarEvents=function(e){"f7DestroyCalendarEvents"in p.container[0]&&p.container[0].f7DestroyCalendarEvents()},p.dateInRange=function(e,a){var t,n=!1;if(!a)return!1;if(o.isArray(a))for(t=0;t<a.length;t++)a[t].from||a[t].to?a[t].from&&a[t].to?e<=new Date(a[t].to).getTime()&&e>=new Date(a[t].from).getTime()&&(n=!0):a[t].from?e>=new Date(a[t].from).getTime()&&(n=!0):a[t].to&&e<=new Date(a[t].to).getTime()&&(n=!0):e===new Date(a[t]).getTime()&&(n=!0);else a.from||a.to?a.from&&a.to?e<=new Date(a.to).getTime()&&e>=new Date(a.from).getTime()&&(n=!0):a.from?e>=new Date(a.from).getTime()&&(n=!0):a.to&&e<=new Date(a.to).getTime()&&(n=!0):"function"==typeof a&&(n=a(new Date(e)));return n},p.daysInMonth=function(e){var a=new Date(e);return new Date(a.getFullYear(),a.getMonth()+1,0).getDate()},p.monthHTML=function(e,a){e=new Date(e);var t=e.getFullYear(),n=e.getMonth();e.getDate();"next"===a&&(e=11===n?new Date(t+1,0):new Date(t,n+1,1)),"prev"===a&&(e=0===n?new Date(t-1,11):new Date(t,n-1,1)),("next"===a||"prev"===a)&&(n=e.getMonth(),t=e.getFullYear());var r=p.daysInMonth(new Date(e.getFullYear(),e.getMonth()).getTime()-864e6),i=p.daysInMonth(e),s=new Date(e.getFullYear(),e.getMonth()).getDay();0===s&&(s=7);var o,l,d,c,u,m,f=[],h=6,g=7,v="",b=0+(p.params.firstDay-1),w=(new Date).setHours(0,0,0,0),C=p.params.minDate?new Date(p.params.minDate).getTime():null,y=p.params.maxDate?new Date(p.params.maxDate).getTime():null;if(p.value&&p.value.length)for(l=0;l<p.value.length;l++)f.push(new Date(p.value[l]).setHours(0,0,0,0));for(l=1;h>=l;l++){var x="";for(d=1;g>=d;d++){var T=d;b++;var k=b-s,P=T-1+p.params.firstDay>6?T-1-7+p.params.firstDay:T-1+p.params.firstDay,S="";if(0>k?(k=r+k+1,S+=" picker-calendar-day-prev",o=new Date(0>n-1?t-1:t,0>n-1?11:n-1,k).getTime()):(k+=1,k>i?(k-=i,S+=" picker-calendar-day-next",o=new Date(n+1>11?t+1:t,n+1>11?0:n+1,k).getTime()):o=new Date(t,n,k).getTime()),o===w&&(S+=" picker-calendar-day-today"),p.params.rangePicker&&2===f.length?o>=f[0]&&o<=f[1]&&(S+=" picker-calendar-day-selected"):f.indexOf(o)>=0&&(S+=" picker-calendar-day-selected"),p.params.weekendDays.indexOf(P)>=0&&(S+=" picker-calendar-day-weekend"),m=!1,p.params.events&&p.dateInRange(o,p.params.events)&&(m=!0),m&&(S+=" picker-calendar-day-has-events"),p.params.rangesClasses)for(c=0;c<p.params.rangesClasses.length;c++)p.dateInRange(o,p.params.rangesClasses[c].range)&&(S+=" "+p.params.rangesClasses[c].cssClass);u=!1,(C&&C>o||y&&o>y)&&(u=!0),p.params.disabled&&p.dateInRange(o,p.params.disabled)&&(u=!0),u&&(S+=" picker-calendar-day-disabled"),o=new Date(o);var M=o.getFullYear(),I=o.getMonth();x+='<div data-year="'+M+'" data-month="'+I+'" data-day="'+k+'" class="picker-calendar-day'+S+'" data-date="'+(M+"-"+I+"-"+k)+'"><span>'+k+"</span></div>"}v+='<div class="picker-calendar-row">'+x+"</div>"}return v='<div class="picker-calendar-month" data-year="'+t+'" data-month="'+n+'">'+v+"</div>"},p.animating=!1,p.updateCurrentMonthYear=function(e){"undefined"==typeof e?(p.currentMonth=parseInt(p.months.eq(1).attr("data-month"),10),p.currentYear=parseInt(p.months.eq(1).attr("data-year"),10)):(p.currentMonth=parseInt(p.months.eq("next"===e?p.months.length-1:0).attr("data-month"),10),p.currentYear=parseInt(p.months.eq("next"===e?p.months.length-1:0).attr("data-year"),10)),p.container.find(".current-month-value").text(p.params.monthNames[p.currentMonth]),p.container.find(".current-year-value").text(p.currentYear)},p.onMonthChangeStart=function(e){p.updateCurrentMonthYear(e),p.months.removeClass("picker-calendar-month-current picker-calendar-month-prev picker-calendar-month-next");var a="next"===e?p.months.length-1:0;p.months.eq(a).addClass("picker-calendar-month-current"),p.months.eq("next"===e?a-1:a+1).addClass("next"===e?"picker-calendar-month-prev":"picker-calendar-month-next"),p.params.onMonthYearChangeStart&&p.params.onMonthYearChangeStart(p,p.currentYear,p.currentMonth)},p.onMonthChangeEnd=function(e,a){p.animating=!1;var t,n,r;p.wrapper.find(".picker-calendar-month:not(.picker-calendar-month-prev):not(.picker-calendar-month-current):not(.picker-calendar-month-next)").remove(),"undefined"==typeof e&&(e="next",a=!0),a?(p.wrapper.find(".picker-calendar-month-next, .picker-calendar-month-prev").remove(),n=p.monthHTML(new Date(p.currentYear,p.currentMonth),"prev"),t=p.monthHTML(new Date(p.currentYear,p.currentMonth),"next")):r=p.monthHTML(new Date(p.currentYear,p.currentMonth),e),("next"===e||a)&&p.wrapper.append(r||t),("prev"===e||a)&&p.wrapper.prepend(r||n),p.months=p.wrapper.find(".picker-calendar-month"),p.setMonthsTranslate(p.monthsTranslate),p.params.onMonthAdd&&p.params.onMonthAdd(p,"next"===e?p.months.eq(p.months.length-1)[0]:p.months.eq(0)[0]),p.params.onMonthYearChangeEnd&&p.params.onMonthYearChangeEnd(p,p.currentYear,p.currentMonth)},p.setMonthsTranslate=function(e){e=e||p.monthsTranslate||0,"undefined"==typeof p.monthsTranslate&&(p.monthsTranslate=e),p.months.removeClass("picker-calendar-month-current picker-calendar-month-prev picker-calendar-month-next");var a=100*-(e+1)*u,t=100*-e*u,n=100*-(e-1)*u;p.months.eq(0).transform("translate3d("+(p.isH?a:0)+"%, "+(p.isH?0:a)+"%, 0)").addClass("picker-calendar-month-prev"),p.months.eq(1).transform("translate3d("+(p.isH?t:0)+"%, "+(p.isH?0:t)+"%, 0)").addClass("picker-calendar-month-current"),p.months.eq(2).transform("translate3d("+(p.isH?n:0)+"%, "+(p.isH?0:n)+"%, 0)").addClass("picker-calendar-month-next")},p.nextMonth=function(e){("undefined"==typeof e||"object"==typeof e)&&(e="",p.params.animate||(e=0));var a=parseInt(p.months.eq(p.months.length-1).attr("data-month"),10),t=parseInt(p.months.eq(p.months.length-1).attr("data-year"),10),n=new Date(t,a),r=n.getTime(),i=p.animating?!1:!0;if(p.params.maxDate&&r>new Date(p.params.maxDate).getTime())return p.resetMonth();if(p.monthsTranslate--,a===p.currentMonth){var s=100*-p.monthsTranslate*u,l=o(p.monthHTML(r,"next")).transform("translate3d("+(p.isH?s:0)+"%, "+(p.isH?0:s)+"%, 0)").addClass("picker-calendar-month-next");p.wrapper.append(l[0]),p.months=p.wrapper.find(".picker-calendar-month"),p.params.onMonthAdd&&p.params.onMonthAdd(p,p.months.eq(p.months.length-1)[0])}p.animating=!0,p.onMonthChangeStart("next");var d=100*p.monthsTranslate*u;p.wrapper.transition(e).transform("translate3d("+(p.isH?d:0)+"%, "+(p.isH?0:d)+"%, 0)"),i&&p.wrapper.transitionEnd(function(){p.onMonthChangeEnd("next")}),p.params.animate||p.onMonthChangeEnd("next")},p.prevMonth=function(e){("undefined"==typeof e||"object"==typeof e)&&(e="",p.params.animate||(e=0));var a=parseInt(p.months.eq(0).attr("data-month"),10),t=parseInt(p.months.eq(0).attr("data-year"),10),n=new Date(t,a+1,-1),r=n.getTime(),i=p.animating?!1:!0;if(p.params.minDate&&r<new Date(p.params.minDate).getTime())return p.resetMonth();if(p.monthsTranslate++,a===p.currentMonth){var s=100*-p.monthsTranslate*u,l=o(p.monthHTML(r,"prev")).transform("translate3d("+(p.isH?s:0)+"%, "+(p.isH?0:s)+"%, 0)").addClass("picker-calendar-month-prev");p.wrapper.prepend(l[0]),p.months=p.wrapper.find(".picker-calendar-month"),p.params.onMonthAdd&&p.params.onMonthAdd(p,p.months.eq(0)[0])}p.animating=!0,p.onMonthChangeStart("prev");var d=100*p.monthsTranslate*u;p.wrapper.transition(e).transform("translate3d("+(p.isH?d:0)+"%, "+(p.isH?0:d)+"%, 0)"),i&&p.wrapper.transitionEnd(function(){p.onMonthChangeEnd("prev")}),p.params.animate||p.onMonthChangeEnd("prev")},p.resetMonth=function(e){"undefined"==typeof e&&(e="");var a=100*p.monthsTranslate*u;p.wrapper.transition(e).transform("translate3d("+(p.isH?a:0)+"%, "+(p.isH?0:a)+"%, 0)")},p.setYearMonth=function(e,a,t){"undefined"==typeof e&&(e=p.currentYear),"undefined"==typeof a&&(a=p.currentMonth),("undefined"==typeof t||"object"==typeof t)&&(t="",p.params.animate||(t=0));var n;if(n=e<p.currentYear?new Date(e,a+1,-1).getTime():new Date(e,a).getTime(),p.params.maxDate&&n>new Date(p.params.maxDate).getTime())return!1;if(p.params.minDate&&n<new Date(p.params.minDate).getTime())return!1;var r=new Date(p.currentYear,p.currentMonth).getTime(),i=n>r?"next":"prev",s=p.monthHTML(new Date(e,a));p.monthsTranslate=p.monthsTranslate||0;var o,l,d=p.monthsTranslate,c=p.animating?!1:!0;n>r?(p.monthsTranslate--,p.animating||p.months.eq(p.months.length-1).remove(),p.wrapper.append(s),p.months=p.wrapper.find(".picker-calendar-month"),o=100*-(d-1)*u,p.months.eq(p.months.length-1).transform("translate3d("+(p.isH?o:0)+"%, "+(p.isH?0:o)+"%, 0)").addClass("picker-calendar-month-next")):(p.monthsTranslate++,p.animating||p.months.eq(0).remove(),p.wrapper.prepend(s),p.months=p.wrapper.find(".picker-calendar-month"),o=100*-(d+1)*u,p.months.eq(0).transform("translate3d("+(p.isH?o:0)+"%, "+(p.isH?0:o)+"%, 0)").addClass("picker-calendar-month-prev")),p.params.onMonthAdd&&p.params.onMonthAdd(p,"next"===i?p.months.eq(p.months.length-1)[0]:p.months.eq(0)[0]),p.animating=!0,p.onMonthChangeStart(i),l=100*p.monthsTranslate*u,p.wrapper.transition(t).transform("translate3d("+(p.isH?l:0)+"%, "+(p.isH?0:l)+"%, 0)"),c&&p.wrapper.transitionEnd(function(){p.onMonthChangeEnd(i,!0)}),p.params.animate||p.onMonthChangeEnd(i)},p.nextYear=function(){p.setYearMonth(p.currentYear+1)},p.prevYear=function(){p.setYearMonth(p.currentYear-1)},p.layout=function(){var e,a="",t="",n=p.value&&p.value.length?p.value[0]:(new Date).setHours(0,0,0,0),r=p.monthHTML(n,"prev"),i=p.monthHTML(n),s=p.monthHTML(n,"next"),o='<div class="picker-calendar-months"><div class="picker-calendar-months-wrapper">'+(r+i+s)+"</div></div>",l="";if(p.params.weekHeader){for(e=0;7>e;e++){var d=e+p.params.firstDay>6?e-7+p.params.firstDay:e+p.params.firstDay,c=p.params.dayNamesShort[d];l+='<div class="picker-calendar-week-day '+(p.params.weekendDays.indexOf(d)>=0?"picker-calendar-week-day-weekend":"")+'"> '+c+"</div>"}l='<div class="picker-calendar-week-days">'+l+"</div>"}t="picker-modal picker-calendar"+(p.params.rangePicker?" picker-calendar-range":"")+(p.params.cssClass?" "+p.params.cssClass:"");var u=p.params.toolbar?p.params.toolbarTemplate.replace(/{{closeText}}/g,p.params.toolbarCloseText):"";p.params.toolbar&&(u=p.params.toolbarTemplate.replace(/{{closeText}}/g,p.params.toolbarCloseText).replace(/{{monthPicker}}/g,p.params.monthPicker?p.params.monthPickerTemplate:"").replace(/{{yearPicker}}/g,p.params.yearPicker?p.params.yearPickerTemplate:""));var m=p.params.header?p.params.headerTemplate.replace(/{{closeText}}/g,p.params.toolbarCloseText).replace(/{{placeholder}}/g,p.params.headerPlaceholder):"",f=p.params.footer?p.params.footerTemplate.replace(/{{closeText}}/g,p.params.toolbarCloseText):"";a='<div class="'+t+'">'+m+f+u+'<div class="picker-modal-inner">'+l+o+"</div></div>",p.pickerHTML=a},p.params.input&&(p.input=o(p.params.input),p.input.length>0&&(p.params.inputReadOnly&&p.input.prop("readOnly",!0),p.inline||p.input.on("click",r),p.params.inputReadOnly&&p.input.on("focus mousedown",function(e){e.preventDefault()}))),!p.inline&&p.params.closeByOutsideClick&&o("html").on("click",s),p.opened=!1,p.open=function(){var e=a(),t=!1;p.opened||(p.value||p.params.value&&(p.value=p.params.value,t=!0),p.layout(),e?(p.pickerHTML='<div class="popover popover-picker-calendar"><div class="popover-inner">'+p.pickerHTML+"</div></div>",p.popover=i.popover(p.pickerHTML,p.params.input,!0),p.container=o(p.popover).find(".picker-modal"),o(p.popover).on("close",function(){l()})):p.inline?(p.container=o(p.pickerHTML),p.container.addClass("picker-modal-inline"),o(p.params.container).append(p.container)):(p.container=o(i.pickerModal(p.pickerHTML)),o(p.container).on("close",function(){l()})),p.container[0].f7Calendar=p,p.wrapper=p.container.find(".picker-calendar-months-wrapper"),p.months=p.wrapper.find(".picker-calendar-month"),p.updateCurrentMonthYear(),p.monthsTranslate=0,p.setMonthsTranslate(),p.initCalendarEvents(),t?p.updateValue():i.params.material&&p.value&&p.updateValue(!0),p.input&&p.input.length>0&&i.params.material&&p.input.trigger("focus")),p.opened=!0,p.initialized=!0,p.params.onMonthAdd&&p.months.each(function(){p.params.onMonthAdd(p,this)}),p.params.onOpen&&p.params.onOpen(p)},p.close=function(){return p.opened&&!p.inline?t()?void i.closeModal(p.popover):void i.closeModal(p.container):void 0},p.destroy=function(){
p.close(),p.params.input&&p.input.length>0&&p.input.off("click focus",r),o("html").off("click",s)},p.inline?p.open():!p.initialized&&p.params.value&&p.setValue(p.params.value),p};i.calendar=function(e){return new C(e)};var y;i.addNotification=function(e){if(e){"undefined"==typeof e.media&&(e.media=i.params.notificationMedia),"undefined"==typeof e.title&&(e.title=i.params.notificationTitle),"undefined"==typeof e.subtitle&&(e.subtitle=i.params.notificationSubtitle),"undefined"==typeof e.closeIcon&&(e.closeIcon=i.params.notificationCloseIcon),"undefined"==typeof e.hold&&(e.hold=i.params.notificationHold),"undefined"==typeof e.closeOnClick&&(e.closeOnClick=i.params.notificationCloseOnClick),"undefined"==typeof e.button&&(e.button=i.params.notificationCloseButtonText&&{text:i.params.notificationCloseButtonText,close:!0}),y||(y=document.createElement("div")),e.material=i.params.material;var a=o(".notifications");0===a.length&&(o("body").append('<div class="notifications list-block'+(e.material?"":" media-list")+'"><ul></ul></div>'),a=o(".notifications"));var t=a.children("ul"),n=i.params.notificationTemplate||'{{#if custom}}<li>{{custom}}</li>{{else}}<li class="notification-item notification-hidden"><div class="item-content">{{#if material}}<div class="item-inner"><div class="item-title">{{js "this.message || this.title || this.subtitle"}}</div>{{#if ../button}}{{#button}}<div class="item-after"><a href="#" class="button {{#if color}}color-{{color}}{{/if}} {{#js_compare "this.close !== false"}}close-notification{{/js_compare}}">{{text}}</a></div>{{/button}}{{/if}}</div>{{else}}{{#if media}}<div class="item-media">{{media}}</div>{{/if}}<div class="item-inner"><div class="item-title-row">{{#if title}}<div class="item-title">{{title}}</div>{{/if}}{{#if closeIcon}}<div class="item-after"><a href="#" class="close-notification"><span></span></a></div>{{/if}}</div>{{#if subtitle}}<div class="item-subtitle">{{subtitle}}</div>{{/if}}{{#if message}}<div class="item-text">{{message}}</div></div>{{/if}}{{/if}}</div></li>{{/if}}';i._compiledTemplates.notification||(i._compiledTemplates.notification=l.compile(n)),y.innerHTML=i._compiledTemplates.notification(e);var r=o(y).children();r.on("click",function(a){var t=!1,n=o(a.target);e.material&&n.hasClass("button")&&e.button&&e.button.onClick&&e.button.onClick.call(n[0],a,r[0]),n.is(".close-notification")||o(a.target).parents(".close-notification").length>0?t=!0:(e.onClick&&e.onClick(a,r[0]),e.closeOnClick&&(t=!0)),t&&i.closeNotification(r[0])}),e.onClose&&r.data("f7NotificationOnClose",function(){e.onClose(r[0])}),e.additionalClass&&r.addClass(e.additionalClass),e.hold&&setTimeout(function(){r.length>0&&i.closeNotification(r[0])},e.hold),t[e.material?"append":"prepend"](r[0]),a.show();var s,p=r.outerHeight();return e.material?(a.transform("translate3d(0, "+p+"px, 0)"),a.transition(0),s=r[0].clientLeft,a.transform("translate3d(0, 0, 0)"),a.transition("")):(r.css("marginTop",-p+"px"),r.transition(0),s=r[0].clientLeft,r.transition(""),r.css("marginTop","0px")),a.transform("translate3d(0, 0,0)"),r.removeClass("notification-hidden"),r[0]}},i.closeNotification=function(e){if(e=o(e),0!==e.length&&!e.hasClass("notification-item-removing")){var a=o(".notifications"),t=e.outerHeight();e.css("height",t+"px").transition(0).addClass("notification-item-removing");e[0].clientLeft;e.css("height","0px").transition(""),e.data("f7NotificationOnClose")&&e.data("f7NotificationOnClose")(),0===a.find(".notification-item:not(.notification-item-removing)").length&&a.transform(""),e.addClass("notification-hidden").transitionEnd(function(){e.remove(),0===a.find(".notification-item").length&&a.hide()})}},i.initTemplate7Templates=function(){window.Template7&&(Template7.templates=Template7.templates||i.params.templates||{},Template7.data=Template7.data||i.params.template7Data||{},Template7.cache=Template7.cache||{},i.templates=Template7.templates,i.template7Data=Template7.data,i.template7Cache=Template7.cache,i.params.precompileTemplates&&o('script[type="text/template7"]').each(function(){var e=o(this).attr("id");e&&(Template7.templates[e]=Template7.compile(o(this).html()))}))};var x=[];return i.initPlugins=function(){for(var e in i.plugins){var a=i.plugins[e](i,i.params[e]);a&&x.push(a)}},i.pluginHook=function(e){for(var a=0;a<x.length;a++)x[a].hooks&&e in x[a].hooks&&x[a].hooks[e](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5])},i.pluginPrevent=function(e){for(var a=!1,t=0;t<x.length;t++)x[t].prevents&&e in x[t].prevents&&x[t].prevents[e](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5])&&(a=!0);return a},i.pluginProcess=function(e,a){for(var t=a,n=0;n<x.length;n++)x[n].preprocess&&e in x[n].preprocess&&(t=x[n].preprocess[e](a,arguments[2],arguments[3],arguments[4],arguments[5],arguments[6]));return t},i.init=function(){i.initTemplate7Templates&&i.initTemplate7Templates(),i.initPlugins&&i.initPlugins(),i.getDeviceInfo&&i.getDeviceInfo(),i.initFastClicks&&i.params.fastClicks&&i.initFastClicks(),i.initClickEvents&&i.initClickEvents(),o(".page:not(.cached)").each(function(){i.initPageWithCallback(this)}),o(".navbar:not(.cached)").each(function(){i.initNavbarWithCallback(this)}),i.initResize&&i.initResize(),i.initPushState&&i.params.pushState&&i.initPushState(),i.initSwipeout&&i.params.swipeout&&i.initSwipeout(),i.initSortable&&i.params.sortable&&i.initSortable(),i.initSwipePanels&&(i.params.swipePanel||i.params.swipePanelOnlyClose)&&i.initSwipePanels(),i.params.material&&i.initMaterialWatchInputs&&i.initMaterialWatchInputs(),i.params.onAppInit&&i.params.onAppInit(),i.pluginHook("appInit")},i.params.init&&i.init(),i};var e=function(){var e=function(e){var a=this,t=0;for(t=0;t<e.length;t++)a[t]=e[t];return a.length=e.length,this},a=function(a,t){var n=[],r=0;if(a&&!t&&a instanceof e)return a;if(a)if("string"==typeof a){var i,s,o;if(a=o=a.trim(),o.indexOf("<")>=0&&o.indexOf(">")>=0){var l="div";for(0===o.indexOf("<li")&&(l="ul"),0===o.indexOf("<tr")&&(l="tbody"),(0===o.indexOf("<td")||0===o.indexOf("<th"))&&(l="tr"),0===o.indexOf("<tbody")&&(l="table"),0===o.indexOf("<option")&&(l="select"),s=document.createElement(l),s.innerHTML=o,r=0;r<s.childNodes.length;r++)n.push(s.childNodes[r])}else for(i=t||"#"!==a[0]||a.match(/[ .<>:~]/)?(t||document).querySelectorAll(a):[document.getElementById(a.split("#")[1])],r=0;r<i.length;r++)i[r]&&n.push(i[r])}else if(a.nodeType||a===window||a===document)n.push(a);else if(a.length>0&&a[0].nodeType)for(r=0;r<a.length;r++)n.push(a[r]);return new e(n)};e.prototype={addClass:function(e){if("undefined"==typeof e)return this;for(var a=e.split(" "),t=0;t<a.length;t++)for(var n=0;n<this.length;n++)"undefined"!=typeof this[n].classList&&this[n].classList.add(a[t]);return this},removeClass:function(e){for(var a=e.split(" "),t=0;t<a.length;t++)for(var n=0;n<this.length;n++)"undefined"!=typeof this[n].classList&&this[n].classList.remove(a[t]);return this},hasClass:function(e){return this[0]?this[0].classList.contains(e):!1},toggleClass:function(e){for(var a=e.split(" "),t=0;t<a.length;t++)for(var n=0;n<this.length;n++)"undefined"!=typeof this[n].classList&&this[n].classList.toggle(a[t]);return this},attr:function(e,a){if(1===arguments.length&&"string"==typeof e)return this[0]?this[0].getAttribute(e):void 0;for(var t=0;t<this.length;t++)if(2===arguments.length)this[t].setAttribute(e,a);else for(var n in e)this[t][n]=e[n],this[t].setAttribute(n,e[n]);return this},removeAttr:function(e){for(var a=0;a<this.length;a++)this[a].removeAttribute(e);return this},prop:function(e,a){if(1===arguments.length&&"string"==typeof e)return this[0]?this[0][e]:void 0;for(var t=0;t<this.length;t++)if(2===arguments.length)this[t][e]=a;else for(var n in e)this[t][n]=e[n];return this},data:function(e,a){if("undefined"!=typeof a){for(var t=0;t<this.length;t++){var n=this[t];n.dom7ElementDataStorage||(n.dom7ElementDataStorage={}),n.dom7ElementDataStorage[e]=a}return this}if(this[0]){if(this[0].dom7ElementDataStorage&&e in this[0].dom7ElementDataStorage)return this[0].dom7ElementDataStorage[e];var r=this[0].getAttribute("data-"+e);return r?r:void 0}},removeData:function(e){for(var a=0;a<this.length;a++){var t=this[a];t.dom7ElementDataStorage&&t.dom7ElementDataStorage[e]&&(t.dom7ElementDataStorage[e]=null,delete t.dom7ElementDataStorage[e])}},dataset:function(){var e=this[0];if(e){var t={};if(e.dataset)for(var n in e.dataset)t[n]=e.dataset[n];else for(var r=0;r<e.attributes.length;r++){var i=e.attributes[r];i.name.indexOf("data-")>=0&&(t[a.toCamelCase(i.name.split("data-")[1])]=i.value)}for(var s in t)"false"===t[s]?t[s]=!1:"true"===t[s]?t[s]=!0:parseFloat(t[s])===1*t[s]&&(t[s]=1*t[s]);return t}},val:function(e){if("undefined"==typeof e)return this[0]?this[0].value:void 0;for(var a=0;a<this.length;a++)this[a].value=e;return this},transform:function(e){for(var a=0;a<this.length;a++){var t=this[a].style;t.webkitTransform=t.MsTransform=t.msTransform=t.MozTransform=t.OTransform=t.transform=e}return this},transition:function(e){"string"!=typeof e&&(e+="ms");for(var a=0;a<this.length;a++){var t=this[a].style;t.webkitTransitionDuration=t.MsTransitionDuration=t.msTransitionDuration=t.MozTransitionDuration=t.OTransitionDuration=t.transitionDuration=e}return this},on:function(e,t,n,r){function i(e){var r=e.target;if(a(r).is(t))n.call(r,e);else for(var i=a(r).parents(),s=0;s<i.length;s++)a(i[s]).is(t)&&n.call(i[s],e)}var s,o,l=e.split(" ");for(s=0;s<this.length;s++)if("function"==typeof t||t===!1)for("function"==typeof t&&(n=arguments[1],r=arguments[2]||!1),o=0;o<l.length;o++)this[s].addEventListener(l[o],n,r);else for(o=0;o<l.length;o++)this[s].dom7LiveListeners||(this[s].dom7LiveListeners=[]),this[s].dom7LiveListeners.push({listener:n,liveListener:i}),this[s].addEventListener(l[o],i,r);return this},off:function(e,a,t,n){for(var r=e.split(" "),i=0;i<r.length;i++)for(var s=0;s<this.length;s++)if("function"==typeof a||a===!1)"function"==typeof a&&(t=arguments[1],n=arguments[2]||!1),this[s].removeEventListener(r[i],t,n);else if(this[s].dom7LiveListeners)for(var o=0;o<this[s].dom7LiveListeners.length;o++)this[s].dom7LiveListeners[o].listener===t&&this[s].removeEventListener(r[i],this[s].dom7LiveListeners[o].liveListener,n);return this},once:function(e,a,t,n){function r(s){t.call(s.target,s),i.off(e,a,r,n)}var i=this;return"function"==typeof a&&(t=arguments[1],n=arguments[2],a=!1),i.on(e,a,r,n)},trigger:function(e,a){for(var t=e.split(" "),n=0;n<t.length;n++)for(var r=0;r<this.length;r++){var i;try{i=new CustomEvent(t[n],{detail:a,bubbles:!0,cancelable:!0})}catch(s){i=document.createEvent("Event"),i.initEvent(t[n],!0,!0),i.detail=a}this[r].dispatchEvent(i)}return this},transitionEnd:function(e){function a(i){if(i.target===this)for(e.call(this,i),t=0;t<n.length;t++)r.off(n[t],a)}var t,n=["webkitTransitionEnd","transitionend","oTransitionEnd","MSTransitionEnd","msTransitionEnd"],r=this;if(e)for(t=0;t<n.length;t++)r.on(n[t],a);return this},animationEnd:function(e){function a(i){for(e(i),t=0;t<n.length;t++)r.off(n[t],a)}var t,n=["webkitAnimationEnd","OAnimationEnd","MSAnimationEnd","animationend"],r=this;if(e)for(t=0;t<n.length;t++)r.on(n[t],a);return this},width:function(){return this[0]===window?window.innerWidth:this.length>0?parseFloat(this.css("width")):null},outerWidth:function(e){if(this.length>0){if(e){var a=this.styles();return this[0].offsetWidth+parseFloat(a.getPropertyValue("margin-right"))+parseFloat(a.getPropertyValue("margin-left"))}return this[0].offsetWidth}return null},height:function(){return this[0]===window?window.innerHeight:this.length>0?parseFloat(this.css("height")):null},outerHeight:function(e){if(this.length>0){if(e){var a=this.styles();return this[0].offsetHeight+parseFloat(a.getPropertyValue("margin-top"))+parseFloat(a.getPropertyValue("margin-bottom"))}return this[0].offsetHeight}return null},offset:function(){if(this.length>0){var e=this[0],a=e.getBoundingClientRect(),t=document.body,n=e.clientTop||t.clientTop||0,r=e.clientLeft||t.clientLeft||0,i=window.pageYOffset||e.scrollTop,s=window.pageXOffset||e.scrollLeft;return{top:a.top+i-n,left:a.left+s-r}}return null},hide:function(){for(var e=0;e<this.length;e++)this[e].style.display="none";return this},show:function(){for(var e=0;e<this.length;e++)this[e].style.display="block";return this},styles:function(){return this[0]?window.getComputedStyle(this[0],null):void 0},css:function(e,a){var t;if(1===arguments.length){if("string"!=typeof e){for(t=0;t<this.length;t++)for(var n in e)this[t].style[n]=e[n];return this}if(this[0])return window.getComputedStyle(this[0],null).getPropertyValue(e)}if(2===arguments.length&&"string"==typeof e){for(t=0;t<this.length;t++)this[t].style[e]=a;return this}return this},each:function(e){for(var a=0;a<this.length;a++)e.call(this[a],a,this[a]);return this},filter:function(a){for(var t=[],n=this,r=0;r<n.length;r++)a.call(n[r],r,n[r])&&t.push(n[r]);return new e(t)},html:function(e){if("undefined"==typeof e)return this[0]?this[0].innerHTML:void 0;for(var a=0;a<this.length;a++)this[a].innerHTML=e;return this},text:function(e){if("undefined"==typeof e)return this[0]?this[0].textContent.trim():null;for(var a=0;a<this.length;a++)this[a].textContent=e;return this},is:function(t){if(!this[0]||"undefined"==typeof t)return!1;var n,r;if("string"==typeof t){var i=this[0];if(i===document)return t===document;if(i===window)return t===window;if(i.matches)return i.matches(t);if(i.webkitMatchesSelector)return i.webkitMatchesSelector(t);if(i.mozMatchesSelector)return i.mozMatchesSelector(t);if(i.msMatchesSelector)return i.msMatchesSelector(t);for(n=a(t),r=0;r<n.length;r++)if(n[r]===this[0])return!0;return!1}if(t===document)return this[0]===document;if(t===window)return this[0]===window;if(t.nodeType||t instanceof e){for(n=t.nodeType?[t]:t,r=0;r<n.length;r++)if(n[r]===this[0])return!0;return!1}return!1},indexOf:function(e){for(var a=0;a<this.length;a++)if(this[a]===e)return a},index:function(){if(this[0]){for(var e=this[0],a=0;null!==(e=e.previousSibling);)1===e.nodeType&&a++;return a}},eq:function(a){if("undefined"==typeof a)return this;var t,n=this.length;return a>n-1?new e([]):0>a?(t=n+a,new e(0>t?[]:[this[t]])):new e([this[a]])},append:function(a){var t,n;for(t=0;t<this.length;t++)if("string"==typeof a){var r=document.createElement("div");for(r.innerHTML=a;r.firstChild;)this[t].appendChild(r.firstChild)}else if(a instanceof e)for(n=0;n<a.length;n++)this[t].appendChild(a[n]);else this[t].appendChild(a);return this},appendTo:function(e){return a(e).append(this),this},prepend:function(a){var t,n;for(t=0;t<this.length;t++)if("string"==typeof a){var r=document.createElement("div");for(r.innerHTML=a,n=r.childNodes.length-1;n>=0;n--)this[t].insertBefore(r.childNodes[n],this[t].childNodes[0])}else if(a instanceof e)for(n=0;n<a.length;n++)this[t].insertBefore(a[n],this[t].childNodes[0]);else this[t].insertBefore(a,this[t].childNodes[0]);return this},prependTo:function(e){return a(e).prepend(this),this},insertBefore:function(e){for(var t=a(e),n=0;n<this.length;n++)if(1===t.length)t[0].parentNode.insertBefore(this[n],t[0]);else if(t.length>1)for(var r=0;r<t.length;r++)t[r].parentNode.insertBefore(this[n].cloneNode(!0),t[r])},insertAfter:function(e){for(var t=a(e),n=0;n<this.length;n++)if(1===t.length)t[0].parentNode.insertBefore(this[n],t[0].nextSibling);else if(t.length>1)for(var r=0;r<t.length;r++)t[r].parentNode.insertBefore(this[n].cloneNode(!0),t[r].nextSibling)},next:function(t){return new e(this.length>0?t?this[0].nextElementSibling&&a(this[0].nextElementSibling).is(t)?[this[0].nextElementSibling]:[]:this[0].nextElementSibling?[this[0].nextElementSibling]:[]:[])},nextAll:function(t){var n=[],r=this[0];if(!r)return new e([]);for(;r.nextElementSibling;){var i=r.nextElementSibling;t?a(i).is(t)&&n.push(i):n.push(i),r=i}return new e(n)},prev:function(t){return new e(this.length>0?t?this[0].previousElementSibling&&a(this[0].previousElementSibling).is(t)?[this[0].previousElementSibling]:[]:this[0].previousElementSibling?[this[0].previousElementSibling]:[]:[])},prevAll:function(t){var n=[],r=this[0];if(!r)return new e([]);for(;r.previousElementSibling;){var i=r.previousElementSibling;t?a(i).is(t)&&n.push(i):n.push(i),r=i}return new e(n)},parent:function(e){for(var t=[],n=0;n<this.length;n++)null!==this[n].parentNode&&(e?a(this[n].parentNode).is(e)&&t.push(this[n].parentNode):t.push(this[n].parentNode));return a(a.unique(t))},parents:function(e){for(var t=[],n=0;n<this.length;n++)for(var r=this[n].parentNode;r;)e?a(r).is(e)&&t.push(r):t.push(r),r=r.parentNode;return a(a.unique(t))},closest:function(a){var t=this;return"undefined"==typeof a?new e([]):(t.is(a)||(t=t.parents(a).eq(0)),t)},find:function(a){for(var t=[],n=0;n<this.length;n++)for(var r=this[n].querySelectorAll(a),i=0;i<r.length;i++)t.push(r[i]);return new e(t)},children:function(t){for(var n=[],r=0;r<this.length;r++)for(var i=this[r].childNodes,s=0;s<i.length;s++)t?1===i[s].nodeType&&a(i[s]).is(t)&&n.push(i[s]):1===i[s].nodeType&&n.push(i[s]);return new e(a.unique(n))},remove:function(){for(var e=0;e<this.length;e++)this[e].parentNode&&this[e].parentNode.removeChild(this[e]);return this},detach:function(){return this.remove()},add:function(){var e,t,n=this;for(e=0;e<arguments.length;e++){var r=a(arguments[e]);for(t=0;t<r.length;t++)n[n.length]=r[t],n.length++}return n}},function(){function t(t){e.prototype[t]=function(e,n,i){var s;if("undefined"==typeof e){for(s=0;s<this.length;s++)r.indexOf(t)<0&&(t in this[s]?this[s][t]():a(this[s]).trigger(t));return this}return this.on(t,e,n,i)}}for(var n="click blur focus focusin focusout keyup keydown keypress submit change mousedown mousemove mouseup mouseenter mouseleave mouseout mouseover touchstart touchend touchmove resize scroll".split(" "),r="resize scroll".split(" "),i=0;i<n.length;i++)t(n[i])}();var t={};a.ajaxSetup=function(e){e.type&&(e.method=e.type),a.each(e,function(e,a){t[e]=a})};var n=0;return a.ajax=function(e){function r(n,r,i){var s=arguments;n&&a(document).trigger(n,r),i&&(i in t&&t[i](s[3],s[4],s[5],s[6]),e[i]&&e[i](s[3],s[4],s[5],s[6]))}var i={method:"GET",data:!1,async:!0,cache:!0,user:"",password:"",headers:{},xhrFields:{},statusCode:{},processData:!0,dataType:"text",contentType:"application/x-www-form-urlencoded",timeout:0},s=["beforeSend","error","complete","success","statusCode"];e.type&&(e.method=e.type),a.each(t,function(e,a){s.indexOf(e)<0&&(i[e]=a)}),a.each(i,function(a,t){a in e||(e[a]=t)}),e.url||(e.url=window.location.toString());var o=e.url.indexOf("?")>=0?"&":"?",l=e.method.toUpperCase();if(("GET"===l||"HEAD"===l||"OPTIONS"===l||"DELETE"===l)&&e.data){var p;p="string"==typeof e.data?e.data.indexOf("?")>=0?e.data.split("?")[1]:e.data:a.serializeObject(e.data),p.length&&(e.url+=o+p,"?"===o&&(o="&"))}if("json"===e.dataType&&e.url.indexOf("callback=")>=0){var d,c="f7jsonp_"+Date.now()+n++,u=e.url.split("callback="),m=u[0]+"callback="+c;if(u[1].indexOf("&")>=0){var f=u[1].split("&").filter(function(e){return e.indexOf("=")>0}).join("&");f.length>0&&(m+="&"+f)}var h=document.createElement("script");return h.type="text/javascript",h.onerror=function(){clearTimeout(d),r(void 0,void 0,"error",null,"scripterror")},h.src=m,window[c]=function(e){clearTimeout(d),r(void 0,void 0,"success",e),h.parentNode.removeChild(h),h=null,delete window[c]},document.querySelector("head").appendChild(h),void(e.timeout>0&&(d=setTimeout(function(){h.parentNode.removeChild(h),h=null,r(void 0,void 0,"error",null,"timeout")},e.timeout)))}("GET"===l||"HEAD"===l||"OPTIONS"===l||"DELETE"===l)&&e.cache===!1&&(e.url+=o+"_nocache="+Date.now());var g=new XMLHttpRequest;g.requestUrl=e.url,g.requestParameters=e,g.open(l,e.url,e.async,e.user,e.password);var v=null;if(("POST"===l||"PUT"===l||"PATCH"===l)&&e.data)if(e.processData){var b=[ArrayBuffer,Blob,Document,FormData];if(b.indexOf(e.data.constructor)>=0)v=e.data;else{var w="---------------------------"+Date.now().toString(16);"multipart/form-data"===e.contentType?g.setRequestHeader("Content-Type","multipart/form-data; boundary="+w):g.setRequestHeader("Content-Type",e.contentType),v="";var C=a.serializeObject(e.data);if("multipart/form-data"===e.contentType){w="---------------------------"+Date.now().toString(16),C=C.split("&");for(var y=[],x=0;x<C.length;x++)y.push('Content-Disposition: form-data; name="'+C[x].split("=")[0]+'"\r\n\r\n'+C[x].split("=")[1]+"\r\n");v="--"+w+"\r\n"+y.join("--"+w+"\r\n")+"--"+w+"--\r\n"}else v="application/x-www-form-urlencoded"===e.contentType?C:C.replace(/&/g,"\r\n")}}else v=e.data;e.headers&&a.each(e.headers,function(e,a){g.setRequestHeader(e,a)}),"undefined"==typeof e.crossDomain&&(e.crossDomain=/^([\w-]+:)?\/\/([^\/]+)/.test(e.url)&&RegExp.$2!==window.location.host),e.crossDomain||g.setRequestHeader("X-Requested-With","XMLHttpRequest"),e.xhrFields&&a.each(e.xhrFields,function(e,a){g[e]=a});var T;return g.onload=function(a){if(T&&clearTimeout(T),g.status>=200&&g.status<300||0===g.status){var n;if("json"===e.dataType)try{n=JSON.parse(g.responseText),r("ajaxSuccess",{xhr:g},"success",n,g.status,g)}catch(i){r("ajaxError",{xhr:g,parseerror:!0},"error",g,"parseerror")}else n="text"===g.responseType||""===g.responseType?g.responseText:g.response,r("ajaxSuccess",{xhr:g},"success",n,g.status,g)}else r("ajaxError",{xhr:g},"error",g,g.status);e.statusCode&&(t.statusCode&&t.statusCode[g.status]&&t.statusCode[g.status](g),e.statusCode[g.status]&&e.statusCode[g.status](g)),r("ajaxComplete",{xhr:g},"complete",g,g.status)},g.onerror=function(e){T&&clearTimeout(T),r("ajaxError",{xhr:g},"error",g,g.status)},r("ajaxStart",{xhr:g},"start",g),r(void 0,void 0,"beforeSend",g),g.send(v),e.timeout>0&&(g.onabort=function(){T&&clearTimeout(T)},T=setTimeout(function(){g.abort(),r("ajaxError",{xhr:g,timeout:!0},"error",g,"timeout"),r("ajaxComplete",{xhr:g,timeout:!0},"complete",g,"timeout")},e.timeout)),g},function(){function e(e){a[e]=function(t,n,r){return a.ajax({url:t,method:"post"===e?"POST":"GET",data:"function"==typeof n?void 0:n,success:"function"==typeof n?n:r,dataType:"getJSON"===e?"json":void 0})}}for(var t="get post getJSON".split(" "),n=0;n<t.length;n++)e(t[n])}(),a.parseUrlQuery=function(e){var a,t,n,r={};if(!(e.indexOf("?")>=0))return r;for(e=e.split("?")[1],t=e.split("&"),a=0;a<t.length;a++)n=t[a].split("="),r[n[0]]=n[1];return r},a.isArray=function(e){return"[object Array]"===Object.prototype.toString.apply(e)?!0:!1},a.each=function(t,n){if("object"==typeof t&&n){var r,i;if(a.isArray(t)||t instanceof e)for(r=0;r<t.length;r++)n(r,t[r]);else for(i in t)t.hasOwnProperty(i)&&n(i,t[i])}},a.unique=function(e){for(var a=[],t=0;t<e.length;t++)-1===a.indexOf(e[t])&&a.push(e[t]);return a},a.serializeObject=a.param=function(e,t){function n(e){if(t.length>0){for(var a="",n=0;n<t.length;n++)a+=0===n?t[n]:"["+encodeURIComponent(t[n])+"]";return a+"["+encodeURIComponent(e)+"]"}return encodeURIComponent(e)}function r(e){return encodeURIComponent(e)}if("string"==typeof e)return e;var i=[],s="&";t=t||[];var o;for(var l in e)if(e.hasOwnProperty(l)){var p;if(a.isArray(e[l])){p=[];for(var d=0;d<e[l].length;d++)a.isArray(e[l][d])||"object"!=typeof e[l][d]?p.push(n(l)+"[]="+r(e[l][d])):(o=t.slice(),o.push(l),o.push(d+""),p.push(a.serializeObject(e[l][d],o)));p.length>0&&i.push(p.join(s))}else"object"==typeof e[l]?(o=t.slice(),o.push(l),p=a.serializeObject(e[l],o),""!==p&&i.push(p)):"undefined"!=typeof e[l]&&""!==e[l]&&i.push(n(l)+"="+r(e[l]))}return i.join(s)},a.toCamelCase=function(e){return e.toLowerCase().replace(/-(.)/g,function(e,a){return a.toUpperCase()})},a.dataset=function(e){return a(e).dataset()},a.getTranslate=function(e,a){var t,n,r,i;return"undefined"==typeof a&&(a="x"),r=window.getComputedStyle(e,null),window.WebKitCSSMatrix?(n=r.transform||r.webkitTransform,n.split(",").length>6&&(n=n.split(", ").map(function(e){return e.replace(",",".")}).join(", ")),i=new WebKitCSSMatrix("none"===n?"":n)):(i=r.MozTransform||r.OTransform||r.MsTransform||r.msTransform||r.transform||r.getPropertyValue("transform").replace("translate(","matrix(1, 0, 0, 1,"),t=i.toString().split(",")),"x"===a&&(n=window.WebKitCSSMatrix?i.m41:16===t.length?parseFloat(t[12]):parseFloat(t[4])),"y"===a&&(n=window.WebKitCSSMatrix?i.m42:16===t.length?parseFloat(t[13]):parseFloat(t[5])),n||0},a.requestAnimationFrame=function(e){return window.requestAnimationFrame?window.requestAnimationFrame(e):window.webkitRequestAnimationFrame?window.webkitRequestAnimationFrame(e):window.mozRequestAnimationFrame?window.mozRequestAnimationFrame(e):window.setTimeout(e,1e3/60)},a.cancelAnimationFrame=function(e){return window.cancelAnimationFrame?window.cancelAnimationFrame(e):window.webkitCancelAnimationFrame?window.webkitCancelAnimationFrame(e):window.mozCancelAnimationFrame?window.mozCancelAnimationFrame(e):window.clearTimeout(e)},a.supportTouch=!!("ontouchstart"in window||window.DocumentTouch&&document instanceof DocumentTouch),a.fn=e.prototype,a.fn.scrollTo=function(e,t,n,r,i){return 4===arguments.length&&"function"==typeof r&&(i=r,r=void 0),this.each(function(){function s(e){void 0===e&&(e=(new Date).getTime()),null===b&&(b=e);var t,p=Math.max(Math.min((e-b)/n,1),0),d="linear"===r?p:.5-Math.cos(p*Math.PI)/2;return g&&(m=o+d*(c-o)),v&&(f=l+d*(u-l)),g&&c>o&&m>=c&&(h.scrollTop=c,t=!0),g&&o>c&&c>=m&&(h.scrollTop=c,t=!0),v&&u>l&&f>=u&&(h.scrollLeft=u,t=!0),v&&l>u&&u>=f&&(h.scrollLeft=u,t=!0),t?void(i&&i()):(g&&(h.scrollTop=m),v&&(h.scrollLeft=f),void a.requestAnimationFrame(s))}var o,l,p,d,c,u,m,f,h=this,g=t>0||0===t,v=e>0||0===e;if("undefined"==typeof r&&(r="swing"),g&&(o=h.scrollTop,n||(h.scrollTop=t)),v&&(l=h.scrollLeft,n||(h.scrollLeft=e)),n){g&&(p=h.scrollHeight-h.offsetHeight,c=Math.max(Math.min(t,p),0)),v&&(d=h.scrollWidth-h.offsetWidth,u=Math.max(Math.min(e,d),0));var b=null;g&&c===o&&(g=!1),v&&u===l&&(v=!1),a.requestAnimationFrame(s)}})},a.fn.scrollTop=function(e,a,t,n){3===arguments.length&&"function"==typeof t&&(n=t,t=void 0);var r=this;return"undefined"==typeof e?r.length>0?r[0].scrollTop:null:r.scrollTo(void 0,e,a,t,n)},a.fn.scrollLeft=function(e,a,t,n){3===arguments.length&&"function"==typeof t&&(n=t,t=void 0);var r=this;return"undefined"==typeof e?r.length>0?r[0].scrollLeft:null:r.scrollTo(e,void 0,a,t,n)},a}();Framework7.$=e;var a=e;window.Dom7=e,Framework7.prototype.support=function(){var e={touch:!!("ontouchstart"in window||window.DocumentTouch&&document instanceof DocumentTouch)};return e}(),Framework7.prototype.device=function(){var a={},t=navigator.userAgent,n=e,r=t.match(/(Android);?[\s\/]+([\d.]+)?/),i=t.match(/(iPad).*OS\s([\d_]+)/),s=t.match(/(iPod)(.*OS\s([\d_]+))?/),o=!i&&t.match(/(iPhone\sOS)\s([\d_]+)/);if(a.ios=a.android=a.iphone=a.ipad=a.androidChrome=!1,r&&(a.os="android",a.osVersion=r[2],a.android=!0,a.androidChrome=t.toLowerCase().indexOf("chrome")>=0),(i||o||s)&&(a.os="ios",a.ios=!0),o&&!s&&(a.osVersion=o[2].replace(/_/g,"."),a.iphone=!0),i&&(a.osVersion=i[2].replace(/_/g,"."),a.ipad=!0),s&&(a.osVersion=s[3]?s[3].replace(/_/g,"."):null,a.iphone=!0),a.ios&&a.osVersion&&t.indexOf("Version/")>=0&&"10"===a.osVersion.split(".")[0]&&(a.osVersion=t.toLowerCase().split("version/")[1].split(" ")[0]),a.webView=(o||i||s)&&t.match(/.*AppleWebKit(?!.*Safari)/i),a.os&&"ios"===a.os){var l=a.osVersion.split(".");a.minimalUi=!a.webView&&(s||o)&&(1*l[0]===7?1*l[1]>=1:1*l[0]>7)&&n('meta[name="viewport"]').length>0&&n('meta[name="viewport"]').attr("content").indexOf("minimal-ui")>=0}var p=n(window).width(),d=n(window).height();a.statusBar=!1,a.webView&&p*d===screen.width*screen.height?a.statusBar=!0:a.statusBar=!1;var c=[];if(a.pixelRatio=window.devicePixelRatio||1,c.push("pixel-ratio-"+Math.floor(a.pixelRatio)),a.pixelRatio>=2&&c.push("retina"),a.os&&(c.push(a.os,a.os+"-"+a.osVersion.split(".")[0],a.os+"-"+a.osVersion.replace(/\./g,"-")),"ios"===a.os))for(var u=parseInt(a.osVersion.split(".")[0],10),m=u-1;m>=6;m--)c.push("ios-gt-"+m);return a.statusBar?c.push("with-statusbar-overlay"):n("html").removeClass("with-statusbar-overlay"),c.length>0&&n("html").addClass(c.join(" ")),a}(),Framework7.prototype.plugins={},window.Template7=function(){function e(e){return"[object Array]"===Object.prototype.toString.apply(e)}function a(e){return"function"==typeof e}function t(e){var a,t,n,r=e.replace(/[{}#}]/g,"").split(" "),i=[];for(t=0;t<r.length;t++){var s=r[t];if(0===t)i.push(s);else if(0===s.indexOf('"'))if(2===s.match(/"/g).length)i.push(s);else{for(a=0,n=t+1;n<r.length;n++)if(s+=" "+r[n],r[n].indexOf('"')>=0){a=n,i.push(s);break}a&&(t=a)}else if(s.indexOf("=")>0){var o=s.split("="),l=o[0],p=o[1];if(2!==p.match(/"/g).length){for(a=0,n=t+1;n<r.length;n++)if(p+=" "+r[n],r[n].indexOf('"')>=0){a=n;break}a&&(t=a)}var d=[l,p.replace(/"/g,"")];i.push(d)}else i.push(s)}return i}function n(a){var n,r,i=[];if(!a)return[];var s=a.split(/({{[^{^}]*}})/);for(n=0;n<s.length;n++){var o=s[n];if(""!==o)if(o.indexOf("{{")<0)i.push({type:"plain",content:o});else{if(o.indexOf("{/")>=0)continue;if(o.indexOf("{#")<0&&o.indexOf(" ")<0&&o.indexOf("else")<0){i.push({type:"variable",contextName:o.replace(/[{}]/g,"")});continue}var l=t(o),p=l[0],d=">"===p,c=[],u={};for(r=1;r<l.length;r++){var m=l[r];e(m)?u[m[0]]="false"===m[1]?!1:m[1]:c.push(m)}if(o.indexOf("{#")>=0){var f,h="",g="",v=0,b=!1,w=!1,C=0;for(r=n+1;r<s.length;r++)if(s[r].indexOf("{{#")>=0&&C++,s[r].indexOf("{{/")>=0&&C--,s[r].indexOf("{{#"+p)>=0)h+=s[r],w&&(g+=s[r]),v++;else if(s[r].indexOf("{{/"+p)>=0){if(!(v>0)){f=r,b=!0;break}v--,h+=s[r],w&&(g+=s[r])}else s[r].indexOf("else")>=0&&0===C?w=!0:(w||(h+=s[r]),w&&(g+=s[r]));b&&(f&&(n=f),i.push({type:"helper",helperName:p,contextName:c,content:h,inverseContent:g,hash:u}))}else o.indexOf(" ")>0&&(d&&(p="_partial",c[0]&&(c[0]='"'+c[0].replace(/"|'/g,"")+'"')),i.push({type:"helper",helperName:p,contextName:c,hash:u}))}}return i}var r=function(e){function a(e,a){return e.content?s(e.content,a):function(){return""}}function t(e,a){return e.inverseContent?s(e.inverseContent,a):function(){return""}}function r(e,a){var t,n,r=0;if(0===e.indexOf("../")){r=e.split("../").length-1;var i=a.split("_")[1]-r;a="ctx_"+(i>=1?i:1),n=e.split("../")[r].split(".")}else 0===e.indexOf("@global")?(a="Template7.global",n=e.split("@global.")[1].split(".")):0===e.indexOf("@root")?(a="root",n=e.split("@root.")[1].split(".")):n=e.split(".");t=a;for(var s=0;s<n.length;s++){var o=n[s];0===o.indexOf("@")?s>0?t+="[(data && data."+o.replace("@","")+")]":t="(data && data."+e.replace("@","")+")":isFinite(o)?t+="["+o+"]":0===o.indexOf("this")?t=o.replace("this",a):t+="."+o}return t}function i(e,a){for(var t=[],n=0;n<e.length;n++)0===e[n].indexOf('"')?t.push(e[n]):t.push(r(e[n],a));return t.join(", ")}function s(e,s){if(s=s||1,e=e||o.template,"string"!=typeof e)throw new Error("Template7: Template must be a string");var l=n(e);if(0===l.length)return function(){return""};var p="ctx_"+s,d="";d+=1===s?"(function ("+p+", data, root) {\n":"(function ("+p+", data) {\n",1===s&&(d+="function isArray(arr){return Object.prototype.toString.apply(arr) === '[object Array]';}\n",d+="function isFunction(func){return (typeof func === 'function');}\n",d+='function c(val, ctx) {if (typeof val !== "undefined" && val !== null) {if (isFunction(val)) {return val.call(ctx);} else return val;} else return "";}\n',d+="root = root || ctx_1 || {};\n"),d+="var r = '';\n";var c;for(c=0;c<l.length;c++){var u=l[c];if("plain"!==u.type){var m,f;if("variable"===u.type&&(m=r(u.contextName,p),d+="r += c("+m+", "+p+");"),"helper"===u.type)if(u.helperName in o.helpers)f=i(u.contextName,p),d+="r += (Template7.helpers."+u.helperName+").call("+p+", "+(f&&f+", ")+"{hash:"+JSON.stringify(u.hash)+", data: data || {}, fn: "+a(u,s+1)+", inverse: "+t(u,s+1)+", root: root});";else{if(u.contextName.length>0)throw new Error('Template7: Missing helper: "'+u.helperName+'"');m=r(u.helperName,p),d+="if ("+m+") {",d+="if (isArray("+m+")) {",d+="r += (Template7.helpers.each).call("+p+", "+m+", {hash:"+JSON.stringify(u.hash)+", data: data || {}, fn: "+a(u,s+1)+", inverse: "+t(u,s+1)+", root: root});",d+="}else {",
d+="r += (Template7.helpers.with).call("+p+", "+m+", {hash:"+JSON.stringify(u.hash)+", data: data || {}, fn: "+a(u,s+1)+", inverse: "+t(u,s+1)+", root: root});",d+="}}"}}else d+="r +='"+u.content.replace(/\r/g,"\\r").replace(/\n/g,"\\n").replace(/'/g,"\\'")+"';"}return d+="\nreturn r;})",eval.call(window,d)}var o=this;o.template=e,o.compile=function(e){return o.compiled||(o.compiled=s(e)),o.compiled}};r.prototype={options:{},partials:{},helpers:{_partial:function(e,a){var t=r.prototype.partials[e];if(!t||t&&!t.template)return"";t.compiled||(t.compiled=i.compile(t.template));var n=this;for(var s in a.hash)n[s]=a.hash[s];return t.compiled(n,a.data,a.root)},escape:function(e,a){if("string"!=typeof e)throw new Error('Template7: Passed context to "escape" helper should be a string');return e.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/"/g,"&quot;")},"if":function(e,t){return a(e)&&(e=e.call(this)),e?t.fn(this,t.data):t.inverse(this,t.data)},unless:function(e,t){return a(e)&&(e=e.call(this)),e?t.inverse(this,t.data):t.fn(this,t.data)},each:function(t,n){var r="",i=0;if(a(t)&&(t=t.call(this)),e(t)){for(n.hash.reverse&&(t=t.reverse()),i=0;i<t.length;i++)r+=n.fn(t[i],{first:0===i,last:i===t.length-1,index:i});n.hash.reverse&&(t=t.reverse())}else for(var s in t)i++,r+=n.fn(t[s],{key:s});return i>0?r:n.inverse(this)},"with":function(e,t){return a(e)&&(e=e.call(this)),t.fn(e)},join:function(e,t){return a(e)&&(e=e.call(this)),e.join(t.hash.delimiter||t.hash.delimeter)},js:function(e,a){var t;return t=e.indexOf("return")>=0?"(function(){"+e+"})":"(function(){return ("+e+")})",eval.call(this,t).call(this)},js_compare:function(e,a){var t;t=e.indexOf("return")>=0?"(function(){"+e+"})":"(function(){return ("+e+")})";var n=eval.call(this,t).call(this);return n?a.fn(this,a.data):a.inverse(this,a.data)}}};var i=function(e,a){if(2===arguments.length){var t=new r(e),n=t.compile()(a);return t=null,n}return new r(e)};return i.registerHelper=function(e,a){r.prototype.helpers[e]=a},i.unregisterHelper=function(e){r.prototype.helpers[e]=void 0,delete r.prototype.helpers[e]},i.registerPartial=function(e,a){r.prototype.partials[e]={template:a}},i.unregisterPartial=function(e,a){r.prototype.partials[e]&&(r.prototype.partials[e]=void 0,delete r.prototype.partials[e])},i.compile=function(e,a){var t=new r(e,a);return t.compile()},i.options=r.prototype.options,i.helpers=r.prototype.helpers,i.partials=r.prototype.partials,i}(),window.Swiper=function(t,n){function r(e){return Math.floor(e)}function i(){v.autoplayTimeoutId=setTimeout(function(){v.params.loop?(v.fixLoop(),v._slideNext(),v.emit("onAutoplay",v)):v.isEnd?n.autoplayStopOnLast?v.stopAutoplay():(v._slideTo(0),v.emit("onAutoplay",v)):(v._slideNext(),v.emit("onAutoplay",v))},v.params.autoplay)}function s(e,t){var n=a(e.target);if(!n.is(t))if("string"==typeof t)n=n.parents(t);else if(t.nodeType){var r;return n.parents().each(function(e,a){a===t&&(r=t)}),r?t:void 0}if(0!==n.length)return n[0]}function o(e,a){a=a||{};var t=window.MutationObserver||window.WebkitMutationObserver,n=new t(function(e){e.forEach(function(e){v.onResize(!0),v.emit("onObserverUpdate",v,e)})});n.observe(e,{attributes:"undefined"==typeof a.attributes?!0:a.attributes,childList:"undefined"==typeof a.childList?!0:a.childList,characterData:"undefined"==typeof a.characterData?!0:a.characterData}),v.observers.push(n)}function l(e,t){e=a(e);var n,r,i,s=v.rtl?-1:1;n=e.attr("data-swiper-parallax")||"0",r=e.attr("data-swiper-parallax-x"),i=e.attr("data-swiper-parallax-y"),r||i?(r=r||"0",i=i||"0"):v.isHorizontal()?(r=n,i="0"):(i=n,r="0"),r=r.indexOf("%")>=0?parseInt(r,10)*t*s+"%":r*t*s+"px",i=i.indexOf("%")>=0?parseInt(i,10)*t+"%":i*t+"px",e.transform("translate3d("+r+", "+i+",0px)")}function p(e){return 0!==e.indexOf("on")&&(e=e[0]!==e[0].toUpperCase()?"on"+e[0].toUpperCase()+e.substring(1):"on"+e),e}if(!(this instanceof Swiper))return new Swiper(t,n);var d={direction:"horizontal",touchEventsTarget:"container",initialSlide:0,speed:300,autoplay:!1,autoplayDisableOnInteraction:!0,autoplayStopOnLast:!1,iOSEdgeSwipeDetection:!1,iOSEdgeSwipeThreshold:20,freeMode:!1,freeModeMomentum:!0,freeModeMomentumRatio:1,freeModeMomentumBounce:!0,freeModeMomentumBounceRatio:1,freeModeSticky:!1,freeModeMinimumVelocity:.02,autoHeight:!1,setWrapperSize:!1,virtualTranslate:!1,effect:"slide",coverflow:{rotate:50,stretch:0,depth:100,modifier:1,slideShadows:!0},flip:{slideShadows:!0,limitRotation:!0},cube:{slideShadows:!0,shadow:!0,shadowOffset:20,shadowScale:.94},fade:{crossFade:!1},parallax:!1,scrollbar:null,scrollbarHide:!0,scrollbarDraggable:!1,scrollbarSnapOnRelease:!1,keyboardControl:!1,mousewheelControl:!1,mousewheelReleaseOnEdges:!1,mousewheelInvert:!1,mousewheelForceToAxis:!1,mousewheelSensitivity:1,hashnav:!1,breakpoints:void 0,spaceBetween:0,slidesPerView:1,slidesPerColumn:1,slidesPerColumnFill:"column",slidesPerGroup:1,centeredSlides:!1,slidesOffsetBefore:0,slidesOffsetAfter:0,roundLengths:!1,touchRatio:1,touchAngle:45,simulateTouch:!0,shortSwipes:!0,longSwipes:!0,longSwipesRatio:.5,longSwipesMs:300,followFinger:!0,onlyExternal:!1,threshold:0,touchMoveStopPropagation:!0,uniqueNavElements:!0,pagination:null,paginationElement:"span",paginationClickable:!1,paginationHide:!1,paginationBulletRender:null,paginationProgressRender:null,paginationFractionRender:null,paginationCustomRender:null,paginationType:"bullets",resistance:!0,resistanceRatio:.85,nextButton:null,prevButton:null,watchSlidesProgress:!1,watchSlidesVisibility:!1,grabCursor:!1,preventClicks:!0,preventClicksPropagation:!0,slideToClickedSlide:!1,lazyLoading:!1,lazyLoadingInPrevNext:!1,lazyLoadingInPrevNextAmount:1,lazyLoadingOnTransitionStart:!1,preloadImages:!0,updateOnImagesReady:!0,loop:!1,loopAdditionalSlides:0,loopedSlides:null,control:void 0,controlInverse:!1,controlBy:"slide",allowSwipeToPrev:!0,allowSwipeToNext:!0,swipeHandler:null,noSwiping:!0,noSwipingClass:"swiper-no-swiping",slideClass:"swiper-slide",slideActiveClass:"swiper-slide-active",slideVisibleClass:"swiper-slide-visible",slideDuplicateClass:"swiper-slide-duplicate",slideNextClass:"swiper-slide-next",slidePrevClass:"swiper-slide-prev",wrapperClass:"swiper-wrapper",bulletClass:"swiper-pagination-bullet",bulletActiveClass:"swiper-pagination-bullet-active",buttonDisabledClass:"swiper-button-disabled",paginationCurrentClass:"swiper-pagination-current",paginationTotalClass:"swiper-pagination-total",paginationHiddenClass:"swiper-pagination-hidden",paginationProgressbarClass:"swiper-pagination-progressbar",observer:!1,observeParents:!1,a11y:!1,prevSlideMessage:"Previous slide",nextSlideMessage:"Next slide",firstSlideMessage:"This is the first slide",lastSlideMessage:"This is the last slide",paginationBulletMessage:"Go to slide {{index}}",runCallbacksOnInit:!0},c=n&&n.virtualTranslate;n=n||{};var u={};for(var m in n)if("object"!=typeof n[m]||null===n[m]||(n[m].nodeType||n[m]===window||n[m]===document||"undefined"!=typeof e&&n[m]instanceof e||"undefined"!=typeof jQuery&&n[m]instanceof jQuery))u[m]=n[m];else{u[m]={};for(var f in n[m])u[m][f]=n[m][f]}for(var h in d)if("undefined"==typeof n[h])n[h]=d[h];else if("object"==typeof n[h])for(var g in d[h])"undefined"==typeof n[h][g]&&(n[h][g]=d[h][g]);var v=this;if(v.params=n,v.originalParams=u,v.classNames=[],"undefined"!=typeof a&&"undefined"!=typeof e&&(a=e),("undefined"!=typeof a||(a="undefined"==typeof e?window.Dom7||window.Zepto||window.jQuery:e))&&(v.$=a,v.currentBreakpoint=void 0,v.getActiveBreakpoint=function(){if(!v.params.breakpoints)return!1;var e,a=!1,t=[];for(e in v.params.breakpoints)v.params.breakpoints.hasOwnProperty(e)&&t.push(e);t.sort(function(e,a){return parseInt(e,10)>parseInt(a,10)});for(var n=0;n<t.length;n++)e=t[n],e>=window.innerWidth&&!a&&(a=e);return a||"max"},v.setBreakpoint=function(){var e=v.getActiveBreakpoint();if(e&&v.currentBreakpoint!==e){var a=e in v.params.breakpoints?v.params.breakpoints[e]:v.originalParams,t=v.params.loop&&a.slidesPerView!==v.params.slidesPerView;for(var n in a)v.params[n]=a[n];v.currentBreakpoint=e,t&&v.destroyLoop&&v.reLoop(!0)}},v.params.breakpoints&&v.setBreakpoint(),v.container=a(t),0!==v.container.length)){if(v.container.length>1){var b=[];return v.container.each(function(){b.push(new Swiper(this,n))}),b}v.container[0].swiper=v,v.container.data("swiper",v),v.classNames.push("swiper-container-"+v.params.direction),v.params.freeMode&&v.classNames.push("swiper-container-free-mode"),v.support.flexbox||(v.classNames.push("swiper-container-no-flexbox"),v.params.slidesPerColumn=1),v.params.autoHeight&&v.classNames.push("swiper-container-autoheight"),(v.params.parallax||v.params.watchSlidesVisibility)&&(v.params.watchSlidesProgress=!0),["cube","coverflow","flip"].indexOf(v.params.effect)>=0&&(v.support.transforms3d?(v.params.watchSlidesProgress=!0,v.classNames.push("swiper-container-3d")):v.params.effect="slide"),"slide"!==v.params.effect&&v.classNames.push("swiper-container-"+v.params.effect),"cube"===v.params.effect&&(v.params.resistanceRatio=0,v.params.slidesPerView=1,v.params.slidesPerColumn=1,v.params.slidesPerGroup=1,v.params.centeredSlides=!1,v.params.spaceBetween=0,v.params.virtualTranslate=!0,v.params.setWrapperSize=!1),("fade"===v.params.effect||"flip"===v.params.effect)&&(v.params.slidesPerView=1,v.params.slidesPerColumn=1,v.params.slidesPerGroup=1,v.params.watchSlidesProgress=!0,v.params.spaceBetween=0,v.params.setWrapperSize=!1,"undefined"==typeof c&&(v.params.virtualTranslate=!0)),v.params.grabCursor&&v.support.touch&&(v.params.grabCursor=!1),v.wrapper=v.container.children("."+v.params.wrapperClass),v.params.pagination&&(v.paginationContainer=a(v.params.pagination),v.params.uniqueNavElements&&"string"==typeof v.params.pagination&&v.paginationContainer.length>1&&1===v.container.find(v.params.pagination).length&&(v.paginationContainer=v.container.find(v.params.pagination)),"bullets"===v.params.paginationType&&v.params.paginationClickable?v.paginationContainer.addClass("swiper-pagination-clickable"):v.params.paginationClickable=!1,v.paginationContainer.addClass("swiper-pagination-"+v.params.paginationType)),(v.params.nextButton||v.params.prevButton)&&(v.params.nextButton&&(v.nextButton=a(v.params.nextButton),v.params.uniqueNavElements&&"string"==typeof v.params.nextButton&&v.nextButton.length>1&&1===v.container.find(v.params.nextButton).length&&(v.nextButton=v.container.find(v.params.nextButton))),v.params.prevButton&&(v.prevButton=a(v.params.prevButton),v.params.uniqueNavElements&&"string"==typeof v.params.prevButton&&v.prevButton.length>1&&1===v.container.find(v.params.prevButton).length&&(v.prevButton=v.container.find(v.params.prevButton)))),v.isHorizontal=function(){return"horizontal"===v.params.direction},v.rtl=v.isHorizontal()&&("rtl"===v.container[0].dir.toLowerCase()||"rtl"===v.container.css("direction")),v.rtl&&v.classNames.push("swiper-container-rtl"),v.rtl&&(v.wrongRTL="-webkit-box"===v.wrapper.css("display")),v.params.slidesPerColumn>1&&v.classNames.push("swiper-container-multirow"),v.device.android&&v.classNames.push("swiper-container-android"),v.container.addClass(v.classNames.join(" ")),v.translate=0,v.progress=0,v.velocity=0,v.lockSwipeToNext=function(){v.params.allowSwipeToNext=!1},v.lockSwipeToPrev=function(){v.params.allowSwipeToPrev=!1},v.lockSwipes=function(){v.params.allowSwipeToNext=v.params.allowSwipeToPrev=!1},v.unlockSwipeToNext=function(){v.params.allowSwipeToNext=!0},v.unlockSwipeToPrev=function(){v.params.allowSwipeToPrev=!0},v.unlockSwipes=function(){v.params.allowSwipeToNext=v.params.allowSwipeToPrev=!0},v.params.grabCursor&&(v.container[0].style.cursor="move",v.container[0].style.cursor="-webkit-grab",v.container[0].style.cursor="-moz-grab",v.container[0].style.cursor="grab"),v.imagesToLoad=[],v.imagesLoaded=0,v.loadImage=function(e,a,t,n,r){function i(){r&&r()}var s;e.complete&&n?i():a?(s=new window.Image,s.onload=i,s.onerror=i,t&&(s.srcset=t),a&&(s.src=a)):i()},v.preloadImages=function(){function e(){"undefined"!=typeof v&&null!==v&&(void 0!==v.imagesLoaded&&v.imagesLoaded++,v.imagesLoaded===v.imagesToLoad.length&&(v.params.updateOnImagesReady&&v.update(),v.emit("onImagesReady",v)))}v.imagesToLoad=v.container.find("img");for(var a=0;a<v.imagesToLoad.length;a++)v.loadImage(v.imagesToLoad[a],v.imagesToLoad[a].currentSrc||v.imagesToLoad[a].getAttribute("src"),v.imagesToLoad[a].srcset||v.imagesToLoad[a].getAttribute("srcset"),!0,e)},v.autoplayTimeoutId=void 0,v.autoplaying=!1,v.autoplayPaused=!1,v.startAutoplay=function(){return"undefined"!=typeof v.autoplayTimeoutId?!1:v.params.autoplay?v.autoplaying?!1:(v.autoplaying=!0,v.emit("onAutoplayStart",v),void i()):!1},v.stopAutoplay=function(e){v.autoplayTimeoutId&&(v.autoplayTimeoutId&&clearTimeout(v.autoplayTimeoutId),v.autoplaying=!1,v.autoplayTimeoutId=void 0,v.emit("onAutoplayStop",v))},v.pauseAutoplay=function(e){v.autoplayPaused||(v.autoplayTimeoutId&&clearTimeout(v.autoplayTimeoutId),v.autoplayPaused=!0,0===e?(v.autoplayPaused=!1,i()):v.wrapper.transitionEnd(function(){v&&(v.autoplayPaused=!1,v.autoplaying?i():v.stopAutoplay())}))},v.minTranslate=function(){return-v.snapGrid[0]},v.maxTranslate=function(){return-v.snapGrid[v.snapGrid.length-1]},v.updateAutoHeight=function(){var e=v.slides.eq(v.activeIndex)[0];if("undefined"!=typeof e){var a=e.offsetHeight;a&&v.wrapper.css("height",a+"px")}},v.updateContainerSize=function(){var e,a;e="undefined"!=typeof v.params.width?v.params.width:v.container[0].clientWidth,a="undefined"!=typeof v.params.height?v.params.height:v.container[0].clientHeight,0===e&&v.isHorizontal()||0===a&&!v.isHorizontal()||(e=e-parseInt(v.container.css("padding-left"),10)-parseInt(v.container.css("padding-right"),10),a=a-parseInt(v.container.css("padding-top"),10)-parseInt(v.container.css("padding-bottom"),10),v.width=e,v.height=a,v.size=v.isHorizontal()?v.width:v.height)},v.updateSlidesSize=function(){v.slides=v.wrapper.children("."+v.params.slideClass),v.snapGrid=[],v.slidesGrid=[],v.slidesSizesGrid=[];var e,a=v.params.spaceBetween,t=-v.params.slidesOffsetBefore,n=0,i=0;if("undefined"!=typeof v.size){"string"==typeof a&&a.indexOf("%")>=0&&(a=parseFloat(a.replace("%",""))/100*v.size),v.virtualSize=-a,v.rtl?v.slides.css({marginLeft:"",marginTop:""}):v.slides.css({marginRight:"",marginBottom:""});var s;v.params.slidesPerColumn>1&&(s=Math.floor(v.slides.length/v.params.slidesPerColumn)===v.slides.length/v.params.slidesPerColumn?v.slides.length:Math.ceil(v.slides.length/v.params.slidesPerColumn)*v.params.slidesPerColumn,"auto"!==v.params.slidesPerView&&"row"===v.params.slidesPerColumnFill&&(s=Math.max(s,v.params.slidesPerView*v.params.slidesPerColumn)));var o,l=v.params.slidesPerColumn,p=s/l,d=p-(v.params.slidesPerColumn*p-v.slides.length);for(e=0;e<v.slides.length;e++){o=0;var c=v.slides.eq(e);if(v.params.slidesPerColumn>1){var u,m,f;"column"===v.params.slidesPerColumnFill?(m=Math.floor(e/l),f=e-m*l,(m>d||m===d&&f===l-1)&&++f>=l&&(f=0,m++),u=m+f*s/l,c.css({"-webkit-box-ordinal-group":u,"-moz-box-ordinal-group":u,"-ms-flex-order":u,"-webkit-order":u,order:u})):(f=Math.floor(e/p),m=e-f*p),c.css({"margin-top":0!==f&&v.params.spaceBetween&&v.params.spaceBetween+"px"}).attr("data-swiper-column",m).attr("data-swiper-row",f)}"none"!==c.css("display")&&("auto"===v.params.slidesPerView?(o=v.isHorizontal()?c.outerWidth(!0):c.outerHeight(!0),v.params.roundLengths&&(o=r(o))):(o=(v.size-(v.params.slidesPerView-1)*a)/v.params.slidesPerView,v.params.roundLengths&&(o=r(o)),v.isHorizontal()?v.slides[e].style.width=o+"px":v.slides[e].style.height=o+"px"),v.slides[e].swiperSlideSize=o,v.slidesSizesGrid.push(o),v.params.centeredSlides?(t=t+o/2+n/2+a,0===e&&(t=t-v.size/2-a),Math.abs(t)<.001&&(t=0),i%v.params.slidesPerGroup===0&&v.snapGrid.push(t),v.slidesGrid.push(t)):(i%v.params.slidesPerGroup===0&&v.snapGrid.push(t),v.slidesGrid.push(t),t=t+o+a),v.virtualSize+=o+a,n=o,i++)}v.virtualSize=Math.max(v.virtualSize,v.size)+v.params.slidesOffsetAfter;var h;if(v.rtl&&v.wrongRTL&&("slide"===v.params.effect||"coverflow"===v.params.effect)&&v.wrapper.css({width:v.virtualSize+v.params.spaceBetween+"px"}),(!v.support.flexbox||v.params.setWrapperSize)&&(v.isHorizontal()?v.wrapper.css({width:v.virtualSize+v.params.spaceBetween+"px"}):v.wrapper.css({height:v.virtualSize+v.params.spaceBetween+"px"})),v.params.slidesPerColumn>1&&(v.virtualSize=(o+v.params.spaceBetween)*s,v.virtualSize=Math.ceil(v.virtualSize/v.params.slidesPerColumn)-v.params.spaceBetween,v.wrapper.css({width:v.virtualSize+v.params.spaceBetween+"px"}),v.params.centeredSlides)){for(h=[],e=0;e<v.snapGrid.length;e++)v.snapGrid[e]<v.virtualSize+v.snapGrid[0]&&h.push(v.snapGrid[e]);v.snapGrid=h}if(!v.params.centeredSlides){for(h=[],e=0;e<v.snapGrid.length;e++)v.snapGrid[e]<=v.virtualSize-v.size&&h.push(v.snapGrid[e]);v.snapGrid=h,Math.floor(v.virtualSize-v.size)-Math.floor(v.snapGrid[v.snapGrid.length-1])>1&&v.snapGrid.push(v.virtualSize-v.size)}0===v.snapGrid.length&&(v.snapGrid=[0]),0!==v.params.spaceBetween&&(v.isHorizontal()?v.rtl?v.slides.css({marginLeft:a+"px"}):v.slides.css({marginRight:a+"px"}):v.slides.css({marginBottom:a+"px"})),v.params.watchSlidesProgress&&v.updateSlidesOffset()}},v.updateSlidesOffset=function(){for(var e=0;e<v.slides.length;e++)v.slides[e].swiperSlideOffset=v.isHorizontal()?v.slides[e].offsetLeft:v.slides[e].offsetTop},v.updateSlidesProgress=function(e){if("undefined"==typeof e&&(e=v.translate||0),0!==v.slides.length){"undefined"==typeof v.slides[0].swiperSlideOffset&&v.updateSlidesOffset();var a=-e;v.rtl&&(a=e),v.slides.removeClass(v.params.slideVisibleClass);for(var t=0;t<v.slides.length;t++){var n=v.slides[t],r=(a-n.swiperSlideOffset)/(n.swiperSlideSize+v.params.spaceBetween);if(v.params.watchSlidesVisibility){var i=-(a-n.swiperSlideOffset),s=i+v.slidesSizesGrid[t],o=i>=0&&i<v.size||s>0&&s<=v.size||0>=i&&s>=v.size;o&&v.slides.eq(t).addClass(v.params.slideVisibleClass)}n.progress=v.rtl?-r:r}}},v.updateProgress=function(e){"undefined"==typeof e&&(e=v.translate||0);var a=v.maxTranslate()-v.minTranslate(),t=v.isBeginning,n=v.isEnd;0===a?(v.progress=0,v.isBeginning=v.isEnd=!0):(v.progress=(e-v.minTranslate())/a,v.isBeginning=v.progress<=0,v.isEnd=v.progress>=1),v.isBeginning&&!t&&v.emit("onReachBeginning",v),v.isEnd&&!n&&v.emit("onReachEnd",v),v.params.watchSlidesProgress&&v.updateSlidesProgress(e),v.emit("onProgress",v,v.progress)},v.updateActiveIndex=function(){var e,a,t,n=v.rtl?v.translate:-v.translate;for(a=0;a<v.slidesGrid.length;a++)"undefined"!=typeof v.slidesGrid[a+1]?n>=v.slidesGrid[a]&&n<v.slidesGrid[a+1]-(v.slidesGrid[a+1]-v.slidesGrid[a])/2?e=a:n>=v.slidesGrid[a]&&n<v.slidesGrid[a+1]&&(e=a+1):n>=v.slidesGrid[a]&&(e=a);(0>e||"undefined"==typeof e)&&(e=0),t=Math.floor(e/v.params.slidesPerGroup),t>=v.snapGrid.length&&(t=v.snapGrid.length-1),e!==v.activeIndex&&(v.snapIndex=t,v.previousIndex=v.activeIndex,v.activeIndex=e,v.updateClasses())},v.updateClasses=function(){v.slides.removeClass(v.params.slideActiveClass+" "+v.params.slideNextClass+" "+v.params.slidePrevClass);var e=v.slides.eq(v.activeIndex);e.addClass(v.params.slideActiveClass);var t=e.next("."+v.params.slideClass).addClass(v.params.slideNextClass);v.params.loop&&0===t.length&&v.slides.eq(0).addClass(v.params.slideNextClass);var n=e.prev("."+v.params.slideClass).addClass(v.params.slidePrevClass);if(v.params.loop&&0===n.length&&v.slides.eq(-1).addClass(v.params.slidePrevClass),v.paginationContainer&&v.paginationContainer.length>0){var r,i=v.params.loop?Math.ceil((v.slides.length-2*v.loopedSlides)/v.params.slidesPerGroup):v.snapGrid.length;if(v.params.loop?(r=Math.ceil((v.activeIndex-v.loopedSlides)/v.params.slidesPerGroup),r>v.slides.length-1-2*v.loopedSlides&&(r-=v.slides.length-2*v.loopedSlides),r>i-1&&(r-=i),0>r&&"bullets"!==v.params.paginationType&&(r=i+r)):r="undefined"!=typeof v.snapIndex?v.snapIndex:v.activeIndex||0,"bullets"===v.params.paginationType&&v.bullets&&v.bullets.length>0&&(v.bullets.removeClass(v.params.bulletActiveClass),v.paginationContainer.length>1?v.bullets.each(function(){a(this).index()===r&&a(this).addClass(v.params.bulletActiveClass)}):v.bullets.eq(r).addClass(v.params.bulletActiveClass)),"fraction"===v.params.paginationType&&(v.paginationContainer.find("."+v.params.paginationCurrentClass).text(r+1),v.paginationContainer.find("."+v.params.paginationTotalClass).text(i)),"progress"===v.params.paginationType){var s=(r+1)/i,o=s,l=1;v.isHorizontal()||(l=s,o=1),v.paginationContainer.find("."+v.params.paginationProgressbarClass).transform("translate3d(0,0,0) scaleX("+o+") scaleY("+l+")").transition(v.params.speed)}"custom"===v.params.paginationType&&v.params.paginationCustomRender&&(v.paginationContainer.html(v.params.paginationCustomRender(v,r+1,i)),v.emit("onPaginationRendered",v,v.paginationContainer[0]))}v.params.loop||(v.params.prevButton&&v.prevButton&&v.prevButton.length>0&&(v.isBeginning?(v.prevButton.addClass(v.params.buttonDisabledClass),v.params.a11y&&v.a11y&&v.a11y.disable(v.prevButton)):(v.prevButton.removeClass(v.params.buttonDisabledClass),v.params.a11y&&v.a11y&&v.a11y.enable(v.prevButton))),v.params.nextButton&&v.nextButton&&v.nextButton.length>0&&(v.isEnd?(v.nextButton.addClass(v.params.buttonDisabledClass),v.params.a11y&&v.a11y&&v.a11y.disable(v.nextButton)):(v.nextButton.removeClass(v.params.buttonDisabledClass),v.params.a11y&&v.a11y&&v.a11y.enable(v.nextButton))))},v.updatePagination=function(){if(v.params.pagination&&v.paginationContainer&&v.paginationContainer.length>0){var e="";if("bullets"===v.params.paginationType){for(var a=v.params.loop?Math.ceil((v.slides.length-2*v.loopedSlides)/v.params.slidesPerGroup):v.snapGrid.length,t=0;a>t;t++)e+=v.params.paginationBulletRender?v.params.paginationBulletRender(t,v.params.bulletClass):"<"+v.params.paginationElement+' class="'+v.params.bulletClass+'"></'+v.params.paginationElement+">";v.paginationContainer.html(e),v.bullets=v.paginationContainer.find("."+v.params.bulletClass),v.params.paginationClickable&&v.params.a11y&&v.a11y&&v.a11y.initPagination()}"fraction"===v.params.paginationType&&(e=v.params.paginationFractionRender?v.params.paginationFractionRender(v,v.params.paginationCurrentClass,v.params.paginationTotalClass):'<span class="'+v.params.paginationCurrentClass+'"></span> / <span class="'+v.params.paginationTotalClass+'"></span>',v.paginationContainer.html(e)),"progress"===v.params.paginationType&&(e=v.params.paginationProgressRender?v.params.paginationProgressRender(v,v.params.paginationProgressbarClass):'<span class="'+v.params.paginationProgressbarClass+'"></span>',v.paginationContainer.html(e)),"custom"!==v.params.paginationType&&v.emit("onPaginationRendered",v,v.paginationContainer[0])}},v.update=function(e){function a(){n=Math.min(Math.max(v.translate,v.maxTranslate()),v.minTranslate()),v.setWrapperTranslate(n),v.updateActiveIndex(),v.updateClasses()}if(v.updateContainerSize(),v.updateSlidesSize(),v.updateProgress(),v.updatePagination(),v.updateClasses(),v.params.scrollbar&&v.scrollbar&&v.scrollbar.set(),e){var t,n;v.controller&&v.controller.spline&&(v.controller.spline=void 0),v.params.freeMode?(a(),v.params.autoHeight&&v.updateAutoHeight()):(t=("auto"===v.params.slidesPerView||v.params.slidesPerView>1)&&v.isEnd&&!v.params.centeredSlides?v.slideTo(v.slides.length-1,0,!1,!0):v.slideTo(v.activeIndex,0,!1,!0),t||a())}else v.params.autoHeight&&v.updateAutoHeight()},v.onResize=function(e){v.params.breakpoints&&v.setBreakpoint();var a=v.params.allowSwipeToPrev,t=v.params.allowSwipeToNext;v.params.allowSwipeToPrev=v.params.allowSwipeToNext=!0,v.updateContainerSize(),v.updateSlidesSize(),("auto"===v.params.slidesPerView||v.params.freeMode||e)&&v.updatePagination(),v.params.scrollbar&&v.scrollbar&&v.scrollbar.set(),v.controller&&v.controller.spline&&(v.controller.spline=void 0);var n=!1;if(v.params.freeMode){var r=Math.min(Math.max(v.translate,v.maxTranslate()),v.minTranslate());v.setWrapperTranslate(r),v.updateActiveIndex(),v.updateClasses(),v.params.autoHeight&&v.updateAutoHeight()}else v.updateClasses(),n=("auto"===v.params.slidesPerView||v.params.slidesPerView>1)&&v.isEnd&&!v.params.centeredSlides?v.slideTo(v.slides.length-1,0,!1,!0):v.slideTo(v.activeIndex,0,!1,!0);v.params.lazyLoading&&!n&&v.lazy&&v.lazy.load(),v.params.allowSwipeToPrev=a,v.params.allowSwipeToNext=t};var w=["mousedown","mousemove","mouseup"];window.navigator.pointerEnabled?w=["pointerdown","pointermove","pointerup"]:window.navigator.msPointerEnabled&&(w=["MSPointerDown","MSPointerMove","MSPointerUp"]),v.touchEvents={start:v.support.touch||!v.params.simulateTouch?"touchstart":w[0],move:v.support.touch||!v.params.simulateTouch?"touchmove":w[1],end:v.support.touch||!v.params.simulateTouch?"touchend":w[2]},(window.navigator.pointerEnabled||window.navigator.msPointerEnabled)&&("container"===v.params.touchEventsTarget?v.container:v.wrapper).addClass("swiper-wp8-"+v.params.direction),v.initEvents=function(e){var a=e?"off":"on",t=e?"removeEventListener":"addEventListener",r="container"===v.params.touchEventsTarget?v.container[0]:v.wrapper[0],i=v.support.touch?r:document,s=v.params.nested?!0:!1;v.browser.ie?(r[t](v.touchEvents.start,v.onTouchStart,!1),i[t](v.touchEvents.move,v.onTouchMove,s),i[t](v.touchEvents.end,v.onTouchEnd,!1)):(v.support.touch&&(r[t](v.touchEvents.start,v.onTouchStart,!1),r[t](v.touchEvents.move,v.onTouchMove,s),r[t](v.touchEvents.end,v.onTouchEnd,!1)),!n.simulateTouch||v.device.ios||v.device.android||(r[t]("mousedown",v.onTouchStart,!1),document[t]("mousemove",v.onTouchMove,s),document[t]("mouseup",v.onTouchEnd,!1))),window[t]("resize",v.onResize),v.params.nextButton&&v.nextButton&&v.nextButton.length>0&&(v.nextButton[a]("click",v.onClickNext),v.params.a11y&&v.a11y&&v.nextButton[a]("keydown",v.a11y.onEnterKey)),v.params.prevButton&&v.prevButton&&v.prevButton.length>0&&(v.prevButton[a]("click",v.onClickPrev),v.params.a11y&&v.a11y&&v.prevButton[a]("keydown",v.a11y.onEnterKey)),v.params.pagination&&v.params.paginationClickable&&(v.paginationContainer[a]("click","."+v.params.bulletClass,v.onClickIndex),v.params.a11y&&v.a11y&&v.paginationContainer[a]("keydown","."+v.params.bulletClass,v.a11y.onEnterKey)),(v.params.preventClicks||v.params.preventClicksPropagation)&&r[t]("click",v.preventClicks,!0)},v.attachEvents=function(){v.initEvents()},v.detachEvents=function(){v.initEvents(!0)},v.allowClick=!0,v.preventClicks=function(e){v.allowClick||(v.params.preventClicks&&e.preventDefault(),v.params.preventClicksPropagation&&v.animating&&(e.stopPropagation(),e.stopImmediatePropagation()))},v.onClickNext=function(e){e.preventDefault(),(!v.isEnd||v.params.loop)&&v.slideNext()},v.onClickPrev=function(e){e.preventDefault(),(!v.isBeginning||v.params.loop)&&v.slidePrev()},v.onClickIndex=function(e){e.preventDefault();var t=a(this).index()*v.params.slidesPerGroup;v.params.loop&&(t+=v.loopedSlides),v.slideTo(t)},v.updateClickedSlide=function(e){var t=s(e,"."+v.params.slideClass),n=!1;if(t)for(var r=0;r<v.slides.length;r++)v.slides[r]===t&&(n=!0);if(!t||!n)return v.clickedSlide=void 0,void(v.clickedIndex=void 0);if(v.clickedSlide=t,v.clickedIndex=a(t).index(),v.params.slideToClickedSlide&&void 0!==v.clickedIndex&&v.clickedIndex!==v.activeIndex){var i,o=v.clickedIndex;if(v.params.loop){if(v.animating)return;i=a(v.clickedSlide).attr("data-swiper-slide-index"),v.params.centeredSlides?o<v.loopedSlides-v.params.slidesPerView/2||o>v.slides.length-v.loopedSlides+v.params.slidesPerView/2?(v.fixLoop(),o=v.wrapper.children("."+v.params.slideClass+'[data-swiper-slide-index="'+i+'"]:not(.swiper-slide-duplicate)').eq(0).index(),setTimeout(function(){v.slideTo(o)},0)):v.slideTo(o):o>v.slides.length-v.params.slidesPerView?(v.fixLoop(),o=v.wrapper.children("."+v.params.slideClass+'[data-swiper-slide-index="'+i+'"]:not(.swiper-slide-duplicate)').eq(0).index(),setTimeout(function(){v.slideTo(o)},0)):v.slideTo(o)}else v.slideTo(o)}};var C,y,x,T,k,P,S,M,I,O,E="input, select, textarea, button",D=Date.now(),L=[];v.animating=!1,v.touches={startX:0,startY:0,currentX:0,currentY:0,diff:0};var B,z;v.onTouchStart=function(e){if(e.originalEvent&&(e=e.originalEvent),B="touchstart"===e.type,B||!("which"in e)||3!==e.which){if(v.params.noSwiping&&s(e,"."+v.params.noSwipingClass))return void(v.allowClick=!0);if(!v.params.swipeHandler||s(e,v.params.swipeHandler)){var t=v.touches.currentX="touchstart"===e.type?e.targetTouches[0].pageX:e.pageX,n=v.touches.currentY="touchstart"===e.type?e.targetTouches[0].pageY:e.pageY;if(!(v.device.ios&&v.params.iOSEdgeSwipeDetection&&t<=v.params.iOSEdgeSwipeThreshold)){if(C=!0,y=!1,x=!0,k=void 0,z=void 0,v.touches.startX=t,v.touches.startY=n,T=Date.now(),v.allowClick=!0,v.updateContainerSize(),v.swipeDirection=void 0,v.params.threshold>0&&(M=!1),"touchstart"!==e.type){var r=!0;a(e.target).is(E)&&(r=!1),document.activeElement&&a(document.activeElement).is(E)&&document.activeElement.blur(),r&&e.preventDefault()}v.emit("onTouchStart",v,e)}}}},v.onTouchMove=function(e){if(e.originalEvent&&(e=e.originalEvent),!B||"mousemove"!==e.type){if(e.preventedByNestedSwiper)return v.touches.startX="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,void(v.touches.startY="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY);if(v.params.onlyExternal)return v.allowClick=!1,void(C&&(v.touches.startX=v.touches.currentX="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,v.touches.startY=v.touches.currentY="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY,T=Date.now()));if(B&&document.activeElement&&e.target===document.activeElement&&a(e.target).is(E))return y=!0,void(v.allowClick=!1);if(x&&v.emit("onTouchMove",v,e),!(e.targetTouches&&e.targetTouches.length>1)){if(v.touches.currentX="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,v.touches.currentY="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY,"undefined"==typeof k){var t=180*Math.atan2(Math.abs(v.touches.currentY-v.touches.startY),Math.abs(v.touches.currentX-v.touches.startX))/Math.PI;k=v.isHorizontal()?t>v.params.touchAngle:90-t>v.params.touchAngle}if(k&&v.emit("onTouchMoveOpposite",v,e),"undefined"==typeof z&&v.browser.ieTouch&&(v.touches.currentX!==v.touches.startX||v.touches.currentY!==v.touches.startY)&&(z=!0),C){if(k)return void(C=!1);if(z||!v.browser.ieTouch){v.allowClick=!1,v.emit("onSliderMove",v,e),e.preventDefault(),v.params.touchMoveStopPropagation&&!v.params.nested&&e.stopPropagation(),y||(n.loop&&v.fixLoop(),S=v.getWrapperTranslate(),v.setWrapperTransition(0),v.animating&&v.wrapper.trigger("webkitTransitionEnd transitionend oTransitionEnd MSTransitionEnd msTransitionEnd"),v.params.autoplay&&v.autoplaying&&(v.params.autoplayDisableOnInteraction?v.stopAutoplay():v.pauseAutoplay()),O=!1,v.params.grabCursor&&(v.container[0].style.cursor="move",v.container[0].style.cursor="-webkit-grabbing",v.container[0].style.cursor="-moz-grabbin",v.container[0].style.cursor="grabbing")),y=!0;var r=v.touches.diff=v.isHorizontal()?v.touches.currentX-v.touches.startX:v.touches.currentY-v.touches.startY;r*=v.params.touchRatio,v.rtl&&(r=-r),v.swipeDirection=r>0?"prev":"next",P=r+S;var i=!0;if(r>0&&P>v.minTranslate()?(i=!1,v.params.resistance&&(P=v.minTranslate()-1+Math.pow(-v.minTranslate()+S+r,v.params.resistanceRatio))):0>r&&P<v.maxTranslate()&&(i=!1,v.params.resistance&&(P=v.maxTranslate()+1-Math.pow(v.maxTranslate()-S-r,v.params.resistanceRatio))),i&&(e.preventedByNestedSwiper=!0),!v.params.allowSwipeToNext&&"next"===v.swipeDirection&&S>P&&(P=S),!v.params.allowSwipeToPrev&&"prev"===v.swipeDirection&&P>S&&(P=S),v.params.followFinger){if(v.params.threshold>0){if(!(Math.abs(r)>v.params.threshold||M))return void(P=S);if(!M)return M=!0,v.touches.startX=v.touches.currentX,v.touches.startY=v.touches.currentY,P=S,void(v.touches.diff=v.isHorizontal()?v.touches.currentX-v.touches.startX:v.touches.currentY-v.touches.startY)}(v.params.freeMode||v.params.watchSlidesProgress)&&v.updateActiveIndex(),v.params.freeMode&&(0===L.length&&L.push({position:v.touches[v.isHorizontal()?"startX":"startY"],time:T}),L.push({position:v.touches[v.isHorizontal()?"currentX":"currentY"],
time:(new window.Date).getTime()})),v.updateProgress(P),v.setWrapperTranslate(P)}}}}}},v.onTouchEnd=function(e){if(e.originalEvent&&(e=e.originalEvent),x&&v.emit("onTouchEnd",v,e),x=!1,C){v.params.grabCursor&&y&&C&&(v.container[0].style.cursor="move",v.container[0].style.cursor="-webkit-grab",v.container[0].style.cursor="-moz-grab",v.container[0].style.cursor="grab");var t=Date.now(),n=t-T;if(v.allowClick&&(v.updateClickedSlide(e),v.emit("onTap",v,e),300>n&&t-D>300&&(I&&clearTimeout(I),I=setTimeout(function(){v&&(v.params.paginationHide&&v.paginationContainer.length>0&&!a(e.target).hasClass(v.params.bulletClass)&&v.paginationContainer.toggleClass(v.params.paginationHiddenClass),v.emit("onClick",v,e))},300)),300>n&&300>t-D&&(I&&clearTimeout(I),v.emit("onDoubleTap",v,e))),D=Date.now(),setTimeout(function(){v&&(v.allowClick=!0)},0),!C||!y||!v.swipeDirection||0===v.touches.diff||P===S)return void(C=y=!1);C=y=!1;var r;if(r=v.params.followFinger?v.rtl?v.translate:-v.translate:-P,v.params.freeMode){if(r<-v.minTranslate())return void v.slideTo(v.activeIndex);if(r>-v.maxTranslate())return void(v.slides.length<v.snapGrid.length?v.slideTo(v.snapGrid.length-1):v.slideTo(v.slides.length-1));if(v.params.freeModeMomentum){if(L.length>1){var i=L.pop(),s=L.pop(),o=i.position-s.position,l=i.time-s.time;v.velocity=o/l,v.velocity=v.velocity/2,Math.abs(v.velocity)<v.params.freeModeMinimumVelocity&&(v.velocity=0),(l>150||(new window.Date).getTime()-i.time>300)&&(v.velocity=0)}else v.velocity=0;L.length=0;var p=1e3*v.params.freeModeMomentumRatio,d=v.velocity*p,c=v.translate+d;v.rtl&&(c=-c);var u,m=!1,f=20*Math.abs(v.velocity)*v.params.freeModeMomentumBounceRatio;if(c<v.maxTranslate())v.params.freeModeMomentumBounce?(c+v.maxTranslate()<-f&&(c=v.maxTranslate()-f),u=v.maxTranslate(),m=!0,O=!0):c=v.maxTranslate();else if(c>v.minTranslate())v.params.freeModeMomentumBounce?(c-v.minTranslate()>f&&(c=v.minTranslate()+f),u=v.minTranslate(),m=!0,O=!0):c=v.minTranslate();else if(v.params.freeModeSticky){var h,g=0;for(g=0;g<v.snapGrid.length;g+=1)if(v.snapGrid[g]>-c){h=g;break}c=Math.abs(v.snapGrid[h]-c)<Math.abs(v.snapGrid[h-1]-c)||"next"===v.swipeDirection?v.snapGrid[h]:v.snapGrid[h-1],v.rtl||(c=-c)}if(0!==v.velocity)p=v.rtl?Math.abs((-c-v.translate)/v.velocity):Math.abs((c-v.translate)/v.velocity);else if(v.params.freeModeSticky)return void v.slideReset();v.params.freeModeMomentumBounce&&m?(v.updateProgress(u),v.setWrapperTransition(p),v.setWrapperTranslate(c),v.onTransitionStart(),v.animating=!0,v.wrapper.transitionEnd(function(){v&&O&&(v.emit("onMomentumBounce",v),v.setWrapperTransition(v.params.speed),v.setWrapperTranslate(u),v.wrapper.transitionEnd(function(){v&&v.onTransitionEnd()}))})):v.velocity?(v.updateProgress(c),v.setWrapperTransition(p),v.setWrapperTranslate(c),v.onTransitionStart(),v.animating||(v.animating=!0,v.wrapper.transitionEnd(function(){v&&v.onTransitionEnd()}))):v.updateProgress(c),v.updateActiveIndex()}return void((!v.params.freeModeMomentum||n>=v.params.longSwipesMs)&&(v.updateProgress(),v.updateActiveIndex()))}var b,w=0,k=v.slidesSizesGrid[0];for(b=0;b<v.slidesGrid.length;b+=v.params.slidesPerGroup)"undefined"!=typeof v.slidesGrid[b+v.params.slidesPerGroup]?r>=v.slidesGrid[b]&&r<v.slidesGrid[b+v.params.slidesPerGroup]&&(w=b,k=v.slidesGrid[b+v.params.slidesPerGroup]-v.slidesGrid[b]):r>=v.slidesGrid[b]&&(w=b,k=v.slidesGrid[v.slidesGrid.length-1]-v.slidesGrid[v.slidesGrid.length-2]);var M=(r-v.slidesGrid[w])/k;if(n>v.params.longSwipesMs){if(!v.params.longSwipes)return void v.slideTo(v.activeIndex);"next"===v.swipeDirection&&(M>=v.params.longSwipesRatio?v.slideTo(w+v.params.slidesPerGroup):v.slideTo(w)),"prev"===v.swipeDirection&&(M>1-v.params.longSwipesRatio?v.slideTo(w+v.params.slidesPerGroup):v.slideTo(w))}else{if(!v.params.shortSwipes)return void v.slideTo(v.activeIndex);"next"===v.swipeDirection&&v.slideTo(w+v.params.slidesPerGroup),"prev"===v.swipeDirection&&v.slideTo(w)}}},v._slideTo=function(e,a){return v.slideTo(e,a,!0,!0)},v.slideTo=function(e,a,t,n){"undefined"==typeof t&&(t=!0),"undefined"==typeof e&&(e=0),0>e&&(e=0),v.snapIndex=Math.floor(e/v.params.slidesPerGroup),v.snapIndex>=v.snapGrid.length&&(v.snapIndex=v.snapGrid.length-1);var r=-v.snapGrid[v.snapIndex];v.params.autoplay&&v.autoplaying&&(n||!v.params.autoplayDisableOnInteraction?v.pauseAutoplay(a):v.stopAutoplay()),v.updateProgress(r);for(var i=0;i<v.slidesGrid.length;i++)-Math.floor(100*r)>=Math.floor(100*v.slidesGrid[i])&&(e=i);return!v.params.allowSwipeToNext&&r<v.translate&&r<v.minTranslate()?!1:!v.params.allowSwipeToPrev&&r>v.translate&&r>v.maxTranslate()&&(v.activeIndex||0)!==e?!1:("undefined"==typeof a&&(a=v.params.speed),v.previousIndex=v.activeIndex||0,v.activeIndex=e,v.rtl&&-r===v.translate||!v.rtl&&r===v.translate?(v.params.autoHeight&&v.updateAutoHeight(),v.updateClasses(),"slide"!==v.params.effect&&v.setWrapperTranslate(r),!1):(v.updateClasses(),v.onTransitionStart(t),0===a?(v.setWrapperTranslate(r),v.setWrapperTransition(0),v.onTransitionEnd(t)):(v.setWrapperTranslate(r),v.setWrapperTransition(a),v.animating||(v.animating=!0,v.wrapper.transitionEnd(function(){v&&v.onTransitionEnd(t)}))),!0))},v.onTransitionStart=function(e){"undefined"==typeof e&&(e=!0),v.params.autoHeight&&v.updateAutoHeight(),v.lazy&&v.lazy.onTransitionStart(),e&&(v.emit("onTransitionStart",v),v.activeIndex!==v.previousIndex&&(v.emit("onSlideChangeStart",v),v.activeIndex>v.previousIndex?v.emit("onSlideNextStart",v):v.emit("onSlidePrevStart",v)))},v.onTransitionEnd=function(e){v.animating=!1,v.setWrapperTransition(0),"undefined"==typeof e&&(e=!0),v.lazy&&v.lazy.onTransitionEnd(),e&&(v.emit("onTransitionEnd",v),v.activeIndex!==v.previousIndex&&(v.emit("onSlideChangeEnd",v),v.activeIndex>v.previousIndex?v.emit("onSlideNextEnd",v):v.emit("onSlidePrevEnd",v))),v.params.hashnav&&v.hashnav&&v.hashnav.setHash()},v.slideNext=function(e,a,t){if(v.params.loop){if(v.animating)return!1;v.fixLoop();v.container[0].clientLeft;return v.slideTo(v.activeIndex+v.params.slidesPerGroup,a,e,t)}return v.slideTo(v.activeIndex+v.params.slidesPerGroup,a,e,t)},v._slideNext=function(e){return v.slideNext(!0,e,!0)},v.slidePrev=function(e,a,t){if(v.params.loop){if(v.animating)return!1;v.fixLoop();v.container[0].clientLeft;return v.slideTo(v.activeIndex-1,a,e,t)}return v.slideTo(v.activeIndex-1,a,e,t)},v._slidePrev=function(e){return v.slidePrev(!0,e,!0)},v.slideReset=function(e,a,t){return v.slideTo(v.activeIndex,a,e)},v.setWrapperTransition=function(e,a){v.wrapper.transition(e),"slide"!==v.params.effect&&v.effects[v.params.effect]&&v.effects[v.params.effect].setTransition(e),v.params.parallax&&v.parallax&&v.parallax.setTransition(e),v.params.scrollbar&&v.scrollbar&&v.scrollbar.setTransition(e),v.params.control&&v.controller&&v.controller.setTransition(e,a),v.emit("onSetTransition",v,e)},v.setWrapperTranslate=function(e,a,t){var n=0,i=0,s=0;v.isHorizontal()?n=v.rtl?-e:e:i=e,v.params.roundLengths&&(n=r(n),i=r(i)),v.params.virtualTranslate||(v.support.transforms3d?v.wrapper.transform("translate3d("+n+"px, "+i+"px, "+s+"px)"):v.wrapper.transform("translate("+n+"px, "+i+"px)")),v.translate=v.isHorizontal()?n:i;var o,l=v.maxTranslate()-v.minTranslate();o=0===l?0:(e-v.minTranslate())/l,o!==v.progress&&v.updateProgress(e),a&&v.updateActiveIndex(),"slide"!==v.params.effect&&v.effects[v.params.effect]&&v.effects[v.params.effect].setTranslate(v.translate),v.params.parallax&&v.parallax&&v.parallax.setTranslate(v.translate),v.params.scrollbar&&v.scrollbar&&v.scrollbar.setTranslate(v.translate),v.params.control&&v.controller&&v.controller.setTranslate(v.translate,t),v.emit("onSetTranslate",v,v.translate)},v.getTranslate=function(e,a){var t,n,r,i;return"undefined"==typeof a&&(a="x"),v.params.virtualTranslate?v.rtl?-v.translate:v.translate:(r=window.getComputedStyle(e,null),window.WebKitCSSMatrix?(n=r.transform||r.webkitTransform,n.split(",").length>6&&(n=n.split(", ").map(function(e){return e.replace(",",".")}).join(", ")),i=new window.WebKitCSSMatrix("none"===n?"":n)):(i=r.MozTransform||r.OTransform||r.MsTransform||r.msTransform||r.transform||r.getPropertyValue("transform").replace("translate(","matrix(1, 0, 0, 1,"),t=i.toString().split(",")),"x"===a&&(n=window.WebKitCSSMatrix?i.m41:16===t.length?parseFloat(t[12]):parseFloat(t[4])),"y"===a&&(n=window.WebKitCSSMatrix?i.m42:16===t.length?parseFloat(t[13]):parseFloat(t[5])),v.rtl&&n&&(n=-n),n||0)},v.getWrapperTranslate=function(e){return"undefined"==typeof e&&(e=v.isHorizontal()?"x":"y"),v.getTranslate(v.wrapper[0],e)},v.observers=[],v.initObservers=function(){if(v.params.observeParents)for(var e=v.container.parents(),a=0;a<e.length;a++)o(e[a]);o(v.container[0],{childList:!1}),o(v.wrapper[0],{attributes:!1})},v.disconnectObservers=function(){for(var e=0;e<v.observers.length;e++)v.observers[e].disconnect();v.observers=[]},v.createLoop=function(){v.wrapper.children("."+v.params.slideClass+"."+v.params.slideDuplicateClass).remove();var e=v.wrapper.children("."+v.params.slideClass);"auto"!==v.params.slidesPerView||v.params.loopedSlides||(v.params.loopedSlides=e.length),v.loopedSlides=parseInt(v.params.loopedSlides||v.params.slidesPerView,10),v.loopedSlides=v.loopedSlides+v.params.loopAdditionalSlides,v.loopedSlides>e.length&&(v.loopedSlides=e.length);var t,n=[],r=[];for(e.each(function(t,i){var s=a(this);t<v.loopedSlides&&r.push(i),t<e.length&&t>=e.length-v.loopedSlides&&n.push(i),s.attr("data-swiper-slide-index",t)}),t=0;t<r.length;t++)v.wrapper.append(a(r[t].cloneNode(!0)).addClass(v.params.slideDuplicateClass));for(t=n.length-1;t>=0;t--)v.wrapper.prepend(a(n[t].cloneNode(!0)).addClass(v.params.slideDuplicateClass))},v.destroyLoop=function(){v.wrapper.children("."+v.params.slideClass+"."+v.params.slideDuplicateClass).remove(),v.slides.removeAttr("data-swiper-slide-index")},v.reLoop=function(e){var a=v.activeIndex-v.loopedSlides;v.destroyLoop(),v.createLoop(),v.updateSlidesSize(),e&&v.slideTo(a+v.loopedSlides,0,!1)},v.fixLoop=function(){var e;v.activeIndex<v.loopedSlides?(e=v.slides.length-3*v.loopedSlides+v.activeIndex,e+=v.loopedSlides,v.slideTo(e,0,!1,!0)):("auto"===v.params.slidesPerView&&v.activeIndex>=2*v.loopedSlides||v.activeIndex>v.slides.length-2*v.params.slidesPerView)&&(e=-v.slides.length+v.activeIndex+v.loopedSlides,e+=v.loopedSlides,v.slideTo(e,0,!1,!0))},v.appendSlide=function(e){if(v.params.loop&&v.destroyLoop(),"object"==typeof e&&e.length)for(var a=0;a<e.length;a++)e[a]&&v.wrapper.append(e[a]);else v.wrapper.append(e);v.params.loop&&v.createLoop(),v.params.observer&&v.support.observer||v.update(!0)},v.prependSlide=function(e){v.params.loop&&v.destroyLoop();var a=v.activeIndex+1;if("object"==typeof e&&e.length){for(var t=0;t<e.length;t++)e[t]&&v.wrapper.prepend(e[t]);a=v.activeIndex+e.length}else v.wrapper.prepend(e);v.params.loop&&v.createLoop(),v.params.observer&&v.support.observer||v.update(!0),v.slideTo(a,0,!1)},v.removeSlide=function(e){v.params.loop&&(v.destroyLoop(),v.slides=v.wrapper.children("."+v.params.slideClass));var a,t=v.activeIndex;if("object"==typeof e&&e.length){for(var n=0;n<e.length;n++)a=e[n],v.slides[a]&&v.slides.eq(a).remove(),t>a&&t--;t=Math.max(t,0)}else a=e,v.slides[a]&&v.slides.eq(a).remove(),t>a&&t--,t=Math.max(t,0);v.params.loop&&v.createLoop(),v.params.observer&&v.support.observer||v.update(!0),v.params.loop?v.slideTo(t+v.loopedSlides,0,!1):v.slideTo(t,0,!1)},v.removeAllSlides=function(){for(var e=[],a=0;a<v.slides.length;a++)e.push(a);v.removeSlide(e)},v.effects={fade:{setTranslate:function(){for(var e=0;e<v.slides.length;e++){var a=v.slides.eq(e),t=a[0].swiperSlideOffset,n=-t;v.params.virtualTranslate||(n-=v.translate);var r=0;v.isHorizontal()||(r=n,n=0);var i=v.params.fade.crossFade?Math.max(1-Math.abs(a[0].progress),0):1+Math.min(Math.max(a[0].progress,-1),0);a.css({opacity:i}).transform("translate3d("+n+"px, "+r+"px, 0px)")}},setTransition:function(e){if(v.slides.transition(e),v.params.virtualTranslate&&0!==e){var a=!1;v.slides.transitionEnd(function(){if(!a&&v){a=!0,v.animating=!1;for(var e=["webkitTransitionEnd","transitionend","oTransitionEnd","MSTransitionEnd","msTransitionEnd"],t=0;t<e.length;t++)v.wrapper.trigger(e[t])}})}}},flip:{setTranslate:function(){for(var e=0;e<v.slides.length;e++){var t=v.slides.eq(e),n=t[0].progress;v.params.flip.limitRotation&&(n=Math.max(Math.min(t[0].progress,1),-1));var r=t[0].swiperSlideOffset,i=-180*n,s=i,o=0,l=-r,p=0;if(v.isHorizontal()?v.rtl&&(s=-s):(p=l,l=0,o=-s,s=0),t[0].style.zIndex=-Math.abs(Math.round(n))+v.slides.length,v.params.flip.slideShadows){var d=v.isHorizontal()?t.find(".swiper-slide-shadow-left"):t.find(".swiper-slide-shadow-top"),c=v.isHorizontal()?t.find(".swiper-slide-shadow-right"):t.find(".swiper-slide-shadow-bottom");0===d.length&&(d=a('<div class="swiper-slide-shadow-'+(v.isHorizontal()?"left":"top")+'"></div>'),t.append(d)),0===c.length&&(c=a('<div class="swiper-slide-shadow-'+(v.isHorizontal()?"right":"bottom")+'"></div>'),t.append(c)),d.length&&(d[0].style.opacity=Math.max(-n,0)),c.length&&(c[0].style.opacity=Math.max(n,0))}t.transform("translate3d("+l+"px, "+p+"px, 0px) rotateX("+o+"deg) rotateY("+s+"deg)")}},setTransition:function(e){if(v.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e),v.params.virtualTranslate&&0!==e){var t=!1;v.slides.eq(v.activeIndex).transitionEnd(function(){if(!t&&v&&a(this).hasClass(v.params.slideActiveClass)){t=!0,v.animating=!1;for(var e=["webkitTransitionEnd","transitionend","oTransitionEnd","MSTransitionEnd","msTransitionEnd"],n=0;n<e.length;n++)v.wrapper.trigger(e[n])}})}}},cube:{setTranslate:function(){var e,t=0;v.params.cube.shadow&&(v.isHorizontal()?(e=v.wrapper.find(".swiper-cube-shadow"),0===e.length&&(e=a('<div class="swiper-cube-shadow"></div>'),v.wrapper.append(e)),e.css({height:v.width+"px"})):(e=v.container.find(".swiper-cube-shadow"),0===e.length&&(e=a('<div class="swiper-cube-shadow"></div>'),v.container.append(e))));for(var n=0;n<v.slides.length;n++){var r=v.slides.eq(n),i=90*n,s=Math.floor(i/360);v.rtl&&(i=-i,s=Math.floor(-i/360));var o=Math.max(Math.min(r[0].progress,1),-1),l=0,p=0,d=0;n%4===0?(l=4*-s*v.size,d=0):(n-1)%4===0?(l=0,d=4*-s*v.size):(n-2)%4===0?(l=v.size+4*s*v.size,d=v.size):(n-3)%4===0&&(l=-v.size,d=3*v.size+4*v.size*s),v.rtl&&(l=-l),v.isHorizontal()||(p=l,l=0);var c="rotateX("+(v.isHorizontal()?0:-i)+"deg) rotateY("+(v.isHorizontal()?i:0)+"deg) translate3d("+l+"px, "+p+"px, "+d+"px)";if(1>=o&&o>-1&&(t=90*n+90*o,v.rtl&&(t=90*-n-90*o)),r.transform(c),v.params.cube.slideShadows){var u=v.isHorizontal()?r.find(".swiper-slide-shadow-left"):r.find(".swiper-slide-shadow-top"),m=v.isHorizontal()?r.find(".swiper-slide-shadow-right"):r.find(".swiper-slide-shadow-bottom");0===u.length&&(u=a('<div class="swiper-slide-shadow-'+(v.isHorizontal()?"left":"top")+'"></div>'),r.append(u)),0===m.length&&(m=a('<div class="swiper-slide-shadow-'+(v.isHorizontal()?"right":"bottom")+'"></div>'),r.append(m)),u.length&&(u[0].style.opacity=Math.max(-o,0)),m.length&&(m[0].style.opacity=Math.max(o,0))}}if(v.wrapper.css({"-webkit-transform-origin":"50% 50% -"+v.size/2+"px","-moz-transform-origin":"50% 50% -"+v.size/2+"px","-ms-transform-origin":"50% 50% -"+v.size/2+"px","transform-origin":"50% 50% -"+v.size/2+"px"}),v.params.cube.shadow)if(v.isHorizontal())e.transform("translate3d(0px, "+(v.width/2+v.params.cube.shadowOffset)+"px, "+-v.width/2+"px) rotateX(90deg) rotateZ(0deg) scale("+v.params.cube.shadowScale+")");else{var f=Math.abs(t)-90*Math.floor(Math.abs(t)/90),h=1.5-(Math.sin(2*f*Math.PI/360)/2+Math.cos(2*f*Math.PI/360)/2),g=v.params.cube.shadowScale,b=v.params.cube.shadowScale/h,w=v.params.cube.shadowOffset;e.transform("scale3d("+g+", 1, "+b+") translate3d(0px, "+(v.height/2+w)+"px, "+-v.height/2/b+"px) rotateX(-90deg)")}var C=v.isSafari||v.isUiWebView?-v.size/2:0;v.wrapper.transform("translate3d(0px,0,"+C+"px) rotateX("+(v.isHorizontal()?0:t)+"deg) rotateY("+(v.isHorizontal()?-t:0)+"deg)")},setTransition:function(e){v.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e),v.params.cube.shadow&&!v.isHorizontal()&&v.container.find(".swiper-cube-shadow").transition(e)}},coverflow:{setTranslate:function(){for(var e=v.translate,t=v.isHorizontal()?-e+v.width/2:-e+v.height/2,n=v.isHorizontal()?v.params.coverflow.rotate:-v.params.coverflow.rotate,r=v.params.coverflow.depth,i=0,s=v.slides.length;s>i;i++){var o=v.slides.eq(i),l=v.slidesSizesGrid[i],p=o[0].swiperSlideOffset,d=(t-p-l/2)/l*v.params.coverflow.modifier,c=v.isHorizontal()?n*d:0,u=v.isHorizontal()?0:n*d,m=-r*Math.abs(d),f=v.isHorizontal()?0:v.params.coverflow.stretch*d,h=v.isHorizontal()?v.params.coverflow.stretch*d:0;Math.abs(h)<.001&&(h=0),Math.abs(f)<.001&&(f=0),Math.abs(m)<.001&&(m=0),Math.abs(c)<.001&&(c=0),Math.abs(u)<.001&&(u=0);var g="translate3d("+h+"px,"+f+"px,"+m+"px)  rotateX("+u+"deg) rotateY("+c+"deg)";if(o.transform(g),o[0].style.zIndex=-Math.abs(Math.round(d))+1,v.params.coverflow.slideShadows){var b=v.isHorizontal()?o.find(".swiper-slide-shadow-left"):o.find(".swiper-slide-shadow-top"),w=v.isHorizontal()?o.find(".swiper-slide-shadow-right"):o.find(".swiper-slide-shadow-bottom");0===b.length&&(b=a('<div class="swiper-slide-shadow-'+(v.isHorizontal()?"left":"top")+'"></div>'),o.append(b)),0===w.length&&(w=a('<div class="swiper-slide-shadow-'+(v.isHorizontal()?"right":"bottom")+'"></div>'),o.append(w)),b.length&&(b[0].style.opacity=d>0?d:0),w.length&&(w[0].style.opacity=-d>0?-d:0)}}if(v.browser.ie){var C=v.wrapper[0].style;C.perspectiveOrigin=t+"px 50%"}},setTransition:function(e){v.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e)}}},v.lazy={initialImageLoaded:!1,loadImageInSlide:function(e,t){if("undefined"!=typeof e&&("undefined"==typeof t&&(t=!0),0!==v.slides.length)){var n=v.slides.eq(e),r=n.find(".swiper-lazy:not(.swiper-lazy-loaded):not(.swiper-lazy-loading)");!n.hasClass("swiper-lazy")||n.hasClass("swiper-lazy-loaded")||n.hasClass("swiper-lazy-loading")||(r=r.add(n[0])),0!==r.length&&r.each(function(){var e=a(this);e.addClass("swiper-lazy-loading");var r=e.attr("data-background"),i=e.attr("data-src"),s=e.attr("data-srcset");v.loadImage(e[0],i||r,s,!1,function(){if(r?(e.css("background-image",'url("'+r+'")'),e.removeAttr("data-background")):(s&&(e.attr("srcset",s),e.removeAttr("data-srcset")),i&&(e.attr("src",i),e.removeAttr("data-src"))),e.addClass("swiper-lazy-loaded").removeClass("swiper-lazy-loading"),n.find(".swiper-lazy-preloader, .preloader").remove(),v.params.loop&&t){var a=n.attr("data-swiper-slide-index");if(n.hasClass(v.params.slideDuplicateClass)){var o=v.wrapper.children('[data-swiper-slide-index="'+a+'"]:not(.'+v.params.slideDuplicateClass+")");v.lazy.loadImageInSlide(o.index(),!1)}else{var l=v.wrapper.children("."+v.params.slideDuplicateClass+'[data-swiper-slide-index="'+a+'"]');v.lazy.loadImageInSlide(l.index(),!1)}}v.emit("onLazyImageReady",v,n[0],e[0])}),v.emit("onLazyImageLoad",v,n[0],e[0])})}},load:function(){var e;if(v.params.watchSlidesVisibility)v.wrapper.children("."+v.params.slideVisibleClass).each(function(){v.lazy.loadImageInSlide(a(this).index())});else if(v.params.slidesPerView>1)for(e=v.activeIndex;e<v.activeIndex+v.params.slidesPerView;e++)v.slides[e]&&v.lazy.loadImageInSlide(e);else v.lazy.loadImageInSlide(v.activeIndex);if(v.params.lazyLoadingInPrevNext)if(v.params.slidesPerView>1||v.params.lazyLoadingInPrevNextAmount&&v.params.lazyLoadingInPrevNextAmount>1){var t=v.params.lazyLoadingInPrevNextAmount,n=v.params.slidesPerView,r=Math.min(v.activeIndex+n+Math.max(t,n),v.slides.length),i=Math.max(v.activeIndex-Math.max(n,t),0);for(e=v.activeIndex+v.params.slidesPerView;r>e;e++)v.slides[e]&&v.lazy.loadImageInSlide(e);for(e=i;e<v.activeIndex;e++)v.slides[e]&&v.lazy.loadImageInSlide(e)}else{var s=v.wrapper.children("."+v.params.slideNextClass);s.length>0&&v.lazy.loadImageInSlide(s.index());var o=v.wrapper.children("."+v.params.slidePrevClass);o.length>0&&v.lazy.loadImageInSlide(o.index())}},onTransitionStart:function(){v.params.lazyLoading&&(v.params.lazyLoadingOnTransitionStart||!v.params.lazyLoadingOnTransitionStart&&!v.lazy.initialImageLoaded)&&v.lazy.load()},onTransitionEnd:function(){v.params.lazyLoading&&!v.params.lazyLoadingOnTransitionStart&&v.lazy.load()}},v.scrollbar={isTouched:!1,setDragPosition:function(e){var a=v.scrollbar,t=v.isHorizontal()?"touchstart"===e.type||"touchmove"===e.type?e.targetTouches[0].pageX:e.pageX||e.clientX:"touchstart"===e.type||"touchmove"===e.type?e.targetTouches[0].pageY:e.pageY||e.clientY,n=t-a.track.offset()[v.isHorizontal()?"left":"top"]-a.dragSize/2,r=-v.minTranslate()*a.moveDivider,i=-v.maxTranslate()*a.moveDivider;r>n?n=r:n>i&&(n=i),n=-n/a.moveDivider,v.updateProgress(n),v.setWrapperTranslate(n,!0)},dragStart:function(e){var a=v.scrollbar;a.isTouched=!0,e.preventDefault(),e.stopPropagation(),a.setDragPosition(e),clearTimeout(a.dragTimeout),a.track.transition(0),v.params.scrollbarHide&&a.track.css("opacity",1),v.wrapper.transition(100),a.drag.transition(100),v.emit("onScrollbarDragStart",v)},dragMove:function(e){var a=v.scrollbar;a.isTouched&&(e.preventDefault?e.preventDefault():e.returnValue=!1,a.setDragPosition(e),v.wrapper.transition(0),a.track.transition(0),a.drag.transition(0),v.emit("onScrollbarDragMove",v))},dragEnd:function(e){var a=v.scrollbar;a.isTouched&&(a.isTouched=!1,v.params.scrollbarHide&&(clearTimeout(a.dragTimeout),a.dragTimeout=setTimeout(function(){a.track.css("opacity",0),a.track.transition(400)},1e3)),v.emit("onScrollbarDragEnd",v),v.params.scrollbarSnapOnRelease&&v.slideReset())},enableDraggable:function(){var e=v.scrollbar,t=v.support.touch?e.track:document;a(e.track).on(v.touchEvents.start,e.dragStart),a(t).on(v.touchEvents.move,e.dragMove),a(t).on(v.touchEvents.end,e.dragEnd)},disableDraggable:function(){var e=v.scrollbar,t=v.support.touch?e.track:document;a(e.track).off(v.touchEvents.start,e.dragStart),a(t).off(v.touchEvents.move,e.dragMove),a(t).off(v.touchEvents.end,e.dragEnd)},set:function(){if(v.params.scrollbar){var e=v.scrollbar;e.track=a(v.params.scrollbar),v.params.uniqueNavElements&&"string"==typeof v.params.scrollbar&&e.track.length>1&&1===v.container.find(v.params.scrollbar).length&&(e.track=v.container.find(v.params.scrollbar)),e.drag=e.track.find(".swiper-scrollbar-drag"),0===e.drag.length&&(e.drag=a('<div class="swiper-scrollbar-drag"></div>'),e.track.append(e.drag)),e.drag[0].style.width="",e.drag[0].style.height="",e.trackSize=v.isHorizontal()?e.track[0].offsetWidth:e.track[0].offsetHeight,e.divider=v.size/v.virtualSize,e.moveDivider=e.divider*(e.trackSize/v.size),e.dragSize=e.trackSize*e.divider,v.isHorizontal()?e.drag[0].style.width=e.dragSize+"px":e.drag[0].style.height=e.dragSize+"px",e.divider>=1?e.track[0].style.display="none":e.track[0].style.display="",v.params.scrollbarHide&&(e.track[0].style.opacity=0)}},setTranslate:function(){if(v.params.scrollbar){var e,a=v.scrollbar,t=(v.translate||0,a.dragSize);e=(a.trackSize-a.dragSize)*v.progress,v.rtl&&v.isHorizontal()?(e=-e,e>0?(t=a.dragSize-e,e=0):-e+a.dragSize>a.trackSize&&(t=a.trackSize+e)):0>e?(t=a.dragSize+e,e=0):e+a.dragSize>a.trackSize&&(t=a.trackSize-e),v.isHorizontal()?(v.support.transforms3d?a.drag.transform("translate3d("+e+"px, 0, 0)"):a.drag.transform("translateX("+e+"px)"),a.drag[0].style.width=t+"px"):(v.support.transforms3d?a.drag.transform("translate3d(0px, "+e+"px, 0)"):a.drag.transform("translateY("+e+"px)"),a.drag[0].style.height=t+"px"),v.params.scrollbarHide&&(clearTimeout(a.timeout),a.track[0].style.opacity=1,a.timeout=setTimeout(function(){a.track[0].style.opacity=0,a.track.transition(400)},1e3))}},setTransition:function(e){v.params.scrollbar&&v.scrollbar.drag.transition(e)}},v.controller={LinearSpline:function(e,a){this.x=e,this.y=a,this.lastIndex=e.length-1;var t,n;this.x.length;this.interpolate=function(e){return e?(n=r(this.x,e),t=n-1,(e-this.x[t])*(this.y[n]-this.y[t])/(this.x[n]-this.x[t])+this.y[t]):0};var r=function(){var e,a,t;return function(n,r){for(a=-1,e=n.length;e-a>1;)n[t=e+a>>1]<=r?a=t:e=t;return e}}()},getInterpolateFunction:function(e){v.controller.spline||(v.controller.spline=v.params.loop?new v.controller.LinearSpline(v.slidesGrid,e.slidesGrid):new v.controller.LinearSpline(v.snapGrid,e.snapGrid))},setTranslate:function(e,a){function t(a){e=a.rtl&&"horizontal"===a.params.direction?-v.translate:v.translate,"slide"===v.params.controlBy&&(v.controller.getInterpolateFunction(a),r=-v.controller.spline.interpolate(-e)),r&&"container"!==v.params.controlBy||(n=(a.maxTranslate()-a.minTranslate())/(v.maxTranslate()-v.minTranslate()),r=(e-v.minTranslate())*n+a.minTranslate()),v.params.controlInverse&&(r=a.maxTranslate()-r),a.updateProgress(r),a.setWrapperTranslate(r,!1,v),a.updateActiveIndex()}var n,r,i=v.params.control;if(v.isArray(i))for(var s=0;s<i.length;s++)i[s]!==a&&i[s]instanceof Swiper&&t(i[s]);else i instanceof Swiper&&a!==i&&t(i)},setTransition:function(e,a){function t(a){a.setWrapperTransition(e,v),0!==e&&(a.onTransitionStart(),a.wrapper.transitionEnd(function(){r&&(a.params.loop&&"slide"===v.params.controlBy&&a.fixLoop(),a.onTransitionEnd())}))}var n,r=v.params.control;if(v.isArray(r))for(n=0;n<r.length;n++)r[n]!==a&&r[n]instanceof Swiper&&t(r[n]);else r instanceof Swiper&&a!==r&&t(r)}},v.parallax={setTranslate:function(){v.container.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(){l(this,v.progress)}),v.slides.each(function(){var e=a(this);e.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(){var a=Math.min(Math.max(e[0].progress,-1),1);l(this,a)})})},setTransition:function(e){"undefined"==typeof e&&(e=v.params.speed),v.container.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(){var t=a(this),n=parseInt(t.attr("data-swiper-parallax-duration"),10)||e;0===e&&(n=0),t.transition(n)})}},v._plugins=[];for(var N in v.plugins){var H=v.plugins[N](v,v.params[N]);H&&v._plugins.push(H)}return v.callPlugins=function(e){for(var a=0;a<v._plugins.length;a++)e in v._plugins[a]&&v._plugins[a][e](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5])},v.emitterEventListeners={},v.emit=function(e){v.params[e]&&v.params[e](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5]);var a;if(v.emitterEventListeners[e])for(a=0;a<v.emitterEventListeners[e].length;a++)v.emitterEventListeners[e][a](arguments[1],arguments[2],arguments[3],arguments[4],arguments[5]);v.callPlugins&&v.callPlugins(e,arguments[1],arguments[2],arguments[3],arguments[4],arguments[5])},v.on=function(e,a){return e=p(e),v.emitterEventListeners[e]||(v.emitterEventListeners[e]=[]),v.emitterEventListeners[e].push(a),v},v.off=function(e,a){var t;if(e=p(e),"undefined"==typeof a)return v.emitterEventListeners[e]=[],v;if(v.emitterEventListeners[e]&&0!==v.emitterEventListeners[e].length){for(t=0;t<v.emitterEventListeners[e].length;t++)v.emitterEventListeners[e][t]===a&&v.emitterEventListeners[e].splice(t,1);return v}},v.once=function(e,a){e=p(e);var t=function(){a(arguments[0],arguments[1],arguments[2],arguments[3],arguments[4]),v.off(e,t)};return v.on(e,t),v},v.a11y={makeFocusable:function(e){return e.attr("tabIndex","0"),e},addRole:function(e,a){return e.attr("role",a),e},addLabel:function(e,a){return e.attr("aria-label",a),e},disable:function(e){return e.attr("aria-disabled",!0),e},enable:function(e){return e.attr("aria-disabled",!1),e},onEnterKey:function(e){13===e.keyCode&&(a(e.target).is(v.params.nextButton)?(v.onClickNext(e),v.isEnd?v.a11y.notify(v.params.lastSlideMessage):v.a11y.notify(v.params.nextSlideMessage)):a(e.target).is(v.params.prevButton)&&(v.onClickPrev(e),v.isBeginning?v.a11y.notify(v.params.firstSlideMessage):v.a11y.notify(v.params.prevSlideMessage)),a(e.target).is("."+v.params.bulletClass)&&a(e.target)[0].click())},liveRegion:a('<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>'),notify:function(e){var a=v.a11y.liveRegion;0!==a.length&&(a.html(""),a.html(e))},init:function(){v.params.nextButton&&v.nextButton&&v.nextButton.length>0&&(v.a11y.makeFocusable(v.nextButton),v.a11y.addRole(v.nextButton,"button"),v.a11y.addLabel(v.nextButton,v.params.nextSlideMessage)),v.params.prevButton&&v.prevButton&&v.prevButton.length>0&&(v.a11y.makeFocusable(v.prevButton),v.a11y.addRole(v.prevButton,"button"),v.a11y.addLabel(v.prevButton,v.params.prevSlideMessage)),a(v.container).append(v.a11y.liveRegion)},initPagination:function(){v.params.pagination&&v.params.paginationClickable&&v.bullets&&v.bullets.length&&v.bullets.each(function(){var e=a(this);v.a11y.makeFocusable(e),v.a11y.addRole(e,"button"),v.a11y.addLabel(e,v.params.paginationBulletMessage.replace(/{{index}}/,e.index()+1))})},destroy:function(){v.a11y.liveRegion&&v.a11y.liveRegion.length>0&&v.a11y.liveRegion.remove()}},v.init=function(){v.params.loop&&v.createLoop(),v.updateContainerSize(),v.updateSlidesSize(),v.updatePagination(),v.params.scrollbar&&v.scrollbar&&(v.scrollbar.set(),v.params.scrollbarDraggable&&v.scrollbar.enableDraggable()),"slide"!==v.params.effect&&v.effects[v.params.effect]&&(v.params.loop||v.updateProgress(),v.effects[v.params.effect].setTranslate()),v.params.loop?v.slideTo(v.params.initialSlide+v.loopedSlides,0,v.params.runCallbacksOnInit):(v.slideTo(v.params.initialSlide,0,v.params.runCallbacksOnInit),0===v.params.initialSlide&&(v.parallax&&v.params.parallax&&v.parallax.setTranslate(),v.lazy&&v.params.lazyLoading&&(v.lazy.load(),v.lazy.initialImageLoaded=!0))),v.attachEvents(),v.params.observer&&v.support.observer&&v.initObservers(),v.params.preloadImages&&!v.params.lazyLoading&&v.preloadImages(),v.params.autoplay&&v.startAutoplay(),v.params.keyboardControl&&v.enableKeyboardControl&&v.enableKeyboardControl(),v.params.mousewheelControl&&v.enableMousewheelControl&&v.enableMousewheelControl(),v.params.hashnav&&v.hashnav&&v.hashnav.init(),v.params.a11y&&v.a11y&&v.a11y.init(),v.emit("onInit",v)},v.cleanupStyles=function(){v.container.removeClass(v.classNames.join(" ")).removeAttr("style"),v.wrapper.removeAttr("style"),v.slides&&v.slides.length&&v.slides.removeClass([v.params.slideVisibleClass,v.params.slideActiveClass,v.params.slideNextClass,v.params.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-column").removeAttr("data-swiper-row"),v.paginationContainer&&v.paginationContainer.length&&v.paginationContainer.removeClass(v.params.paginationHiddenClass),v.bullets&&v.bullets.length&&v.bullets.removeClass(v.params.bulletActiveClass),v.params.prevButton&&a(v.params.prevButton).removeClass(v.params.buttonDisabledClass),v.params.nextButton&&a(v.params.nextButton).removeClass(v.params.buttonDisabledClass),v.params.scrollbar&&v.scrollbar&&(v.scrollbar.track&&v.scrollbar.track.length&&v.scrollbar.track.removeAttr("style"),v.scrollbar.drag&&v.scrollbar.drag.length&&v.scrollbar.drag.removeAttr("style"))},v.destroy=function(e,a){v.detachEvents(),v.stopAutoplay(),v.params.scrollbar&&v.scrollbar&&v.params.scrollbarDraggable&&v.scrollbar.disableDraggable(),v.params.loop&&v.destroyLoop(),a&&v.cleanupStyles(),v.disconnectObservers(),v.params.keyboardControl&&v.disableKeyboardControl&&v.disableKeyboardControl(),v.params.mousewheelControl&&v.disableMousewheelControl&&v.disableMousewheelControl(),v.params.a11y&&v.a11y&&v.a11y.destroy(),v.emit("onDestroy"),e!==!1&&(v=null)},v.init(),v}},Swiper.prototype={isSafari:function(){var e=navigator.userAgent.toLowerCase();return e.indexOf("safari")>=0&&e.indexOf("chrome")<0&&e.indexOf("android")<0}(),isUiWebView:/(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent),isArray:function(e){return"[object Array]"===Object.prototype.toString.apply(e)},browser:{ie:window.navigator.pointerEnabled||window.navigator.msPointerEnabled,ieTouch:window.navigator.msPointerEnabled&&window.navigator.msMaxTouchPoints>1||window.navigator.pointerEnabled&&window.navigator.maxTouchPoints>1
},device:function(){var e=navigator.userAgent,a=e.match(/(Android);?[\s\/]+([\d.]+)?/),t=e.match(/(iPad).*OS\s([\d_]+)/),n=e.match(/(iPod)(.*OS\s([\d_]+))?/),r=!t&&e.match(/(iPhone\sOS)\s([\d_]+)/);return{ios:t||r||n,android:a}}(),support:{touch:window.Modernizr&&Modernizr.touch===!0||function(){return!!("ontouchstart"in window||window.DocumentTouch&&document instanceof DocumentTouch)}(),transforms3d:window.Modernizr&&Modernizr.csstransforms3d===!0||function(){var e=document.createElement("div").style;return"webkitPerspective"in e||"MozPerspective"in e||"OPerspective"in e||"MsPerspective"in e||"perspective"in e}(),flexbox:function(){for(var e=document.createElement("div").style,a="alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "),t=0;t<a.length;t++)if(a[t]in e)return!0}(),observer:function(){return"MutationObserver"in window||"WebkitMutationObserver"in window}()},plugins:{}}}();
//# sourceMappingURL=framework7.min.js.map

/*
 jQuery Masked Input Plugin
 Copyright (c) 2007 - 2014 Josh Bush (digitalbush.com)
 Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
 Version: 1.4.0
 */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a("object"==typeof exports?require("jquery"):jQuery)}(function(a){var b,c=navigator.userAgent,d=/iphone/i.test(c),e=/chrome/i.test(c),f=/android/i.test(c);a.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},autoclear:!0,dataName:"rawMaskFn",placeholder:"-"},a.fn.extend({caret:function(a,b){var c;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof a?(b="number"==typeof b?b:a,this.each(function(){this.setSelectionRange?this.setSelectionRange(a,b):this.createTextRange&&(c=this.createTextRange(),c.collapse(!0),c.moveEnd("character",b),c.moveStart("character",a),c.select())})):(this[0].setSelectionRange?(a=this[0].selectionStart,b=this[0].selectionEnd):document.selection&&document.selection.createRange&&(c=document.selection.createRange(),a=0-c.duplicate().moveStart("character",-1e5),b=a+c.text.length),{begin:a,end:b})},unmask:function(){return this.trigger("unmask")},mask:function(c,g){var h,i,j,k,l,m,n,o;if(!c&&this.length>0){h=a(this[0]);var p=h.data(a.mask.dataName);return p?p():void 0}return g=a.extend({autoclear:a.mask.autoclear,placeholder:a.mask.placeholder,completed:null},g),i=a.mask.definitions,j=[],k=n=c.length,l=null,a.each(c.split(""),function(a,b){"?"==b?(n--,k=a):i[b]?(j.push(new RegExp(i[b])),null===l&&(l=j.length-1),k>a&&(m=j.length-1)):j.push(null)}),this.trigger("unmask").each(function(){function h(){if(g.completed){for(var a=l;m>=a;a++)if(j[a]&&C[a]===p(a))return;g.completed.call(B)}}function p(a){return g.placeholder.charAt(a<g.placeholder.length?a:0)}function q(a){for(;++a<n&&!j[a];);return a}function r(a){for(;--a>=0&&!j[a];);return a}function s(a,b){var c,d;if(!(0>a)){for(c=a,d=q(b);n>c;c++)if(j[c]){if(!(n>d&&j[c].test(C[d])))break;C[c]=C[d],C[d]=p(d),d=q(d)}z(),B.caret(Math.max(l,a))}}function t(a){var b,c,d,e;for(b=a,c=p(a);n>b;b++)if(j[b]){if(d=q(b),e=C[b],C[b]=c,!(n>d&&j[d].test(e)))break;c=e}}function u(){var a=B.val(),b=B.caret();if(a.length<o.length){for(A(!0);b.begin>0&&!j[b.begin-1];)b.begin--;if(0===b.begin)for(;b.begin<l&&!j[b.begin];)b.begin++;B.caret(b.begin,b.begin)}else{for(A(!0);b.begin<n&&!j[b.begin];)b.begin++;B.caret(b.begin,b.begin)}h()}function v(){A(),B.val()!=E&&B.change()}function w(a){if(!B.prop("readonly")){var b,c,e,f=a.which||a.keyCode;o=B.val(),8===f||46===f||d&&127===f?(b=B.caret(),c=b.begin,e=b.end,e-c===0&&(c=46!==f?r(c):e=q(c-1),e=46===f?q(e):e),y(c,e),s(c,e-1),a.preventDefault()):13===f?v.call(this,a):27===f&&(B.val(E),B.caret(0,A()),a.preventDefault())}}function x(b){if(!B.prop("readonly")){var c,d,e,g=b.which||b.keyCode,i=B.caret();if(!(b.ctrlKey||b.altKey||b.metaKey||32>g)&&g&&13!==g){if(i.end-i.begin!==0&&(y(i.begin,i.end),s(i.begin,i.end-1)),c=q(i.begin-1),n>c&&(d=String.fromCharCode(g),j[c].test(d))){if(t(c),C[c]=d,z(),e=q(c),f){var k=function(){a.proxy(a.fn.caret,B,e)()};setTimeout(k,0)}else B.caret(e);i.begin<=m&&h()}b.preventDefault()}}}function y(a,b){var c;for(c=a;b>c&&n>c;c++)j[c]&&(C[c]=p(c))}function z(){B.val(C.join(""))}function A(a){var b,c,d,e=B.val(),f=-1;for(b=0,d=0;n>b;b++)if(j[b]){for(C[b]=p(b);d++<e.length;)if(c=e.charAt(d-1),j[b].test(c)){C[b]=c,f=b;break}if(d>e.length){y(b+1,n);break}}else C[b]===e.charAt(d)&&d++,k>b&&(f=b);return a?z():k>f+1?g.autoclear||C.join("")===D?(B.val()&&B.val(""),y(0,n)):z():(z(),B.val(B.val().substring(0,f+1))),k?b:l}var B=a(this),C=a.map(c.split(""),function(a,b){return"?"!=a?i[a]?p(b):a:void 0}),D=C.join(""),E=B.val();B.data(a.mask.dataName,function(){return a.map(C,function(a,b){return j[b]&&a!=p(b)?a:null}).join("")}),B.one("unmask",function(){B.off(".mask").removeData(a.mask.dataName)}).on("focus.mask",function(){if(!B.prop("readonly")){clearTimeout(b);var a;E=B.val(),a=A(),b=setTimeout(function(){z(),a==c.replace("?","").length?B.caret(0,a):B.caret(a)},10)}}).on("blur.mask",v).on("keydown.mask",w).on("keypress.mask",x).on("input.mask paste.mask",function(){B.prop("readonly")||setTimeout(function(){var a=A(!0);B.caret(a),h()},0)}),e&&f&&B.off("input.mask").on("input.mask",u),A()})}})});