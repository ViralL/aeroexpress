'use strict';

// ready

var myApp = new Framework7();

// Inline with custom toolbar
var $$ = Dom7;
var monthNames = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август' , 'Сентябрь' , 'Октябрь', 'Ноябрь', 'Декабрь'];
var dayNames = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
var calendarInline = myApp.calendar({
    input: '#calendar-events',
    container: '#ks-calendar-inline-container',
    weekHeader: true,
    header: false,
    footer: false,
    dateFormat: 'dd.mm.yyyy',
    dayNamesShort: dayNames,
    toolbarTemplate:
    '<div class="toolbar calendar-custom-toolbar">' +
    '<div class="toolbar-inner">' +
    '<div class="left">' +
    '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' +
    '</div>' +
    '<div class="center"></div>' +
    '<div class="right">' +
    '<a href="#" class="link icon-only"><i class="icon icon-forward"></i></a>' +
    '</div>' +
    '</div>' +
    '</div>',
    onOpen: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
        $$('.calendar-custom-toolbar .left .link').on('click', function () {
            calendarInline.prevMonth();
        });
        $$('.calendar-custom-toolbar .right .link').on('click', function () {
            calendarInline.nextMonth();
        });
    },
    onMonthYearChangeStart: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
    }
});
var calendarInline2 = myApp.calendar({
    input: '#calendar-events2',
    container: '#ks-calendar-inline-container2',
    weekHeader: true,
    header: false,
    footer: false,
    dateFormat: 'dd.mm.yyyy',
    dayNamesShort: dayNames,
    toolbarTemplate:
    '<div class="toolbar calendar-custom-toolbar">' +
    '<div class="toolbar-inner">' +
    '<div class="left">' +
    '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' +
    '</div>' +
    '<div class="center"></div>' +
    '<div class="right">' +
    '<a href="#" class="link icon-only"><i class="icon icon-forward"></i></a>' +
    '</div>' +
    '</div>' +
    '</div>',
    onOpen: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
        $$('.calendar-custom-toolbar .left .link').on('click', function () {
            calendarInline.prevMonth();
        });
        $$('.calendar-custom-toolbar .right .link').on('click', function () {
            calendarInline.nextMonth();
        });
    },
    onMonthYearChangeStart: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
    }
});
var calendarInline3 = myApp.calendar({
    input: '#calendar-events3',
    container: '#ks-calendar-inline-container3',
    weekHeader: true,
    header: false,
    footer: false,
    dateFormat: 'dd.mm.yyyy',
    dayNamesShort: dayNames,
    toolbarTemplate:
    '<div class="toolbar calendar-custom-toolbar">' +
    '<div class="toolbar-inner">' +
    '<div class="left">' +
    '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' +
    '</div>' +
    '<div class="center"></div>' +
    '<div class="right">' +
    '<a href="#" class="link icon-only"><i class="icon icon-forward"></i></a>' +
    '</div>' +
    '</div>' +
    '</div>',
    onOpen: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
        $$('.calendar-custom-toolbar .left .link').on('click', function () {
            calendarInline.prevMonth();
        });
        $$('.calendar-custom-toolbar .right .link').on('click', function () {
            calendarInline.nextMonth();
        });
    },
    onMonthYearChangeStart: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
    }
});
var calendarDefault1 = myApp.calendar({
    input: '#ks-calendar-default1',
    weekHeader: true,
    header: false,
    footer: true,
    dateFormat: 'dd.mm.yyyy',
    dayNamesShort: dayNames,
    toolbarTemplate:
    '<div class="toolbar calendar-custom-toolbar">' +
    '<div class="toolbar-inner">' +
    '<div class="left">' +
    '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' +
    '</div>' +
    '<div class="center"></div>' +
    '<div class="right">' +
    '<a href="#" class="link icon-only"><i class="icon icon-forward"></i></a>' +
    '</div>' +
    '</div>' +
    '</div>',
    onOpen: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
        $$('.calendar-custom-toolbar .left .link').on('click', function () {
            calendarDefault1.prevMonth();
        });
        $$('.calendar-custom-toolbar .right .link').on('click', function () {
            calendarDefault1.nextMonth();
        });
    },
    onMonthYearChangeStart: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
    }
});
var calendarDefault2 = myApp.calendar({
    input: '#ks-calendar-default2',
    weekHeader: true,
    header: false,
    footer: true,
    dateFormat: 'dd.mm.yyyy',
    dayNamesShort: dayNames,
    toolbarTemplate:
    '<div class="toolbar calendar-custom-toolbar">' +
    '<div class="toolbar-inner">' +
    '<div class="left">' +
    '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' +
    '</div>' +
    '<div class="center"></div>' +
    '<div class="right">' +
    '<a href="#" class="link icon-only"><i class="icon icon-forward"></i></a>' +
    '</div>' +
    '</div>' +
    '</div>',
    onOpen: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
        $$('.calendar-custom-toolbar .left .link').on('click', function () {
            calendarDefault2.prevMonth();
        });
        $$('.calendar-custom-toolbar .right .link').on('click', function () {
            calendarDefault2.nextMonth();
        });
    },
    onMonthYearChangeStart: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
    }
});
var calendarRange1 = myApp.calendar({
    input: '#ks-calendar-range1',
    weekHeader: true,
    header: false,
    footer: true,
    dateFormat: 'dd.mm.yyyy',
    dayNamesShort: dayNames,
    toolbarTemplate:
    '<div class="toolbar calendar-custom-toolbar">' +
    '<div class="toolbar-inner">' +
    '<div class="left">' +
    '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' +
    '</div>' +
    '<div class="center"></div>' +
    '<div class="right">' +
    '<a href="#" class="link icon-only"><i class="icon icon-forward"></i></a>' +
    '</div>' +
    '</div>' +
    '</div>',
    onOpen: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
        $$('.calendar-custom-toolbar .left .link').on('click', function () {
            calendarRange1.prevMonth();
        });
        $$('.calendar-custom-toolbar .right .link').on('click', function () {
            calendarRange1.nextMonth();
        });
    },
    onMonthYearChangeStart: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
    },
    rangePicker: true
});
var calendarRange2 = myApp.calendar({
    input: '#ks-calendar-range2',
    weekHeader: true,
    header: false,
    footer: true,
    dateFormat: 'dd.mm.yyyy',
    dayNamesShort: dayNames,
    toolbarTemplate:
    '<div class="toolbar calendar-custom-toolbar">' +
    '<div class="toolbar-inner">' +
    '<div class="left">' +
    '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' +
    '</div>' +
    '<div class="center"></div>' +
    '<div class="right">' +
    '<a href="#" class="link icon-only"><i class="icon icon-forward"></i></a>' +
    '</div>' +
    '</div>' +
    '</div>',
    onOpen: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
        $$('.calendar-custom-toolbar .left .link').on('click', function () {
            calendarRange2.prevMonth();
        });
        $$('.calendar-custom-toolbar .right .link').on('click', function () {
            calendarRange2.nextMonth();
        });
    },
    onMonthYearChangeStart: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
    },
    rangePicker: true
});
var calendarRange3 = myApp.calendar({
    input: '#ks-calendar-range3',
    weekHeader: true,
    header: false,
    footer: true,
    dateFormat: 'dd.mm.yyyy',
    dayNamesShort: dayNames,
    toolbarTemplate:
    '<div class="toolbar calendar-custom-toolbar">' +
    '<div class="toolbar-inner">' +
    '<div class="left">' +
    '<a href="#" class="link icon-only"><i class="icon icon-back"></i></a>' +
    '</div>' +
    '<div class="center"></div>' +
    '<div class="right">' +
    '<a href="#" class="link icon-only"><i class="icon icon-forward"></i></a>' +
    '</div>' +
    '</div>' +
    '</div>',
    onOpen: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
        $$('.calendar-custom-toolbar .left .link').on('click', function () {
            calendarRange3.prevMonth();
        });
        $$('.calendar-custom-toolbar .right .link').on('click', function () {
            calendarRange3.nextMonth();
        });
    },
    onMonthYearChangeStart: function (p) {
        $$('.calendar-custom-toolbar .center').text(monthNames[p.currentMonth]);
    },
    rangePicker: true
});

$$('.open-about').on('click', function () {
    var myPopup = $(this).attr('data-popup');
//    console.log(myPopup);
    myApp.popup(myPopup);
    var mySwiper = myApp.swiper('.swiper-container', {
        paginationHide: false,
        pagination: '.swiper-pagination',
        loop: true,
        hashnav: true,
        hashnavWatchState: true,
        paginationClickable: true,
        paginationType: "fraction",
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'
    });
});
$$('.open-pop').on('click', function () {
    var myPopup = $(this).attr('data-popup');
    myApp.popup(myPopup);
});


//slider
var mySwiper3 = myApp.swiper('.swiper-3', {
    pagination:'.swiper-3 .swiper-pagination',
    // spaceBetween: 40,
    // slidesPerView: 3,
    slidesPerView: 'auto',
    loop: true,
    centeredSlides: true
});
//slider

// ready
$(document).ready(function() {

    $('.picker-calendar-day').click(function () {
        //var ValYear = $(this).attr('data-year');
        //var valDay = $(this).attr('data-day');
        //var valMonth = $(this).attr('data-month');
        $(this).closest('.accordion-item').addClass('active');
        //$('.date-name--js').html('<span class="text--info">' + valDay + '.' + valMonth + '.' + ValYear + '</span>');
        ticketsDone();
    });
    $('.tariff--js input').click(function () {
        var valName = $(this).val();
        $(this).closest('.accordion-item').addClass('active');
        $(this).closest('.accordion-item').find('.tariff-name--js').html('<span class="text--info">' + valName + '</span>');
        ticketsDone();
    });
    $('.route--js input').click(function () {
        var valName = $(this).val();
        $(this).closest('.accordion-item').addClass('active');
        $(this).closest('.accordion-item').find('.route-name--js').html('<span class="text--info">' + valName + '</span>');
        ticketsDone();
    });
    $('.fly--js input').click(function () {
        var valName = $(this).val();
        $(this).closest('.accordion-item').addClass('active');
        $(this).closest('.accordion-item').find('.fly-name--js').html('<span class="text--info">' + valName + '</span>');
        ticketsDone();
    });
    $('.ticket--js input').click(function () {
        var valName = $(this).val();
        $(this).closest('.accordion-item').addClass('active');
        $(this).closest('.accordion-item').find('.ticket-name--js').html('<span class="text--info">' + valName + '</span>');
        ticketsDone();
    });
    $('.document--js input').click(function () {
        var valName = $(this).val();
        $(this).closest('.accordion-item').addClass('active');
        $(this).closest('.accordion-item').find('.document-name--js').html('<span class="text--info">' + valName + '</span>');
        $(this).closest('.accordion-item').find('.all-name--js .submain').html('<span class="text--info">' + valName + '</span>');
        ticketsDone();
    });
    $('.all--js input').click(function () {
        var valName = $(this).val();
        $(this).closest('.accordion-item').addClass('active');
        $(this).closest('.accordion-item').find('.all-name--js .main').html('<span class="text--info">' + valName + '</span>');
        ticketsDone();
    });

    function ticketsDone () {
        if ($(".form-item--js.active").length == $(".form-item--js").length) {
            var btnTct = $('.btn--tckjs').attr('data-text');
            $('.btn--tckjs').attr('disabled', false).text(btnTct);
            $('.ticket-block').show();
        }
    }

    function ticketsError () {
        if ($(".form-item--js.active").length !== $(".form-item--js").length) {
            $('.btn--tckjs').attr('disabled', true).text('ЗАПОЛНИТЕ ВСЕ ПОЛЯ');
        }
    }


    $('.form-control').each(function() {
        $(this).on('focus', function() {
            $(this).closest('.form-item--js').addClass('active').find('.item-content').addClass('focus-state').find('.floating-label').addClass('focus-state');
            //ticketsDone();
        });
        $(this).on('blur', function() {
            if ($(this).val().length == 0) {
                //$(this).removeClass('focus-state');
                $(this).closest('.form-item--js').removeClass('active').find('.item-content').removeClass('focus-state').find('.floating-label').removeClass('focus-state');
                ticketsError();
            }
        });
        $(this).on('keydown', function() {
            if ($(this).val().length > 0) {
                ticketsDone();
            } else {
                ticketsError();
            }
        });
        if ($(this).val() != '') $(this).closest('.form-item--js').addClass('active').find('.item-content').addClass('focus-state').find('.floating-label').addClass('focus-state');
    });

    $('.checked--js input').click(function () {

        if($(this).prop('checked')) {
            $(this).closest('.check--js').next().children().addClass('active');
        } else {
            $(this).closest('.check--js').next().children().removeClass('active');
        }
    });



    // mask phone {maskedinput}
    $("[name=cnumb]").mask("9999 9999 9999 9999");
    $("[name=cmonth]").mask("99");
    $("[name=cyear]").mask("9999");
    $("[name=ccvv]").mask("999");
    // mask phone


});